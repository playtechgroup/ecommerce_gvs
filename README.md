# Inmunotek

Inmunotek is a Laravel project

## Usage

```bash
composer install
npm install
php artisan passport:install
php artisan serve
```

## Built With

 * [Bootstrap](https://getbootstrap.com/)
 * [Laravel](https://laravel.com/)

## Authors

* **Andres F Satizabal C** - *App development*
