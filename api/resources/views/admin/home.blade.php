@extends('layouts.app')
@section('content')
<div class="row">
@include('flash::message')
    <div class="col-md-12">
        <h3>Bienvenido {{Auth::user()->name}}</h3>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>{{$product}}</h3>
                <p>Productos</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="/products" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{$categories}}</h3>
                <p>Categorias</p>
            </div>
            <div class="icon">
                <i class="ion  ion-android-cart"></i>
            </div>
            <a href="/categories" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{$users}}</h3>
                <p>Usuarios</p>
            </div>
            <div class="icon">
                <i class="ion ion-person"></i>
            </div>
            <a href="/users" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Accesos directos</h3>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="/createcategory">
        <div class="info-box bg-light-blue-gradient">
            <div class="info-box-content">
                <span>Crear nueva categoría</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box ion-arrow-graph-up-right -->
    </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="/create">
        <div class="info-box bg-purple-gradient">
            <div class="info-box-content">
                <span>Crear nuevo producto</span>
            </div>
        </div>
    </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="/createdevolution">
        <div class="info-box bg-red-gradient">
            <div class="info-box-content">
                <span>Crear nueva devolución</span>
            </div>
        </div>
    </a>
    </div>
</div>
@endsection