@extends('layouts.app')
@section('content')
<div class="container">
    <div class="py-1 text-center">
      <h2>Editar Usuario</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST"  action="/edituserform/{{$user->id}}"
        enctype="multipart/form-data" >
        <input type="hidden" class="form-control" name="actualEmail" value="{{$user->email}}">
        @csrf
        <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Nombre</label>
              <input type="text" class="form-control" id="firstName" name="name" placeholder="Ingrese el nombre" value="{{$user->name}}"  required>
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Correo</label>
              <input type="text" class="form-control" id="email" name="email" placeholder="Ingrese el correo electrónico"  value="{{$user->email}}"  required>
              <div class="invalid-feedback">
                Valid email is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Rol</label>
              <select type="text" class="form-control" id="role" name="role" required>
              @if($user->Role_id == "1")
                <option value="1" selected>Administrador</option>
              @else
                <option value="1">Administrador</option>
              @endif
              @if($user->Role_id == "2")
                <option value="2" selected>Cliente</option>
              @endif
              @if($user->Role_id == "3")
                <option value="3" selected>Proveedor</option>
              @endif
              @if($user->Role_id == "4")
                <option value="4" selected>Inventario</option>
              @else
                <option value="4">Inventario</option>
              @endif
              @if($user->Role_id == "5")
                <option value="5" selected>Publicador</option>
              @else
                <option value="5">Publicador</option>
              @endif
              </select>
              <div class="invalid-feedback">
                Valid role is required.
              </div>
            </div>
          </div>
          
          @if($user->rol->name === 'Costumer')
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Puede ver precios</label>
              <input type="checkbox" name="active"  id="{{$user->id}}" @if(!$user->linked) checked @endif>
              <label for="{{$user->id}}"></label>
            </div>
          </div>
          @endif
            <button class="btn btn-primary " type="submit">Guardar</button>
            <a href="{{ route('users') }}" type="button" class="btn btn-danger">
                <i class="fa fa-back" aria-hidden="true"></i> Cancelar
            </a>
        </form>
      </div>
    </div>
  </div>
@endsection
