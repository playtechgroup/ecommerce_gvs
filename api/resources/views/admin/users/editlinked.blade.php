@extends('layouts.app')
@section('content')
<div class="container">
    <div class="py-1 text-center">
      <h2>Editar médico agremiado</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST"  action="/editlinkedform/{{$user->id}}"
        enctype="multipart/form-data" >
        <input type="hidden" class="form-control" name="actualEmail" value="{{$user->email}}">
        @csrf
        <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Nombre</label>
              <input type="text" class="form-control" id="firstName" name="name" placeholder="Ingrese el nombre" value="{{$user->name}}"  required>
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Correo</label>
              <input type="text" class="form-control" id="email" name="email" placeholder="Ingrese el correo electrónico"  value="{{$user->email}}"  required>
              <div class="invalid-feedback">
                Valid email is required.
              </div>
            </div>
          </div>
            <button class="btn btn-primary " type="submit">Guardar</button>
            <a href="{{ route('linked') }}" type="button" class="btn btn-danger">
                <i class="fa fa-back" aria-hidden="true"></i> Cancelar
            </a>
        </form>
      </div>
    </div>
  </div>
@endsection
