@extends('layouts.app')
@section('content')
<div class="container">
    <div class="py-1 text-center">
      <h2>Crear Usuario</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST" action="{{ url('/createuserform') }}"
        enctype="multipart/form-data" >
        @csrf
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Nombre</label>
              <input type="text" class="form-control" id="firstName" name="name" placeholder="Ingrese el nombre"  required>
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Correo</label>
              <input type="text" class="form-control" id="email" name="email" placeholder="Ingrese el correo electrónico"  required>
              <div class="invalid-feedback">
                Valid email is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Rol</label>
              <select type="text" class="form-control" id="role" name="role" required>
                <option value="1">Administrador</option>
                <option value="4">Inventario</option>
                <option value="5">Publicador</option>
              </select>
              <div class="invalid-feedback">
                Valid role is required.
              </div>
            </div>
          </div>
          <button class="btn btn-primary btn-lg btn-block" type="submit">Guardar</button>
        </form>
      </div>
    </div>
  </div>
@endsection
