@extends('layouts.app')
@section('content')
<div class="container">
    <div class="py-1 text-center">
      <h2>Información de perfil</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST"  action="/editProfileForm/{{$profile->id}}"
        enctype="multipart/form-data" >
        <input type="hidden" class="form-control" name="actualEmail" value="{{$profile->email}}">
        @csrf
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Nombre</label>
               <input type="text" class="form-control" id="firstName" name="name" value="{{$profile->name}}"  required>
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Correo electrónico</label>
               <input type="text" class="form-control" id="email" name="email" value="{{$profile->email}}"  required>
               @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
              <div class="invalid-feedback">
                Valid email is required.
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
                <table class="table table-responsive my-datatable">
                  <tbody>
                      <tr>
                          <td>
                            <label for="firstName">¿Cambiar contraseña?</label>
                            <input type="checkbox" name="check" id="check" onclick="mostrarCambiarContraseña()">
                              <label for="check" style="margin-top: 5px; margin-left: 20px;"></label>
                          </td>
                      </tr>
                  </tbody>
                </table>
              
            </div>
          </div>

          <div id="mostrar" class="row" hidden>
            <div class="col-md-12 mb-3">
              <label for="firstName">Contraseña</label>
              <input type="password" class="form-control auto-complete-off" id="pass" name="pass"  placeholder="Ingrese la nueva contraseña" autocomplete="new-password">
              <a onclick="viewPassword()" href="#" style="position: absolute; top: 36px; right: 30px;"><i id="eye" class="fa fa-eye"></i></a>
              <div class="invalid-feedback">
                Valid password is required.
              </div>
            </div>
          </div>


            <button class="btn btn-primary " type="submit">Guardar</button>
            <a href="{{ route('home') }}" type="button" class="btn btn-danger">
                <i class="fa fa-back" aria-hidden="true"></i> Cancelar
            </a>
        </form>
      </div>
    </div>
  </div>
<script type="application/javascript">

$(function(){  
        var passElem = $("input#pass");
        passElem.focus(function() { 
            passElem.prop("type", "password");                                             
        });
    });

  window.onload = function () {
    document.getElementById("mostrar").hidden=true;
  }

  function mostrarCambiarContraseña(){
    if (document.getElementById("check").checked)
      document.getElementById("mostrar").hidden=false;
    else
      document.getElementById("mostrar").hidden=true;
  }

  function viewPassword(){
    if (document.getElementById("pass").type=="password"){

      document.getElementById("pass").type="text";
      document.getElementById("eye").className="fa fa-eye-slash";
    } else {
      document.getElementById("pass").type="password";
      document.getElementById("eye").className="fa fa-eye";
    }
  }
</script>
@endsection
