@extends('layouts.app')
@section('content')
<div class="container">
    @include('flash::message')
    <div class="col-md-12 text-center">
        <h2>Categorias</h2>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a href="{{ route('createcategory') }}" type="button" class="btn btn-primary paddingbuttons"> <i
                        class="fa fa-plus" aria-hidden="true"></i> Crear Categoria</a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Imagen</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                        <tr>
                            <td scope="row">{{$category->id}}</td>
                            <td>{{$category->name}}</td>
                            <td>
                                <img data-img='true' style="width:100px " src="{{$category->url_img}}" alt="nohay..">
                            </td>
                            <td>
                                <input type="checkbox" name="active"  id="{{$category->id}}" onclick="actualizar_estado(this,'/deletecategory','{{$category->id}}')"  @if($category->active) checked @endif>
                                <label for="{{$category->id}}"></label>
                            </td>
                            <td>
                                <a href="/viewsubcategory/{{$category->id}}" class="m5">
                                    <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                                </a>
                                <a href="/editcategory/{{$category->id}}" class="m5">
                                    <span><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                </a>
                                <!-- <a onclick="global_delete(this)" id="{{$category->id}}" title="La categoria" name="deletecategory" class="m5">
                                    <span><i class="fa fa-trash" aria-hidden="true"></i></span>ññ
                                </a> -->
                            </td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
        </div>
    </div>
</div>
<script type="application/javascript">
    window.onload = function () {
        initmodal();
    }
</script>

@endsection
