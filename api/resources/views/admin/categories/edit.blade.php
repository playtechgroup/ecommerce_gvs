@extends('layouts.app')
@section('content')
<div class="container">
    <div class="py-1 text-center">
      <h2>Editar Categoria</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST"  action="/editcategoryform/{{$Category->id}}"
        enctype="multipart/form-data" >
        @csrf
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Nombre</label>
               <input type="text" class="form-control" id="firstName" name="name" value="{{$Category->name}}"  required>
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12  mb-3">
              <label for="address">Imagen</label>
              <img src="{{$Category->url_img}}" style="width:100px" alt="No imagen">
                <input type="file" class="form-control" value="" name="url_img"  >
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>
          </div>
            <button class="btn btn-primary " type="submit">Guardar</button>
            <a href="{{ route('categories') }}" type="button" class="btn btn-danger">
                <i class="fa fa-back" aria-hidden="true"></i> Cancelar
            </a>
        </form>
      </div>
    </div>
  </div>
@endsection
