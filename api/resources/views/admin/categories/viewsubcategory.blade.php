@extends('layouts.app')
@section('content')
<div class="container">
@include('flash::message')
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title ">Categoria</h3>
                </div>
                <div class="panel-body ">

                    <div class="row">
                        <div class="col-md-6">
                            <h5>Nombre:</h5>
                            <p>{{$CategoryProduct->name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Imagen:</h5>
                            <p><a href="{{$CategoryProduct->url_img}}" target="_blank">Ver Imagen</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title ">Sub Categorias</h3>
                      <div class="btn-group pull-right" role="group" aria-label="...">
                            <a href="/createsubcategories/{{$CategoryProduct->id}}" type="button" class="btn btn-primary paddingbuttons">
                               <i class="fa fa-plus" aria-hidden="true"></i> Crear Subcategoria</a>
                        </div>

                <div class="panel-body ">
                    <div class="table-responsive">
                        <table class="table table-responsive my-datatable">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Acciones</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($SubcategoryProduct as $SubcategoryProducts)
                                <tr>
                                    <td scope="row">{{$SubcategoryProducts->id}}</td>
                                    <td>{{$SubcategoryProducts->name}}</td>
                                    <td>
                                        <input type="checkbox" name="active" id="{{$SubcategoryProducts->id}}" onclick="actualizar_estado(this,'/statesubcategory','{{$SubcategoryProducts->id}}')"  @if($SubcategoryProducts->active) checked @endif>
                                        <label for="{{$SubcategoryProducts->id}}"></label>
                                    </td>
                                    <td>
                                        <a href="/editsubcategories/{{$SubcategoryProducts->id}}" class="m5">
                                            <span><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="btn-group pull-left" role="group">
                <div class="col-md-12 mb-3">
                    <a href="{{ route('categories') }}" type="button" class="btn btn-primary">
                        <i class="fa fa-back" aria-hidden="true"></i> Volver
                    </a>
                <div class="col-md-12 mb-3">
        </div>
    </div>
</div>
@endsection
