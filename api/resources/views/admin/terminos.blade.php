@extends('layouts.app')
@section('content')
<div class="container" id="main"></div>
@endsection
<script>
    const url = window.location.origin;
    fetch(url+"/api/terminos").then(function(response) {
        return response.text().then(function(text) {
            document.getElementById('main').innerHTML = text;
        });
    });
</script>