@extends('layouts.app')
@section('content')
<div class="container">
    <div class="py-1 text-center">
        <h2>Crear Subcategoria</h2>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <form class="needs-validation" method="POST" action="/createsubcategoriesform/{{$id}}" enctype="multipart/form-data" >
            @csrf
            <div class="row">
                <div class="col-md-12 mb-3">
                    <label for="firstName">Nombre</label>
                    <input type="text" class="form-control" id="firstName" name="name" placeholder="Ingrese el nombre"  required>
                    <div class="invalid-feedback">
                        Valid first name is required.
                    </div>
                </div>
            </div>

            <button class="btn btn-primary btn-lg btn-block" type="submit">Guardar</button>
            </form>
        </div>
    </div>
  </div>
@endsection
