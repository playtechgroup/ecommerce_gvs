@extends('layouts.app')
@section('content')
<div class="container">
    @include('flash::message')
    <div class="col-md-12 text-center">
        <h2>Ordenes</h2>
    </div>
    <div class="row">
        <!-- <div class="col-md-12">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a href="{{ route('createcategory') }}" type="button" class="btn btn-primary paddingbuttons"> <i
                        class="fa fa-plus" aria-hidden="true"></i> </a>
            </div>
        </div> -->
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Fecha compra</th>
                            <th scope="col">Metodo de pago</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Nombre comprador</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Total</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                       <!-- dd($Order); -->
                       @foreach($Order as $key => $value)
                        <tr>
                            <td scope="row">{{$value->id}}</td>
                            <td>{{$value->date}}</td>
                            <td>{{$value->paymentmethod->name}}</td>
                            <td>{{$value->orderstatus->name}}</td>
                            <td>{{$value->costumer->user->name}}</td>
                            <td>
                            <input type="checkbox" name="active"  id="{{$value->id}}" onclick="actualizar_estado(this,'/deleteorder','{{$value->id}}')"  @if($value->active) checked @endif>
                                <label for="{{$value->id}}"></label></td>
                            <td>${{$value->total}}</td>

                            <td>
                                <a href="/orderdetail/{{$value->id}}" class="m5">
                                    <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                                </a>
                            </td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
        </div>
    </div>
</div>
<script type="application/javascript">
    window.onload = function () {
        // initmodal();
    }
</script>

@endsection
