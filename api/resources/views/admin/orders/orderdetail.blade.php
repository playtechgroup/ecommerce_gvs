@extends('layouts.app')
@section('content')
<div class="container">
    @include('flash::message')
     <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title ">Orden {{$Order->id}}</h3>
                </div>
                <div class="panel-body ">
                    <div class="row ">
                        <div class="col-md-6 ">
                            <h5>Fecha </h5>
                            <p>{{$Order->date}}</p>
                        </div>
                        <div class="col-md-6">
                            <h5>Comprador</h5>
                            <p>{{$Order->costumer->user->name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Metodo de pago:</h5>
                            <p>{{$Order->paymentmethod->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <h5>Estado de la orden:</h5>
                            <p>{{$Order->orderstatus->name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Cuantas cuotas mensuales desea habilitar para este pedido</h5>
                            <p>{{$Order->commentary}}</p>
                        </div>
                        <div class="col-md-6">
                            <h5>Observaciones</h5>
                            <p>{{$Order->commentary}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">Nombre Producto</th>
                            <th scope="col">Proveedor</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Calificacion</th>
                            <th scope="col">precio total</th>
                            <th scope="col">precio total con impuestos</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php($contador=0)
                       @foreach($Products as $key => $value)
                      <tr>
                            <td scope="row">{{$value->product->name}}</td>
                            <td scope="row">{{$value->product->provider->user->name}}</td>
                            <td scope="row">{{$value->quantity}}</td>
                            <td scope="row">{{$value->rate}}</td>
                            <td scope="row">${{ number_format($value->product->price*$value->quantity)}}</td>
                            <td scope="row">${{ number_format(($value->product->price * $value->quantity)*(($value->tax/100)*1)+($value->product->price * $value->quantity))}}</td>
                             @php($contador+=($value->product->price * $value->quantity)*(($value->tax/100)*1)+($value->product->price * $value->quantity))
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <h2>Total <span>${{number_format($contador)}}</span></h2>
        </div>
        <div class="btn-group pull-left" role="group">
            <a href="{{ route('orders') }}" type="button" class="btn btn-primary">
                <i class="fa fa-back" aria-hidden="true"></i> Volver
            </a>
        </div>
    </div>
</div>
@endsection
