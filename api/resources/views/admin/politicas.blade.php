@extends('layouts.app')
@section('content')
<div class="container" id="main"></div>
@endsection
<script>
    var url= window.location.origin;
    fetch(url+"/api/politicas").then(function(response) {
        return response.text().then(function(text) {
            document.getElementById('main').innerHTML = text;
        });
    });
</script>