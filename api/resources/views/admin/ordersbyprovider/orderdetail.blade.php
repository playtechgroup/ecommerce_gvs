@extends('layouts.app')
@section('content')
<div class="container">
    @include('flash::message')
     <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title ">Orden {{$Order->id}}</h3>
                </div>
                <div class="panel-body ">
                    <div class="row ">
                        <div class="col-md-6 ">
                            <h5>Fecha </h5>
                            <p>{{$Order->date}}</p>
                        </div>
                        <div class="col-md-6">
                            <h5>Comprador</h5>
                            <p>{{$Order->costumer->user->name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Metodo de pago:</h5>
                            <p>{{$Order->paymentmethod->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <h5>Estado de la orden:</h5>
                            <select  onchange="update_statusorden(this,'{{$Order->id}}')" id="{{$Order->id}}" id="update_order_status"  name="update_order_status" title="Estado orden" class="custom-select d-block w-50">
                                @foreach($OrderStatus as $row)
                                  @if($row->id==$Order->orderstatus->id)
                                  <option value="{{$row->id}}" selected="selected"> {{$row->name}}</option>
                                  @else
                                  <option value="{{$row->id}}"> {{$row->name}} </option>
                                  @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <h5>Dirección de entrega:</h5>
                            <p>{{$Order->address->address}}</p>
                        </div>
                        <div class="col-md-6">
                            <h5>Ciudad de entrega:</h5>
                            <p>{{$Order->address->city->name}} - {{$Order->address->city->deparment->name}}</p>
                        </div>
                    </div>
                </div>  
                <div class="row col-md-12">
                    <div class="btn-group pull-right" role="group" aria-label="...">
                        <a href="" data-toggle="modal" data-target="#exampleModal" type="button" class="btn btn-primary paddingbuttons"> <i
                                class="fa fa-plus" aria-hidden="true"></i> Nueva notificación</a>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
        
        <div class="col-md-12">
        <h3 class="panel-title ">Productos</h3>
            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">Nombre Producto</th>
                            <th scope="col">Proveedor</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Calificacion</th>
                            <th scope="col">precio total</th>
                            <th scope="col">precio total con impuestos</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php($contador=0)
                       @foreach($Products as $key => $value)
                      <tr>
                            <td scope="row">{{$value->product->name}}</td>
                            <td scope="row">{{$value->product->provider->user->name}}</td>
                            <td scope="row">{{$value->quantity}}</td>
                            <td scope="row">{{$value->rate}}</td>
                            <td scope="row">${{ number_format($value->product->price*$value->quantity)}}</td>
                            <td scope="row">${{ number_format(($value->product->price * $value->quantity)*(($value->tax/100)*1)+($value->product->price * $value->quantity))}}</td>
                             @php($contador+=($value->product->price * $value->quantity)*(($value->tax/100)*1)+($value->product->price * $value->quantity))
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <h2>Total <span>${{number_format($contador)}}</span></h2>
        </div>

        <div class="col-md-12 mt-4 bm-4">
        <h3 class="panel-title ">Pagos realizados</h3>
            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Moneda</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php($contador=0)
                       @foreach($Bills as $key => $value)
                      <tr>
                            <td scope="row">{{$value->id}}</td>
                            <td scope="row">{{$value->coin}}</td>
                            <td scope="row">$ {{number_format($value->value)}}</td>
                            <td scope="row">{{$value->billstatus->name}}</td>
                            <td scope="row">{{$value->date}}</td>
                       </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="btn-group pull-left" role="group">
            <a href="{{ route('orders') }}" type="button" class="btn btn-primary">
                <i class="fa fa-back" aria-hidden="true"></i> Volver
            </a>
        </div>
    </div>
    
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 10000;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva notificación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="needs-validation" method="POST"  action="/sendnotification/{{$Order->id}}"
        enctype="multipart/form-data" >
        @csrf
          <div class="form-group">
            <input type="hidden" class="form-control" name="costumerId" value="{{$Order->Costumer_id}}">
            <input type="hidden" class="form-control" name="costumerName" value="{{$Order->costumer->user->name}}">
            <input type="hidden" class="form-control" name="costumerEmail" value="{{$Order->costumer->user->email}}">
            <label for="message-text" class="col-form-label">Notificación:</label>
            <textarea class="form-control" id="message-text" name="message"></textarea>
          </div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" onclick="">Enviar</button>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

@endsection
