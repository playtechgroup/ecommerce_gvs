@extends('layouts.app')

@section('content')
<div class="container">
@include('flash::message')
    <div class="row">

        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title ">{{$provider->user->name}}</h3>
                </div>
                <div class="panel-body ">
                    <div class="row ">
                        <div class="col-md-6 ">
                            <h5>Identificacion:</h5>
                            <p>{{$provider->typeIdentification->name}}. {{$provider->identification}}</p>

                        </div>
                        <div class="col-md-6">
                            <h5>Email:</h5>
                            <p>{{$provider->user->email}}</p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Direccion:</h5>
                            <p>{{$provider->address}}</p>

                        </div>
                        <div class="col-md-6">
                            <h5>Telefono:</h5>
                            <p>{{$provider->phone}}</p>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5>Persona de Contacto:</h5>
                            <p>{{$provider->contact_person}}</p>

                        </div>
                        <div class="col-md-6">
                            <h5>Ciudad:</h5>
                            <p>{{$provider->city->name}}</p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Certificado Camara de Comercio:</h5>
                            <p></p><a href="{{$provider->trade_certificate}}" target="_blank">Ver
                                Certificado</a></p>
                        </div>
                        <div class="col-md-6">
                            <h5>Rut:</h5>
                            <p><a href="{{$provider->tax_registration}}" target="_blank">Ver Rut</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title ">Productos</h3>
                </div>
                <div class="panel-body ">

                    <div class="table-responsive">
                        <table class="table table-responsive my-datatable">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">IVA</th>
                                    <th scope="col">Categoria</th>
                                    <th scope="col">Subcategoria</th>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($productos as $producto)
                                <tr>
                                    <td scope="row">{{$producto->id}}</td>
                                        <td>{{$producto->name}}</td>
                                        <td>{{$producto->description}}</td>
                                        <td>$ {{number_format($producto->price)}}</td>
                                        <td>{{$producto->tax}}</td>
                                        <td>{{$producto->subcategoryproduct->categoryproduct->name}}</td>
                                        <td>{{$producto->subcategoryproduct->name}}</td>
                                        <td>@if($producto->active) Activo @else Inactivo @endif</td>

                                        <td>
                                            <a href="/viewproduct/{{$provider->id}}" class="m5">
                                                <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                                            </a>
                                            <a href="/editproduct/{{$provider->id}}" class="m5">
                                                <span><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                            </a>
                                            <a onclick="" class="m5">
                                                <span><i class="fa fa-trash" aria-hidden="true"></i></span>
                                            </a>
                                        </td>

                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>



        <div class="btn-group pull-left" role="group">
            <a href="{{ route('provider') }}" type="button" class="btn btn-primary">
                <i class="fa fa-back" aria-hidden="true"></i> Volver
            </a>
        </div>
    </div>


</div>

@endsection
