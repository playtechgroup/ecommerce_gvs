@extends('layouts.app')

@section('content')
<div class="container">
    @include('flash::message')
    <div class="col-md-12 text-center">

        <h2>Proveedores</h2>


    </div>




    <div class="row">

        <div class="col-md-12">
        <div class="btn-group pull-right" role="group" aria-label="..." >
            <a  href="{{ route('createprovider') }}"  type="button" class="btn btn-primary paddingbuttons"> <i class="fa fa-plus" aria-hidden="true"></i> Crear
                Provedor</a>
        </div>

    </div>


        <div class="col-md-12">

            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Identificacion</th>
                            <th scope="col">Email</th>
                            <th scope="col">Dirección</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Ciudad</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($providers as $provider)
                        <tr>
                            <td scope="row">{{$provider->id}}</td>
                            <td>{{$provider->user->name}}</td>
                            <td>{{$provider->typeIdentification->name}} {{$provider->identification}}</td>
                            <td>{{$provider->user->email}}</td>
                            <td>{{$provider->address}}</td>
                            <td>{{$provider->phone}}</td>
                            <td>{{$provider->city->name}}</td>
                            <td>
                                <a href="/viewprovider/{{$provider->id}}" class="m5">
                                    <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                                </a>
                                <a href="/editprovider/{{$provider->id}}" class="m5">
                                    <span><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                </a>
                                <!-- <a onclick="deleteprovider({{$provider->id}})" class="m5"> -->
                                <a onclick="global_delete(this)" id="{{$provider->id}}" title="El proveedor" name="deleteprovider" class="m5">
                                    <span><i class="fa fa-trash" aria-hidden="true"></i></span>
                                </a>


                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
