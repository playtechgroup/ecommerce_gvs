@extends('layouts.app')
@section('content')
<div class="container">
    <div class="py-1 text-center">
      <h2>Crear Proveedores</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST" action="{{ url('/createproviderform') }}"
        enctype="multipart/form-data" >
        @csrf
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="firstName">Nombre</label>
              <input type="text" class="form-control" id="firstName" name="name" value="{{ old('name') }}" placeholder="Ingrese el nombre"  required>
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>
            <div class="col-md-3 mb-3">
              <label for="lastName">Tipo de Identificacion</label>
              <select class="custom-select d-block w-100" id="typeidentification"  name="TypeIdentification_id"  required>
              <option value="" >Seleccione</option>
              @foreach($typeIdentification as $ti) 
              @if((old('TypeIdentification_id'))==($ti->id))
              <option value="{{$ti->id}}" selected="selected">
                {{$ti->name}}
                 @else
                <option value="{{$ti->id}}">
                  {{$ti->name}}
                </option>
                @endif
              </option>
              @endforeach
              </select>
              <div class="invalid-feedback">
                Valid last name is required.
              </div>
            </div>
            <div class="col-md-3 mb-3">
              <label for="lastName">Identificacion</label>
              <input type="number" class="form-control" id="lastName" placeholder="identification" name="identification" value="{{ old('identification') }}" required>
              <div class="invalid-feedback">
                Valid last name is required.
              </div>
            </div>
          </div>
          <div class="mb-3">
            <label for="email">Email <span class="text-muted"></span></label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="you@example.com" required>
            @error('email')
              <div class="invalid-feedback" style="display:block">
              {{ $message }}
              </div>
              @enderror
          </div>
          <div class="mb-3">
            <label for="address">Direccion</label>
            <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" placeholder="calle 85 # 85" required>
            <div class="invalid-feedback">
              Please enter your shipping address.
            </div>
          </div>
          <div class="mb-3">
            <label for="address">Telefono</label>
            <input type="text" class="form-control" id="phone" name="phone"  value="{{ old('phone') }}"  placeholder="315859654" required>
            <div class="invalid-feedback">
              Please enter your shipping address.
            </div>
          </div>
          <div class="row">
            <div class="col-md-6  mb-3">
              <label for="address">Persona de Contacto</label>
              <input type="text" class="form-control" id="personcontact"  name="contactperson" value="{{ old('contactperson') }}"  placeholder="Paola Cardenas" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>
            <div class="col-md-6 mb-3">
              <label for="country">Ciudad</label>
              <select class="custom-select d-block w-100" id="city" name="city" required>
              <option value="">Seleccione</option>
              @foreach($ciudad as $ci)
                @if($ci->id==old('city'))
                <option value="{{$ci->id}}" selected="selected">
                  {{$ci->name}}
              @else
              <option value="{{$ci->id}}">
              {{$ci->name}}
              </option>
              @endif
                </option>
                @endforeach
              </select>
              <div class="invalid-feedback">
                Please select a valid country.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6  mb-3">
              <label for="address">Certificado Camara de Comercio</label>
              <input type="file" class="form-control" name="trade_certificate" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>
            <div class="col-md-6  mb-3">
              <label for="address">Rut</label>
              <input type="file" class="form-control" name="tax_registration" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>
          </div>
          <button class="btn btn-primary btn-lg btn-block" type="submit">Guardar</button>
        </form>
      </div>
    </div>
  </div>
@endsection
