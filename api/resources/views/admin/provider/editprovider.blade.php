@extends('layouts.app')
@section('content')
<div class="container">
    <div class="py-1 text-center">
      <h2>Editar Proveedor</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST" action="/editproviderform/{{$provider->id}}"
        enctype="multipart/form-data" >
        @csrf
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="firstName">Nombre</label>
              <input type="text" class="form-control" id="firstName" name="name" value="{{$provider->user->name}}"  required>
              <!-- <div class="invalid-feedback" style="display:block">
                Valid first name is required.
              </div> -->
            </div>
            <div class="col-md-3 mb-3">
              <label for="lastName">Tipo de Identificacion</label>
              <select class="custom-select d-block w-100" id="typeidentification" value="{{$provider->TypeIdentification_id}}"  name="TypeIdentification_id" required>
              @foreach($typeIdentification as $ti) 
              @if(($provider->TypeIdentification_id)==($ti->id))
              <option value="{{$ti->id}}" selected="selected">
                {{$ti->name}}
                 @else
                <option value="{{$ti->id}}">
                  {{$ti->name}}
                </option>
                @endif
              </option>
              @endforeach
              </select>
              <div class="invalid-feedback">
                Valid last name is required.
              </div>
            </div>
            <div class="col-md-3 mb-3">
              <label for="lastName">Identificacion</label>
              <input type="text" class="form-control" id="lastName" value="{{$provider->TypeIdentification_id}}" name="identification"  required>
              <div class="invalid-feedback">
                Valid last name is required.
              </div>
            </div>
          </div>
          <div class="mb-3">
            <label for="email">Email <span class="text-muted"></span></label>
            <input type="email" class="form-control" id="email" name="email"  value="{{$provider->user->email}}" required>
         
              @error('email')
              <div class="invalid-feedback" style="display:block">
              {{ $message }}
              </div>
              @enderror
            
           
          </div>
          <div class="mb-3">
            <label for="address">Direccion</label>
            <input type="text" class="form-control" id="address" name="address"  value="{{$provider->address}}"  required>
            <div class="invalid-feedback">
              Please enter your shipping address.
            </div>
          </div>
          <div class="mb-3">
            <label for="address">Telefono</label>
            <input type="text" class="form-control" id="phone" name="phone" value="{{$provider->phone}}" required>
            <div class="invalid-feedback">
              Please enter your shipping address.
            </div>
          </div>
          <div class="row">
            <div class="col-md-6  mb-3">
              <label for="address">Persona de Contacto</label>
              <input type="text" class="form-control" id="personcontact"  name="contactperson" value="{{$provider->contact_person}}" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>
            <div class="col-md-6 mb-3">
              <label for="country">Ciudad</label>
              <select class="custom-select d-block w-100" id="city" name="City_id" required>
               
                @foreach($ciudad as $ci)
                @if($ci->id==$provider->City_id)
                <option value="{{$ci->id}}" selected="selected">
                  {{$ci->name}}
              @else
              <option value="{{$ci->id}}">
              {{$ci->name}}
              </option>
              @endif
                </option>
                @endforeach
              </select>
              <div class="invalid-feedback">
                Please select a valid country.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6  mb-3">
              <label for="address">Certificado Camara de Comercio</label>
           <p> <a  href="{{$provider->trade_certificate}}" target="_blank">Ver
                                Certificado</a></p>  
              <input type="file" class="form-control" name="trade_certificate" >
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>
            <div class="col-md-6  mb-3">
              <label for="address">Rut</label>
             <p><a  href="{{$provider->tax_registration}}" target="_blank">Ver Rut</a></p> 
              <input type="file" class="form-control" name="tax_registration" >
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>
          </div>
          <button class="btn btn-primary " type="submit">Guardar</button>
        
            <a href="{{ route('provider') }}" type="button" class="btn btn-danger">
                <i class="fa fa-back" aria-hidden="true"></i> Cancelar
            </a>
       
        </form>
      </div>
    </div>
  </div>
@endsection
