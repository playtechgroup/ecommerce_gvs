@extends('layouts.app')
@section('content')
  <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
<div class="container">
    <div class="py-1 text-center">
      <h2>Crear devolución</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST" action="{{ url('/createdevolutionform') }}"
        enctype="multipart/form-data" >
        @csrf
          <input type="hidden" class="form-control" id="valores" name="valores">
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Motivo</label>
              <input type="text" class="form-control" id="reason" name="reason" placeholder="Ingrese el motivo"  required>
              <div class="invalid-feedback">
                Valid reason is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4  mb-3">
              <label for="address">Productos</label>
              <select class="form-control" name="products" id="products">
              @foreach($products as $product)
              <option value="{{$product->id}}">{{$product->name}}</option>
              @endforeach
              </select>
              <div class="invalid-feedback">
                Please enter the products.
              </div>
            </div>

            <div class="col-md-4  mb-3">
              <label for="address">Cantidad</label>
              <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Ingrese la cantidad">
              <div class="invalid-feedback">
                Please enter the quantity.
              </div>
            </div>
            
            <div class="col-md-4 mt-4">
              <button class="btn btn-primary btn-lg btn-block" type="button" onclick="agregarProducto()">Agregar</button>
            </div>
          </div>

          <div class="col-md-12 text-center">
            <h2>Productos en devolución</h2>
          </div>
          <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-responsive my-datatable"> id="tableProductos">
                    <thead>
                        <tr>
                            <th scope="col">Producto</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
          
          <button id="guardar" class="btn btn-primary btn-lg btn-block" type="submit">Guardar</button>
        </form>
      </div>
    </div>
  </div>
  <script type="application/javascript">
  $(function(){
      document.getElementById("guardar").disabled=true;   
   });
   function agregarProducto() {
     let productoActual = document.getElementById("products").value;
     let productoActualNombre = document.getElementById("products").options[document.getElementById("products").selectedIndex].text;
     let cantidadActual = document.getElementById("quantity").value;
     if (document.getElementById("valores").value == ""){
      var productos = '{"elements":[{"product":"'+productoActual+'","productName":"'+productoActualNombre+'","quantity":"'+cantidadActual+'"}]}';
      document.getElementById("valores").value=productos;
      document.getElementById("guardar").disabled=false;   
     } else {
      var productos = '{"product":"'+productoActual+'","productName":"'+productoActualNombre+'","quantity":"'+cantidadActual+'"}';
      var arr = JSON.parse(document.getElementById("valores").value);
      let encontrado = false;
      arr.elements.forEach(element => {
        if (element.product==productoActual){
          let sumar = +cantidadActual;
          element.quantity = +element.quantity +sumar;
          encontrado=true;
        }
      });
      if (!encontrado){
        var arr2 = JSON.parse(productos);
        arr.elements.push(arr2);
      }
      document.getElementById("valores").value=JSON.stringify(arr);
      document.getElementById("guardar").disabled=false; 
     }
      var arr = JSON.parse(document.getElementById("valores").value);
      let myhtml='<thead><tr><th scope="col">Producto</th><th scope="col">Cantidad</th><th scope="col">Acciones</th></tr></thead><tbody>';
      arr.elements.forEach(element => {
        myhtml += '<tr><th scope="col">'+element.productName+'</th><th scope="col">'+element.quantity+'</th><th scope="col"><a style="color: #3c8dbc; cursor:pointer;" onclick="eliminarProducto('+element.product+')" class="m5"><span><i class="fa fa-trash" aria-hidden="true"></i></span></a></th></tr>';
      });
      myhtml +='</tbody>';
      document.getElementById("tableProductos").innerHTML=myhtml;
   }

   
   function eliminarProducto(producto) {
      var arr = JSON.parse(document.getElementById("valores").value);
      let indexEliminar = 0;
      arr.elements.forEach(element => {
        if (element.product==producto){
          indexEliminar = arr.elements.indexOf(element);
        }
      });
      arr.elements.splice(indexEliminar,1);
      let myhtml='<thead><tr><th scope="col">Producto</th><th scope="col">Cantidad</th><th scope="col">Acciones</th></tr></thead><tbody>';
      arr.elements.forEach(element => {
        myhtml += '<tr><th scope="col">'+element.productName+'</th><th scope="col">'+element.quantity+'</th><th scope="col"><a style="color: #3c8dbc; cursor:pointer;" onclick="eliminarProducto('+element.product+')" class="m5"><span><i class="fa fa-trash" aria-hidden="true"></i></span></a></th></tr>';
      });
      myhtml +='</tbody>';
      document.getElementById("tableProductos").innerHTML=myhtml;
      if (arr.elements.length==0){
      document.getElementById("guardar").disabled=true; 
        document.getElementById("valores").value="";
      }
      else{
      document.getElementById("guardar").disabled=false; 
        document.getElementById("valores").value=JSON.stringify(arr);
      }
   }
  </script>
@endsection
