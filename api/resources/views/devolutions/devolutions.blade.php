@extends('layouts.app')
@section('content')
<div class="container">
    @include('flash::message')
    <div class="col-md-12 text-center">
        <h2>Devoluciones</h2>
    </div>
          <input type="hidden" class="form-control" id="valores" name="valores" value="{{$devolutions}}">
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a href="{{ route('createdevolution') }}" type="button" class="btn btn-primary paddingbuttons"> <i
                        class="fa fa-plus" aria-hidden="true"></i> Crear Devolución</a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Motivo</th>
                            <th scope="col">Productos</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($devolutions as $devolution)
                        <tr>
                            <td scope="row">{{$devolution->id}}</td>
                            <td>{{$devolution->reason}}</td>
                            <td id="{{$devolution->id.$devolution->reason}}"></td>
                            <td>
                                <input type="checkbox" name="active"  id="{{$devolution->id}}" onclick="actualizar_estado(this,'/deletedevolution','{{$devolution->id}}')"  @if($devolution->active) checked @endif>
                                <label for="{{$devolution->id}}"></label>
                            </td>
                            <td>
                                <a href="/editdevolution/{{$devolution->id}}" class="m5">
                                    <span><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                </a>
                            </td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
        </div>
    </div>
</div>
<script type="application/javascript">
    window.onload = function () {
        initmodal();
        var arr = JSON.parse(document.getElementById("valores").value);
        arr.forEach(element => {
            arr2= JSON.parse(element.products);
            let producto = "<ul>";
            arr2.elements.forEach(elemento => {
            let cantidad = elemento.quantity== 1 ? "unidad" : "unidades";
                producto += "<li>"+elemento.productName +": "+elemento.quantity+" "+cantidad+"</li>";
            });
            producto += "</ul>";
            document.getElementById(element.id+element.reason).innerHTML=producto; 
        });
    }

</script>

@endsection
