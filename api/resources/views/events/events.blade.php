@extends('layouts.app')
@section('content')
<div class="container">
    @include('flash::message')
    <div class="col-md-12 text-center">
        <h2>Eventos</h2>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-left" role="group" aria-label="...">
                <a href="{{ route('createtype') }}" type="button" class="btn btn-primary paddingbuttons"> <i
                        class="fa fa-plus" aria-hidden="true"></i> Crear tipo de evento</a>
            </div>
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a href="{{ route('createevent') }}" type="button" class="btn btn-primary paddingbuttons"> <i
                        class="fa fa-plus" aria-hidden="true"></i> Crear Evento</a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Título</th>
                            <th scope="col">Foto</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($events as $event)
                        <tr>
                            <td scope="row">{{$event->id}}</td>
                            <td>{{$event->title}}</td>
                            <td>
                                <img data-img='true' style="width:100px " src="{{$event->photo}}" alt="nohay..">
                            </td>
                            <td>
                                {{$event->eventType->description}}
                            </td>
                            <td> {!! $event->description !!} </td>
                            <td> {{$event->date }} </td>
                            <td>
                                <input type="checkbox" name="active"  id="{{$event->id}}" onclick="actualizar_estado(this,'/deleteevent','{{$event->id}}')"  @if($event->active) checked @endif>
                                <label for="{{$event->id}}"></label>
                            </td>
                            <td>
                                <a href="/editevent/{{$event->id}}" class="m5">
                                    <span><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                </a>
                            </td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
        </div>
    </div>
</div>
<script type="application/javascript">
    window.onload = function () {
        initmodal();
    }
</script>

@endsection
