@extends('layouts.app')
@section('content')
  <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
<div class="container">
    <div class="py-1 text-center">
      <h2>Crear tipo de evento</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST" action="{{ url('/createeventtype') }}"
        enctype="multipart/form-data" >
        @csrf
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Descripcion</label>
              <input type="text" class="form-control" id="description" name="description" placeholder="Ingrese la descripción"  required>
              <div class="invalid-feedback">
                Valid description is required.
              </div>
            </div>
          </div>
          <button class="btn btn-primary btn-lg btn-block" type="submit">Guardar</button>
        </form>
      </div>
    </div>
  </div>
@endsection
