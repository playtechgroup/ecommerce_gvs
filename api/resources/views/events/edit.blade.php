@extends('layouts.app')
@section('content')
  <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
<div class="container">
    <div class="py-1 text-center">
      <h2>Editar evento</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST"  action="/editeventform/{{$event->id}}"
        enctype="multipart/form-data" >
        @csrf
        <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Título</label>
              <input type="text" class="form-control" id="title" name="title" placeholder="Ingrese el título" value="{{$event->title}}" required>
              <div class="invalid-feedback">
                Valid title is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12  mb-3">
              <label for="address">Foto</label>
              <img src="{{$event->photo}}" style="width:100px" alt="No imagen">
                <input type="file" class="form-control" value="" name="photo" id="photo" >
              <div class="invalid-feedback">
                Please enter the event photo.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12  mb-3">
              <label for="address">Tipo de evento</label>
              <select class="form-control" name="eventId" required>
              @foreach($eventTypes as $eventType)
              @if($event->event_type_id == $eventType->id)
              <option value="{{$eventType->id}}" selected>{{$eventType->description}}</option>
              @else
              <option value="{{$eventType->id}}">{{$eventType->description}}</option>
              @endif
              @endforeach
              </select>
              <div class="invalid-feedback">
                Please enter the event type.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Fecha del evento</label>
              <input type="date" class="form-control" id="date" name="date"  value="{{$event->date}}" placeholder="Ingrese la fecha" required>
              <div class="invalid-feedback">
                Valid date is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12  mb-3">
              <label for="address">Descripción</label>
              <textarea class="ckeditor" name="description" id="description" rows="10" cols="80">
              {{$event->description}}
              </textarea>
              <div class="invalid-feedback">
                Please enter the description.
              </div>
            </div>
          </div>
            <button class="btn btn-primary " type="submit">Guardar</button>
            <a href="{{ route('events') }}" type="button" class="btn btn-danger">
                <i class="fa fa-back" aria-hidden="true"></i> Cancelar
            </a>
        </form>
      </div>
    </div>
  </div>
@endsection
