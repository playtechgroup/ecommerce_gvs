<aside class="main-sidebar">
    <!-- <aside class="main-sidebar" style="background-color: #fff;"> -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <!-- treeview -->
            <li class="header"><a  href="{{url('/home')}}"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
            <li><a class="" href="/categories"><i class="fa fa-flag"></i> <span>Categorias</span></a></li>
            <li><a class="" href="/users"><i class="fa fa-user"></i> <span>Usuarios</span></a></li>
            <li><a class="" href="/products"><i class="fa fa-shopping-cart"></i> <span>Productos</span></a></li>
            <li><a class="" href="/orders"><i class="fa fa-handshake-o"></i> <span>Pedidos</span></a></li>
            <li><a class="" href="/devolutions"><i class="fa fa-archive"></i> <span>Devoluciones</span></a></li>
            <li><a class="" href="/notices"><i class="fa fa-file"></i> <span>Blog</span></a></li>
            <li><a class="" href="/clinical"><i class="fa fa-book"></i> <span>Estudios Clínicos</span></a></li>
            <li><a class="" href="/events"><i class="fa fa-calendar"></i> <span>Eventos</span></a></li>
            <!-- <li><a class="" href="/linked"><i class="fa fa-users"></i> <span>Médicos agremiados</span></a></li> -->
            <!-- <li class="treeview menu-open">
                <a href="#"><i class="fa fa-truck"></i> <span>Provedores</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                  </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('provider') }}">Ver Provedores</a></li>
                    <li><a href="{{route('createprovider')}}">Crear Provedores</a></li>
                </ul>
            </li> -->
            <!-- <li>&nbsp;&nbsp;&nbsp;</li>
            <li>&nbsp;&nbsp;&nbsp;</li>
            <li><a class="" href="/terminos"><span>Terminos y condiciones</span></a></li>
            <li><a class="" href="/politicas"><span>Politica de datos</span></a></li>
            <li><a href="https://wa.me/573166672265" target="_blank" ><span>Ayuda</span></a></li> -->
        </ul>
        <ul>
        </ul>
    </section>
</aside>