<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo" style="background-color: #fff !important">
    <span class="logo-mini">
        <img class="logo-lg imgHome" src="{{ asset('logotipo.png') }}" alt="">
    </span>
    <img class="logo-lg imgHome" src="{{asset('logoGVS.svg')}}" alt="">
    </a>
    <nav class="navbar navbar-static-top ">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav nameUser">
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" v-pre>
                    <i class="fa fa-user-circle"></i> {{ Auth::user()->rol->name }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                    <a class="dropdown-item" href="/perfil">
                        {{ __('Perfil') }}
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        {{ __('Salir') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
