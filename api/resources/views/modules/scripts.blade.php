<!-- jQuery 2.2.3 -->
<script type="application/javascript" src= "{{ asset('lib/JQuery/jquery-2.2.3.min.js') }}" defer></script>
<script type="application/javascript" src= "{{ asset('lib/iCheck/icheck.min.js') }}" defer></script>



<!-- jQuery UI 1.11.4 -->
<script type="application/javascript" src="{{ asset('lib/JQuery/jquery-ui.min.js')}}" defer></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script type="application/javascript" defer>
setTimeout(() => {
  $.widget.bridge('uibutton', $.ui.button);
}, 800);

</script>


<!-- Bootstrap 3.3.6 -->

<script type="application/javascript" src= "{{ asset('lib/bootstrap/js/bootstrap.min.js') }}" defer></script>

<!-- Morris.js charts -->
<script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js" defer></script>

<script type="application/javascript" src= "{{ asset('lib/morris/morris.min.js') }}" defer></script>
<!-- Sparkline -->
<script type="application/javascript" src= "{{ asset('lib/sparkline/jquery.sparkline.min.js') }}" defer></script>
<!-- jvectormap -->
<script type="application/javascript" src= "{{ asset('lib/jvectormap/jquery-jvectormap-1.2.2.min.js') }}" defer></script>
<script type="application/javascript" src= "{{ asset('lib/jvectormap/jquery-jvectormap-world-mill-en.js') }}" defer></script>
<!-- jQuery Knob Chart -->
<script type="application/javascript" src= "{{ asset('lib/knob/jquery.knob.js') }}" defer></script>
<!-- daterangepicker -->
<script type="application/javascript" src="{{ asset('lib/moment/moment.min.js') }}"defer></script>
<script type="application/javascript" src= "{{ asset('lib/daterangepicker/daterangepicker.js') }}" defer></script>
<!-- datepicker -->
<script type="application/javascript" src= "{{ asset('lib/datepicker/bootstrap-datepicker.js') }}" defer></script>
<!-- Bootstrap WYSIHTML5 -->
<script type="application/javascript" src= "{{ asset('lib/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" defer></script>
<!-- Slimscroll -->
<script type="application/javascript" src= "{{ asset('lib/slimScroll/jquery.slimscroll.min.js') }}" defer></script>
<!-- FastClick -->
<script type="application/javascript" src= "{{ asset('lib/fastclick/fastclick.js') }}" defer></script>
<!-- AdminLTE App -->

<script type="application/javascript" src= "{{ asset('lib/dist/js/app.min.js') }}" defer></script>

<!-- PACE -->
<script type="application/javascript" src= "{{ asset('lib/pace/pace.min.js') }}" defer></script>
<!-- FullCalendar -->

<!-- select2-->
<script type="application/javascript" src= "{{ asset('lib/select2/select2.full.min.js') }}" defer></script>

<!-- popper-->
<script type="application/javascript" src= "{{ asset('lib/popper/popper.min.js') }}" defer></script>
<!-- Bootstrap Time Picker -->
<script type="application/javascript" src= "{{ asset('lib/timepicker/bootstrap-timepicker.min.js') }}" defer></script>
<script type="application/javascript" src= "{{ asset('js/caredosimetry.js') }}" defer></script>
<script type="application/javascript" src= "{{ asset('js/asynchronousUX.js') }}" defer></script>
<script type="application/javascript" src= "{{ asset('js/sweetalert.min.js') }}" defer></script>
<script type="application/javascript" src= "{{ asset('js/validator.min.js') }}" defer></script>
<script type="application/javascript" src="{{ asset('lib/datatable/datatables.min.js') }}" defer></script>
<script type="text/javascript" src="{{asset('libs\Buttons-1.6.3\js\dataTables.buttons.min.js')}}" defer></script>
<script type="text/javascript" src="{{asset('libs\Buttons-1.6.3\js\buttons.flash.min.js')}}" defer></script>
<script type="text/javascript" src="{{asset('libs\Buttons-1.6.3\js\buttons.html5.min.js')}}" defer></script>
<script type="text/javascript" src="{{asset('libs\Buttons-1.6.3\js\buttons.print.min.js')}}" defer></script>
