<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0.0
  </div>
  <strong>Copyright &copy; 2020 <a target="_blank" href="#">Inmunotek App</a> </strong> All rights
  reserved.
</footer>
