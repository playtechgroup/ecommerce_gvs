@extends('layouts.app')
@section('content')
<div class="row">
@include('flash::message')
    <div class="col-md-12">
        <h3>Bienvenido {{Auth::user()->name}}</h3>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{$notices}}</h3>
                <p>Noticias</p>
            </div>
            <div class="icon">
                <i class="ion ion-chatbubbles"></i>
            </div>
            <a href="/notices" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{$events}}</h3>
                <p>Eventos</p>
            </div>
            <div class="icon">
                <i class="ion  ion-calendar"></i>
            </div>
            <a href="/events" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Accesos directos</h3>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="/createnotice">
        <div class="info-box bg-light-blue-gradient">
            <div class="info-box-content">
                <span>Crear nueva noticia</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box ion-arrow-graph-up-right -->
    </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="/createevent">
        <div class="info-box bg-purple-gradient">
            <div class="info-box-content">
                <span>Crear nuevo evento</span>
            </div>
        </div>
    </a>
    </div>
</div>
@endsection