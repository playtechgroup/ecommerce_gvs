@extends('layouts.app')
@section('content')
<div class="container">
    @include('flash::message')
    <div class="col-md-12 text-center">
        <h2>Estudios Clínicos</h2>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a href="{{ route('createclinical') }}" type="button" class="btn btn-primary paddingbuttons"> <i
                        class="fa fa-plus" aria-hidden="true"></i> Crear Noticia</a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Título</th>
                            <th scope="col">Foto</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($notices as $notice)
                        <tr>
                            <td scope="row">{{$notice->id}}</td>
                            <td>{{$notice->title}}</td>
                            <td>
                                <img data-img='true' style="width:100px " src="{{$notice->photo}}" alt="nohay..">
                            </td>
                            <td> {!! $notice->description !!} </td>
                            <td>
                                <input type="checkbox" name="active"  id="{{$notice->id}}" onclick="actualizar_estado(this,'/deletenotice','{{$notice->id}}')"  @if($notice->active) checked @endif>
                                <label for="{{$notice->id}}"></label>
                            </td>
                            <td>
                                <a href="/editclinical/{{$notice->id}}" class="m5">
                                    <span><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                </a>
                            </td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
        </div>
    </div>
</div>
<script type="application/javascript">
    window.onload = function () {
        initmodal();
    }
</script>

@endsection
