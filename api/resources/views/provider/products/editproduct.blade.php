@extends('layouts.app')
<style>
   #skills > span >span >span>.select2-selection__rendered{
   overflow: auto !important;
   height: 100px;
   }
</style>
@section('content')
<div class="container">
   <div class="py-1 text-center">
      <h2>Editar Producto</h2>
   </div>
   <div class="row">
      <div class="col-md-10 offset-md-1">
         @if ($errors->any())
         <div class="alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         <form class="needs-validation" method="POST" action="{{ url('/editproductform') }}"
            enctype="multipart/form-data">
            @csrf
            <div class="row">
               <div class="col-md-3 mb-3">
                  <label for="lastName">Código (Obligatorio)</label>
                  <input type="text" class="form-control" id="code" name="code" value="{{$product->code}}"
                     placeholder="Ingrese el código del producto" required>
                  <div class="invalid-feedback">
                     Valid code is required.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                  <label for="firstName">Nombre (Obligatorio)</label>
                  <input type="hidden" class="form-control" id="firstName" name="idProduct"
                     value="{{$product->id}}">
                  <input type="text" class="form-control" id="firstName" name="name" value="{{$product->name}}"
                     placeholder="Ingrese el nombre" required>
                  <div class="invalid-feedback">
                     Valid first name is required.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                  <label for="lastName">Categoria (Obligatorio)</label>
                  <select class="custom-select d-block w-100" id="typeidentification" name="category"
                     onchange="subcategory(this.value)" required>
                     <option value="">Seleccione</option>
                     @foreach($categories as $ti)
                     @if(($product->subcategoryproduct->categoryproduct->id)==($ti->id))
                     <option value="{{$ti->id}}" selected="selected">
                        {{$ti->name}}
                        @else
                     <option value="{{$ti->id}}">
                        {{$ti->name}}
                     </option>
                     @endif
                     </option>
                     @endforeach
                  </select>
                  <div class="invalid-feedback">
                     Valid last name is required.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                  <label for="lastName">SubCategoria (Obligatorio)</label>
                  <select class="custom-select d-block w-100" id="SubcategoryProduct_id"
                     name="SubcategoryProduct_id" required>
                     <option value="{{$product->subcategoryproduct->id}}">{{$product->subcategoryproduct->name}}
                     </option>
                  </select>
                  <div class="invalid-feedback">
                     Valid last name is required.
                  </div>
               </div>
            </div>
            <div class="mb-3">
               <label for="email">Descripción (Obligatorio) <span class="text-muted"></span></label>
               <textarea class="form-control" name="description" id="" cols="30" rows="5" placeholder="example"
                  required>{{$product->description}}</textarea>
               <div class="invalid-feedback">
                  Valid last name is required.
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 mb-3">
                  <label for="address">Precio de venta (Obligatorio)</label>
                  <input type="text" class="form-control valorescomas" id="price" name="price"
                     value="{{$product->price}}" placeholder="$ 50.000" required>
                  <div class="invalid-feedback">
                     Please enter product price.
                  </div>
               </div>
               <div class="col-md-6 mb-3">
                  <label for="address">IVA % (Obligatorio)</label>
                  <input type="number" min="0" max="19" class="form-control" id="tax" name="tax"
                     value="{{$product->tax}}" placeholder="19%" required>
                  <div class="invalid-feedback">
                     Please enter the product tax.
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 mb-3">
                  <label for="address">Stock (Obligatorio)</label>
                  <input type="number" min="0" max="999999" class="form-control" id="stock" name="stock" value="{{$product->stock}}"
                     placeholder="Ingrese el inventario del producto" required>
                  <div class="invalid-feedback">
                     Please enter the product stock.
                  </div>
               </div>
               <div class="col-md-6 mb-3">
                  <label for="address">Stock mínimo (Obligatorio)</label>
                  <input type="number" min="0" max="999999" class="form-control" id="minimumStock" name="minimumStock" value="{{$product->minimumStock}}"
                     placeholder="Ingrese el inventario mínimo del producto" required>
                  <div class="invalid-feedback">
                     Please enter the minimum stock of the product.
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 mb-3">
                  <label for="photos">Fotos del producto Min 1 Max 5 (800px*800px) </label>
                  <div style="display: flex ;margin:10px ;">
                     @php
                     $fotos=0;
                     @endphp
                     @foreach ($files as $item)
                     @if ($item->type=="photo")
                     @php
                     $fotos+=1;
                     @endphp
                     <div>
                        <span style="color:red; position:absolute; margin:5px ; font-weight:900"><a
                           style="color:red;cursor: pointer;"
                           onclick="deletefile({{$item->id}})">X</a></span>
                        <img style="max-width: 100px;height: auto;margin-right: 10px;" src="{{$item->link}}"
                           alt="">
                     </div>
                     @endif
                     @endforeach
                  </div>
                  @if ($fotos>4)
                  Para poder agregar una foto elimine una
                  @else
                  <input type="file" class="form-control" name="photos[]"
                     accept="image/x-png,image/gif,image/jpeg" multiple>
                  @endif
                  <p class="help-block">Seleccione varias fotos del producto con la tecla CTRL </p>
               </div>
               <div class="col-md-6 mb-3">
                  <label for="photos">Registro invima del producto</label>
                  <div style="display: flex ;margin:10px ;">
                     @php
                     $file=0;
                     @endphp
                     @foreach ($files as $item)
                     @if ($item->name=="Invima")
                     @php
                     $file+=1;
                     @endphp
                     <div style="max-width: 100px;">
                        <span style="color:red; position:absolute; margin:5px ; font-weight:900"><a
                           style="color:red;cursor: pointer;" onclick="deletefile({{$item->id}})">X</a></span>
                        <object data="{{$item->link}}" type="application/pdf">
                           <embed src="{{$item->link}}" type="application/pdf" />
                        </object>
                     </div>
                     @endif
                     @endforeach
                  </div>
                  @if ($file>0)
                  Para poder agregar o editar el archivo eliminelo
                  @else
                  <input type="file" class="form-control" name="invima" accept="application/pdf">
                  @endif
                  <div class="invalid-feedback">
                     Please enter invima register.
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 mb-3">
                  <label for="photos">Especificaciones del producto</label>
                  <div style="display: flex ;margin:10px ">
                     @php
                     $file=0;
                     @endphp
                     @foreach ($files as $item)
                     @if ($item->name=="Especificaciones")
                     @php
                     $file+=1;
                     @endphp
                     <div style="max-width: 100px;">
                        <span style="color:red; position:absolute; margin:5px ; font-weight:900"><a
                           style="color:red;cursor: pointer;" onclick="deletefile({{$item->id}})">X</a></span>
                        <object data="{{$item->link}}" type="application/pdf">
                           <embed src="{{$item->link}}" type="application/pdf" />
                        </object>
                     </div>
                     @endif
                     @endforeach
                  </div>
                  @if ($file>0)
                  Para poder agregar o editar el archivo eliminelo
                  @else
                  <input type="file" class="form-control" name="espefification" accept="application/pdf">
                  @endif
                  <div class="invalid-feedback">
                     Please enter product specifications.
                  </div>
               </div>
               <div class="col-md-6 mb-3">
                  <label for="photos">Política de devolución</label>
                  <div style="display: flex ;margin:10px ">
                     @php
                     $file=0;
                     @endphp
                     @foreach ($files as $item)
                     @if ($item->name=="Política de devolución")
                     @php
                     $file+=1;
                     @endphp
                     <div>
                        <span style="color:red; position:absolute; margin:5px ; font-weight:900"><a
                           style="color:red;cursor: pointer;" onclick="deletefile({{$item->id}})">X</a></span>
                        <object data="{{$item->link}}" type="application/pdf">
                           <embed src="{{$item->link}}" type="application/pdf" />
                        </object>
                     </div>
                     @endif
                     @endforeach
                  </div>
                  @if ($file>0)
                  Para poder agregar o editar el archivo eliminelo
                  @else
                  <input type="file" class="form-control" name="garanty" accept="application/pdf">
                  @endif
                  <div class="invalid-feedback">
                     Please enter warranty policies.
                  </div>
               </div>
            </div>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Guardar</button>
         </form>
      </div>
   </div>
</div>
<script type="application/javascript">
       function deletefile(id) {
   
           $.ajax({
               type: "POST",
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               url: '/deletefile',
               data: {
                   'id': id
               },
               dataType: "json",
               success: function (res) {
                   console.log(res);
                   if (res.response) {
                       window.location.reload();
                   }
   
   
               },
               error: function (jqXHR, exception) {
                   console.log(jqXHR);
               }
           });
       }
   
       function subcategory(e) {
   
           $.ajax({
               type: "GET",
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               url: '/getsubcategory/' + e,
               dataType: "json",
               success: function (res) {
                   console.log(res);
                   var subcategory = '<option value="">Seleccione</option>';
                   var element = document.getElementById('SubcategoryProduct_id');
                   console.log(element);
   
                   if (res.response) {
                       for (var i = 0; i < res.items.length; i++) {
                           subcategory += '<option value="' + res.items[i].id + '">' + res.items[i].name +
                               '</option>';
                           element.innerHTML = subcategory;
   
                       }
   
                   }
   
               },
               error: function (jqXHR, exception) {
                   console.log(jqXHR);
               }
           });
   
       }
   
       function readURL(input) {
           if (input.files && input.files[0]) {
               var reader = new FileReader();
   
               reader.onload = function (e) {
                   $('#blah').attr('src', e.target.result);
               }
   
               reader.readAsDataURL(input.files[0]); // convert to base64 string
           }
       }
   
</script>
@endsection