@extends('layouts.app')
@section('content')
<div class="container">
    @include('flash::message')
    <div class="col-md-12 text-center">
        <h2>Productos</h2>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a href="{{ route('createproducts') }}" type="button" class="btn btn-primary paddingbuttons"> <i
                        class="fa fa-plus" aria-hidden="true"></i> Crear Producto</a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-responsive my-datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col">Precio</th>
                            <th scope="col">IVA</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Subcategoria</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($productos as $producto)
                        <tr>
                            <td scope="row">{{$producto->id}}</td>
                            <td>{{$producto->name}}</td>
                            <td>{{$producto->description}}</td>
                            <td>$ {{number_format($producto->price)}}</td>
                            <td>{{$producto->tax}}</td>
                            <td>{{$producto->subcategoryproduct->categoryproduct->name}}</td>
                            <td>{{$producto->subcategoryproduct->name}}</td>
                            <td>

                                <input type="checkbox" name="active"  id="{{$producto->id}}" onclick="actualizar_estado(this,'/updatestateproduct','{{$producto->id}}')"  @if($producto->active) checked @endif>
                                <label for="{{$producto->id}}"></label>
                            </td>


                            <td>
                                <a href="/viewproduct/{{$producto->id}}" class="m5">
                                    <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                                </a>
                                <a href="/editproduct/{{$producto->id}}" class="m5">
                                    <span><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                </a>
                                <a onclick="" class="m5">
                                    <span><i class="fa fa-trash" aria-hidden="true"></i></span>
                                </a>
                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">

        </div>
    </div>
</div>
<script type="application/javascript">
    window.onload = function () {
        var id_1 = "1";
        var id_2 = "2";
        var id_3 = "3";
        initmodal();
    }

</script>

@endsection
