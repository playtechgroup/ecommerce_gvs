@extends('layouts.app')

@section('content')
<div class="container">
@include('flash::message')
    <div class="row">

        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title ">{{$product->name}}</h3>
                </div>
                <div class="panel-body ">
                    <div class="row ">
                        <div class="col-md-6 ">
                            <h5>Descripción:</h5>
                            <p>{{$product->description}}</p>

                        </div>
                        <div class="col-md-6">
                            <h5>Precio:</h5>
                            <p>{{number_format($product->price)}}</p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Código:</h5>
                            <p>{{$product->code}}</p>

                        </div>
                        <div class="col-md-6">
                            <h5>Estado:</h5>
                            <p>{{$product->active}}</p>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5>Categoria:</h5>
                            <p>{{$product->subcategoryproduct->categoryproduct->name}}</p>

                        </div>
                        <div class="col-md-6">
                            <h5>Subcategoria:</h5>
                            <p>{{$product->subcategoryproduct->name}}</p>

                        </div>
                    </div>
                    
                    <div class="row ">
                        <div class="col-md-6 ">
                            <h5>Stock:</h5>
                            <p>{{$product->stock}}</p>

                        </div>
                        <div class="col-md-6">
                            <h5>Stock mínimo:</h5>
                            <p>{{$product->minimumStock}}</p>

                        </div>
                    </div>

                    
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>IVA:</h5>
                            <p>{{$product->tax}}</p>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title ">Archivos</h3>
                </div>
                <div class="panel-body ">

                    <div class="table-responsive">
                        <table class="table table-responsive my-datatable">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Archivo</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($files as $file)
                                <tr>
                                    <td scope="row">{{$file->id}}</td>
                                        <td>{{$file->name}}</td>
                                        <td> <img data-img='true' style="width:100px " src="{{$file->link}}" alt="nohay.."> </td>



                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>


        <div class="btn-group" role="group">
            <a href="{{ route('products') }}" type="button" class="btn btn-primary  margin">
                <i class="fa fa-back" aria-hidden="true"></i> Volver
            </a>
            <a href="/editproduct/{{$product->id}}" type="button" class="btn btn-warning margin ">
                <i class="fa fa-back" aria-hidden="true"></i> Editar Producto
            </a>
        </div>
    </div>


</div>

@endsection
