@extends('layouts.app')
@section('content')
<div class="row">
@include('flash::message')
    <div class="col-md-12">
        <h3>Bienvenido {{Auth::user()->name}}</h3>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>{{$product}}</h3>
                <p>Productos</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="/products" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{$orders}}</h3>
                <p>Ordenes</p>
            </div>
            <div class="icon">
                <i class="ion  ion-android-cart"></i>
            </div>
            <a href="/ordersbyprovider" class="small-box-footer">Mas informacion <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Accesos directos</h3>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="/create">
        <div class="info-box bg-light-blue-gradient">
            <div class="info-box-content">
                <span> <a href="{{route('createproducts')}}" style="color: #fff">Crear nuevo producto</a></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box ion-arrow-graph-up-right -->
    </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="/ordersbyprovider">
        <div class="info-box bg-purple-gradient">
            <div class="info-box-content">
                <span>Historial de Ordenes</span>
            </div>
        </div>
    </a>
    </div>
</div>
@endsection
