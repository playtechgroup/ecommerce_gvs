<li class="nav-item">
    <a class="nav-link" href="/home">Inicio</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="/categories">Categorias</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="/orders">Mis ordenes</a>
</li>
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Provedores</a>
    <div class="dropdown-menu">
        <a class="dropdown-item" href="{{ route('provider') }}">Ver Provedores</a>
        <a class="dropdown-item" href="{{route('createprovider')}}">Crear Provedores</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false" v-pre>
    {{ Auth::user()->rol->name }} <span class="caret"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</li>