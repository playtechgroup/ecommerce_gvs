<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('ITK-mini-trans.gif')}}" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Admin') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/src.js') }}" defer></script>
    
    @include('modules.scripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        class="href">
    <link href="{{ asset('libs/font-awesome-4.7.0/css/font-awesome.css')}}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">


    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('lib/ionicons-2.0.1/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('lib/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/dist/css/AdminLTE.css') }}">
    <!-- AdminLTE  -->
    <link rel="stylesheet" href="{{ asset('lib/dist/css/skins/_all-skins.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('lib/iCheck/flat/blue.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('lib/morris/morris.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('lib/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('lib/datepicker/datepicker3.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('lib/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <!-- Pace -->
    <link rel="stylesheet" href="{{ asset('lib/pace/pace.min.css') }}">
    <!-- Own stylesheet -->
    <link rel="stylesheet" href="{{ asset('lib/styles/style.css') }}">
    <!-- select2-->
    <link rel="stylesheet" href="{{ asset('lib/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/caredosimetry.css') }}">
    
    <link rel="stylesheet" href="{{ asset('lib/datatable/datatables.min.css') }}">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

</head>

<body class="hold-transition skin-blue-light sidebar-mini">

        @guest
        <div id="app">
        <main class="" style="">
            @yield('content')
        </main>
    </div>
        @else
        <div id="app" class="wrapper">



            @include('modules.header')
            @if(Auth::user()->rol->id=='1')
                @include('modules.aside')
            @elseif(Auth::user()->rol->id=='3')
                @include('modules.asideprovider')
            @elseif(Auth::user()->rol->id=='4')
                @include('modules.asideinventory')
            @elseif(Auth::user()->rol->id=='5')
                @include('modules.asidepublisher')
            @endif



            <!-- Content Wrapper. Contains page content -->
            <div id="asyncLoadArea" class="content-wrapper">
                <!-- Content Header (Page header) -->

                    <section class="content">


                        @yield('content')


                    </section>


            </div>




            @include('modules.footer')

    </div>
    @endguest

</body>

</html>
