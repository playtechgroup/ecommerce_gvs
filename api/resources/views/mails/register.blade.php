<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<title>Helppiu</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>

<body style="margin: 0; padding: 0;">
	<table align="center" cellpadding="0" cellspacing="30" width="600">

		<tr>
			<td align="center" >
				<img src="http://app.helppiu.com/img/logo.png" alt="helppiu" width="300" height="auto" style="display: block;" />
				<h1 style="font-family: Arial; color: #ffcf01;text-transform: capitalize; text-align: center;">¡Hola {{$data['name']}}!  <br> Te damos la bienvenida a <strong
					style="color: #ffcf01;">Helppiu</strong> <br></h1>
				<h4 style="font-family: Arial; color: gray;"> Una aplicación móvil para pedir servicios a
					domicilio de forma segura con Helppiu tendrás la oportunidad de contactar a las mejores empresas de
					servicios para que puedan ir a tu casa u oficina de forma rápida y eficiente.</h4>
			</td>
		</tr>
		<tr>
			<td align="center">
				<img src="{{asset('img/slider-caption.png')}}" alt="app" width="40%" height="auto"
					style="display: block;" />
			</td>
		</tr>
		<tr style="margin-bottom: 20px">
			<td align="center" >
				<a style="
					font-family: Arial;
				    background: #ffcf01;
				    color: #fff;
				   padding: 8px;
				    text-decoration: none;
				    font-size: 15px;
				    text-transform: uppercase;
				    letter-spacing: 1px;" href="https://app.helppiu.com/terminos">Terminos y Condiciones</a>

			</td>

		</tr>

		<tr>
			<td align="center"> <a style="
					font-family: Arial;
				    background: #ffcf01;
				    color: #fff;
					padding: 8px;
				    text-decoration: none;
				    font-size: 15px;
				    text-transform: uppercase;
					letter-spacing: 1px;" href="https://app.helppiu.com/politicaprivacidad">Política de Protección de datos personales</a>
			</td>

		</tr>

		<tr>
			<td align="center" bgcolor="#ffcf01" >
				<h4 style="font-family: Arial; color: #fff;">Nuestras redes
					sociales</h4>
				<a href="https://www.facebook.com/helppiu"><img src="https://app.helppiu.com/img/facebook-square-brands.png" alt="" width="40px" ></a>
					<a href="https://www.instagram.com/helppiu">
						<img src="https://app.helppiu.com/img/instagram-brands.png" width="40px" alt="">
					</a>
			</td>
		</tr>


	</table>
</body>

</html>
