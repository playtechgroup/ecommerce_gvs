<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<title>Helppiu</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>

<body style="margin: 0; padding: 0;">
	<table align="center" cellpadding="0" cellspacing="30" width="600">

		<tr>
			<td align="center">
				<img src="http://app.helppiu.com/img/logo.png" alt="helppiu" width="300" height="auto"
                    style="display: block;" />
                    <p style="text-align:center;font-family: sans-serif;">Hola,

                        Hemos realizado unos cambios en nuestros Términos de Servicio, puedes consultar la última versión de los Términos
                        @if ($tipo==1)
                        <a  href="http://app.helppiu.com/terminos" target="_blank">aquí.</a>
                        @else
                        <a  href="http://app.helppiu.com/terminos/empresa" target="_blank">aquí.</a>
                        @endif
						<br>
						<br>
                        Gracias por ser parte de nuestra comunidad Helppiu.

                    <h4>
                       <a href="https://app.helppiu.com/" target="_blank">Haz clic en este enlace para
                            ingresar y aceptarlos</a>
                    </h4>




			</td>
		</tr>


		<tr>
			<td align="center"> <a style="
					font-family: Arial;
				    background: #ffcf01;
				    color: #fff;
					padding: 8px;
				    text-decoration: none;
				    font-size: 15px;
				    text-transform: uppercase;
					letter-spacing: 1px;" href="http://app.helppiu.com/politicaprivacidad">Política de Protección de datos
					personales</a>
			</td>

		</tr>

		<tr>
			<td align="center" bgcolor="#ffcf01">
				<h4 style="font-family: Arial; color: #fff;">Nuestras redes
					sociales</h4>
				<a href="https://www.facebook.com/helppiu"><img
						src="http://app.helppiu.com/img/facebook-square-brands.png" alt="" width="40px"></a>
				<a href="https://www.instagram.com/helppiu">
					<img src="http://app.helppiu.com/img/instagram-brands.png" width="40px" alt="">
				</a>
			</td>
		</tr>


	</table>
</body>

</html>
