<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<title>Inmunotek</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>

<body style="margin: 0; padding: 0;">
	<table align="center" cellpadding="0" cellspacing="0" width="600">
		<tr>
			<td align="center" bgcolor="#fffff" style="padding: 20px 0;"></td>
		</tr>
		<tr>
			<td align="center" bgcolor="#fffff" style="padding: 20px 0;">
				<img src="https://inmunotek.evocompany.co/marca.png" alt="Inmunotek" width="300" height="auto" style="display: block;" />
				<h1 style="font-family: Arial; color: #5f95ed;text-transform: capitalize;">¿Has olvidado tu contraseña?
				</h1>
				<h4 style="font-family: Arial; color: #000;"><strong style="color: #5f95ed;">Inmunotek.app</strong>
					informa que se ha solicitado restablecer la contraseña</h4>

					<h3 style="font-family: Arial; color: #5f95ed;padding-bottom: 10px;">Para restablecer tu contraseña es
						necesario aceptar este mensaje y seguir los pasos posteriores</h3>
					<a style="
						font-family: Arial;
						background:#5f95ed;
						color: #fff;
						padding: 15px 50px;
						text-decoration: none;
						font-size: 15px;
						text-transform: uppercase;
						letter-spacing: 1px;"
						href="{{url('/')}}/auth/reestablecer/email/{{$data['email']}}/token/{{$data['token']}}">Restablecer
						contraseña</a>
			</td>
		</tr>


	</table>
</body>

</html>
