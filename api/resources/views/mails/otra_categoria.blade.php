<h1>Declaracion personalizada para el usuario {{$nombre}} {{$apellido}}</h1>

<p>
Por favor comunicarse con el usuario {{$nombre}} {{$apellido}} por el correo {{$email}}; para poder culminar su declaracion ya que no se encuentra en ninguno de los formulados estipulados en TaxRenta:
	<ul>
		<li>Formulario 210</li>
		<li>Formulario 230</li>
		<li>Formulario 240</li>
	</ul>
</p>