@extends('layouts.app')
@section('content')
  <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
<div class="container">
    <div class="py-1 text-center">
      <h2>Editar noticia</h2>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="needs-validation" method="POST"  action="/editnoticeform/{{$notice->id}}"
        enctype="multipart/form-data" >
        @csrf
        <div class="row">
            <div class="col-md-12 mb-3">
              <label for="firstName">Título</label>
              <input type="text" class="form-control" id="title" name="title" placeholder="Ingrese el título" value="{{$notice->title}}" required>
              <div class="invalid-feedback">
                Valid title is required.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12  mb-3">
              <label for="address">Foto</label>
              <img src="{{$notice->photo}}" style="width:100px" alt="No imagen">
                <input type="file" class="form-control" value="" name="photo" id="photo" >
              <div class="invalid-feedback">
                Please enter the notice photo.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12  mb-3">
              <label for="address">Descripción</label>
              <textarea class="ckeditor" name="description" id="description" rows="10" cols="80">
              {{$notice->description}}
              </textarea>
              <div class="invalid-feedback">
                Please enter the description.
              </div>
            </div>
          </div>
            <button class="btn btn-primary " type="submit">Guardar</button>
            <a href="{{ route('notices') }}" type="button" class="btn btn-danger">
                <i class="fa fa-back" aria-hidden="true"></i> Cancelar
            </a>
        </form>
      </div>
    </div>
  </div>
@endsection
