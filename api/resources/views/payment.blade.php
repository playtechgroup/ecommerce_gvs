@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <br>
            <div class="panel panel-default" style="text-align: center">
                <div class="panel-heading">
                    <img id="logoLogin" src="{{asset('marca.png')}}" alt="" style="margin-bottom:20px;">
                    <h1 style="margin-bottom:20px;">¡Gracias por tu Pago!</h1>
                    <h2>Información de la orden:</h2><br>
                </div>

                <div class="panel-body" style="text-align: left; padding: 10px; border: 1px solid #005782; border-radius: 10px;">
                    <h4>Orden {{$Order->id}}</h4>
                    <h4>Fecha:</h4>
                    <p> {{$Order->date}}</p>
                    <h4>Estado de la orden:</h4>
                    <p> {{$Order->orderstatus->name}}</p>
                    <h4>Valor del pago:</h4>
                    <p> ${{number_format($valor)}}</p>
                    <h4>Estado del pago:</h4>
                    <p> {{$estado_pago}}</p>
                    <h4>Observaciones:</h4>
                    <p>{{$Order->commentary? $Order->commentary : "No hay observaciones"}}</p>
                    <h4>Productos:</h4>
                        <ul>
                        @foreach($Products as $key => $value)
                            <li><b>{{$value->product->name}} <span style="color:#005782"> x{{$value->quantity}}</span></b></li>
                        @endforeach
                        </ul>
                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
