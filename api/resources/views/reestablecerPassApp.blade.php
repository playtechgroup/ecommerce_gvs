<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Cambiar Contraseña </title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('img/logo.png') }}" type="image/x-icon">
    <!-- Scripts -->
    <script>
        window.Laravel = {
            !!json_encode([
                'csrfToken' => csrf_token(),
            ]) !!
        };
	</script>
	 <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="row" style="padding:20px">
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4"></div>
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">Restablecer contraseña</h4>
			</div>
			<div class="panel-body">
					@if(isset($messagetrue))
						<br>
							<div class="alert alert-success" role="alert">
								{{$messagetrue}}
							</div>
						@else
				<form method="post" action="{{url('auth/nuevoPassApp')}}">

					<input type="hidden" name="email" value="{{$email_new}}">
								<input type="hidden" name="token" value="{{$token_new}}">	
							
								

					<div class="form-group has-feedback">
						<input type="password" name="password" class="form-control" value="{{old('password')}}" placeholder="Digite nueva contraseña" required>
					    <i class="glyphicon glyphicon-lock form-control-feedback"></i>
						<div class="text-danger">{{$errors->formulario->first('password')}}</div>
					</div>

					<div class="form-group has-feedback">
						<input type="password" name="password_new" class="form-control" value="{{old('password_new')}}" placeholder="Repetir contraseña" required>
					    <i class="glyphicon glyphicon-lock form-control-feedback"></i>
						<div class="text-danger">{{$errors->formulario->first('password_new')}}</div>
					</div>

					<button type="submit" class="btn btn-warning btn-block">Actualizar contraseña</button>
					@endif

						@if(Session::has('messagefail'))
						<br>
							<div class="alert alert-danger" role="alert">
								{{Session::get('messagefail')}}
							</div>
						@endif
						{{csrf_field()}}
				</form>
			</div>
		</div>
	
	</div>
	<div class="col-xs-0 col-sm-0 col-md-3 col-lg-4"></div>
	
	</div>
	</body>

