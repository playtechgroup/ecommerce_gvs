<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// ::::::::::::::::::::::::::HOME::::::::::::::::::::::::::::::::::::
Route::get('category_products_menu','User\ApiHomeUserController@category_products_menu');
Route::get('notifications_home_new_products','User\ApiHomeUserController@notifications_home_new_products');
Route::get('notifications_not_read/{id}','User\ApiHomeUserController@notifications_not_read');
// ::::::::::::::::::::::::SUBCATEGORY List::::::::::::::::::::::::::::::::::::
Route::get('list_subcategory_ofcategory/{id}','User\ApiSubcategoryUserController@list_subcategory_ofcategory');
// ::::::::::::::::::::::::INVOICE::::::::::::::::::::::::::::::::::::
Route::get('send_invoce/{id}','User\ApiInvoiceUserController@send_invoce');
// ::::::::::::::::::::::::SEND PAYMET METHOD::::::::::::::::::::::::::::::::::::
Route::get('payment_method','User\ApiPaymentmethodUserController@payment_method');
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Route::get('countries', 'User\ApiUserController@country');
Route::get('departments/{id}', 'User\ApiUserController@department');
Route::get('cities/{id}', 'User\ApiUserController@city');
Route::get('typeidentification', 'User\ApiUserController@typeidentification');
Route::get('typecostumer', 'User\ApiUserController@typecostumer');
// :::::::::::::::::::::::::PRODUCTS::::::::::::::::::::::::::::::::::::::::
Route::get('products_list/{type}/{id}', 'User\ApiProductUserController@products_list');
Route::get('products_list', 'User\ApiProductUserController@all');
Route::get('products_list_price/{type}/{id}/{filtro}', 'User\ApiProductUserController@products_list_price');
Route::get('rate/{type}/{id}/{filtro}', 'User\ApiProductUserController@rate');
// :::::::::::::::::::::::::ORDERS::::::::::::::::::::::::::::::::::::::::
Route::post('createorder', 'User\ApiOrderUserController@createorder');
Route::get('historyorders/{id}', 'User\ApiOrderUserController@historyorders');
Route::get('notificationorders/{id}', 'User\ApiOrderUserController@notificationorders');
Route::post('newnotification', 'User\ApiOrderUserController@newNotification');
// :::::::::::::::::::::::::ADDRESS::::::::::::::::::::::::::::::::::::::::
Route::post('createaddress', 'User\ApiAddressController@create');
Route::get('addresses/{id}', 'User\ApiAddressController@index');
Route::post('editprofile', 'User\ApiUserController@editprofile');
Route::post('editpassword', 'User\ApiUserContrasena@editpassword');
// :::::::::::::::::::::: OTROS :::::::::::::::::::::::::::::::::::
Route::get('terminos', 'Web\ApiOtrosController@terminos');
Route::get('terminosusuario', 'Web\ApiOtrosController@userterms');
Route::get('politicas', 'Web\ApiOtrosController@politicas');
Route::get('ayuda', 'Web\ApiOtrosController@ayuda');
Route::post('resetpassword','User\ApiUserContrasena@reestablecerPass');
Route::get('getnotices','User\ApiBlogController@index');
Route::get('getclinical','User\ApiBlogController@indexClinical');
Route::get('getevents','User\ApiEventsController@index');
Route::post('getLinked','User\ApiHomeUserController@getLinked');
// :::::::::::::::::Pay::::::::::::::::::::::::::.
Route::get('pago/respuesta', 'User\ApiPayUserController@pagoConfirmationget');
Route::post('pago/confirmation','User\ApiPayUserController@pagoConfirmation');
Route::get('getPagoConfirmation/{id}','User\ApiPayUserController@getPagoConfirmation');
// :::::::::::::::::Paytucompra::::::::::::::::::::::::::.
Route::post('pago/respuestatucompra', 'User\ApiPayUserController@pagoRetorno');
Route::post('pago/confirmationtucompra','User\ApiPayUserController@pagoConfirmacion');
Route::get('getBills/{id}','User\ApiPayUserController@getBills');

Route::group(['prefix' => 'auth'], function () {
    //Rutas sin token
    Route::post('signup', 'User\ApiUserController@signup');
    Route::post('login', 'User\ApiUserController@login');
    Route::group(['middleware' => 'auth:api'], function() {
        //Rutas con token
        Route::get('home', function(){
            return response("<h1>Welcome Covidcap</h1>");
        });
    });
});
