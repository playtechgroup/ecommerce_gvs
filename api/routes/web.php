<?php

use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    if(Auth::check())
    {

        if(Auth::user()->Role_id==1)
        {
            return redirect('/home');
        }
        else if(Auth::user()->Role_id==2)
        {
            return redirect('/homeProvider');
        }
        else if (Auth::user()->Role_id==3)
        {
            return redirect('/homeProveedor');
        }
        else if(Auth::user()->Role_id==4)
        {
            return redirect('/homeInventory');
        }
        else if(Auth::user()->Role_id==5)
        {
            return redirect('/homePublisher');
        }
    }else{
        return view('auth/login');
    }
    });
Auth::routes();
// Route::group(['middleware' => ['role:Admin|Inventory']]);

Route::middleware(['auth', 'role:Admin'])->group(function () {
Route::get('/home', 'Web\HomeController@index')->name('home');
//   ::::::::::::::::::::::::::USERS::::::::::::::::::::::::::::::::::::
Route::get('/users', 'Web\UsersController@index')->name('users');
Route::get('/createuser', 'Web\UsersController@create')->name('createuser');
Route::post('/createuserform', 'Web\UsersController@createuserform')->name('createuserform');
Route::post('/deleteuser', 'Web\UsersController@deleteuser')->name('deleteuser');
//Editar
Route::get('/edituser/{id}', 'Web\UsersController@edituser')->name('edituser');
Route::post('/edituserform/{id}', 'Web\UsersController@edituserform')->name('edituserform');
//   ::::::::::::::::::::::::::LINKED USERS::::::::::::::::::::::::::::::::::::
Route::get('/linked', 'Web\UsersController@indexlinked')->name('linked');
Route::get('/createlinked', 'Web\UsersController@createlinked')->name('createlinked');
Route::post('/createlinkedform', 'Web\UsersController@createlinkedform')->name('createlinkedform');
Route::post('/deletelinked', 'Web\UsersController@deletelinked')->name('deletelinked');
//Editar
Route::get('/editlinked/{id}', 'Web\UsersController@editlinked')->name('editlinked');
Route::post('/editlinkedform/{id}', 'Web\UsersController@editlinkedform')->name('editlinkedform');
});


Route::middleware(['auth', 'role:Provider'])->group(function () {
    Route::get('/homeProveedor', 'Web\HomeController@indexprovider')->name('homeprovider');
});

Route::middleware(['auth', 'role:Publisher'])->group(function () {
    Route::get('/homePublisher', 'Web\HomeController@indexpublisher')->name('homepublisher');
});

Route::middleware(['auth', 'role:Inventory'])->group(function () {
    Route::get('/homeInventory', 'Web\HomeController@indexinventory')->name('homeinventory');
});


Route::middleware(['auth'])->group(function () {
    //   ::::::::::::::::::::::::::CATEGORY::::::::::::::::::::::::::::::::::::
    Route::get('/categories', 'Web\CategoryController@index')->name('categories');
    // Route::get('/categories', 'Web\CategoryController@index')->name('categories')->middleware('auth','role:Admin','role:Inventory');
    Route::get('/createcategory', 'Web\CategoryController@create')->name('createcategory');
    Route::post('/createcategoryform', 'Web\CategoryController@createcategoryform')->name('createcategoryform');
    Route::post('/deletecategory', 'Web\CategoryController@deletecategory')->name('deletecategory');
    //Editar
    Route::get('/editcategory/{id}', 'Web\CategoryController@editcategory')->name('editcategory');
    Route::post('/editcategoryform/{id}', 'Web\CategoryController@editcategoryform')->name('editcategoryform');
    //Ver
    Route::get('/viewsubcategory/{id}', 'Web\CategoryController@viewsubcategory')->name('viewsubcategory');
    //   ::::::::::::::::::::::::::::::::::::::::::::::::::Subcategoria:::::::::::::::::::::::::::::::::::::::::::::::::
    //CREATE SUBCATEGORY
    Route::get('/createsubcategories/{id}', 'Web\SubcategoriesController@createsubcategories')->name('createsubcategories');
    Route::post('/createsubcategoriesform/{id}', 'Web\SubcategoriesController@createsubcategoriesform')->name('createsubcategoriesform');
    Route::get('/createsubcategory', 'Web\ProviderController@viewcreate')->name('createprovider');
    //EDITAR SUBCATEGORY
    Route::get('/editsubcategories/{id}', 'Web\SubcategoriesController@editsubcategories')->name('editsubcategories');
    Route::post('/editsubcategoriesform/{id}', 'Web\SubcategoriesController@editsubcategoriesform')->name('editsubcategoriesform');
    // DELETE SUBCATEGORY
    Route::post('/statesubcategory', 'Web\SubcategoriesController@statesubcategory')->name('statesubcategory');

    //   ::::::::::::::::::::::::::Provider::::::::::::::::::::::::::::::::::::
    //Listar
    Route::get('/provider', 'Web\ProviderController@index')->name('provider');
    //Crear
    Route::post('/createproviderform', 'Web\ProviderController@create')->name('createproviderform');
    //Ver
    Route::get('/viewprovider/{id}', 'Web\ProviderController@viewprovider')->name('viewprovider');
    //Editar
    Route::get('/editprovider/{id}', 'Web\ProviderController@editprovider')->name('editprovider');
    Route::post('/editproviderform/{id}', 'Web\ProviderController@editproviderform')->name('editproviderform');
    //Eliminar
    Route::post('/deleteprovider', 'Web\ProviderController@deleteprovider')->name('deleteprovider');
    //   ::::::::::::::::::::::::::Productos::::::::::::::::::::::::::::::::::::
    Route::get('/products', 'Web\ProductsController@index')->name('products');
    Route::get('/create', 'Web\ProductsController@create')->name('createproducts');
    Route::post('/createproductform', 'Web\ProductsController@createproductform')->name('createproductform');
    Route::get('/editproduct/{id}', 'Web\ProductsController@editproduct')->name('editproduct');
    Route::post('/editproductform', 'Web\ProductsController@editproductform')->name('editproductform');
    Route::get('/getsubcategory/{id}', 'Web\ProductsController@getsubcategory');
    Route::post('/deletefile', 'Web\ProductsController@deletefile');
    Route::get('/viewproduct/{id}', 'Web\ProductsController@viewproduct')->name('viewproduct');
    Route::post('/updatestateproduct', 'Web\ProductsController@update_product_status');//ordersbyprovider

    //   :::::::::::::::::::::::::::: ORDERS :::::::::::::::::::::::::::::::::::
    Route::get('/orders', 'Web\OrderController@index')->name('orders');//INDEX
    // Route::get('/orderdetail/{id}', 'Web\OrderController@orderdetail')->name('orderdetail');//orderdetail
    Route::post('/deleteorder', 'Web\OrderController@deleteorder')->name('deleteorder');
    //   :::::::::::::::::::::::::::::::::::::
    Route::get('/ordersbyprovider', 'Web\OrderProviderController@index')->name('ordersbyprovider');//ordersbyprovider
    Route::get('/orderdetail/{id}', 'Web\OrderProviderController@orderdetail')->name('orderdetail');//ordersbyprovider
    Route::post('/update_order_status', 'Web\OrderProviderController@update_order_status')->name('update_order_status');//ordersbyprovider
    Route::post('/sendnotification/{id}', 'Web\OrderProviderController@newNotification')->name('sendnotification');//ordersbyprovider

    // ::::::::::::::::::::::::::::noticias::::::::::::::::::::::::::::
    Route::get('/notices', 'Web\NoticesController@index')->name('notices');
    Route::get('/createnotice', 'Web\NoticesController@create')->name('createnotice');
    Route::post('/createnoticeform', 'Web\NoticesController@createnoticeform')->name('createnoticeform');
    Route::post('/deletenotice', 'Web\NoticesController@deletenotice')->name('deletenotice');
    //Editar
    Route::get('/editnotice/{id}', 'Web\NoticesController@editnotice')->name('editnotice');
    Route::post('/editnoticeform/{id}', 'Web\NoticesController@editnoticeform')->name('editnoticeform');

    
    // ::::::::::::::::::::::::::::estudios clínicos::::::::::::::::::::::::::::
    Route::get('/clinical', 'Web\ClinicalController@index')->name('clinical');
    Route::get('/createclinical', 'Web\ClinicalController@create')->name('createclinical');
    Route::post('/createclinicalform', 'Web\ClinicalController@createclinicalform')->name('createclinicalform');
    Route::post('/deleteclinical', 'Web\ClinicalController@deleteclinical')->name('deleteclinical');
    //Editar
    Route::get('/editclinical/{id}', 'Web\ClinicalController@editclinical')->name('editclinical');
    Route::post('/editclinicalform/{id}', 'Web\ClinicalController@editclinicalform')->name('editclinicalform');
    //::::::::::::::::::::::::::::eventos::::::::::::::::::::::::::::
    Route::get('/events', 'Web\EventsController@index')->name('events');
    Route::get('/createevent', 'Web\EventsController@create')->name('createevent');
    Route::get('/createtype', 'Web\EventsController@createtype')->name('createtype');
    Route::post('/createeventform', 'Web\EventsController@createeventform')->name('createeventform');
    Route::post('/createeventtype', 'Web\EventsController@createeventtype')->name('createeventtype');
    Route::post('/deleteevent', 'Web\EventsController@deleteevent')->name('deleteevent');
    //Editar
    Route::get('/editevent/{id}', 'Web\EventsController@editevent')->name('editevent');
    Route::post('/editeventform/{id}', 'Web\EventsController@editeventform')->name('editeventform');
    //::::::::::::::::::::::::::::devoluciones::::::::::::::::::::::::::::
    Route::get('/devolutions', 'Web\DevolutionsController@index')->name('devolutions');
    Route::get('/createdevolution', 'Web\DevolutionsController@create')->name('createdevolution');
    Route::post('/createdevolutionform', 'Web\DevolutionsController@createdevolutionform')->name('createdevolutionform');
    Route::post('/deletedevolution', 'Web\DevolutionsController@deletedevolution')->name('deletedevolution');
    //Editar
    Route::get('/editdevolution/{id}', 'Web\DevolutionsController@editdevolution')->name('editdevolution');
    Route::post('/editdevolutionform/{id}', 'Web\DevolutionsController@editdevolutionform')->name('editdevolutionform');

    Route::post('/updatestateuser', 'Web\UsersController@update_linked_status');//ordersbyprovider
});
// ::::::::::::::::::::::::::::::::::
// OTROS
// ::::::::::::::::::::::::::::::::::
Route::get('/perfil', 'Web\Otroscontroller@perfil')->name('perfil');
Route::post('/editProfileForm/{id}', 'Web\Otroscontroller@editProfileForm')->name('editProfileForm');
Route::get('/politicas', 'Web\Otroscontroller@politicas')->name('politicas');
Route::get('/terminos', 'Web\Otroscontroller@terminos')->name('terminos');
Route::get("auth/reestablecer/email/{email}/token/{token}","User\ApiUserContrasena@recuperarPassApp") ;
Route::post('resetpassword','Auth\ForgotPasswordController@reestablecerPass');
Route::post("auth/nuevoPassApp","User\ApiUserContrasena@Controles_nuevoPassApp");
Route::get('/payment', 'User\ApiPayUserController@index')->name('payment');
Route::get('welcome', function () {

return View('mails.welcome');

});
