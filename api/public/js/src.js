function initmodal() {
    // modal hadler
    const array_img = document.querySelectorAll('td [data-img=true]');
    array_img.forEach(inp => inp.addEventListener('click', e => preview(inp))); /*asigno evenlistener a cada elemento del nodo*/
    const modal       = document.getElementById("myModal");
    function preview(params) {
        var modalImg        = document.getElementById("img01");
        modal.style.display = "block";
        modalImg.src        = params.src;
    }
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
}
function actualizar_estado(obj,ruta,id) {
    let data= {
        id: obj.id,
        param: obj.checked
    };
    console.log(data)
    console.log(data)
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: ruta,
        data: {
            id: obj.id,
            param: obj.checked
        },
        dataType: "json",
        success: function (res) {
            console.log(res);
            if (res.r) {
                swal("Se ha actualizado correctamente", {
                    icon: "success",
                });
            } else {
                swal("No se pudo eliminar la empresa", {
                    icon: "warning",
                });
            }
        },
        error: function (jqXHR, exception) {
            console.log('ERROR AL GUARDAR', exception);
        }
    });
}
function update_statusorden(obj) {
    swal({
        title: "ATENCIÓN",
        text: "Desear Cambiar el estado de la orden?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willUpdate) => {
        if (willUpdate) {
            let e = document.getElementById(obj.id);
            let value_selected = e.options[e.selectedIndex].value;
            console.log(value_selected)
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/update_order_status",
                data: {
                    id       : obj.id,
                    new_state: value_selected
                },
                dataType: "json",
                success: function (res) {
                    console.log(res);
                    if (res.r) {
                        swal("Se ha actualizado correctamente", {
                            icon: "success",
                            timer: 1300,
                            buttons: false,
                        });
                    } else {
                        swal("No se pudo eliminar la empresa", {
                            icon: "warning",
                            timer: 1300,
                            buttons: false,
                        });
                    }
                },
                error: function (jqXHR, exception) {
                    console.log('ERROR AL GUARDAR', exception);
                }
            });
        }
    });
}

//*****Ejemplo para usar la libreria de swal*****/////
// swal({ text: "Hello world!" });
// swal("Good job!", "You clicked the button!", "success");
// swal({
//     title: "Are you sure?",
//     text: "Once deleted, you will not be able to recover this imaginary file!",
//     icon: "warning",
//     buttons: true,
//     dangerMode: true,
//   })
//   .then((willDelete) => {
//     if (willDelete) {
//       swal("Poof! Your imaginary file has been deleted!", {
//         icon: "success",
//       });
//     } else {
//       swal("Your imaginary file is safe!");
//     }
//   });
// function deleteprovider(id){
// swal({
//     title: "ATENCIÓN",
//     text: "Desear eliminar el proveedor,una vez eliminado, no será posible recuperar la información",
//     icon: "warning",
//     buttons: true,
//     dangerMode: true,
//   })
//   .then((willDelete) => {
//     if (willDelete) {
//         $.ajax({
//             type: "POST",
//             headers: {
//                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             },
//             url: '/deleteprovider',
//             data: {
//                 id: id
//             },
//             dataType: "json",
//             success: function (res) {
//                 console.log(res);
//                 if (res.r) {
//                     swal("La empresa ha sido eliminada correctamente", {
//                         icon: "success",
//                     }).then(() => {
//                         location.reload(true);
//                     });
//                 }else{
//                     swal("No se pudo eliminar la empresa", {
//                         icon: "warning",
//                     });
//                 }
//             },
//             error: function (jqXHR, exception) {
//             }
//         });
//     }
//   });
// }
"use strict";

const FloatLabel = (() => {
    // add active class and placeholder
    const handleFocus = e => {
        const target = e.target;
        target.parentNode.classList.add('active');
        target.setAttribute('placeholder', target.getAttribute('data-placeholder'));
    }; // remove active class and placeholder


    const handleBlur = e => {
        const target = e.target;

        if (!target.value) {
            target.parentNode.classList.remove('active');
        }

        target.removeAttribute('placeholder');
    }; // register events


    const bindEvents = element => {
        const floatField = element.querySelector('input');
        floatField.addEventListener('focus', handleFocus);
        floatField.addEventListener('blur', handleBlur);
    }; // get DOM elements


    const init = () => {
        const floatContainers = document.querySelectorAll('.float-container');
        floatContainers.forEach(element => {
            if (element.querySelector('input').value) {
                element.classList.add('active');
            }

            bindEvents(element);
        });
    };

    return {
        init: init
    };
})();


window.onload = function () {
    initmodal();
    $('.my-datatable').DataTable({
        "order": [
            [0, "desc"]
        ],
        language: {
            search: "Buscar en tabla:"
        },
        dom: 'Bfrtip',
        s: [{
            extend: 'excel',
            text: 'Exportar a excel'
        }]
    });
}

FloatLabel.init();
