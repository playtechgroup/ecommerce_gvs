const url = '';

$(document).ready(function () {


  //EMPRESAS
  var btnAgregar = document.querySelector('#btnAgregar');


  if (btnAgregar != null) {
    btnAgregar.onclick = function () {
      var contElement = document.querySelector("#contenedor-elemento");
      contElement.style.display = "none";
      var addElement = document.querySelector("#add-elemento");
      addElement.style.display = "block";
    }

  }



  var btnVolver = document.querySelector('#tabElementos');
  if (btnVolver != null) {
    btnVolver.onclick = function () {
      var addElement = document.querySelector("#add-elemento");
      addElement.style.display = "none";
      var contElement = document.querySelector("#contenedor-elemento");
      contElement.style.display = "block";
    }
  }

let lola=  $('.js-example-basic-multiple').select2();
console.log(lola);


  $("#select_empresa").change(function () {

    if ($(this).val() == 1) {
      var element1 = document.querySelector("#body_table_uno");
      element1.style.display = "block";
      var element2 = document.querySelector("#body_table_dos");
      element2.style.display = "none";
    }
    if ($(this).val() == 2) {
      var element1 = document.querySelector("#body_table_uno");
      element1.style.display = "none";
      var element2 = document.querySelector("#body_table_dos");
      element2.style.display = "block";
    }

  });

  $("#select-inDosimetros").change(function () {

    if ($(this).val() == "anillo") {
      var element1 = document.querySelector(".boxAnillo");
      element1.style.display = "block";
      var element2 = document.querySelector(".boxOcular");
      element2.style.display = "none";
      var element2 = document.querySelector(".boxCuerpo");
      element2.style.display = "none";
    }
    if ($(this).val() == "ocular") {
      var element1 = document.querySelector(".boxAnillo");
      element1.style.display = "none";
      var element2 = document.querySelector(".boxOcular");
      element2.style.display = "block";
      var element2 = document.querySelector(".boxCuerpo");
      element2.style.display = "none";

    }

    if ($(this).val() == "cuerpo") {
      var element1 = document.querySelector(".boxAnillo");
      element1.style.display = "none";
      var element2 = document.querySelector(".boxOcular");
      element2.style.display = "none";
      var element2 = document.querySelector(".boxCuerpo");
      element2.style.display = "block";

    }
    if ($(this).val() == "todos") {
      var element1 = document.querySelector(".boxAnillo");
      element1.style.display = "block";
      var element2 = document.querySelector(".boxOcular");
      element2.style.display = "block";
      var element2 = document.querySelector(".boxCuerpo");
      element2.style.display = "block";

    }

  });

  //filtro cristales
  $("#select-filtro-cristales").change(function () {
    valor = $(this).val();
    $(location).attr('href', url + '/dosimetros/inventario/cristales/' + valor);

  });
  //filtro estados empresa
  $("#select-filtro-estado-empresa").change(function () {
    valor = $(this).val();
    $(location).attr('href', url + '/empresas/estado/' + valor);
  });
  //filtro pendientes empresa y sede
  $("#select-filtro-empresas-pendientes").change(function () {
    valor = $(this).val();

    $(location).attr('href', url + '/home/empresas/' + valor);

  });
  $("#select-filtro-sede-pendientes").change(function () {
    valor = $(this).val();
    $(location).attr('href', url + '/home/sede/' + valor);

  });

  //filtro empleados
  $("#select-filtro").change(function () {
    valor = $(this).val();
    $(location).attr('href', url + '/empleados/sede/' + valor);

  });

  $("#Select-empresa-crear").change(function () {
    valor = $(this).val();
    var info = {
      "id": valor
    }
    console.log(info);
    $.ajax({
      url: "/crearEmpleadoSelect",
      type: "POST",
      contentType: 'application/json',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json'
      },
      data: JSON.stringify(info),
      success: function (r) {
        console.log(r);
        if (r[0]["respuesta"]) {
          var sedes = r[0]["sedes"];
          $('#contenedor-depart').empty();
          $('#select-editarSede').empty();
          var sel = '<option value="ninguna">Seleccione</option>';
          $('#select-editarSede').append(sel);
          for (var i in sedes) {
            var row = '<option value="' + sedes[i].id + '">' + sedes[i].nombre + '</option>';
            $('#select-editarSede').append(row);
            console.log(sedes[i].nombre)
          }
        } else {
          $('#select-editarSede').empty();
          console.log("no hay nada");
        }
      }
    });
  });


  // editar sede  filtro
  $("#select-editarSede").change(function () {
    valor = $(this).val();
    var info = {
      "id": valor
    }
    console.log(info);
    $.ajax({
      url: "/editarEmpleadoSelect",
      type: "POST",
      contentType: 'application/json',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json'
      },
      data: JSON.stringify(info),
      success: function (r) {
        console.log(r);
        if (r[0]["respuesta"]) {
          var departamentos = r[0]["departamentos"];

          $('#contenedor-depart').empty();

          for (var i in departamentos) {
            var row = '<div class="row" style="margin-bottom: 10px; display:flex; justify-content: center;"><div class="col-xs-8"><input type="checkbox" class="checkEmpleado" name="" value="' + departamentos[i].id + '" id="' + departamentos[i].id + '"><label for="' + departamentos[i].id + '"></label><p style="margin-left: 7%;">' + departamentos[i].nombre + '</p></div></div>';
            $('#contenedor-depart').append(row);
            console.log(departamentos[i].nombre)
          }

        } else {
          $('#contenedor-depart').empty();
          console.log("no hay nada");
        }
      }

    });

  });

  $('.despachoRapido').on("click", function () {
    fecha_inicio = $(this).attr('data-fi');
    fecha_final = $(this).attr('data-ff');
    sede = $(this).attr('data-sede');
    departamento = $(this).attr('data-id');
   var location=  url + '/asignacion/rapida/' + departamento + '/' + sede + '/' + fecha_inicio + '/' + fecha_final;
    window.open(location,'_blank');

  });


  $("#select-filtro-empresas-empleados").change(function () {
    valor = $(this).val();
    $(location).attr('href', url + '/empleados/empresa/' + valor);

  });
  $('#btnBusqueda-empleados').on("click", function () {
    valor = $('#inputBusqueda').val();
    if (!isNaN(valor)) {
      $(location).attr('href', url + '/empleados/buscar/' + valor)
    }

  });
  //filtros empresa
  $('#btnBusqueda-empresa').on("click", function () {
    valor = $('#inputBusquedaE').val();
    console.log(valor);
    if (!isNaN(valor)) {
      $(location).attr('href', url + '/empresas/buscar/' + valor)
    }
  });
    //filtros dosimetro
    $('#btnBusqueda-dosimetros').on("click", function () {
      valor = $('#inputBusquedaDosimetro').val();
      console.log(valor);
      if (valor!="") {
        $(location).attr('href', url + '/dosimetro/buscar/' + valor)
      }
    });

  //filtro empresa asignacion
  $("#select-empresas-asignacion").change(function () {
    valor = $(this).val();
    $(location).attr('href', url + '/asignacionInicio/empresa/' + valor);

  });
  //filtro sede asignacion
  $("#select-sedes-asignacion").change(function () {
    valor = $(this).val();
    $(location).attr('href', url + '/asignacionInicio/sede/' + valor);

  });

  //filtro dosimetros
  $("#select-filtro-dosimetros").change(function () {
    valor = $(this).val();
    $(location).attr('href', url + '/dosimetros/estado/' + valor);

  });
  //filtro dosimetros inventario
  $("#select-filtro-inDosimetros").change(function () {
    valor = $(this).val();
    $(location).attr('href', url + '/dosimetros/inventario/estado/' + valor);
  });
  //filtro tipo de contrato
  $("#select_tipo_contrato").change(function () {
    valor = $(this).val();
    if (valor == 2) {
      $("#inputCantidad").prop("disabled", false);

    } else {
      $("#inputCantidad").prop("disabled", true);
    }
  });
  $("#select_tipo_contrato_editar").change(function () {
    valor = $(this).val();
    if (valor == 2) {
      $("#inputCantidadEditar").prop("disabled", false);

    } else {
      $("#inputCantidadEditar").prop("disabled", true);
    }
  });
  $("#select_tipo_contrato_empresa").change(function () {
    valor = $(this).val();
    if (valor == 2) {
      $("#inputCantidadEmpresa").prop("disabled", false);

    } else {
      $("#inputCantidadEmpresa").prop("disabled", true);
    }
  });

  $('#btn-tipo-dosimetro').on("click", function () {
    valor = $('#select-tipo-dosimetro').val();

    var box = document.querySelector("#box-agregar-dosimetro");
    box.style.display = "block";

    if (valor == "Anillo") {
      var anillo = document.querySelector("#div-anillo");
      anillo.style.display = "block";
      var cuerpo = document.querySelector("#div-cuerpo");
      cuerpo.style.display = "none";
      var ocular = document.querySelector("#div-ocular");
      ocular.style.display = "none";
    }

    if (valor == "Ocular") {
      var anillo = document.querySelector("#div-anillo");
      anillo.style.display = "none";
      var cuerpo = document.querySelector("#div-cuerpo");
      cuerpo.style.display = "none";
      var ocular = document.querySelector("#div-ocular");
      ocular.style.display = "block";
    }

    if (valor == "Cuerpo") {
      var anillo = document.querySelector("#div-anillo");
      anillo.style.display = "none";
      var cuerpo = document.querySelector("#div-cuerpo");
      cuerpo.style.display = "block";
      var ocular = document.querySelector("#div-ocular");
      ocular.style.display = "none";
    }


  });

  //ALERT ELIMINAR DOSIMETROS
  $(".eliminarCuerpo").click(function () {
    var valor = $(this).attr("data-id");
    swal("Desea eliminar el dosimetro?", {
      buttons: {
        Si: true,
        cancel: "No",
      }
    }).then((val) => {
      if (val == "Si") {
        $(location).attr('href', url + '/eliminarCuerpo/' + valor)
      }
    });
  });

  $(".eliminarDepart").click(function () {
    var valor = $(this).attr("data-id");
    swal("Desea eliminar el Departamento?", {
      buttons: {
        Si: true,
        cancel: "No",
      }
    }).then((val) => {
      if (val == "Si") {
        $(location).attr('href', url + '/eliminarDepartamento/' + valor)
      }
    });
  });

  $(".eliminarOcular").click(function () {
    var valor = $(this).attr("data-id");
    swal("Desea eliminar el dosimetro?", {
      buttons: {
        Si: true,
        cancel: "No",
      }
    }).then((val) => {
      if (val == "Si") {
        $(location).attr('href', url + '/eliminarOcular/' + valor)
      }
    });

  });

  $(".eliminarAnillo").click(function () {
    var valor = $(this).attr("data-id");
    swal("Desea eliminar el dosimetro?", {
      buttons: {
        Si: true,
        cancel: "No",
      }
    }).then((val) => {
      if (val == "Si") {
        $(location).attr('href', url + '/eliminarAnillo/' + valor)
      }
    });

  });

  //ALERT ELIMINAR
  $(".eliminarEmpleado").click(function () {
    var valor = $(this).attr("data-id");
    swal("Desea eliminar el empleado?", {
      buttons: {
        Si: true,
        cancel: "No",
      }
    }).then((val) => {
      if (val == "Si") {
        $(location).attr('href', url + '/eliminarEmpleado/' + valor)
      }
    });
  });

  $(".eliminarEmpresa").click(function () {
    var valor = $(this).attr("data-id");
    swal("Desea eliminar la empresa?", {
      buttons: {
        Si: true,
        cancel: "No",
      }
    }).then((val) => {
      if (val == "Si") {
        $(location).attr('href', url + '/eliminarEmpresa/' + valor)
      }
    });
  });

  $(".eliminarSede").click(function () {
    var valor = $(this).attr("data-id");
    swal("Desea eliminar la sede?", {
      buttons: {
        Si: true,
        cancel: "No",
      }
    }).then((val) => {
      if (val == "Si") {
        $(location).attr('href', url + '/eliminarSede/' + valor)
      }
    });
  });

  //editar empleado
  $(".editarEmpleado").click(function () {
    var valor = $(this).attr("data-id");
    $(location).attr('href', url + '/editEmpleado/' + valor);
  });
  //editar empresa
  $(".editarEmpresa").click(function () {
    var valor = $(this).attr("data-id");
    $(location).attr('href', url + '/editEmpresa/' + valor);
  });
  //ver sedes
  $(".verSedes").click(function () {
    var valor = $(this).attr("data-id");
    $(location).attr('href', url + '/sedes/' + valor);
  });
  //editar sede
  $(".editarSede").click(function () {
    var valor = $(this).attr("data-id");
    $(location).attr('href', url + '/editSede/' + valor);
  });

  //editar dosimetros
  $(".editarAnillo").click(function () {
    var valor = $(this).attr("data-id");
    var tipo = $(this).attr("data-ventana");
    $(location).attr('href', url + '/editAnillo/' + valor + '/' + tipo);
  });

  $(".editarOcular").click(function () {
    var valor = $(this).attr("data-id");
    var tipo = $(this).attr("data-ventana");
    $(location).attr('href', url + '/editOcular/' + valor + '/' + tipo);
  });
  $(".editarCuerpo").click(function () {
    var valor = $(this).attr("data-id");
    var tipo = $(this).attr("data-ventana");
    $(location).attr('href', url + '/editCuerpo/' + valor + '/' + tipo);
  });
  //ir listado dosimetros asignados por acta
  $(".verActa").click(function () {
    var valor = $(this).attr("data-id");
    console.log(valor);
    //  $(location).attr('href', url + '/pdfActa/' + valor);

    window.open(url + '/pdfActa/' + valor, '_blank');
  });
  //ir generar etiquetas
  $(".generarEtiquetas").click(function () {
    var valor = $(this).attr("data-id");
    window.open(url + '/etiquetas/' + valor, '_blank');
  });
  //ir listado dosimetros asignados por acta
  $(".editarActa").click(function () {
    var valor = $(this).attr("data-id");
    console.log(valor);
    window.open(url + '/editarActa/' + valor,'_blank');
  });
  //ir listado dosimetros asignados por acta
  $(".listadoDosimetrosAsignados").click(function () {
    var valor = $(this).attr("data-id");
    console.log(valor);
    $(location).attr('href', url + '/dosimetrosAsignados/' + valor);
  });

  //ir listado reporte dosimetria pdf
  $(".verReporte").click(function () {
    var valor = $(this).attr("data-id");
    window.open(url + '/pdfReporte/' + valor, '_blank');
  });
  //ir reporte de dosimetria
  $(".listadodelReporte").click(function () {
    var valor = $(this).attr("data-id");
    console.log(valor);
    $(location).attr('href', url + '/reporte/' + valor);
  });
  //select de checkbox
  $('#anillo_todos').change(function () {
    $("#anillo_check").attr('checked', false);
    $("#tapaA_check").attr('checked', false);
    $("#cristal_check").attr('checked', false);
  });
  $('#anillo_check').change(function () {
    $("#anillo_todos").attr('checked', false);
  });
  $('#tapaA_check').change(function () {
    $("#anillo_todos").attr('checked', false);
  });
  $('#cristal_check').change(function () {
    $("#anillo_todos").attr('checked', false);
  });

  $('#ocular_todos').change(function () {
    $("#diademaOcular_check").attr('checked', false);
    $("#cristalO_check").attr('checked', false);
  });
  $('#diademaOcular_check').change(function () {
    $("#ocular_todos").attr('checked', false);
  });
  $('#cristalO_check').change(function () {
    $("#ocular_todos").attr('checked', false);
  });

  $('#cuerpo_todos').change(function () {
    $("#tapaC_check").attr('checked', false);
    $("#cajaC_check").attr('checked', false);
    $("#tarjeta_check").attr('checked', false);
    $("#porta_tarjeta_check").attr('checked', false);
  });
  $('#tapaC_check').change(function () {
    $("#cuerpo_todos").attr('checked', false);
  });
  $('#cajaC_check').change(function () {
    $("#cuerpo_todos").attr('checked', false);
  });
  $('#tarjeta_check').change(function () {
    $("#cuerpo_todos").attr('checked', false);
  });
  $('#porta_tarjeta_check').change(function () {
    $("#cuerpo_todos").attr('checked', false);
  });


  //filtro con checkbox
  $("#check_filtrar_anillo").on('click', function () {
    var array = [];
    i = 0;
    if ($("#anillo_todos").prop('checked')) {
      $(location).attr('href', url + '/inventarioDosimetros');
    } else {
      if ($("#anillo_check").prop('checked')) {
        array[i] = 1;
        i++;
      } else {
        array[i] = 0;
        i++;
      }

      if ($("#tapaA_check").prop('checked')) {
        array[i] = 1;
        i++;
      } else {
        array[i] = 0;
        i++;
      }
      if ($("#cristal_check").prop('checked')) {
        array[i] = 1;
        i++;
      } else {
        array[i] = 0;
        i++;
      }
      $(location).attr('href', url + '/dosimetros/inventario/tipoA/' + array);

    }

  });

  $("#check_filtrar_ocular").on('click', function () {
    var array = [];
    i = 0;
    if ($("#ocular_todos").prop('checked')) {
      $(location).attr('href', url + '/inventarioDosimetros');
    } else {
      if ($("#diademaOcular_check").prop('checked')) {
        array[i] = 1;
        i++;
      } else {
        array[i] = 0;
        i++;
      }
      if ($("#cristalO_check").prop('checked')) {
        array[i] = 1;
        i++;
      } else {
        array[i] = 0;
        i++;
      }
      $(location).attr('href', url + '/dosimetros/inventario/tipoO/' + array);
    }
  });

  $("#check_filtrar_cuerpo").on('click', function () {
    var array = [];
    i = 0;
    if ($("#cuerpo_todos").prop('checked')) {
      $(location).attr('href', url + '/inventarioDosimetros');
    } else {
      if ($("#tapaC_check").prop('checked')) {
        array[i] = 1;
        i++;
      } else {
        array[i] = 0;
        i++;
      }
      if ($("#cajaC_check").prop('checked')) {
        array[i] = 1;
        i++;
      } else {
        array[i] = 0;
        i++;
      }
      if ($("#tarjeta_check").prop('checked')) {
        array[i] = 1;
        i++;
      } else {
        array[i] = 0;
        i++;
      }
      if ($("#porta_tarjeta_check").prop('checked')) {
        array[i] = 1;
        i++;
      } else {
        array[i] = 0;
        i++;
      }
      $(location).attr('href', url + '/dosimetros/inventario/tipo/' + array);
    }
  });



  $("#allCheck").on('click', function () {



    if ($(this).prop("checked")) {
      $('.checkEmployes').each((e, a) => {

        $(a)[0].checked = true;
      });

    } else {
      $('.checkEmployes').each((e, a) => {
        $(a)[0].checked = false;



      });

    }



  });



  $('.inputsD').keyup( delay(function (e) {
    // []

    var lector =localStorage.getItem('lector');
    if(!lector){
    swal("Desea activar el uso con lector de codigo de barras", {
      buttons: {
        Si: true,
        cancel: "No",
      }
    }).then((val) => {
      if (val == "Si") {
        localStorage.setItem('lector',true);
        swal("Activado", "Funcion Activada", "success");
      }
    });
  }else{
    var index = $('.inputsD').index(this) + 3;
    $('.inputsD').eq(index).focus();

  }





    if (e.target.value > 0) {
      $(this)[0].value = parseInt(e.target.value, 10);
    }

var total=0;
$('.inputsD').each((e,a)=>{
  console.log($(a).val());
if($(a).val()){
  total=total+1;
}
});
console.log(total);
$('#totalAsignados')[0].innerHTML='Total Asignados '+total;
  },200));

  $('.vcedula').on('keyup', function (e) {
    var cedula = $("input[name='cedula']").val();
    var Empresa_id = $("select[name='Empresa_id']").val();


    console.log(Empresa_id);
if(Empresa_id!='ninguna'){

  console.log(cedula);
  $.ajax({
    url: "/vcedula",
    type: "POST",
    contentType: 'application/json',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'Content-Type': 'application/json'
    },
    data: JSON.stringify({
      cedula: cedula,
      empresa: Empresa_id,

    }),
    success: function (r) {
      console.log(r);
      if(r.length>0){
        swal("Atención", "El N° identificacion ya existe creado en una de las sedes de la empresa ", "warning");
      }
    }
  });

}



  });


  $('#idBrand').on('keyup', '.inputsD', function (e) {

    if (e.keyCode == '38') {
      var index = $('.inputsD').index(this) - 3;
      $('.inputsD').eq(index).focus();

      // up arrow
    } else if (e.keyCode == '40') {
      var index = $('.inputsD').index(this) + 3;
      $('.inputsD').eq(index).focus();
    } else if (e.keyCode == '37') {
      var index = $('.inputsD').index(this) - 1;
      $('.inputsD').eq(index).focus();
    } else if (e.keyCode == '39') {
      var index = $('.inputsD').index(this) + 1;
      $('.inputsD').eq(index).focus();
    }


  });


  /** Ajax */
  $("#bCedula").on('keyup', function () {

    var se = $("input[name='sede']").val();
    var dep = $("input[name='departamento']").val();

    $.ajax({
      url: "/buscarEmpleado",
      type: "POST",
      contentType: 'application/json',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        cedula: $(this).val(),
        sede: se,
        depto: dep
      }),
      success: function (r) {
        console.log(r);
        if (r.length > 0) {
          var res = [];
          r.forEach(element => {
            // var con = '<tr><td><input type="checkbox" class="checkEmployes" name="check' + element.id + '" value="' + element.id + '" "> </td> <td>' + element.apellido + '</td><td>' + element.nombre + '</td><td>' + element.cedula + '</td><td>' + element.ocupacion + '</td><td><input type="text" name="Dosimetro_Cuerpo_id' + element.id + '" data-id="' + element.id + '" class="inputsD form-control pull-right "></td><td><input type="text" name="Dosimetro_Ocular_id' + element.id + '" data-id="' + element.id + '"  class="inputsD form-control pull-right inputsD"></td> <td><input type="text" name="Dosimetro_Anillo_id' + element.id + '" data-id="' + element.id + '"  class="inputsD form-control pull-right inputsD"></td></tr>';
            var con = '<tr><td><input type="checkbox" class="checkEmployes" name="check' + element.id + '" value="' + element.id + '" ">  <label for="' + element.id + '"></label> </td> <td>' + element.apellido + '</td><td>' + element.nombre + '</td><td>' + element.cedula + '</td><td>' + element.ocupacion + '</td><td><input type="text" name="Dosimetro_Cuerpo_id' + element.id + '" data-id="' + element.id + '" class="inputsD form-control pull-right "></td><td><input type="text" name="Dosimetro_Ocular_id' + element.id + '" data-id="' + element.id + '"  class="inputsD form-control pull-right inputsD"></td> <td><input type="text" name="Dosimetro_Anillo_id' + element.id + '" data-id="' + element.id + '"  class="inputsD form-control pull-right inputsD"></td></tr>';

            res.push(con);

            $('#idBrand').html(res);
          });

        } else {

          var con = '<tr><td>No hay nada  </td></tr>';

          $('#idBrand').html(con);

        }


      }
    });



  });



  $("#add").click(function () {
    var lastField = $("#buildyourform div:last");
    var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
    var fieldWrapper = $("<div class=\" fieldwrapper\" id=\"field" + intId + "\" style=\"display: flex; height: 43px;\"/>");
    fieldWrapper.data("idx", intId);
    var fName = $(" <div class=\"form-group\"> <input type=\"text\" name=\"departamento\" class=\"form-control departamento\" required/> <div class=\"help-block with-errors\"></div></div>");
    // var fType = $("<select class=\"fieldtype\"><option value=\"checkbox\">Checked</option><option value=\"textbox\">Text</option><option value=\"textarea\">Paragraph</option></select>");
    var removeButton = $("<input type=\"button\" class=\"remove btn btn-default\" style=\"margin-left: 5px; height: 34px;\" value=\"X\" />");

    removeButton.click(function () {
      $(this).parent().remove();
    });
    fieldWrapper.append(fName);
    // fieldWrapper.append(fType);
    fieldWrapper.append(removeButton);
    $("#buildyourform").append(fieldWrapper);

  });

  $("#add2").click(function () {
    var lastField = $("#buildyourformEmpresa div:last");
    var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
    var fieldWrapper = $("<div class=\" fieldwrapper\" id=\"field" + intId + "\" style=\"display: flex; height: 43px;\"/>");
    fieldWrapper.data("idx", intId);
    var fName = $(" <div class=\"form-group\"> <input type=\"text\" name=\"departamento\" class=\"form-control departamento\" required/> <div class=\"help-block with-errors\"></div></div>");
    // var fType = $("<select class=\"fieldtype\"><option value=\"checkbox\">Checked</option><option value=\"textbox\">Text</option><option value=\"textarea\">Paragraph</option></select>");
    var removeButton = $("<input type=\"button\" class=\"remove btn btn-default\" style=\"margin-left: 5px; height: 34px;\" value=\"X\" />");

    removeButton.click(function () {
      $(this).parent().remove();
    });
    fieldWrapper.append(fName);
    // fieldWrapper.append(fType);
    fieldWrapper.append(removeButton);
    $("#buildyourformEmpresa").append(fieldWrapper);

  });



});

function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}
function mandarAd() {
  document.getElementById("btsubmitActa").innerHTML = "Enviando...";

  document.getElementById("btsubmitActa").disabled = true;
  console.log(document.getElementById("btsubmitActa"));
  console.log($(this));

  var info = {
    "empleados": [],
    "data": {
      "dpto": $("input[name='departamento']").val(),
      "fecha_inicio": $("input[name='fecha_inicio']").val(),
      "fecha_final": $("input[name='fecha_final']").val(),
      'controlDosimetro': $("input[name='Dosimetro_Control']").val(),
      'controlDosimetroAnillo': $("input[name='Dosimetro_Control_Anillo']").val(),
      'controlDosimetroOcular': $("input[name='Dosimetro_Control_Ocular']").val()

    }
  };
  var hayCheck = false;
  var okCampos = false;
  var repeatC = false;
  var repeatA = false;
  var repeatO = false;
  $('.checkEmployes').each((e, a) => {

    if ($(a)[0].checked) {
      hayCheck = true;
      console.log(hayCheck);

      var valor_id = $(a)[0].value;
      var Dosimetro_Cuerpo = $("input[name='Dosimetro_Cuerpo_id" + valor_id + "']").val();
      var Dosimetro_Ocular = $("input[name='Dosimetro_Ocular_id" + valor_id + "']").val();
      var Dosimetro_Anillo = $("input[name='Dosimetro_Anillo_id" + valor_id + "']").val();
      if (Dosimetro_Cuerpo == "" && Dosimetro_Ocular == "" && Dosimetro_Anillo == "") {
        swal("Error", "Por favor llene los campos", "warning");
        okCampos = false;
      } else {
        okCampos = true;
      }
      info.empleados.push({
        "id": valor_id,
        "valorCuerpo": Dosimetro_Cuerpo,
        "valorOcular": Dosimetro_Ocular,
        "valorAnillo": Dosimetro_Anillo
      });
    }
  });

  repeatC = checkDuplicateInObject("valorCuerpo", info.empleados);
  repeatA = checkDuplicateInObject("valorAnillo", info.empleados);
  repeatO = checkDuplicateInObject("valorOcular", info.empleados);


  if (hayCheck && okCampos && !repeatC && !repeatA && !repeatO) {
    $.ajax({
      url: "/asignacion/guardar",
      type: "POST",
      contentType: 'application/json',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json'
      },
      data: JSON.stringify(info),
      success: function (r) {
        console.log(r);

        if (!r[1].res) {

          r[1].error.forEach(data => {
            $("#" + data.campo + data.cod).html(data.msj);
         document.getElementById("btsubmitActa").disabled = false;
         document.getElementById("btsubmitActa").innerHTML = "Guardar cambios y generar acta";
            console.log($("#" + data.campo + data.cod));
          });

        } else {
          console.log("Perfecto");
          $(location).attr('href', url + '/dosimetrosAsignados/' + r[1].actaId);
        }
      },
      error: (r) => {}
    });
  } else if (!hayCheck) {
    document.getElementById("btsubmitActa").disabled = false;
    document.getElementById("btsubmitActa").innerHTML = "Guardar cambios y generar acta";
    swal("Error", "Por favor seleccione empleados ", "warning");
  } else if (repeatC || repeatA || repeatO) {
    document.getElementById("btsubmitActa").disabled = false;
    document.getElementById("btsubmitActa").innerHTML = "Guardar cambios y generar acta";
    swal("Error", "Dosimetros Repetidos ", "warning");
  }
}

function mandarEditAd() {
  var info = {
    "empleados": [],
    "data": {
      "acta": $("input[name='acta']").val(),
      'controlDosimetro': $("input[name='Dosimetro_Control']").val(),
      'controlDosimetroAnillo': $("input[name='Dosimetro_Control_Anillo']").val(),
      'controlDosimetroOcular': $("input[name='Dosimetro_Control_Ocular']").val(),
      'fecha_inicio': $("input[name='fecha_inicio']").val(),
      'fecha_final': $("input[name='fecha_final']").val()

    }
  };
  var hayCheck = true;
  var okCampos = true;
  var repeatC = false;
  var repeatA = false;
  var repeatO = false;
  $('.checkEmployes').each((e, a) => {

    if ($(a)[0].checked) {
      hayCheck = true;
      console.log(hayCheck);

      var valor_id = $(a)[0].value;
      var Dosimetro_Cuerpo = $("input[name='Dosimetro_Cuerpo_id" + valor_id + "']").val();
      var Dosimetro_Ocular = $("input[name='Dosimetro_Ocular_id" + valor_id + "']").val();
      var Dosimetro_Anillo = $("input[name='Dosimetro_Anillo_id" + valor_id + "']").val();
      if (Dosimetro_Cuerpo == "" && Dosimetro_Ocular == "" && Dosimetro_Anillo == "") {
        swal("Error", "Por favor llene los campos", "warning");
        okCampos = false;
      } else {
        okCampos = true;
      }
      info.empleados.push({
        "id": valor_id,
        "valorCuerpo": Dosimetro_Cuerpo,
        "valorOcular": Dosimetro_Ocular,
        "valorAnillo": Dosimetro_Anillo
      });
    }
  });

  console.log(info);

  repeatC = checkDuplicateInObject("valorCuerpo", info.empleados);
  repeatA = checkDuplicateInObject("valorAnillo", info.empleados);
  repeatO = checkDuplicateInObject("valorOcular", info.empleados);


  if (hayCheck && okCampos && !repeatC && !repeatA && !repeatO) {
    $.ajax({
      url: "/asignacion/editar",
      type: "POST",
      contentType: 'application/json',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json'
      },
      data: JSON.stringify(info),
      success: function (r) {
        console.log(r);

        if (!r[1].res) {

          r[1].error.forEach(data => {
            $("#" + data.campo + data.cod).html(data.msj);
            console.log($("#" + data.campo + data.cod));
          });

        } else {
          console.log("Perfecto");
          $(location).attr('href', url + '/dosimetrosAsignados/' + r[1].actaId);
        }
      },
      error: (r) => {}
    });
  } else if (!hayCheck) {
    swal("Error", "Por favor seleccione empleados ", "warning");
  } else if (repeatC || repeatA || repeatO) {
    swal("Error", "Dosimetros Repetidos ", "warning");
  }
}

function eliminarActa() {
  var info={
    "acta": $("input[name='acta']").val()

  }

  //console.log(info);

  $.ajax({
    url: "/acta/delete",
    type: "POST",
    contentType: 'application/json',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(info),
    success: function (r) {
      console.log(r);

     if(r.res) {
        console.log("Perfecto");
        $(location).attr('href', url + '/listadoActas');
      }else{
        console.log(r);

        swal("Error", "El acta no se puede eliminar por que tiene dosimetros asignados", "warning");
      }
    },
    error: (r) => {}
  });


}

function eliminarAsignaciones() {
  var info = {
    "empleados": [],
    "data": {
      "acta": $("input[name='acta']").val(),

    }
  };
  var hayCheck = true;
  var okCampos = true;
  var repeatC = false;
  var repeatA = false;
  var repeatO = false;
  $('.checkEmployes').each((e, a) => {

    if ($(a)[0].checked) {
      hayCheck = true;
      console.log(hayCheck);

      var valor_id = $(a)[0].value;
      var Dosimetro_Cuerpo = $("input[name='Dosimetro_Cuerpo_id" + valor_id + "']").val();
      var Dosimetro_Ocular = $("input[name='Dosimetro_Ocular_id" + valor_id + "']").val();
      var Dosimetro_Anillo = $("input[name='Dosimetro_Anillo_id" + valor_id + "']").val();
      if (Dosimetro_Cuerpo == "" && Dosimetro_Ocular == "" && Dosimetro_Anillo == "") {
        swal("Error", "Por favor llene los campos", "warning");
        okCampos = false;
      } else {
        okCampos = true;
      }
      info.empleados.push({
        "id": valor_id,
        "valorCuerpo": Dosimetro_Cuerpo,
        "valorOcular": Dosimetro_Ocular,
        "valorAnillo": Dosimetro_Anillo
      });
    }
  });



  if (hayCheck && okCampos && !repeatC && !repeatA && !repeatO) {
    $.ajax({
      url: "/asignacion/delete",
      type: "POST",
      contentType: 'application/json',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 'application/json'
      },
      data: JSON.stringify(info),
      success: function (r) {
        console.log(r);

        if (!r[1].res) {

          r[1].error.forEach(data => {
            $("#" + data.campo).html(data.msj);
            console.log($("#" + data.campo + data.cod));
          });

        } else {
          console.log("Perfecto");
          $(location).attr('href', url + '/dosimetrosAsignados/' + r[1].actaId);
        }
      },
      error: (r) => {}
    });
  } else if (!hayCheck) {
    swal("Error", "Por favor seleccione empleados ", "warning");
  } else if (repeatC || repeatA || repeatO) {
    swal("Error", "Dosimetros Repetidos ", "warning");
  }
}

function checkDuplicateInObject(propertyName, inputArray) {
  var seenDuplicate = false,
    testObject = {};

  inputArray.map(function (item) {
    var itemPropertyName = item[propertyName];
    if (itemPropertyName != "") {
      if (itemPropertyName in testObject) {
        testObject[itemPropertyName].duplicate = true;
        item.duplicate = true;
        seenDuplicate = true;
      } else {
        testObject[itemPropertyName] = item;
        delete item.duplicate;
      }
    }


  });

  return seenDuplicate;
}

function crearSedeDos() {

  $('#sedeDos').validator('update');
  // console.log($('#sedeDos'));

  $("#sedeDos").validator('update').on('submit', function (e) {
    console.log(e);

    if (e.isDefaultPrevented()) {

    } else {
      e.preventDefault();
      console.log('esta bueno');

      var info = {
        "departamentos": [],
        "data": {
          "nombre": $(" #sedeDos input[name='nombre']").val(),
          "personacontacto": $(" #sedeDos input[name='personacontacto']").val(),
          "cargodelcontacto": $(" #sedeDos input[name='cargodelcontacto']").val(),
          "telefono": $(" #sedeDos input[name='telefono']").val(),
          "correo": $(" #sedeDos input[name='correo']").val(),
          "direccion": $(" #sedeDos input[name='direccion']").val(),
          "cantidad_dosimetros": $(" #sedeDos input[name='cantidad_dosimetros']").val(),
          "Ciudad_id": $(" #sedeDos select[name='Ciudad_id']").val(),
          "Empresa_id": $(" #sedeDos input[name='Empresa_id']").val(),
          "Tipo_Corte_Despacho_id": $(" #sedeDos select[name='Tipo_Corte_Despacho_id']").val(),
          "Tipo_contrato_id": $(" #sedeDos select[name='Tipo_contrato_id']").val(),
          "prioridad": $(" #sedeDos select[name='prioridad']").val()

        }
      };

      console.log($("#sedeDos select[name='prioridad']").val());


      info.departamentos.push($('.departamento').serializeArray());
      $.ajax({
        url: "/crearSede",
        type: "POST",
        contentType: 'application/json',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(info),
        success: function (r) {

          if (r[0]["respuesta"]) {
            swal("Ok", "Sede creada con éxito!", "success");
            $(location).attr('href', url + '/sedes/' + r[0]["idEmpresa"]);

          }



        }

      });


    }


  });


}
function crearCiudad(){
  $('#ciudad').validator().on('submit', function (e) {


    if (e.isDefaultPrevented()) {

    } else {
      e.preventDefault();

      var ciudad= {'ciudad':$(" #ciudad input[name='nombreCiudad']").val()};

      $.ajax({
        url: "/crearCiudad",
        type: "POST",
        contentType: 'application/json',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(ciudad),
        success: function (r) {

          if (r) {
             swal("Ok", "Ciudad creada con éxito!", "success");
             $(" #ciudad input[name='nombreCiudad']").val('');

          }  else {
            swal("Error!", "La Ciudad no se pudo crear", "error");

          }

        }

      });


    }


  });

}

function crearEmpresa() {

  console.log($('#empresa').validator('update'));

  $('#empresa').validator().on('submit', function (e) {
    console.log(e);

    if (e.isDefaultPrevented()) {

    } else {
      e.preventDefault();
      console.log('esta bueno');

      var checkbox1 = document.getElementById("checkSi");
      var checkbox2 = document.getElementById("checkNo");

      if (checkbox1.checked == true) {
        optionsRadios = "option1";
      }
      var optionsRadios;

      if (checkbox2.checked == true) {
        optionsRadios = "option2";
      }

      var info = {
        "departamentos": [],
        "data": {
          "nombre": $(" #empresa input[name='nombre']").val(),
          "nit": $(" #empresa input[name='nit']").val(),
          "optionsRadios": optionsRadios,
          "fecha_ingreso": $(" #empresa input[name='fecha_ingreso']").val(),
          "personacontacto": $(" #empresa input[name='personacontacto']").val(),
          "cargodelcontacto": $(" #empresa input[name='cargodelcontacto']").val(),
          "telefono": $(" #empresa input[name='telefono']").val(),
          "correo": $(" #empresa input[name='correo']").val(),
          "direccion": $(" #empresa input[name='direccion']").val(),
          "cantidad_dosimetros": $(" #empresa input[name='cantidad_dosimetros']").val(),
          "Ciudad_id": $(" #empresa select[name='Ciudad_id']").val(),
          "Empresa_id": $(" #empresa select[name='Empresa_id']").val(),
          "Tipo_Corte_Despacho_id": $(" #empresa select[name='Tipo_Corte_Despacho_id']").val(),
          "Tipo_contrato_id": $(" #empresa select[name='Tipo_contrato_id']").val()

        }
      };

      console.log(info);


      info.departamentos.push($('#empresa .departamento').serializeArray());
      $.ajax({
        url: "/crearEmpresa",
        type: "POST",
        contentType: 'application/json',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(info),
        success: function (r) {

          if (r[0]["respuesta"] == true) {

            swal("Ok", "Empresa creada con éxito!", "success");
            $(location).attr('href', url + '/empresas');

          } else if (r[0]["respuesta"] == "error") {
            swal("Error!", "Ya existe un registro con el nit ingresado", "error");

          } else {
            swal("Error!", "La empresa no se pudo crear", "error");

          }

        }

      });


    }


  });




}


function crearSede() {

  console.log('crear sede');


  $('#sede').validator('update');

  $("#sede").validator('update').on('submit', function (e) {
    console.log(e);

    if (e.isDefaultPrevented()) {

    } else {
      e.preventDefault();
      console.log('esta bueno');
      var info = {
        "departamentos": [],
        "data": {
          "nombre": $(" #sede input[name='nombre']").val(),
          "personacontacto": $(" #sede input[name='personacontacto']").val(),
          "cargodelcontacto": $(" #sede input[name='cargodelcontacto']").val(),
          "telefono": $(" #sede input[name='telefono']").val(),
          "correo": $(" #sede input[name='correo']").val(),
          "direccion": $(" #sede input[name='direccion']").val(),
          "cantidad_dosimetros": $(" #sede input[name='cantidad_dosimetros']").val(),
          "Ciudad_id": $(" #sede select[name='Ciudad_id']").val(),
          "Empresa_id": $(" #sede select[name='Empresa_id']").val(),
          "Tipo_Corte_Despacho_id": $(" #sede select[name='Tipo_Corte_Despacho_id']").val(),
          "Tipo_contrato_id": $(" #sede select[name='Tipo_contrato_id']").val(),
          "prioridad": $(" #sedeDos select[name='prioridad']").val()

        }
      };
      info.departamentos.push($('.departamento').serializeArray());
      $.ajax({
        url: "/crearSede",
        type: "POST",
        contentType: 'application/json',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(info),
        success: function (r) {

          if (r[0]["respuesta"]) {
            swal("Ok", "Sede creada con éxito!", "success");
            $(location).attr('href', url + '/sedes/' + r[0]["idEmpresa"]);
          }
        }
      });
    }
  });
}

function editarDepart() {
  var depart = $('.depart').serializeArray();
  depart = JSON.stringify(depart);
  $(location).attr('href', url + '/editarDepartamento/' + depart);

}



function crearEmpleado() {

  $('#crearEmpleado').validator('update');
  $("#crearEmpleado").validator('update').on('submit', function (e) {

    if (e.isDefaultPrevented()) {

    } else {
      e.preventDefault();
      var info = {
        "departamentos": [],
        "data": {
          "Sede_id": $("select[name='Sede_id']").val(),
          "cedula": $("input[name='cedula']").val(),
          "nombre": $("input[name='nombre']").val(),
          "apellido": $("input[name='apellido']").val(),
          "genero": $("select[name='genero']").val(),
          "Tipo_Ocupacion_id": $("select[name='Tipo_Ocupacion_id']").val(),
          "ocupacion": $("input[name='ocupacion']").val(),
          "fecha_ingreso": $("input[name='fecha_ingreso']").val(),
          "tdosimetro": $("input[name='tdosimetro']").val(),
        }
      };
      var hayCheck = false;
      var okCampos = false;
      var depar = [];

      var n = $(".checkEmpleado").filter(":checked");

      if (n.length > 0) {
        for (let i = 0; i < n.length; i++) {
          depar[i] = n[i].value;

        }
        console.log(depar);
        info.departamentos.push(depar);
        hayCheck = true;
      }

      if (hayCheck) {
        console.log("entro al ajax");
        console.log(info);
        $.ajax({
          url: "/crearEmpleado",
          type: "POST",
          contentType: 'application/json',
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json'
          },
          data: JSON.stringify(info),
          success: function (r) {
            console.log(r);
            if (r[0]["respuesta"]) {
              swal("Ok", "Empleado creado con éxito!", "success");
              $(location).attr('href', url + '/empleados');
            }else{
              swal("Error", "El No. de identificación ya tiene un registro en esa sede", "warning");
            }

          }
        });
      } else if (!hayCheck) {
        swal("Error", "Por favor seleccione departamentos", "warning");
      }
    }
  });
}

function editarEmpleado() {

  $('#editarEmpleado').validator('update');
  $("#editarEmpleado").validator('update').on('submit', function (e) {

    if (e.isDefaultPrevented()) {

    } else {
      e.preventDefault();
      var info = {
        "departamentos": [],
        "data": {
          "id": $("input[name='id']").val(),
          "Sede_id": $("select[name='Sede_id']").val(),
          "cedula": $("input[name='cedula']").val(),
          "nombre": $("input[name='nombre']").val(),
          "apellido": $("input[name='apellido']").val(),
          "genero": $("select[name='genero']").val(),
          "Tipo_Ocupacion_id": $("select[name='Tipo_Ocupacion_id']").val(),
          "ocupacion": $("input[name='ocupacion']").val(),
          "fecha_ingreso": $("input[name='fecha_ingreso']").val(),
          "activo": $("select[name='activo']").val(),
          "tdosimetro": $("input[name='tdosimetro']").val()
        }
      };
      var hayCheck = false;
      var okCampos = false;
      var depar = [];

      var n = $(".checkEmpleado").filter(":checked");

      if (n.length > 0) {
        for (let i = 0; i < n.length; i++) {
          depar[i] = n[i].value;

        }
        info.departamentos.push(depar);
        hayCheck = true;
      }

      if (hayCheck) {
        console.log("entro al ajax");
        $.ajax({
          url: "/editarEmpleado",
          type: "POST",
          contentType: 'application/json',
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json'
          },
          data: JSON.stringify(info),
          success: function (r) {
            console.log(r);

            if (r[0]["respuesta"]) {
              console.log("respuesta")
              swal("Ok", "Empleado modificado con éxito!", "success");
              window.history.back();
            } else {
              swal("Error!", " El No. de identificación ya tiene un registro en la sede", "error");

            }

          }
        });
      } else if (!hayCheck) {
        swal("Error", "Por favor seleccione departamentos", "warning");
      }
    }
  });

}
