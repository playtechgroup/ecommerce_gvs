<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Shipping_method;
use App\Product;

class Shipping extends Model
{
      //

      protected $table    = 'shipping';
      public $timestamps  = false;
      protected $fillable = [
           'name','quantity','regions','value','ShippingMethod_id','product_id','delivery_days'
      ];

      public function shippingmethod(){
        return $this->belongsto('App\Shipping_method', 'ShippingMethod_id');
       }

       public function product(){
        return $this->belongsTo('App\Product', 'product_id');
       }

}
