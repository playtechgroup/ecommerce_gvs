<?php

namespace App;
use App\Costumer;
use App\City;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table='address';
    public $timestamps = false;
    protected $fillable = [
        'name','address','Costumer_id','City_id'
    ];



    public function costumer(){
        return $this->belongsTo('App\Costumer','Costumer_id');
    }

    public function city(){
        return $this->belongsTo('App\City','City_id');
    }


}
