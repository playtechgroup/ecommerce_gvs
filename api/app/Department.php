<?php

namespace App;
use App\Country;
use App\City;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $table='department';
    public $timestamps = false;
    protected $fillable = [
        'name', 'Country_id'
    ];

    public function country(){
        return $this->belongsto('App\Country', 'Country_id');
       }

    public function cities(){
        return $this->hasmany('App\City');
       }

}
