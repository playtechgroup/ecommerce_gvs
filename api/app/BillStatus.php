<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bill;

class BillStatus extends Model
{

    protected $table= "billstatus";
   public $timestamps = false;
   protected $fillable = [
       'name'
   ];

   public function bills(){
    return $this->hasMany('App\Bill');
    }
}
