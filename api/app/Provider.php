<?php
use App\User;
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Product;
use App\City;
use App\TypeIdentification;

class Provider extends Model
{
      //
      protected $table='provider';
   public $timestamps = false;
   protected $fillable = [
       'identification', 'address','contact_person','phone', 'trade_certificate','tax_registration','users_id', 'City_id', 'TypeIdentification_id'
   ];

   public function user(){
    return $this->belongsTo('App\User','users_id');
}

public function typeIdentification(){
    return $this->belongsTo('App\TypeIdentification','TypeIdentification_id');
}

    public function city(){
    return $this->belongsTo('App\City','City_id');
    }

    public function provider(){
        return $this->belongsTo('App\Provider','Provider_id');
        }

    public function products(){
    return $this->hasMany('App\Product');
    }

}
