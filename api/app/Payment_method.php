<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
class Payment_method extends Model
{
    protected $table    = 'payment_method';
    public $timestamps  = false;
    protected $fillable = [
         'name'
    ];
    public function orders(){
        return $this->hasMany('App\Order');
    }
}
