<?php

namespace App;
use App\User;
use App\Typeidentification;
use App\TypeCostummer;
use App\City;
use App\Order;
use App\Notification;

use Illuminate\Database\Eloquent\Model;

class Costumer extends Model
{
   //
   protected $table='costumer';
   public $timestamps = false;
   protected $fillable = [
       'address', 'identification', 'phone', 'occupation', 'users_id', 'TypeIdentification_id', 'TypeCostumer_id', 'City_id'
   ];

   public function user(){
    return $this->belongsto('App\User', 'users_id');
   }

   public function typeidentification(){
    return $this->belongsto('App\Typeidentification', 'TypeIdentification_id');
   }

   public function typecostumer(){
    return $this->belongsto('App\TypeCostummer', 'TypeCostummer_id');
   }

   public function city(){
    return $this->belongsto('App\City', 'City_id');
   }

   public function orders(){
    return $this->hasMany('App\Order');
    }

    public function notiications(){
        return $this->hasMany('App\Notification');
    }
}
