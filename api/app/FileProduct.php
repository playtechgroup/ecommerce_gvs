<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class FileProduct extends Model
{
     //
     protected $table='fileproduct';
     public $timestamps = false;
     protected $fillable = [
         'name','link','Product_id','type'
     ];

     public function product(){
        return $this->belongsTo('App\Product', 'Product_id');
       }


}
