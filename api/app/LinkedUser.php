<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkedUser extends Model
{
    protected $table='linked_users';
    public $timestamps = false;
    protected $fillable = [
        'name', 'email'
    ];

}
