<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Order;
use App\BillStatus;

class Bill extends Model
{
     //
     protected $table ="bill";
     public $timestamps = false;
     protected $fillable = [
         'order_id','date','value','coin','BillStatus_id'
     ];
    public function order(){
        return $this->belongsto('App\Order', 'order_id');
    }

    public function billstatus(){
        return $this->belongsto('App\BillStatus', 'BillStatus_id');
    }
}
