<?php

namespace App;
use App\Department;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
   //
   protected $table="country";
   public $timestamps = false;
   protected $fillable = [
       'name'
   ];

   public function departments(){
    return $this->hasMany('App\Department', 'Department_id');
   }

}
