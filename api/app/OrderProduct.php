<?php

namespace App;

use App\Product;
use App\Order;
use App\OrderProductStatus;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_has_product';
    public $timestamps = false;
    protected $fillable = [
         'order_id','Product_id','OrderProductStatus_id','quantity','rate'
     ];


    public function product(){
        return $this->belongsTo('App\Product','Product_id');
    }

    public function order(){
        return $this->belongsTo('App\Order','order_id');
    }

    public function orderproductstat(){
        return $this->belongsTo('App\OrderProductStatus','OrderProductStatus_id');
    }


}
