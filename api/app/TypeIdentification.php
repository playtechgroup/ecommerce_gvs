<?php

use App\Provider;
namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeIdentification extends Model
{
    //
    protected $table='typeidentification';
    public $timestamps = false;
    protected $fillable = [
        'name'
    ];

    public function costumers(){

        return $this->hasMany('App\Costumer');
    }

    public function providers(){
        return $this->hasMany('App\Provider');
       }
}
