<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\CategoryProduct;
use App\Product;
use App\Notification;
use App\User;
use Datetime;



class ApiHomeUserController extends Controller


{
    public function category_products_menu()
    {
        $menu = CategoryProduct::with('subcategoryproducts')
                    ->where('active','1')
                    ->get();
        return response()->json($menu);
    }
    public function notifications_home_new_products()
    {
        //$fecha = date( "Y-m-d", strtotime('-90 day'));
        $new_product = Product::with('fileproducts','provider.user','provider.city','subcategoryproduct.categoryproduct')
                     //->where("date_create",'>', $fecha)
                    ->where('active',1)->take(10)->orderBy('date_create', 'DESC')->get();
        return response()->json(['response'=>true,'products'=>$new_product,'title'=>'Lo Ultimo']);
    }
    public function notifications_not_read($id)
    {
        $not_read = Notification::where('Costumer_id',$id)
                    ->where('state','=','0')
                    ->get();
        return response()->json($not_read);
    }
    public function getLinked(Request $request)
    {
        $emailExists = User::where('email',$request->email)->first();
        $emailExists->linked=($emailExists->linked?'1':'0');
        return response()->json($emailExists);
    }
}
