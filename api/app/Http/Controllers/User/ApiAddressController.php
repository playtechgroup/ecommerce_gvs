<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Address;
use App\Costumer;
use App\User;

class ApiAddressController extends Controller
{

//initial flow
public function index($idCostumer)
{
    $address=Address::where('Costumer_id',$idCostumer)->get();
    return response()->json($address);

}

public function create(Request $request)
{
    $costumer = Costumer::where('users_id',$request->Costumer_id)->first();
    $address=new Address([
        'name'=>$request->name,
        'address'=>$request->address,
        'Costumer_id'=>$costumer->id,
        'City_id'=>$request->City_id['id']
    ]);
    $save=$address->save();
    if(isset($save)){
        return response()->json(array('response' => true,'address'=>$address), 200);
    }
    else {
        return response()->json(array('response' => false ,'error'=>'Los cambios no han sido almacenados'), 200);
    }

}


}
