<?php

namespace App\Http\Controllers\User;

use DB;
use App\Events;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiEventsController extends Controller
{ 
    public function index()
    {
        $events = DB::table('events')
        ->join('event_type', 'event_type.id', '=', 'events.event_type_id')
        ->select('events.*', 'event_type.description as descripctionType')->get();
        return response()->json($events);

    }
}
