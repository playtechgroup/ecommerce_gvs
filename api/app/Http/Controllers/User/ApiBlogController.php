<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Notices;
use Illuminate\Http\Request;

class ApiBlogController extends Controller
{
    
    public function index()
    {
        $notices = Notices::where('tipe',0)->get();
        return response()->json($notices);

    }
    
    public function indexClinical()
    {
        $notices = Notices::where('tipe',1)->get();
        return response()->json($notices);

    }
}
