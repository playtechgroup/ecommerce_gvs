<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;

    use Illuminate\Http\Request;
    use App\SubcategoryProduct;

    class ApiSubcategoryUserController extends Controller
    {
        public function list_subcategory_ofcategory($id)
        {
            $cat = SubcategoryProduct::where('CategoryProduct_id',$id)
                        ->orderBy('name', 'asc')
                        ->get();
            return response()->json($cat);
        }
    }