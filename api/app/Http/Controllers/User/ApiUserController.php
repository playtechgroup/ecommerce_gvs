<?php
// namespace App\Http\Controllers;
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Country;
use App\Department;
use App\City;
use App\User;
use App\Costumer;
use App\LinkedUser;
use App\TypeIdentification;
use App\TypeCostumer;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class ApiUserController extends Controller
{
    public function signup(Request $request)
    {
        $emailExists=false;
        $emailExists    = User::where('email',$request->email)->first();
        if ($emailExists){
            $respuesta['errorInfo'] = 'El email ingresado ya existe';
            return response()->json(array(
                'response' => false,
                'message' => $respuesta
            ) , 200);
        } else {
            //User
            $name               = $request->name;
            $email              = $request->email;
            $password           = $request->password;
            $role               = 2;
            //Costumer
            $address            = $request->address;
            $phone              = $request->phone;
            $identification     = $request->identification;
            $occupation         = ($request->occupation)?$request->occupation:'pendiente';
            $typecostumer       = $request->typecostumer;
            $typeidentification = $request->typeidentification;
            $city               = $request->city;
            $user = new User(['name' => $name,
            'password' => Hash::make($password) ,
            'email' => $email,
            'linked' => 1,
            'Role_id' => $role]);
            try
            {
                $token = $user->createToken('gvs')->accessToken;
                $user['token'] = $token;
                $ok = $user->save();
                if ($ok)
                {
                    $costumer = new Costumer(['address' => $address,
                    'identification' => $identification,
                    'phone' => $phone,
                    'occupation' => $occupation,
                    'users_id' => $user->id,
                    'TypeIdentification_id' => $typeidentification,
                    'TypeCostumer_id' => $typecostumer,
                    'City_id' => $city]);
                    $costumer->save();
                    $respuesta['errorInfo'] = 'Cuenta creada exitosamente';

                    $linked = LinkedUser::where('email',$request->email)->first();

                    if ($linked){
                        $user->linkedId = $linked->id;
                    } else {
                        $user->linkedId = 0;
                    }
                    return response()
                        ->json(array(
                        'response' => true,
                        'message' => $respuesta,
                        'info' => $user
                    ) , 200);
                }
            }
            catch(\Throwable $th)
            {
                return response()->json(array(
                    'response' => false,
                    'message' => $th
                ) , 200);
            }
        }
        //  echo($request->name);

    }
    public function country()
    {
        $country = Country::where('id', 1)->get();
        return response()
            ->json($country);
    }
    public function department($idPais)
    {
        $dpt = Department::where('Country_id', $idPais)->get();
        return response()
            ->json($dpt);
    }
    public function city($idDpt)
    {
        $city = City::where('Department_id', $idDpt)->get();
        return response()
            ->json($city);
    }
    public function login(Request $request)
    {
        if (Auth::attempt($request->only('email', 'password')))
        {
            $user = Auth::user();
            $token = $user->createToken('gvs')->accessToken;
            $user = User::where('id', '=', $user->id)
                ->first();
         

           

           
                $info = DB::table('users')
                ->join('costumer', 'users.id', '=', 'costumer.users_id')
                ->select('users.*', 'costumer.address', 'costumer.identification', 'costumer.phone', 'costumer.occupation', 'costumer.users_id', 'costumer.TypeIdentification_id', 'costumer.TypeCostumer_id', 'costumer.City_id', 'costumer.id as costumerId')
                ->where('users.id', '=', $user->id)->first();
             
         
            return response()
                ->json(array(
                'response' => true,
                'info' => $info
            ) , 200);
        }
        else
        {
            $user = User::where('email', $request->email)
                ->first();
            if ($user)
            {
                return response()->json(['response' => false, 'error' => 'Contraseña incorrecta'], 201);
            }
            else
            {
                return response()
                    ->json(['response' => false, 'error' => 'Usuario no registrado'], 201);
            }
        }
    }
    public function typeidentification()
    {
        $typeidentification = TypeIdentification::all();
        return response()->json($typeidentification);
    }
    public function typecostumer()
    {
        $typecostumer = TypeCostumer::all();
        return response()->json($typecostumer);
    }
    public function editprofile(Request $request)
    {
        //user
        $name               = $request->name;
        $email              = $request->email;
        //Costumer
        $idCostumer         = $request->costumerId;
        $address            = $request->address;
        $phone              = $request->phone;
        $identification     = $request->identification;
        $occupation         = $request->occupation;
        $typecostumer       = $request->TypeCostumer_id;
        $typeidentification = $request->TypeIdentification_id;
        $city               = $request->City_id;
        $costumer           = Costumer::find($idCostumer);
        $type=($request->type)?$request->type:'null';

        if($type=="token"){
           $user=User::find($request->data['users_id']);
           $user->token=$request->token;
           $user->save();
           $response = array(
            'response' => true,
            'message' => 'Token guardado'
        );
        return response()->json($response);

        }
        if (isset($costumer))
        {
            $uservalidation = User::where('id', 'NOT LIKE', $costumer->users_id)
                ->where('email', $email)->first();
            if (isset($uservalidation))
            {
                $response = array(
                    'response' => false,
                    'error' => 'Email ya se encuentra registrado'
                );
            }
            else
            {
                $user = User::find($costumer->users_id);
                $user->name = $name;
                $user->email = $email;
                try
                {
                    $saveU = $user->save();
                    if ($saveU)
                    {
                        $costumer->address = $address;
                        $costumer->phone = $phone;
                        $costumer->occupation = $occupation;
                        $costumer->identification = $identification;
                        $costumer->TypeIdentification_id = $typeidentification;
                        $costumer->TypeCostumer_id = $typecostumer;
                        $costumer->City_id = $city;
                        //  dd($costumer);
                        $saveC = $costumer->save();
                        if ($saveC)
                        {

                            $info = DB::table('users')
                            ->join('costumer', 'users.id', '=', 'costumer.users_id')
                            ->select('users.*', 'costumer.address', 'costumer.identification', 'costumer.phone', 'costumer.occupation', 'costumer.users_id', 'costumer.TypeIdentification_id', 'costumer.TypeCostumer_id', 'costumer.City_id', 'costumer.id as costumerId')
                            ->where('users.id', '=', $user->id)->first();
                            $response = array(
                                'response' => true,
                                'profile' => $info
                            );
                        }
                    }
                    else
                    {
                        $response = array(
                            'response' => false
                        );
                    }
                }
                catch(\Throwable $th)
                {
                    $response = array(
                        'response' => false,
                        'error' => $th
                    );
                }
            }
        }
        else
        {
            $response = array(
                'response' => false,
                'error' => 'Cliente no encontrado'
            );
        }
        return response()->json($response);
    }
}

