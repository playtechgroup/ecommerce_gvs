<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\Bill;

class ApiInvoiceUserController extends Controller
{
    public function send_invoce($id)
    {
        $invoice = Order::with('bills')->where('id',$id)
                    ->get();
        return response()->json($invoice);
    }
}
