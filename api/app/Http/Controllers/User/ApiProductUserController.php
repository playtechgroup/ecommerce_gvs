<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\SubcategoryProduct;
use App\CategoryProduct;
use App\Product;
use App\OrderProduct;
use App\Order;



class ApiProductUserController extends Controller

{

    public function all()
    {
        $products=Product::with('fileproducts','provider.user','provider.city','subcategoryproduct.categoryproduct','shipping','shipping.shippingmethod')->where('active',1)->get();
        foreach ($products as $key => $value) {
            $value->rate= $this->getRate($value->id);
            $value->freelocal=5000;
            $value->freenational=10000;
            $value->freeespecial=15000;
        }
        return response()->json(['response'=>true,'products'=>$products]);
    }

    public function products_list($type,$id)
    {
        if($type=='category'){

            $list  = Product::with('provider.user')->whereHas('subcategoryproduct', function ($query) use($id) {
                $query->where('CategoryProduct_id',$id);
            })->where('active',1)->get();

        }elseif($type=='subcategory'){
            $list  = Product::with('provider.user')->where('SubcategoryProduct_id',$id)->where('active',1)
            ->get();
        }else{
            return response()->json(['reponse'=>false,'message'=>'type no found only accept category or subcategory']);
        }

        if($list){
            foreach ($list as $key => $value) {

                $value->rate= $this->getRate($value->id);
               }


        return response()->json($list);
        }

    }

    public function products_list_price($type,$id,$filtro)
    {

        if($filtro=='ASC' || $filtro=='DESC' ){
        if($type=='category'){

            $list  = Product::with('provider.user')->where('active','1')->whereHas('subcategoryproduct', function ($query) use($id) {
                $query->where('CategoryProduct_id',$id);
            })->orderBy('price', $filtro)
            ->get();

        }else if($type=='subcategory'){
            $list  = Product::with('provider.user')->where('active','=','1')->where('SubcategoryProduct_id',$id)
            ->orderBy('price', $filtro)
            ->get();
        }else{
            return response()->json(['reponse'=>false,'message'=>'type no found only accept category or subcategory']);
        }

            if($list){
                foreach ($list as $key => $value) {

                    $value->rate= $this->getRate($value->id);
                }
            return response()->json($list);
            }
        }else{
            return response()->json(['reponse'=>false,'message'=>'filter no found only accept ASC or DESC']);
        }
    }


    public function rate($type,$id,$filtro)
    {

                //Consulto todos los productos de una subcategoria o categoria
        if($type=='category'){

            $products_qualification  = Product::with('provider.user')->where('active','1')->whereHas('subcategoryproduct', function ($query) use($id) {
                $query->where('CategoryProduct_id',$id);
            })->get();

        }else if($type=='subcategory'){
            $products_qualification  = Product::with('provider.user')->where('active','1')->where('SubcategoryProduct_id',$id)
            ->get();
        }else{
            return response()->json(['reponse'=>false,'message'=>'type no found only accept category or subcategory']);
        }

       //Recorro los productos y busco si han sido comprados y tienen una calificacion diferente de 0 si la tiene se hace un promedio de los mismos
       foreach ($products_qualification as $key => $value) {
        $value->rate= $this->getRate($value->id);
       }

       //luego valido el tipo de filtro que desea si de forma DESC O ASC
       if($filtro=='DESC'){

        $products_qualification=  $products_qualification->sortByDesc('rate');
       }elseif ($filtro=='ASC') {

        $products_qualification=  $products_qualification->sortBy('rate');
       }else{
        return response()->json(['reponse'=>false,'message'=>'filter no found only accept ASC or DESC']);
       }

            return response()->json($products_qualification);
    }


    public function getRate($product){

        $orderProduct= OrderProduct::selectRaw("AVG(NULLIF(rate,0)) as rate")->where('Product_id',$product)->groupBy('order_id')->first();
            //le agreago a los productos consultados anteriormente un campo de RATE y le pongo el promedio de calificacion si no tiene le pongo 0
         return ($orderProduct && $orderProduct->rate>0)?number_format($orderProduct->rate,1):0;

    }

}
