<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Mail;
use App\User;
use Str;
use Illuminate\Support\Facades\Hash;
//use Ese
class ApiUserContrasena extends Controller
{





public function reestablecerPass(Request $request){
    $email= $request['email'];
            $user = User::where('email', $request['email'])->where('Role_id',2)->first();
			if ($user){
                $user->remember_token=Str::random(100);
                $user->save();
				$data['email'] = $email;
				$data['token'] = $user->remember_token;
				try {
					$sendmail=Mail::send('mails.restablecerPass',['data'=>$data],function($mail)use($data){
						$mail->from('inmunotek@evocompany.co', 'INMUNOTEK');
						$mail->subject('Reestablecer contraseña');
						$mail->to($data['email']);

					});

					$json= array(
						'response' => true,
						'mensaje' => 'Se ha enviado un correo para restablecer la contraseña, por favor entrar y seguir los pasos.'
						);


				} catch (Exception $e) {

					$json= array(
						'response' => false,
						'mensaje' => 'Ocurrio un error intente mas tarde'
						);
				}

			   return json_encode($json);


			}else
			 {
				$json= array(
					'estado' => false,
					'mensaje' => 'El email solicitado no se encuentra registrado en la plataforma.'
					);

			   return json_encode($json);

			}

		}






		public function recuperarPassApp($email,$token){

				$results = DB::select('select remember_token from users where email= ?',array($email));


				$code = json_encode($results);// codifico los datos (el array del while))
				$decode=json_decode($code, true); //decodifico lo anterior...

				$token_var=$decode[0]['remember_token'];

				if ($token_var==$token) {
					return View('reestablecerPassApp', ['email_new'=>$email,'token_new'=>$token]);
				}else{
					return 'Link de correo vencido, por favor intente otra vez en Recuperar Contraseña';
				}
		}

		public function Controles_nuevoPassApp(Request $request){

			/*Los dos datos email y token llegan a la vista de reestablecerPassApp pero no se envian, por ende el controlador no tiene token y email*/
			$password = $request->input("password");
			$password_new = $request->input("password_new");
			$token = $request->input("token");
			$email = $request->input("email");

			if ($password==$password_new) {

			DB::update('update users set password= ?, remember_token=? where email = ?',array(bcrypt($password),Str::random(100),$email));

			return View('reestablecerPassApp',['messagetrue'=>'Tu contraseña fue actualizada de manera exitosa. Ahora entra y pide tus productos']);

			}else{
			return View('reestablecerPassApp',['email_new'=>$email,'token_new'=>$token,'messagefail'=>'Ocurrio un problema la contraseña digitada no coincide.']);
			}
		}

		public function editpassword(Request $request)
    {
        $email              = $request->email;
        $password         = $request->password;

           $user=User::where('email',$email)->first();
           $user->token=$request->token;
		   $user->password= Hash::make($password);
           $user->save();

		   if ($user){ 
			$response = array(
				'response' => true,
				'message' => 'La contraseña fue actualizada exitosamente'
				);
			} else {
                $response = array(
                    'response' => false,
                    'message' => 'Hubo un error cambiando la contraseña'
                );
			}
        return response()->json($response);
    }
}
