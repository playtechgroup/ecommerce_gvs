<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Payment_method;

class ApiPaymentmethodUserController extends Controller
{
    public function payment_method()
    {
        $py = Payment_method::all();
        return response()->json($py);
    }
}
