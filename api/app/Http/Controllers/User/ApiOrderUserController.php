<?php

namespace App\Http\Controllers\User;

use Mail;
use Datetime;
use App\Order;
use App\Product;
use App\Provider;
use App\Notification;
use App\OrderProduct;
use App\Costumer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiOrderUserController extends Controller
{
    //


public function createorder(Request $request)
{

    $mycostumer = Costumer::where('users_id',$request->idCostumer)->first();
    $idCostumer=$mycostumer->id;
    $products=$request->products;
    $payment_method=$request->payment_method;
    $address=$request->idaddress;
    $products=json_decode($products);
    $status=6;
    // echo $idCostumer;
    // //echo $products;
    // echo $payment_method;
    // echo $address['id'];
    // return ;
    $fecha = date("Y-m-d");
    $response=array('response'=>false,"data"=>array());
    $hasorder=false;
    $productsName = "";
    foreach ($products as $key => $value) {

        $product=Product::find($value->id);
        $provider=Provider::find($product->Provider_id);

        $productsName .= $product->name . ', ';
        try {
            //code...

                //Busca si existe una orden ya creada de ese mismo provedor
                // $ordersearch=Order::whereHas('products',function ($query1) use($provider) {

                //     $query1->whereHas('provider', function ($query2) use($provider) {

                //         $query2->where('id',$provider->id);
                //     });
                // })->where('date',$fecha)->where('Costumer_id',$idCostumer)->first();

              //Si tiene orden agrega el producto a esa misma orden
                if($hasorder){

                    $orderproduct=new OrderProduct([
                        'order_id'=>$order->id,
                        'Product_id'=>$product->id,
                        'OrderProductStatus_id' => 4,
                        'quantity'=>$value->quantity


                    ]);

                    $response['response']=($orderproduct->save())?true:false;

                    try {
                    /**Notificando al provedor de una nueva orden */
                    $email=$orderproduct->product->provider->user->email;
                    $sendmail1=Mail::send('mails.notificaciones',['title'=>'Nuevo Pedido','mensaje'=>'Tienes un nuevo pedido de '.$orderproduct->product->name],function($mail)use($email){
                        $mail->from('inmunotek@evocompany.co', 'INMUNOTEK');
                        $mail->subject('Tienes un nuevo pedido');
                        $mail->to($email);
                    });
                    }catch(Exception $e){
                        // Never reached
                    }

                }else{

                    $order=new Order([
                        'date'=>  $fecha    ,
                        'Costumer_id'=>    $idCostumer  ,
                        'Payment_method_id'=>  $payment_method    ,
                        'OrderStatus_id' => $status,
                        'Address_id'=>$address,
                        'shipping_value' => $request->shippingvalue
                    ]);
                     //Si no tiene tiene orden  crea una nueva
                    $order->save();


                    if($order){

                        $message="Su orden ".$order->id." con los productos ".$productsName." ha sido creada.";
                        $fecha = date("Y-m-d");

                        $notification=new Notification([
                            'message'=>$message,
                            'state'=>1,
                            'date' => $fecha,
                            'Costumer_id'=>$idCostumer
                        ]);

                        $ok = $notification->save();


                        $orderproduct=new OrderProduct([
                            'order_id'=>$order->id,
                            'Product_id'=>$product->id,
                            'OrderProductStatus_id' => 4,
                            'quantity'=>$value->quantity
                        ]);
                        $orderproduct->save();

                            $theemail=$orderproduct->product->provider->user->email;
                        $response['response']=($orderproduct->save())?true:false;
                        $hasorder=true;

                        $database = app('firebase.database');
                        $database->getReference('/ordenes/' . $order->id . '/estado')->set($status);

                    }
                }

            }catch(Exception $e){
                $response['data']=$e;
            }


    }

    if($order){
          try {
             /**Notificando al provedor de una nueva orden */
          $mail1=Mail::send('mails.notificaciones',['title'=>'Nuevo Pedido','mensaje'=>'Tienes un nuevo pedido de '.$orderproduct->product->name],function($mail)use($theemail){
            $mail->from('inmunotek@evocompany.co', 'INMUNOTEK');
            $mail->subject('Tienes un nuevo pedido');
            $mail->to($theemail);
        });
          }
        catch (\Throwable $th) {
            $response['data']=$th;
        }
    }
    if($response['response']){
        $mail2=Mail::send('mails.notificaciones',['title'=>'Nuevo Pedido','mensaje'=>'Tienes un nuevo pedido'],function($mail)use($orderproduct){
            $mail->from('inmunotek@evocompany.co', 'INMUNOTEK');
            $mail->subject('Tienes un nuevo pedido');
            $mail->to('appdigitalco@gmail.com');
        });
        $response['data']=Order::with('products')->where('id',$order->id)->first();
    }
    return response()->json($response);

}

public function historyorders($idCostumer)
{

    $costumer = Costumer::where('users_id',$idCostumer)->first();
  $orders=Order::with('products','orderstatus','paymentmethod')->where('Costumer_id',$costumer->id)->orderBy('id', 'desc')->get();
    foreach ($orders as $key => $value) {
        foreach ($value->products as $key => $value2) {
           $orderp= OrderProduct::where('order_id',$value->id)->where('Product_id',$value2->id)->first();
          if ($orderp){$value2->quantity=$orderp->quantity;}
        }
    }
   return response()->json($orders);

}

public function notificationorders($id)
{

    $notification= Notification::where('Costumer_id',$id)
    ->where('state','=','1')->orderBy('id', 'desc')
    ->get();

    return response()->json($notification);

}



public function newNotification(Request $request)
{

    $message=$request->message;
    $costumerId=$request->costumerId;
    $fecha = date("Y-m-d");

    $notification=new Notification([
        'message'=>$message,
        'state'=>1,
        'date' => $fecha,
        'Costumer_id'=>$costumerId
    ]);

    $ok = $notification->save();

    if ($ok){
        return response()->json(true);
    } else {
        return response()->json(false);
    }

}

}
