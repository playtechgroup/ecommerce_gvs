<?php
namespace App\Http\Controllers\User;

use App\Bill;
use DB;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderProduct;
use App\Product;
use Illuminate\Http\Request;

class ApiPayUserController extends Controller
{

// Get respuesta
    public function pagoConfirmationget(Request $request)
    {

        $estado_pago = $_GET["transactionState"];
        $valor = $_GET["TX_VALUE"];
        // $descripcion = $_GET["description"];
        // $referencia = $_GET["referenceCode"];
        $idOrder = $_GET["extra1"];
        // $idEmpresa = $_GET["extra2"];
        // $moneda = $_GET["currency"];
        // $tarjeta = $_GET["lapPaymentMethodType"];
        //dd($request);
        $Order = Order::find($idOrder);
        $Products = OrderProduct::where('order_id', $idOrder)->get();
        return view('payment', compact('Order', 'Products', 'valor', 'estado_pago'));
        //HTML gracias por tu pago, cuanto pagó, info relevante
    }

    public function index()
    {
        $Order = Order::find(48);
        $Products = OrderProduct::where('order_id', 48)->get();
        return view('payment', compact('Order', 'Products'));
    }

    //post confirmation
    public function pagoConfirmation(Request $request)
    {

        //dd($request);

        $estado_pago = $request["state_pol"];
        $valor = $request["value"];
        // $descripcion = $request["description"];
        // $referencia = $request["reference_sale"];
        $idOrder = $request["extra1"];
        // $idEmpresa = $request["extra2"];
        $moneda = $request["currency"];
        // $tarjeta = $request["payment_method_name"];

        if ($estado_pago == 4) {
            $Order = Order::find($idOrder);
            $Order->OrderStatus_id = 1;
            $saved = $Order->save();

            $contador_total = 0;
            $orderproducts = OrderProduct::where('order_id', $idOrder)->get();
            foreach ($orderproducts as $key => $value) {
                $product = Product::find($value->Product_id);
                $newStock = $product->stock - $value->quantity;
                $product->stock = $newStock;
                $ok = $product->save();
                $contador_total+=($value->product->price * $value->quantity)*(($value->tax/100)*1)+($value->product->price * $value->quantity);
            }

            $total=number_format($contador_total);
            echo $contador_total;
            $bill = new Bill([
                'order_id' => $idOrder,
                'value' => $valor,
                'coin' => $moneda,
                'BillStatus_id' => $estado_pago,
            ]);
            $bill->save();

            if (isset($saved)) {
                $database = app('firebase.database');
                $database->getReference('/ordenes/' . $idOrder . '/estado')->set(1);
            }

        }
        //Actualizar estado de la orden y cambiar firebase extra1=idOrder

    }

    //tucompra
    //post confirmation
    public function pagoConfirmacion(Request $request)
    {
        // $tipodepago = $request->campoExtra4;
        $transaccionAprobada = $request->transaccionAprobada;
        // $codigoFactura = $request->codigoFactura;
        $valorFactura = $request->valorFactura;
        // $codigoAutorizacion = $request->codigoAutorizacion;
        // $numeroTransaccion = $request->numeroTransaccion;
        // $firmaTuCompra = $request->firmaTuCompra;
        $campoExtra1 = $request->campoExtra1;
        // $campoExtra2 = $request->campoExtra2;
        // $campoExtra3 = $request->campoExtra3;
        // $campoExtra4 = $request->campoExtra4;
        // $campoExtra5 = $request->campoExtra5;
        // $campoExtra6 = $request->campoExtra6;
        // $campoExtra7 = $request->campoExtra7;
        // $campoExtra8 = $request->campoExtra8;
        // $campoExtra9 = $request->campoExtra9;
        // $metodoPago = $request->metodoPago;
        // $nombreMetodo = $request->nombreMetodo;
        // $finalizar = $request->finalizar;
        $estado_pago = $transaccionAprobada;
        $valor = $valorFactura;
        $idOrder = $campoExtra1;
        $moneda = $request->nombreMetodo;
        $fecha = date("Y-m-d");

        if ($estado_pago == "1") {

            $contador_total = 0;
            $orderproducts = OrderProduct::where('order_id', $idOrder)->get();
            foreach ($orderproducts as $key => $value) {
                $product = Product::find($value->Product_id);
                $newStock = $product->stock - $value->quantity;
                $product->stock = $newStock;
                $ok = $product->save();
                $contador_total+=($value->product->price * $value->quantity)*(($value->tax/100)*1)+($value->product->price * $value->quantity);
            }

            
            $results = DB::select('SELECT sum(product.price+(product.price*(product.tax/100))) as precio FROM `order_has_product`,product WHERE order_has_product.Product_id=product.id and order_id=?',array($idOrder));
            $code = json_encode($results);// codifico los datos (el array del while))
            $decode=json_decode($code, true); //decodifico lo anterior...
            $price=$decode[0]['precio'];
            $bill = new Bill([
                'order_id' => $idOrder,
                'date' => $fecha,
                'value' => $valor,
                'coin' => $moneda,
                'BillStatus_id' => $estado_pago,
            ]);
            $bill->save();

            $totalBills = 0;
            $bills = Bill::where('order_id', $idOrder)->get();
            foreach ($bills as $key => $value) {
                $totalBills = $totalBills + $value->value;
            }

            if ($totalBills == $price){
                $Order = Order::find($idOrder);
                $Order->OrderStatus_id = 1;
                $saved = $Order->save();
            }
            
            if (isset($saved)) {
                $database = app('firebase.database');
                $database->getReference('/ordenes/' . $idOrder . '/estado')->set(1);
            }

        }


        return response()->json(true, 200);

    }

    // url de retorno
    public function pagoRetorno(Request $request)
    {

        $estado_pago = $request->transaccionAprobada;
        $valor = $request->valorFactura;
        $idOrder = $request->campoExtra1;
        $Order = Order::find($idOrder);
        $Products = OrderProduct::where('order_id', $idOrder)->get();
        return view('payment', compact('Order', 'Products', 'valor', 'estado_pago'));
    }

    public function getPagoConfirmation($idOrder)
    {
        $Order = Order::find($idOrder);
        if ($Order->OrderStatus_id == 1) {
            return response()->json(['r' => true]);
        } else {
            return response()->json(['r' => false]);
        }

    }

    public function getBills($idOrder)
    {
        $totalBills = 0;
        $bills = Bill::where('order_id', $idOrder)->get();
        foreach ($bills as $key => $value) {
            $totalBills = $totalBills + $value->value;
        }
        return response($totalBills);

    }
}
