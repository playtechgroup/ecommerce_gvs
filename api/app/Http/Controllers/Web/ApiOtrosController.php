<?php
namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;

class ApiOtrosController extends Controller
{
    public function terminos()
    {

        return '<div class="col-12">AQUÍ VAN LOS TÉRMINOS Y CONDICIONES</div>' ;
    }

    public function politicas(){
        return '<div class="col-12">AQUÍ VAN LAS POLÍTICAS</div>' ;
    }
    public function ayuda()
    {
        return "esto es una prueba ayuda";
    }

    public function userterms()
    {
        return '<style type="text/css">
            @import url(https://themes.googleusercontent.com/fonts/css?kit=soa_V42eXREs8LDkwBiWS64fnxpW6PVRuPCJ3776g6VKDTn8LERXlzF7PmED91NiTGckFRjXKfVJY4BMWL-AuQ);

            .lst-kix_list_4-1>li {
                counter-increment: lst-ctn-kix_list_4-1
            }

            ol.lst-kix_list_7-0 {
                list-style-type: none
            }

            .lst-kix_list_2-1>li {
                counter-increment: lst-ctn-kix_list_2-1
            }

            .lst-kix_list_8-1>li {
                counter-increment: lst-ctn-kix_list_8-1
            }

            ol.lst-kix_list_8-2.start {
                counter-reset: lst-ctn-kix_list_8-2 0
            }

            ol.lst-kix_list_3-1.start {
                counter-reset: lst-ctn-kix_list_3-1 0
            }

            ol.lst-kix_list_7-4.start {
                counter-reset: lst-ctn-kix_list_7-4 0
            }

            .lst-kix_list_5-0>li {
                counter-increment: lst-ctn-kix_list_5-0
            }

            .lst-kix_list_7-0>li {
                counter-increment: lst-ctn-kix_list_7-0
            }

            ol.lst-kix_list_2-3.start {
                counter-reset: lst-ctn-kix_list_2-3 0
            }

            ol.lst-kix_list_7-5 {
                list-style-type: none
            }

            ol.lst-kix_list_7-6 {
                list-style-type: none
            }

            ol.lst-kix_list_7-7 {
                list-style-type: none
            }

            ol.lst-kix_list_7-8 {
                list-style-type: none
            }

            ol.lst-kix_list_7-1 {
                list-style-type: none
            }

            ol.lst-kix_list_1-5.start {
                counter-reset: lst-ctn-kix_list_1-5 0
            }

            ol.lst-kix_list_7-2 {
                list-style-type: none
            }

            ol.lst-kix_list_7-3 {
                list-style-type: none
            }

            ol.lst-kix_list_7-4 {
                list-style-type: none
            }

            ol.lst-kix_list_5-3.start {
                counter-reset: lst-ctn-kix_list_5-3 0
            }

            .lst-kix_list_2-3>li {
                counter-increment: lst-ctn-kix_list_2-3
            }

            .lst-kix_list_4-3>li {
                counter-increment: lst-ctn-kix_list_4-3
            }

            ol.lst-kix_list_4-5.start {
                counter-reset: lst-ctn-kix_list_4-5 0
            }

            .lst-kix_list_1-2>li {
                counter-increment: lst-ctn-kix_list_1-2
            }

            ol.lst-kix_list_3-7.start {
                counter-reset: lst-ctn-kix_list_3-7 0
            }

            .lst-kix_list_5-2>li {
                counter-increment: lst-ctn-kix_list_5-2
            }

            ol.lst-kix_list_8-8.start {
                counter-reset: lst-ctn-kix_list_8-8 0
            }

            .lst-kix_list_3-2>li {
                counter-increment: lst-ctn-kix_list_3-2
            }

            .lst-kix_list_7-2>li {
                counter-increment: lst-ctn-kix_list_7-2
            }

            ol.lst-kix_list_8-7.start {
                counter-reset: lst-ctn-kix_list_8-7 0
            }

            .lst-kix_list_5-0>li:before {
                content: ""counter(lst-ctn-kix_list_5-0, decimal) ". "
            }

            .lst-kix_list_5-4>li {
                counter-increment: lst-ctn-kix_list_5-4
            }

            .lst-kix_list_1-4>li {
                counter-increment: lst-ctn-kix_list_1-4
            }

            ol.lst-kix_list_1-6.start {
                counter-reset: lst-ctn-kix_list_1-6 0
            }

            .lst-kix_list_5-3>li:before {
                content: ""counter(lst-ctn-kix_list_5-3, decimal) ". "
            }

            .lst-kix_list_5-2>li:before {
                content: ""counter(lst-ctn-kix_list_5-2, lower-roman) ". "
            }

            .lst-kix_list_8-3>li {
                counter-increment: lst-ctn-kix_list_8-3
            }

            .lst-kix_list_5-1>li:before {
                content: ""counter(lst-ctn-kix_list_5-1, lower-latin) ". "
            }

            .lst-kix_list_5-7>li:before {
                content: ""counter(lst-ctn-kix_list_5-7, lower-latin) ". "
            }

            .lst-kix_list_5-6>li:before {
                content: ""counter(lst-ctn-kix_list_5-6, decimal) ". "
            }

            .lst-kix_list_5-8>li:before {
                content: ""counter(lst-ctn-kix_list_5-8, lower-roman) ". "
            }

            .lst-kix_list_5-4>li:before {
                content: ""counter(lst-ctn-kix_list_5-4, lower-latin) ". "
            }

            .lst-kix_list_5-5>li:before {
                content: ""counter(lst-ctn-kix_list_5-5, lower-roman) ". "
            }

            ol.lst-kix_list_1-0.start {
                counter-reset: lst-ctn-kix_list_1-0 0
            }

            .lst-kix_list_6-1>li:before {
                content: "o  "
            }

            .lst-kix_list_6-3>li:before {
                content: "\0025cf   "
            }

            .lst-kix_list_6-0>li:before {
                content: "-  "
            }

            .lst-kix_list_6-4>li:before {
                content: "o  "
            }

            .lst-kix_list_3-0>li {
                counter-increment: lst-ctn-kix_list_3-0
            }

            ol.lst-kix_list_4-0.start {
                counter-reset: lst-ctn-kix_list_4-0 0
            }

            .lst-kix_list_3-6>li {
                counter-increment: lst-ctn-kix_list_3-6
            }

            .lst-kix_list_6-2>li:before {
                content: "\0025aa   "
            }

            .lst-kix_list_2-5>li {
                counter-increment: lst-ctn-kix_list_2-5
            }

            .lst-kix_list_2-8>li {
                counter-increment: lst-ctn-kix_list_2-8
            }

            ol.lst-kix_list_3-2.start {
                counter-reset: lst-ctn-kix_list_3-2 0
            }

            .lst-kix_list_6-8>li:before {
                content: "\0025aa   "
            }

            .lst-kix_list_6-5>li:before {
                content: "\0025aa   "
            }

            .lst-kix_list_6-7>li:before {
                content: "o  "
            }

            ol.lst-kix_list_2-4.start {
                counter-reset: lst-ctn-kix_list_2-4 0
            }

            .lst-kix_list_6-6>li:before {
                content: "\0025cf   "
            }

            ol.lst-kix_list_1-3 {
                list-style-type: none
            }

            ol.lst-kix_list_1-4 {
                list-style-type: none
            }

            .lst-kix_list_2-7>li:before {
                content: ""counter(lst-ctn-kix_list_2-7, lower-latin) ". "
            }

            .lst-kix_list_2-7>li {
                counter-increment: lst-ctn-kix_list_2-7
            }

            ol.lst-kix_list_1-5 {
                list-style-type: none
            }

            .lst-kix_list_7-4>li:before {
                content: ""counter(lst-ctn-kix_list_7-4, lower-latin) ". "
            }

            .lst-kix_list_7-6>li:before {
                content: ""counter(lst-ctn-kix_list_7-6, decimal) ". "
            }

            ol.lst-kix_list_1-6 {
                list-style-type: none
            }

            ol.lst-kix_list_1-0 {
                list-style-type: none
            }

            .lst-kix_list_2-5>li:before {
                content: ""counter(lst-ctn-kix_list_2-5, lower-roman) ". "
            }

            ol.lst-kix_list_1-1 {
                list-style-type: none
            }

            ol.lst-kix_list_1-2 {
                list-style-type: none
            }

            .lst-kix_list_7-2>li:before {
                content: ""counter(lst-ctn-kix_list_7-2, lower-roman) ". "
            }

            .lst-kix_list_7-6>li {
                counter-increment: lst-ctn-kix_list_7-6
            }

            .lst-kix_list_8-6>li {
                counter-increment: lst-ctn-kix_list_8-6
            }

            ol.lst-kix_list_4-6.start {
                counter-reset: lst-ctn-kix_list_4-6 0
            }

            ol.lst-kix_list_3-0.start {
                counter-reset: lst-ctn-kix_list_3-0 0
            }

            .lst-kix_list_5-7>li {
                counter-increment: lst-ctn-kix_list_5-7
            }

            .lst-kix_list_7-7>li {
                counter-increment: lst-ctn-kix_list_7-7
            }

            .lst-kix_list_7-8>li:before {
                content: ""counter(lst-ctn-kix_list_7-8, lower-roman) ". "
            }

            ol.lst-kix_list_4-3.start {
                counter-reset: lst-ctn-kix_list_4-3 0
            }

            ol.lst-kix_list_1-7 {
                list-style-type: none
            }

            .lst-kix_list_4-7>li {
                counter-increment: lst-ctn-kix_list_4-7
            }

            ol.lst-kix_list_1-8 {
                list-style-type: none
            }

            .lst-kix_list_7-8>li {
                counter-increment: lst-ctn-kix_list_7-8
            }

            ol.lst-kix_list_2-5.start {
                counter-reset: lst-ctn-kix_list_2-5 0
            }

            .lst-kix_list_2-6>li {
                counter-increment: lst-ctn-kix_list_2-6
            }

            .lst-kix_list_4-1>li:before {
                content: ""counter(lst-ctn-kix_list_4-1, lower-latin) ". "
            }

            ol.lst-kix_list_7-3.start {
                counter-reset: lst-ctn-kix_list_7-3 0
            }

            .lst-kix_list_4-3>li:before {
                content: ""counter(lst-ctn-kix_list_4-3, decimal) ". "
            }

            .lst-kix_list_4-5>li:before {
                content: ""counter(lst-ctn-kix_list_4-5, lower-roman) ". "
            }

            ol.lst-kix_list_5-7.start {
                counter-reset: lst-ctn-kix_list_5-7 0
            }

            .lst-kix_list_1-8>li {
                counter-increment: lst-ctn-kix_list_1-8
            }

            ol.lst-kix_list_1-4.start {
                counter-reset: lst-ctn-kix_list_1-4 0
            }

            .lst-kix_list_5-5>li {
                counter-increment: lst-ctn-kix_list_5-5
            }

            .lst-kix_list_3-5>li {
                counter-increment: lst-ctn-kix_list_3-5
            }

            ol.lst-kix_list_1-1.start {
                counter-reset: lst-ctn-kix_list_1-1 0
            }

            .lst-kix_list_3-4>li {
                counter-increment: lst-ctn-kix_list_3-4
            }

            ol.lst-kix_list_4-4.start {
                counter-reset: lst-ctn-kix_list_4-4 0
            }

            ol.lst-kix_list_1-3.start {
                counter-reset: lst-ctn-kix_list_1-3 0
            }

            ol.lst-kix_list_2-8.start {
                counter-reset: lst-ctn-kix_list_2-8 0
            }

            ol.lst-kix_list_8-8 {
                list-style-type: none
            }

            ol.lst-kix_list_1-2.start {
                counter-reset: lst-ctn-kix_list_1-2 0
            }

            ol.lst-kix_list_7-6.start {
                counter-reset: lst-ctn-kix_list_7-6 0
            }

            ol.lst-kix_list_8-4 {
                list-style-type: none
            }

            ol.lst-kix_list_8-5 {
                list-style-type: none
            }

            ol.lst-kix_list_8-6 {
                list-style-type: none
            }

            ol.lst-kix_list_8-7 {
                list-style-type: none
            }

            ol.lst-kix_list_8-0 {
                list-style-type: none
            }

            ol.lst-kix_list_8-1 {
                list-style-type: none
            }

            .lst-kix_list_1-1>li:before {
                content: ""counter(lst-ctn-kix_list_1-1, lower-latin) ". "
            }

            ol.lst-kix_list_8-2 {
                list-style-type: none
            }

            ol.lst-kix_list_8-3 {
                list-style-type: none
            }

            .lst-kix_list_8-5>li {
                counter-increment: lst-ctn-kix_list_8-5
            }

            .lst-kix_list_1-3>li:before {
                content: ""counter(lst-ctn-kix_list_1-3, decimal) ". "
            }

            .lst-kix_list_4-8>li {
                counter-increment: lst-ctn-kix_list_4-8
            }

            .lst-kix_list_1-7>li:before {
                content: ""counter(lst-ctn-kix_list_1-7, lower-latin) ". "
            }

            ol.lst-kix_list_5-8.start {
                counter-reset: lst-ctn-kix_list_5-8 0
            }

            ol.lst-kix_list_2-7.start {
                counter-reset: lst-ctn-kix_list_2-7 0
            }

            .lst-kix_list_1-3>li {
                counter-increment: lst-ctn-kix_list_1-3
            }

            .lst-kix_list_1-5>li:before {
                content: ""counter(lst-ctn-kix_list_1-5, lower-roman) ". "
            }

            .lst-kix_list_5-6>li {
                counter-increment: lst-ctn-kix_list_5-6
            }

            ol.lst-kix_list_7-5.start {
                counter-reset: lst-ctn-kix_list_7-5 0
            }

            .lst-kix_list_2-1>li:before {
                content: ""counter(lst-ctn-kix_list_2-1, lower-latin) ". "
            }

            .lst-kix_list_2-3>li:before {
                content: ""counter(lst-ctn-kix_list_2-3, decimal) ". "
            }

            .lst-kix_list_4-2>li {
                counter-increment: lst-ctn-kix_list_4-2
            }

            ol.lst-kix_list_3-1 {
                list-style-type: none
            }

            ol.lst-kix_list_3-2 {
                list-style-type: none
            }

            .lst-kix_list_3-1>li {
                counter-increment: lst-ctn-kix_list_3-1
            }

            ol.lst-kix_list_3-3 {
                list-style-type: none
            }

            ol.lst-kix_list_3-4.start {
                counter-reset: lst-ctn-kix_list_3-4 0
            }

            .lst-kix_list_5-1>li {
                counter-increment: lst-ctn-kix_list_5-1
            }

            ol.lst-kix_list_3-4 {
                list-style-type: none
            }

            ol.lst-kix_list_3-0 {
                list-style-type: none
            }

            .lst-kix_list_1-1>li {
                counter-increment: lst-ctn-kix_list_1-1
            }

            .lst-kix_list_7-1>li {
                counter-increment: lst-ctn-kix_list_7-1
            }

            ol.lst-kix_list_2-6.start {
                counter-reset: lst-ctn-kix_list_2-6 0
            }

            .lst-kix_list_3-0>li:before {
                content: ""counter(lst-ctn-kix_list_3-0, decimal) ". "
            }

            ol.lst-kix_list_7-7.start {
                counter-reset: lst-ctn-kix_list_7-7 0
            }

            .lst-kix_list_3-1>li:before {
                content: ""counter(lst-ctn-kix_list_3-1, lower-latin) ". "
            }

            .lst-kix_list_3-2>li:before {
                content: ""counter(lst-ctn-kix_list_3-2, lower-roman) ". "
            }

            .lst-kix_list_8-1>li:before {
                content: ""counter(lst-ctn-kix_list_8-1, lower-latin) ". "
            }

            ol.lst-kix_list_1-8.start {
                counter-reset: lst-ctn-kix_list_1-8 0
            }

            .lst-kix_list_4-0>li {
                counter-increment: lst-ctn-kix_list_4-0
            }

            .lst-kix_list_8-2>li:before {
                content: ""counter(lst-ctn-kix_list_8-2, lower-roman) ". "
            }

            .lst-kix_list_8-0>li {
                counter-increment: lst-ctn-kix_list_8-0
            }

            .lst-kix_list_3-5>li:before {
                content: ""counter(lst-ctn-kix_list_3-5, lower-roman) ". "
            }

            .lst-kix_list_3-4>li:before {
                content: ""counter(lst-ctn-kix_list_3-4, lower-latin) ". "
            }

            .lst-kix_list_3-3>li:before {
                content: ""counter(lst-ctn-kix_list_3-3, decimal) ". "
            }

            ol.lst-kix_list_3-5 {
                list-style-type: none
            }

            ol.lst-kix_list_3-6 {
                list-style-type: none
            }

            .lst-kix_list_8-0>li:before {
                content: ""counter(lst-ctn-kix_list_8-0, decimal) ". "
            }

            ol.lst-kix_list_3-7 {
                list-style-type: none
            }

            ol.lst-kix_list_3-8 {
                list-style-type: none
            }

            .lst-kix_list_8-7>li:before {
                content: ""counter(lst-ctn-kix_list_8-7, lower-latin) ". "
            }

            .lst-kix_list_3-8>li:before {
                content: ""counter(lst-ctn-kix_list_3-8, lower-roman) ". "
            }

            .lst-kix_list_8-5>li:before {
                content: ""counter(lst-ctn-kix_list_8-5, lower-roman) ". "
            }

            .lst-kix_list_8-6>li:before {
                content: ""counter(lst-ctn-kix_list_8-6, decimal) ". "
            }

            .lst-kix_list_2-0>li {
                counter-increment: lst-ctn-kix_list_2-0
            }

            .lst-kix_list_8-3>li:before {
                content: ""counter(lst-ctn-kix_list_8-3, decimal) ". "
            }

            .lst-kix_list_3-6>li:before {
                content: ""counter(lst-ctn-kix_list_3-6, decimal) ". "
            }

            .lst-kix_list_3-7>li:before {
                content: ""counter(lst-ctn-kix_list_3-7, lower-latin) ". "
            }

            .lst-kix_list_8-4>li:before {
                content: ""counter(lst-ctn-kix_list_8-4, lower-latin) ". "
            }

            ol.lst-kix_list_5-0.start {
                counter-reset: lst-ctn-kix_list_5-0 0
            }

            ol.lst-kix_list_8-5.start {
                counter-reset: lst-ctn-kix_list_8-5 0
            }

            ol.lst-kix_list_4-2.start {
                counter-reset: lst-ctn-kix_list_4-2 0
            }

            .lst-kix_list_8-8>li:before {
                content: ""counter(lst-ctn-kix_list_8-8, lower-roman) ". "
            }

            ol.lst-kix_list_2-2 {
                list-style-type: none
            }

            ol.lst-kix_list_2-3 {
                list-style-type: none
            }

            ol.lst-kix_list_2-4 {
                list-style-type: none
            }

            ol.lst-kix_list_7-2.start {
                counter-reset: lst-ctn-kix_list_7-2 0
            }

            ol.lst-kix_list_2-5 {
                list-style-type: none
            }

            .lst-kix_list_4-4>li {
                counter-increment: lst-ctn-kix_list_4-4
            }

            ol.lst-kix_list_2-0 {
                list-style-type: none
            }

            ol.lst-kix_list_2-1 {
                list-style-type: none
            }

            .lst-kix_list_4-8>li:before {
                content: ""counter(lst-ctn-kix_list_4-8, lower-roman) ". "
            }

            .lst-kix_list_4-7>li:before {
                content: ""counter(lst-ctn-kix_list_4-7, lower-latin) ". "
            }

            ol.lst-kix_list_5-6.start {
                counter-reset: lst-ctn-kix_list_5-6 0
            }

            ol.lst-kix_list_4-1.start {
                counter-reset: lst-ctn-kix_list_4-1 0
            }

            .lst-kix_list_7-3>li {
                counter-increment: lst-ctn-kix_list_7-3
            }

            ol.lst-kix_list_4-8.start {
                counter-reset: lst-ctn-kix_list_4-8 0
            }

            .lst-kix_list_8-4>li {
                counter-increment: lst-ctn-kix_list_8-4
            }

            ol.lst-kix_list_3-3.start {
                counter-reset: lst-ctn-kix_list_3-3 0
            }

            ol.lst-kix_list_2-6 {
                list-style-type: none
            }

            ol.lst-kix_list_2-7 {
                list-style-type: none
            }

            ol.lst-kix_list_2-8 {
                list-style-type: none
            }

            ol.lst-kix_list_7-8.start {
                counter-reset: lst-ctn-kix_list_7-8 0
            }

            ol.lst-kix_list_7-1.start {
                counter-reset: lst-ctn-kix_list_7-1 0
            }

            ol.lst-kix_list_8-6.start {
                counter-reset: lst-ctn-kix_list_8-6 0
            }

            .lst-kix_list_3-3>li {
                counter-increment: lst-ctn-kix_list_3-3
            }

            ol.lst-kix_list_5-5.start {
                counter-reset: lst-ctn-kix_list_5-5 0
            }

            ol.lst-kix_list_8-0.start {
                counter-reset: lst-ctn-kix_list_8-0 0
            }

            .lst-kix_list_7-0>li:before {
                content: ""counter(lst-ctn-kix_list_7-0, upper-roman) ". "
            }

            .lst-kix_list_2-2>li {
                counter-increment: lst-ctn-kix_list_2-2
            }

            ol.lst-kix_list_4-7.start {
                counter-reset: lst-ctn-kix_list_4-7 0
            }

            ol.lst-kix_list_5-0 {
                list-style-type: none
            }

            .lst-kix_list_2-6>li:before {
                content: ""counter(lst-ctn-kix_list_2-6, decimal) ". "
            }

            .lst-kix_list_3-7>li {
                counter-increment: lst-ctn-kix_list_3-7
            }

            ol.lst-kix_list_5-1 {
                list-style-type: none
            }

            ol.lst-kix_list_5-2 {
                list-style-type: none
            }

            .lst-kix_list_2-4>li:before {
                content: ""counter(lst-ctn-kix_list_2-4, lower-latin) ". "
            }

            .lst-kix_list_2-8>li:before {
                content: ""counter(lst-ctn-kix_list_2-8, lower-roman) ". "
            }

            .lst-kix_list_7-1>li:before {
                content: ""counter(lst-ctn-kix_list_7-1, lower-latin) ". "
            }

            .lst-kix_list_7-5>li:before {
                content: ""counter(lst-ctn-kix_list_7-5, lower-roman) ". "
            }

            ol.lst-kix_list_5-4.start {
                counter-reset: lst-ctn-kix_list_5-4 0
            }

            .lst-kix_list_7-3>li:before {
                content: ""counter(lst-ctn-kix_list_7-3, decimal) ". "
            }

            ol.lst-kix_list_5-1.start {
                counter-reset: lst-ctn-kix_list_5-1 0
            }

            ol.lst-kix_list_5-7 {
                list-style-type: none
            }

            ol.lst-kix_list_5-8 {
                list-style-type: none
            }

            ol.lst-kix_list_5-3 {
                list-style-type: none
            }

            .lst-kix_list_8-7>li {
                counter-increment: lst-ctn-kix_list_8-7
            }

            ol.lst-kix_list_5-4 {
                list-style-type: none
            }

            .lst-kix_list_1-7>li {
                counter-increment: lst-ctn-kix_list_1-7
            }

            ol.lst-kix_list_3-8.start {
                counter-reset: lst-ctn-kix_list_3-8 0
            }

            ol.lst-kix_list_5-5 {
                list-style-type: none
            }

            ol.lst-kix_list_5-6 {
                list-style-type: none
            }

            .lst-kix_list_7-7>li:before {
                content: ""counter(lst-ctn-kix_list_7-7, lower-latin) ". "
            }

            ol.lst-kix_list_8-1.start {
                counter-reset: lst-ctn-kix_list_8-1 0
            }

            .lst-kix_list_7-5>li {
                counter-increment: lst-ctn-kix_list_7-5
            }

            .lst-kix_list_5-8>li {
                counter-increment: lst-ctn-kix_list_5-8
            }

            .lst-kix_list_4-0>li:before {
                content: ""counter(lst-ctn-kix_list_4-0, decimal) ". "
            }

            .lst-kix_list_3-8>li {
                counter-increment: lst-ctn-kix_list_3-8
            }

            .lst-kix_list_4-6>li {
                counter-increment: lst-ctn-kix_list_4-6
            }

            ol.lst-kix_list_1-7.start {
                counter-reset: lst-ctn-kix_list_1-7 0
            }

            .lst-kix_list_4-4>li:before {
                content: ""counter(lst-ctn-kix_list_4-4, lower-latin) ". "
            }

            ol.lst-kix_list_2-2.start {
                counter-reset: lst-ctn-kix_list_2-2 0
            }

            .lst-kix_list_1-5>li {
                counter-increment: lst-ctn-kix_list_1-5
            }

            .lst-kix_list_4-2>li:before {
                content: ""counter(lst-ctn-kix_list_4-2, lower-roman) ". "
            }

            .lst-kix_list_4-6>li:before {
                content: ""counter(lst-ctn-kix_list_4-6, decimal) ". "
            }

            ol.lst-kix_list_7-0.start {
                counter-reset: lst-ctn-kix_list_7-0 0
            }

            ol.lst-kix_list_4-0 {
                list-style-type: none
            }

            ol.lst-kix_list_4-1 {
                list-style-type: none
            }

            ol.lst-kix_list_4-2 {
                list-style-type: none
            }

            ol.lst-kix_list_4-3 {
                list-style-type: none
            }

            .lst-kix_list_2-4>li {
                counter-increment: lst-ctn-kix_list_2-4
            }

            ol.lst-kix_list_3-6.start {
                counter-reset: lst-ctn-kix_list_3-6 0
            }

            ul.lst-kix_list_6-6 {
                list-style-type: none
            }

            ul.lst-kix_list_6-7 {
                list-style-type: none
            }

            .lst-kix_list_5-3>li {
                counter-increment: lst-ctn-kix_list_5-3
            }

            ul.lst-kix_list_6-4 {
                list-style-type: none
            }

            ul.lst-kix_list_6-5 {
                list-style-type: none
            }

            ul.lst-kix_list_6-8 {
                list-style-type: none
            }

            ol.lst-kix_list_4-8 {
                list-style-type: none
            }

            .lst-kix_list_7-4>li {
                counter-increment: lst-ctn-kix_list_7-4
            }

            .lst-kix_list_1-0>li:before {
                content: ""counter(lst-ctn-kix_list_1-0, decimal) ". "
            }

            ol.lst-kix_list_4-4 {
                list-style-type: none
            }

            ul.lst-kix_list_6-2 {
                list-style-type: none
            }

            ol.lst-kix_list_4-5 {
                list-style-type: none
            }

            ul.lst-kix_list_6-3 {
                list-style-type: none
            }

            .lst-kix_list_1-2>li:before {
                content: ""counter(lst-ctn-kix_list_1-2, lower-roman) ". "
            }

            ol.lst-kix_list_2-0.start {
                counter-reset: lst-ctn-kix_list_2-0 0
            }

            ol.lst-kix_list_4-6 {
                list-style-type: none
            }

            ul.lst-kix_list_6-0 {
                list-style-type: none
            }

            ol.lst-kix_list_4-7 {
                list-style-type: none
            }

            ul.lst-kix_list_6-1 {
                list-style-type: none
            }

            ol.lst-kix_list_8-4.start {
                counter-reset: lst-ctn-kix_list_8-4 0
            }

            .lst-kix_list_1-4>li:before {
                content: ""counter(lst-ctn-kix_list_1-4, lower-latin) ". "
            }

            ol.lst-kix_list_3-5.start {
                counter-reset: lst-ctn-kix_list_3-5 0
            }

            .lst-kix_list_1-0>li {
                counter-increment: lst-ctn-kix_list_1-0
            }

            .lst-kix_list_8-8>li {
                counter-increment: lst-ctn-kix_list_8-8
            }

            .lst-kix_list_1-6>li {
                counter-increment: lst-ctn-kix_list_1-6
            }

            .lst-kix_list_1-6>li:before {
                content: ""counter(lst-ctn-kix_list_1-6, decimal) ". "
            }

            .lst-kix_list_2-0>li:before {
                content: ""counter(lst-ctn-kix_list_2-0, decimal) ". "
            }

            ol.lst-kix_list_2-1.start {
                counter-reset: lst-ctn-kix_list_2-1 0
            }

            ol.lst-kix_list_8-3.start {
                counter-reset: lst-ctn-kix_list_8-3 0
            }

            .lst-kix_list_4-5>li {
                counter-increment: lst-ctn-kix_list_4-5
            }

            .lst-kix_list_1-8>li:before {
                content: ""counter(lst-ctn-kix_list_1-8, lower-roman) ". "
            }

            .lst-kix_list_2-2>li:before {
                content: ""counter(lst-ctn-kix_list_2-2, lower-roman) ". "
            }

            ol.lst-kix_list_5-2.start {
                counter-reset: lst-ctn-kix_list_5-2 0
            }

            .lst-kix_list_8-2>li {
                counter-increment: lst-ctn-kix_list_8-2
            }

            ol {
                margin: 0;
                padding: 0
            }

            table td,
            table th {
                padding: 0
            }

            .c4 {
                margin-left: 36pt;
                padding-top: 0pt;
                padding-left: 0pt;
                padding-bottom: 0pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: justify
            }

            .c2 {
                padding-top: 0pt;
                text-indent: -36pt;
                padding-bottom: 0pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: justify;
                height: 11pt
            }

            .c15 {
                padding-top: 0pt;
                text-indent: -36pt;
                padding-bottom: 0pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: left;
                height: 11pt
            }

            .c28 {
                padding-top: 0pt;
                padding-left: 18pt;
                padding-bottom: 0pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: center
            }

            .c10 {
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: justify;
                height: 11pt
            }

            .c23 {
                padding-top: 0pt;
                padding-bottom: 8pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: left;
                height: 11pt
            }

            .c1 {
                color: #000000;
                font-weight: 400;
                text-decoration: none;
                vertical-align: baseline;
                font-size: 12pt;
                font-family: "Arial Narrow";
                font-style: normal
            }

            .c6 {
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.0;
                orphans: 2;
                widows: 2;
                text-align: justify
            }

            .c14 {
                padding-top: 0pt;
                padding-bottom: 8pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: center
            }

            .c11 {
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: justify
            }

            .c16 {
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: left
            }

            .c5 {
                padding-top: 0pt;
                padding-bottom: 8pt;
                line-height: 1.0791666666666666;
                orphans: 2;
                widows: 2;
                text-align: justify
            }

            .c3 {
                font-size: 12pt;
                font-family: "Arial Narrow";
                color: #000000;
                font-weight: 400
            }

            .c17 {
                color: #7a7c84;
                font-weight: 400;
                font-size: 11pt;
                font-family: "Arial"
            }

            .c0 {
                font-size: 12pt;
                font-family: "Arial Narrow";
                font-weight: 700
            }

            .c29 {
                -webkit-text-decoration-skip: none;
                text-decoration: underline;
                text-decoration-skip-ink: none
            }

            .c21 {
                font-weight: 400;
                font-size: 12pt;
                font-family: "Calibri"
            }

            .c7 {
                font-size: 12pt;
                font-family: "Arial Narrow";
                font-weight: 400
            }

            .c19 {
                font-weight: 400;
                font-size: 11pt;
                font-family: "Calibri"
            }

            .c13 {
                text-decoration: none;
                vertical-align: baseline;
                font-style: normal
            }

            .c27 {
                font-size: 11.5pt;
                font-family: "Arial Narrow";
                font-weight: 400
            }

            .c30 {
                margin-left: -31.5pt;
                text-indent: 31.5pt
            }

            .c26 {
                margin-left: 32.8pt;
                padding-left: 3.2pt
            }

            .c20 {
                max-width: 441.9pt;
                padding: 70.8pt 85pt 70.8pt 85pt
            }

            .c18 {
                padding: 0;
                margin: 0
            }

            .c12 {
                margin-left: 36pt;
                padding-left: 0pt
            }

            .c22 {
                margin-left: 36pt
            }

            .c9 {
                background-color: #ffffff
            }

            .c25 {
                margin-left: 54pt
            }

            .c31 {
                font-style: italic
            }

            .c24 {
                height: 11pt
            }

            .c8 {
                color: #000000
            }

            .title {
                padding-top: 24pt;
                color: #000000;
                font-weight: 700;
                font-size: 36pt;
                padding-bottom: 6pt;
                font-family: "Calibri";
                line-height: 1.0791666666666666;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left
            }

            .subtitle {
                padding-top: 18pt;
                color: #666666;
                font-size: 24pt;
                padding-bottom: 4pt;
                font-family: "Georgia";
                line-height: 1.0791666666666666;
                page-break-after: avoid;
                font-style: italic;
                orphans: 2;
                widows: 2;
                text-align: left
            }

            li {
                color: #000000;
                font-size: 11pt;
                font-family: "Calibri"
            }

            p {
                margin: 0;
                color: #000000;
                font-size: 11pt;
                font-family: "Calibri"
            }

            h1 {
                padding-top: 24pt;
                color: #000000;
                font-weight: 700;
                font-size: 24pt;
                padding-bottom: 6pt;
                font-family: "Calibri";
                line-height: 1.0791666666666666;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left
            }

            h2 {
                padding-top: 18pt;
                color: #000000;
                font-weight: 700;
                font-size: 18pt;
                padding-bottom: 4pt;
                font-family: "Calibri";
                line-height: 1.0791666666666666;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left
            }

            h3 {
                padding-top: 14pt;
                color: #000000;
                font-weight: 700;
                font-size: 14pt;
                padding-bottom: 4pt;
                font-family: "Calibri";
                line-height: 1.0791666666666666;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left
            }

            h4 {
                padding-top: 12pt;
                color: #000000;
                font-weight: 700;
                font-size: 12pt;
                padding-bottom: 2pt;
                font-family: "Calibri";
                line-height: 1.0791666666666666;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left
            }

            h5 {
                padding-top: 11pt;
                color: #000000;
                font-weight: 700;
                font-size: 11pt;
                padding-bottom: 2pt;
                font-family: "Calibri";
                line-height: 1.0791666666666666;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left
            }

            h6 {
                padding-top: 10pt;
                color: #000000;
                font-weight: 700;
                font-size: 10pt;
                padding-bottom: 2pt;
                font-family: "Calibri";
                line-height: 1.0791666666666666;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left
            }
        </style>
    <p class="c14"><span class="c0 c13 c8">T&Eacute;RMINOS Y CONDICIONES PARA USAR LA PLATAFORMA LINKIU</span></p>
    <p class="c14 c24"><span class="c0 c13 c8"></span></p>
    <p class="c5" id="h.gjdgxs"><span class="c0">NEURON GROUP S.A.S.,</span><span class="c7">&nbsp;domiciliada en la ciudad
            de Bogot&aacute; D.C. (Cundinamarca) e identificada con NIT. 901395477-2, sociedad </span><span
            class="c3">constituida bajo las leyes de la Rep&uacute;blica de Colombia e inscrita en el registro mercantil de
            la C&aacute;mara de </span><span class="c7">Comercio de Bogot&aacute; D.C., se denominar&aacute; en adelante por
            su propia marca </span><span class="c0">&ldquo;LINKIU</span><span class="c7">&rdquo; o como &ldquo;</span><span
            class="c0">EL OPERADOR&rdquo;.</span></p>
    <p class="c5"><span class="c0">EL CONSUMIDOR, </span><span class="c7">persona natural</span><span class="c0">,
        </span><span class="c7">en la Rep&uacute;blica de Colombia, quien va adquirir un producto o servicio por medio de la
            plataforma </span><span class="c0">LINKIU</span><span class="c7">, se denominar&aacute; en adelante </span><span
            class="c0">&ldquo;EL CONSUMIDOR&rdquo;</span><span class="c7">&nbsp;o &ldquo;</span><span class="c0">EL
            CLIENTE&rdquo;.</span><span class="c1">&nbsp; </span></p>
    <p class="c5"><span class="c7">EL CONSUMIDOR acuerda aceptar los siguientes T&eacute;rminos y Condiciones para usar la
            plataforma denominada &ldquo;</span><span class="c0">LINKIU</span><span class="c1">&rdquo; que se regir&aacute;
            por las cl&aacute;usulas que se enuncian a continuaci&oacute;n, previo a las siguientes </span></p>
    <ol class="c18 lst-kix_list_7-0 start" start="1">
        <li class="c22 c28"><span class="c0 c13 c8">CONSIDERACIONES </span></li>
    </ol>
    <p class="c15 c25"><span class="c0 c13 c8"></span></p>
    <ol class="c18 lst-kix_list_8-0 start" start="1">
        <li class="c11 c26"><span class="c0">LINKIU</span><span class="c3">&nbsp;se muestra al p&uacute;blico como una
                plataforma digital en la que ofrece y </span><span class="c7">promociona </span><span class="c1">diferentes
                productos y servicios de Empresas, quien en adelante se denominar&aacute; &ldquo;LA EMPRESA&rdquo; o
                &ldquo;EL ESTABLECIMIENTO&rdquo;.</span></li>
    </ol>
    <p class="c10 c22"><span class="c1"></span></p>
    <ol class="c18 lst-kix_list_8-0" start="2">
        <li class="c11 c26"><span class="c3">La visualizaci&oacute;n en la plataforma de los Consumidores sobre los
                productos y servicios que ofrece La Empresa permite que los consumidores soliciten la entrega del producto o
                prestaci&oacute;n del servicio directamente al Establecimiento, siendo </span><span
                class="c0">LINKIU</span><span class="c1">&nbsp;el medio exclusivo para que CLIENTE y EMPRESA generen una
                vinculaci&oacute;n contractual directa, cuyo contacto ser&aacute; inmediato una vez el Cliente compre el
                producto o solicite el servicio requerido. </span></li>
    </ol>
    <p class="c10 c22"><span class="c1"></span></p>
    <ol class="c18 lst-kix_list_8-0" start="3">
        <li class="c11 c26"><span class="c1">Una vez sea notificado a la Empresa la solicitud y haya aceptado la misma, ella
                debe de encargarse que por sus propios medios sea prestado el servicio o entregado el producto en
                &oacute;ptimas condiciones.</span></li>
    </ol>
    <p class="c10"><span class="c1"></span></p>
    <p class="c5"><span class="c7">Por lo anteriormente planteado El Operador otorga autorizaci&oacute;n de uso de la
            plataforma y EL CONSUMIDOR manifiesta la aceptaci&oacute;n de estos T&eacute;rminos y Condiciones para usar la
            plataforma </span><span class="c0">LINKIU</span><span class="c1">, el cual se regir&aacute; por las siguientes
        </span></p>
    <ol class="c18 lst-kix_list_7-0" start="2">
        <li class="c28 c22"><span class="c0 c13 c8">CLAUSULAS</span></li>
    </ol>
    <p class="c15 c25"><span class="c0 c13 c8"></span></p>
    <p class="c11"><span class="c0 c8">PRIMERA. &ndash; REGISTRO: </span><span class="c3">Para usar la plataforma
            denominada</span><span class="c0 c8">&nbsp;&ldquo;LINKIU&rdquo; </span><span class="c3">a trav&eacute;s de las
            tiendas virtuales como app store, google play o play store debe descargar la APP LINKIU, para los efectos de
            estos T&eacute;rminos y Condiciones ser&aacute; llamada &ldquo;LA PLATAFORMA&rdquo;, la p&aacute;gina web
        </span><span class="c7 c29">www.linkiu.app</span><span class="c7">&nbsp;</span><span class="c3">servir&aacute; como
            medio de informaci&oacute;n acerca de los productos y/o servicios ofrecidos. Cuando se haya descargado la
            plataforma deber&aacute; crear una cuenta, donde se va a solicitar datos como nombre completo, c&eacute;dula,
            n&uacute;mero telef&oacute;nico y direcci&oacute;n de correo electr&oacute;nico, datos solicitados con el fin
            principal de identificaci&oacute;n y conocimiento de prestaci&oacute;n del servicio por parte de La Empresa, una
            vez ingresados los datos y le d&eacute; clic en &ldquo;Aceptar T&eacute;rminos y Condiciones para usar la
            plataforma &ldquo;</span><span class="c0">LINKIU</span><span class="c1">&rdquo; y &ldquo;Aceptar la
            Pol&iacute;tica de Tratamiento de Datos Personales&rdquo;, tendr&aacute; acceso a la plataforma virtual.</span>
    </p>
    <p class="c2"><span class="c1"></span></p>
    <p class="c11"><span class="c0 c8">Par&aacute;grafo 1:</span><span class="c1">&nbsp;La cuenta de cada consumidor es
            personal e intransferible, por lo tanto, los Consumidores no est&aacute;n facultados para ceder sus datos ni el
            uso de la plataforma a terceros, situaci&oacute;n que en caso de presentarse llevar&aacute; a cabo la
            cancelaci&oacute;n de la cuenta.</span></p>
    <p class="c2"><span class="c1"></span></p>
    <p class="c11"><span class="c0 c8">Par&aacute;grafo 2:</span><span class="c3">&nbsp;En caso de olvidar su usuario o
            contrase&ntilde;a o usurpaci&oacute;n de estos, El Consumidor se obliga a informar al Operador a trav&eacute;s
            de la opci&oacute;n &ldquo;</span><span class="c7">olvid&eacute;</span><span class="c1">&nbsp;mi
            contrase&ntilde;a&rdquo;, para de este modo lograr restablecerla. &nbsp; </span></p>
    <p class="c2"><span class="c1"></span></p>
    <p class="c11"><span class="c0 c8">Par&aacute;grafo 3: </span><span class="c3">El Consumidor autoriza al Operador para
            el uso de cookies en toda actividad de uso que realice de la plataforma. </span></p>
    <p class="c2"><span class="c0 c13 c9 c8"></span></p>
    <p class="c11"><span class="c0 c9 c8">SEGUNDA. - &nbsp;DESCRIPCI&Oacute;N DE PRODUCTOS/SERVICIOS: </span><span
            class="c3 c9">El Operador</span><span class="c0 c9 c8">&nbsp;</span><span class="c1 c9">cuenta con una
            Plataforma virtual en la que exhibe y ofrece diferentes productos y servicios de Empresas, los cuales
            est&aacute;n divididos en categor&iacute;as de productos m&eacute;dicos como tapabocas, guantes, caretas,
            ventiladores para asistencia respiratoria, entre otras. El Consumidor de acuerdo a su necesidad puede ingresar a
            las categor&iacute;as y ver las listas de los productos y/o servicios de las Empresas, a la vez dando clic en
            cada una de ellos se podr&aacute;n evidenciar la descripci&oacute;n de los productos y/o servicios
            espec&iacute;ficos, su respectivo valor, especificaciones como cantidad, calidad y sus t&eacute;rminos y
            condiciones en caso de tenerlas; Al seleccionar el requerimiento, la plataforma pedir&aacute; se confirme la
            direcci&oacute;n de entrega, luego se debe dar clic en comprar para generar la orden de compra, en donde se
            visualizar&aacute; la siguiente informaci&oacute;n: </span></p>
    <ul class="c18 lst-kix_list_6-0 start">
        <li class="c4"><span class="c3 c9">Direcci&oacute;n de env&iacute;o, en donde se puede especificar si es casa,
                apartamento, oficina, etc.</span></li>
        <li class="c4"><span class="c3 c9">Selecci&oacute;n de pago (d&eacute;bito o cr&eacute;dito); </span></li>
        <li class="c4"><span class="c3 c9">Valor del producto y el valor de env&iacute;o</span></li>
        <li class="c5 c12"><span class="c3 c9">Valor total que deber&aacute; cancelar. </span></li>
    </ul>
    <p class="c5"><span class="c1 c9">Una vez lleno esos datos, dar&aacute; clic en comprar o cancelar compra, caso tal en
            que haya decidido comprar el producto o solicitar el servicio, le ser&aacute; notificado a La Empresa el
            requerimiento del Consumidor, quien deber&aacute; aceptar la orden de compra y actualizar el estado de la orden
            a &ldquo;recibida&rdquo;. &nbsp;Ahora bien, &nbsp;una vez la orden sea recibida, se le informar&aacute; y
            notificar&aacute; al Consumidor sobre actualizaciones en la &nbsp;fecha de entrega del producto o
            finalizaci&oacute;n del servicio, confirmaci&oacute;n de pago y por &uacute;ltimo, El Consumidor podr&aacute;
            calificar el producto y/o el servicio de cero a cinco estrellas, donde cero a uno se considera como un muy mal
            producto y/o servicio, de dos a tres como regular, cuatro bueno y cinco como excelente producto y/o servicio,
            esto dependiendo de la satisfacci&oacute;n del Consumidor. &nbsp;</span></p>
    <p class="c5"><span class="c0 c9">Par&aacute;grafo 1:</span><span class="c1 c9">&nbsp;La direcci&oacute;n de solicitud
            debe de encontrarse dentro de la cobertura de entrega y/o prestaci&oacute;n de servicio, en caso de no
            encontrarse habilitada se notificar&aacute; el rechazo de la misma. </span></p>
    <p class="c11"><span class="c0 c9 c8">Par&aacute;grafo 2: </span><span class="c3 c9">Las Empresas tienen la potestad de
            ACTIVAR o INACTIVAR sus productos ante el p&uacute;blico. Es por ello que, cuando el producto est&aacute;
            inactivo, no aparecer&aacute; en ese momento en la plataforma virtual.</span><span
            class="c0 c13 c9 c8">&nbsp;</span></p>
    <p class="c11"><span class="c0 c9 c8">Par&aacute;grafo 3: </span><span class="c3 c9">El Consumidor no podr&aacute;
            cancelar el servicio o revertir la compra una vez la transacci&oacute;n sea aprobada por el valor del producto
            y/o servicio. </span></p>
    <p class="c10"><span class="c1"></span></p>
    <p class="c11"><span class="c3">&nbsp;</span><span class="c0 c8">TERCERA. &ndash; PAGOS Y FACTURACI&Oacute;N:
        </span><span class="c1">La plataforma de los Consumidores cuenta con la opci&oacute;n de pago virtual con tarjeta de
            d&eacute;bito y/o cr&eacute;dito.</span></p>
    <p class="c10"><span class="c1"></span></p>
    <p class="c11"><span class="c3">Con respecto a la facturaci&oacute;n El Operador emite el recibo de la orden de compra
            electr&oacute;nicamente una vez la transacci&oacute;n sea aprobada. Si El Consumidor requiere especificar
            mayores detalles en la Factura debe de indicarlo a La Empresa. A trav&eacute;s de su n&uacute;mero
            telef&oacute;nico o WhatsApp, El Consumidor puede llamar y comunicar qu&eacute; requiere de la misma para que
            pueda ser entregada por </span><span class="c0">LINKIU</span></p>
    <p class="c10"><span class="c1"></span></p>
    <p class="c11"><span class="c0 c8">Par&aacute;grafo</span><span class="c1">: El detalle de todos los productos y/o
            servicios solicitados se podr&aacute;n evidenciar en el icono &ldquo;ver m&aacute;s&rdquo;. </span></p>
    <p class="c10"><span class="c0 c13 c8"></span></p>
    <p class="c11"><span class="c0 c8">CUARTA. - RELACI&Oacute;N JUR&Iacute;DICA: </span><span class="c0">LINKIU</span><span
            class="c1">&nbsp;media como una plataforma virtual que permite se genere una relaci&oacute;n contractual directa
            entre la Empresa y El Consumidor, sea de compraventa, de prestaci&oacute;n de servicios o cualquier otra
            relaci&oacute;n contractual l&iacute;cita sin mediar en ning&uacute;n momento alg&uacute;n tipo de
            representaci&oacute;n, de agencia, de mandato o de comisi&oacute;n, de parte del Operador, tampoco es proveedor,
            productor, expendedor, agente, distribuidor, ni comercializador de los productos &nbsp;y/o servicios ofrecidos
            en la Plataforma. </span></p>
    <p class="c2"><span class="c1"></span></p>
    <p class="c11"><span class="c3">Es claro para El Consumidor que no existe ning&uacute;n v&iacute;nculo laboral entre El
            Operador y Empresa o entre El Operador y </span><span class="c0">LINKIU</span><span class="c1">. Para El
            Consumidor su &uacute;nica vinculaci&oacute;n contractual ser&aacute; directamente con La Empresa. </span></p>
    <p class="c11"><span class="c1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <p class="c11"><span class="c0 c8">QUINTA. &ndash; ALCANCE DE LA INFORMACI&Oacute;N: </span><span
            class="c0">LINKIU</span><span class="c3">&nbsp;ha dispuesto sus mejores esfuerzos para que la informaci&oacute;n
            y los materiales aqu&iacute; contenidos, incluyendo los textos, gr&aacute;ficos, enlaces y dem&aacute;s
            &iacute;tems, sean id&oacute;neos para la correcta operaci&oacute;n por parte del cliente. Sin embargo, El
            Operador no asumir&aacute; pagos por concepto de da&ntilde;os directos, indirectos, </span><span
            class="c7">incidentales</span><span class="c3">, consecuenciales, punitivos, incluyendo p&eacute;rdidas y lucro
            cesante que subjetivamente se imputen a (i) cualquier informaci&oacute;n transmitida v&iacute;a la P&aacute;gina
            Web de </span><span class="c0">LINKIU</span><span class="c3">&nbsp;por parte de terceros o (ii) el
            funcionamiento o no de cualquier plataforma o insumo web de </span><span class="c0">LINKIU</span><span
            class="c3">&nbsp;quien no asumir&aacute; monto alguno por da&ntilde;os o p&eacute;rdidas de equipo o software
            que se imputen subjetivamente a virus, defectos o mal funcionamiento relativos a accesos o utilizaci&oacute;n
            del sitio web no imputables a </span><span class="c0">LINKIU</span><span class="c1">.</span></p>
    <p class="c10"><span class="c0 c13 c8"></span></p>
    <p class="c11"><span class="c0 c8">SEXTA. &ndash; PETICIONES, QUEJAS Y RECLAMOS: </span><span class="c1">Dando clic al
            bot&oacute;n &ldquo;ayuda&rdquo;, El Consumidor podr&aacute; radicar sus peticiones, quejas o reclamos siempre y
            cuando las mismas se realicen por defectos o por calidad del producto y/o servicio, tal que El Operador
            podr&aacute; supervisar el rendimiento de los productos y/o servicios de La Empresa para permanencia de
            &eacute;sta en La Plataforma virtual. </span></p>
    <p class="c10"><span class="c1"></span></p>
    <p class="c11"><span class="c1">El Consumidor podr&aacute; tambi&eacute;n notificar v&iacute;a correo a
            appdigitalco@gmail.com haciendo su solicitud formal de reclamaci&oacute;n y/o devoluci&oacute;n con la
            indicaci&oacute;n precisa de todos los datos de compra y aportando todas las pruebas de la
            transacci&oacute;n.</span></p>
    <p class="c11"><span class="c1">En un promedio de 2 a 3 d&iacute;as h&aacute;biles se proceder&aacute; a informar al
            consumidor si su reclamo es aceptado o rechazado. El consumidor deber&aacute; efectuar su reclamaci&oacute;n
            dentro de los 5 d&iacute;as siguientes a la recepci&oacute;n del producto para ejercer el derecho de retracto
            del que trata el Art&iacute;culo 47 de la Ley 1480 de 2011, por lo que fuera de este t&eacute;rmino NO se
            aceptar&aacute; ninguna reclamaci&oacute;n.</span></p>
    <p class="c11"><span class="c3">Los pagos efectuados mediante pasarela de pagos, se </span><span
            class="c7">mantendr&aacute;n</span><span class="c1">&nbsp;congelados los fondos recaudados a trav&eacute;s de la
            respectiva pasarela de pagos y se proceder&aacute; con la misma a reintegrar los recursos al consumidor una vez
            sea aceptada su reclamaci&oacute;n y devoluci&oacute;n.</span></p>
    <p class="c10"><span class="c1"></span></p>
    <p class="c11"><span class="c1">La Empresa se compromete a responder de manera oportuna cada petici&oacute;n, queja o
            reclamo del Consumidor directa y oportunamente. </span></p>
    <p class="c10"><span class="c1"></span></p>
    <p class="c11"><span class="c3">&nbsp;</span><span class="c0 c13 c8">S&Eacute;PTIMA. &ndash; OBLIGACIONES DEL
            CONSUMIDOR: &nbsp;</span></p>
    <p class="c2"><span class="c0 c13 c8"></span></p>
    <ol class="c18 lst-kix_list_1-0 start" start="1">
        <li class="c4"><span class="c3 c9">Suministrar informaci&oacute;n veraz y fidedigna al momento de crear su Cuenta de
                Usuario;&nbsp;</span></li>
        <li class="c4"><span class="c3">No divulgar, ni ceder su nombre de usuario y contrase&ntilde;a de acceso a la
                plataforma </span><span class="c0">LINKIU</span><span class="c3">, ni permitir su uso a terceros</span><span
                class="c0 c8">; </span></li>
        <li class="c4"><span class="c3 c9">Notificar a </span><span class="c0">LINKIU</span><span class="c3 c9">&nbsp;de
                cualquier uso no autorizado de su Registro, nombre de usuario o contrase&ntilde;a de acceso y/o </span><span
                class="c7 c9">p&eacute;rdida</span><span class="c3 c9">&nbsp;de los mismos; </span></li>
        <li class="c4"><span class="c1">Abstenerse de (a) realizar actividades de ingenier&iacute;a inversa, desmonte,
                descompile o intente revelar el c&oacute;digo fuente o la estructura, arquitectura u organizaci&oacute;n de
                todas o alguna de las partes de los servicios (b) rente, alquile, revenda, distribuya o utilice los
                servicios para fines comerciales, sin autorizaci&oacute;n de sus titulares. (c) haga parte en cualquier
                actividad o tarea que interfiera o interrumpa los servicios.</span></li>
        <li class="c4"><span class="c1">Mantener sus datos actualizados y confirmados como direcci&oacute;n, tel&eacute;fono
                para la entrega de los productos;</span></li>
        <li class="c4"><span class="c1">Cerciorarse antes de confirmar la solicitud del servicio que todos los datos y
                productos y/o servicios seleccionados correspondan con lo que El Consumidor requiere;</span></li>
        <li class="c4"><span class="c3">Pagar oportunamente a </span><span class="c0">LINKIU</span><span class="c1">&nbsp;la
                contraprestaci&oacute;n econ&oacute;mica por el producto y/o servicio prestado; </span></li>
        <li class="c4"><span class="c1">Para el tr&aacute;mite de pago virtual, garantizar que sus datos asociados al medio
                de pago correspondiente, como n&uacute;mero de tarjeta, entre otros, son de su propiedad y que tiene los
                fondos suficientes para procesar el pago; </span></li>
        <li class="c4"><span class="c1">Realizar cualquier queja, petici&oacute;n o reclamo a La Empresa de manera oportuna,
                es decir, en el momento en el cual se presente la inconformidad o falencia;</span></li>
        <li class="c4"><span class="c1">Leer los T&eacute;rminos y Condiciones oportunamente al momento de usar la
                plataforma virtual, para evitar futuras inconformidades;</span></li>
        <li class="c4"><span class="c3">Abstenerse de usar la propiedad intelectual perteneciente a </span><span
                class="c0">LINKIU</span><span class="c1">&nbsp;sin la debida autorizaci&oacute;n del Operador;</span></li>
        <li class="c5 c12"><span class="c3">Obrar con lealtad y honradez en el uso de la plataforma </span><span
                class="c0">LINKIU</span><span class="c1">. </span></li>
    </ol>
    <p class="c5 c24"><span class="c1"></span></p>
    <p class="c5 c30"><span class="c0">OCTAVA. &ndash; OBLIGACIONES DE LA EMPRESA:</span></p>
    <ol class="c18 lst-kix_list_2-0 start" start="1">
        <li class="c4"><span class="c3">Mantener informaci&oacute;n actualizada, cierta, suficiente, clara sobre los
                productos y/o servicios que </span><span class="c7">ofrecen</span><span class="c1">, con sus debidas
                especificaciones si ha de tenerlas como calidad, cantidad, caracter&iacute;sticas, con el fin de que Los
                Clientes tengan exactitud de lo que van a adquirir; </span></li>
        <li class="c4"><span class="c1">Incluir el precio en los productos y/o servicios ofrecidos con valor por
                env&iacute;o o cualquier posible recargo por hora de entrega, impuesto u otro aplicable, valores que
                aparecer&aacute;n como saldo total sin especificaci&oacute;n;</span></li>
        <li class="c4"><span class="c1">Entregar y/o prestar el servicio al Cliente oportunamente y en los t&eacute;rminos
                definidos previamente a la aceptaci&oacute;n de la solicitud realizada por El Consumidor; </span></li>
        <li class="c4"><span class="c1">Responder las peticiones, quejas o reclamos que realicen Los Clientes, la cual
                ser&aacute; reenviada directamente a La Empresa;</span></li>
        <li class="c4"><span class="c1">Brindar siempre un trato excelente a los Clientes, haciendo todo lo posible para que
                se sientan satisfechos por la prestaci&oacute;n del servicio;</span></li>
        <li class="c5 c12"><span class="c1">Obrar con lealtad y honradez frente al Consumidor.</span></li>
    </ol>
    <p class="c16"><span class="c0 c13 c8">NOVENA. &ndash; OBLIGACIONES DEL OPERADOR:</span></p>
    <p class="c15"><span class="c1"></span></p>
    <ol class="c18 lst-kix_list_3-0 start" start="1">
        <li class="c4"><span class="c3">Proveer la interfaz e infraestructura virtual al Consumidor que le permita
                visualizar los productos y/o servicios ofrecidos de Las Empresas, permitiendo que puedan hacer pedidos
                directamente al Establecimiento;</span></li>
        <li class="c4"><span class="c3">Informar al Consumidor sobre las modificaciones de uso que se genere en la
                plataforma </span><span class="c0">LINKIU</span><span class="c3">;</span></li>
        <li class="c4"><span class="c3">Previo a la confirmaci&oacute;n de comprar un producto o solicitar un servicio,
                presentar al Consumidor un resumen de todos los productos y/o servicios seleccionados, la cual ser&aacute;
                una descripci&oacute;n de cada uno de ellos, como precio individual por unidad, n&uacute;mero de unidades,
                precio total de la transacci&oacute;n;</span></li>
        <li class="c4"><span class="c3">Disponer en la plataforma la opci&oacute;n de peticiones, quejas y reclamos;</span>
        </li>
        <li class="c4"><span class="c3 c9">Proteger toda la informaci&oacute;n y registro relacionado del Consumidor la cual
                se albergar&aacute; en servidores de terceros y esta </span><span class="c7 c9">viajar&aacute;</span><span
                class="c3 c9">&nbsp;segura mediante un certificado SSL (Secure Sockets Layer o capa de conexi&oacute;n
                segura), es un est&aacute;ndar de seguridad global que permite la transferencia de datos cifrados entre un
                navegador y un servidor web, espec&iacute;ficamente Amazon en la Regi&oacute;n Am&eacute;rica del Sur
                (S&atilde;o Paulo) y varias zonas de disponibilidad y centros de datos, que cumplen con los m&aacute;s altos
                est&aacute;ndares de seguridad e idoneidad;</span></li>
        <li class="c5 c12"><span class="c3 c9">Obrar con lealtad y honradez frente al Consumidor.</span></li>
    </ol>
    <p class="c5"><span class="c0">D&Eacute;CIMA. &ndash; RESPONSABILIDAD: </span><span class="c7">&nbsp;El Operador
            act&uacute;a como una plataforma virtual que permite que La Empresa ofrezca sus productos y/o servicios, toda la
            publicidad, nombres, marcas, lemas y dem&aacute;s aparecen en la plataforma porque se encuentra facultado para
            hacerlo en virtud de las decisiones 351 de 1993 y 486 de 2000 de la Comunidad Andina de Naciones y la ley 23 de
            1982. </span></p>
    <p class="c5"><span class="c1">El Operador no garantiza o compromete su responsabilidad frente a lo ofrecido por La
            Empresa ya que la plataforma es un medio de visualizaci&oacute;n y contrataci&oacute;n directa entre EMPRESA Y
            CLIENTE, Por lo que es plena responsabilidad del Consumidor verificar los t&eacute;rminos y condiciones de los
            productos y/o servicios ofrecidos por La Empresa. </span></p>
    <p class="c5"><span class="c7">Es por ello que, El Consumidor entiende que El Operador no responder&aacute; por
            ning&uacute;n da&ntilde;o o perjuicio causado por La Empresa. </span><span class="c0">LINKIU</span><span
            class="c7">&nbsp;actuar&aacute; de buena fe ante toda reclamaci&oacute;n del Consumidor al enviar tal reclamo a
            La Empresa y supervisar la soluci&oacute;n que la Empresa brinde al Consumidor. </span><span
            class="c0 c8 c13">&nbsp;</span></p>
    <p class="c5"><span class="c7">El Consumidor entiende que El Operador es responsable de los Plataformas web externos o
            recursos vinculados o referenciados en el Plataforma. </span><span class="c0">LINKIU</span><span
            class="c7">&nbsp;no respalda ni es responsable de ning&uacute;n contenido, publicidad, productos u otros
            materiales disponibles en dichas Plataformas o recursos. El Consumidor acepta que </span><span
            class="c0">LINKIU</span><span class="c7">&nbsp;no ser&aacute; responsable, directa o indirectamente, por
            cualquier da&ntilde;o o p&eacute;rdida causada por el uso de cualquier contenido, bienes o servicios disponibles
            en Plataformas web o recursos externos a </span><span class="c0">LINKIU</span><span class="c1">.</span></p>
    <p class="c5"><span class="c0">D&Eacute;CIMA PRIMERA. &ndash; INDEMNIDAD: </span><span class="c7">&nbsp;Los clientes
            aceptan mantener indemne y libre de responsabilidades a </span><span class="c0">LINKIU</span><span class="c1">,
            y a cada uno de sus oficiales, directores, agentes, socios de marca compartida, otros socios y empleados
            respectivos de todos los da&ntilde;os, ya sean directos o indirectos, derivados o de otro tipo, p&eacute;rdidas,
            responsabilidades, costes y gastos (incluidas, sin limitaci&oacute;n, las tasas de contabilidad y abogados
            razonables), que resulten de cualquier reclamaci&oacute;n, demanda, acci&oacute;n, procedimiento (ya sea ante un
            &aacute;rbitro, tribunal, mediador u otro) o investigaci&oacute;n realizada por cualquier tercera parte (cada
            una es una &ldquo;Reclamaci&oacute;n&rdquo;) debida o que surja de su contenido, &nbsp;su infracci&oacute;n de
            estos t&eacute;rminos y condiciones; y/o su infracci&oacute;n de cualquier derecho de otra persona.</span></p>
    <p class="c5"><span class="c0">D&Eacute;CIMA SEGUNDA. &ndash; USO DE MARCAS COMERCIALES: </span><span
            class="c7">&nbsp;La oferta de los Servicios no debe constituir la concesi&oacute;n, por implicaci&oacute;n,
            impedimento o de otro modo, de licencia o derecho algunos para usar cualquier marca comercial mostrada en
            relaci&oacute;n con los servicios sin el consentimiento previo por escrito de </span><span
            class="c0">LINKIU</span><span class="c7">&nbsp;espec&iacute;fico para cada uso. Las marcas comerciales no pueden
            utilizarse para menospreciar a </span><span class="c0">LINKIU</span><span class="c7">, a cualquier tercera parte
            o a los productos o servicios de </span><span class="c0">LINKIU</span><span class="c7">&nbsp;o un tercero, o de
            cualquier forma (a juicio exclusivo de </span><span class="c0">LINKIU</span><span class="c7">) que pueda
            da&ntilde;ar cualquier buena fe en las marcas comerciales. El uso de cualquier marca comercial como parte de un
            enlace o desde cualquier Plataforma est&aacute; prohibido salvo que </span><span class="c0">LINKIU</span><span
            class="c1">&nbsp;apruebe el establecimiento de dicho enlace mediante consentimiento previo por escrito para cada
            enlace. Toda la buena fe generada a partir del uso de cualquier marca comercial del Operador, redundar&aacute;
            en beneficio del Operador.</span></p>
    <p class="c5"><span class="c0">D&Eacute;CIMA TERCERA. - PROPIEDAD INTELECTUAL: </span><span class="c1">NEURON GROUP
            S.A.S., es el titular de todos los derechos que en virtud de las leyes de propiedad intelectual se le reconocen
            al creador de una obra. El contenido como contrato, texto, im&aacute;genes, logos, botones, iconos, marcas
            comerciales y otros, son propiedad intelectual del Operador (a excepci&oacute;n de material propiedad de
            terceros previamente autorizados y de Plataformas web de im&aacute;genes de contenido gratuito y libres de
            derechos de autor) y est&aacute;n protegidos bajo los derechos de autor, marcas y en general derechos de
            propiedad intelectual. </span></p>
    <p class="c5"><span class="c7">Todo el material inform&aacute;tico, gr&aacute;fico, publicitario, fotogr&aacute;fico, de
            multimedia, audiovisual y de dise&ntilde;o, as&iacute; como todos los contenidos, textos y bases de datos
            puestos a su disposici&oacute;n en las respectivas plataformas, (salvo el material de terceros usado previamente
            y con la debida autorizaci&oacute;n) est&aacute;n protegidos por derechos de autor y/o propiedad industrial cuyo
            titular es NEURON GROUP S.A.S., o en algunos casos, de terceros que han autorizado su uso o explotaci&oacute;n.
            Igualmente, el uso en la Plataforma </span><span class="c0">LINKIU</span><span class="c1">&nbsp;de algunos
            materiales de propiedad de terceros se encuentra expresamente autorizado por la ley o por dichos terceros. Todos
            los contenidos est&aacute;n protegidos por las normas sobre derecho de autor y por todas las normas nacionales e
            internacionales que le sean aplicables.</span></p>
    <p class="c5"><span class="c0">D&Eacute;CIMA CUARTA. - CANCELACI&Oacute;N Y/O SUSPENSI&Oacute;N DE LA CUENTA:
        </span><span class="c7">Podr&aacute; ser cancelado o suspendido</span><span class="c0">&nbsp;</span><span
            class="c1">el uso de la plataforma por algunas de las siguientes causas:</span></p>
    <ol class="c18 lst-kix_list_4-0 start" start="1">
        <li class="c16 c12"><span class="c1">Porque El Operador en cualquier momento suspenda o cancele las cuentas de
                manera unilateral;</span></li>
        <li class="c12 c16"><span class="c1">Por el incumplimiento de las obligaciones contenidas en este documento;</span>
        </li>
        <li class="c16 c12"><span class="c3">Por realizar acciones que est&eacute;n en contra de las leyes colombianas, la
                buena costumbre y buena fe de </span><span class="c0">LINKIU</span><span class="c1">&nbsp;y/o terceros o por
                detrimento de la plataforma;</span></li>
        <li class="c16 c12"><span class="c3">Por usar la propiedad intelectual perteneciente a </span><span
                class="c0">LINKIU</span><span class="c1">&nbsp;sin la debida autorizaci&oacute;n del Operador;</span></li>
        <li class="c16 c12"><span class="c3 c9">Usar programas, software o aparatos autom&aacute;ticos o manuales para
                monitorear o copiar la informaci&oacute;n o cualquier tipo de contenido del Sitio Web o del App;</span></li>
        <li class="c16 c12"><span class="c1">Por todo tipo de actuaci&oacute;n en contravenci&oacute;n a estos
                T&eacute;rminos y condiciones;</span></li>
        <li class="c5 c12"><span class="c1">Y cualquier otra causal tipificada en la norma colombiana que amerite la
                cancelaci&oacute;n y/o suspensi&oacute;n del uso de la plataforma. </span></li>
    </ol>
    <p class="c5"><span class="c0">Par&aacute;grafo 1: &nbsp; </span><span class="c7">En cualquier momento El Consumidor
            podr&aacute; desistir de seguir usando la Plataforma Virtual de manera definitiva, en caso tal debe de informar
            a El Operador de su decisi&oacute;n a trav&eacute;s del correo electr&oacute;nico appdigitalco@gmail.com, para
            cancelar su cuenta.</span><span class="c0 c13 c8">&nbsp;</span></p>
    <p class="c5"><span class="c0">D&Eacute;CIMA QUINTA. &ndash; MODIFICACIONES DE ESTOS T&Eacute;RMINOS Y CONDICIONES:
        </span><span class="c1">Los presentes T&eacute;rminos y Condiciones podr&aacute;n ser modificados por El Operador,
            con el fin de mejorar el servicio, es por ello que cada vez que sean realizados cambios ser&aacute;n notificados
            al correo electr&oacute;nico del Consumidor para su conocimiento. </span></p>
    <p class="c5"><span class="c1">En caso de no estar de acuerdo con los mismos, podr&aacute; abstenerse de ingresar y usar
            la plataforma, raz&oacute;n por la cual podr&aacute; desistir de seguir usando la Plataforma Virtual o
            raz&oacute;n por la cual ser&aacute; una causal de cancelaci&oacute;n de su cuenta. </span></p>
    <p class="c5"><span class="c0">D&Eacute;CIMA SEXTA. &ndash; TRATAMIENTOS DE DATOS PERSONALES: </span><span
            class="c7">Para efecto del uso de la Plataforma virtual </span><span class="c0">LINKIU</span><span class="c7">,
            NEURON GROUP S.A.S. </span><span class="c3 c9">de acuerdo con la Ley Estatutaria 1581 de 2012 </span><span
            class="c7 c31">&ldquo;Por la cual se dictan disposiciones generales para la protecci&oacute;n de datos
            personales&rdquo;</span><span class="c3 c9">&nbsp;y dem&aacute;s normas complementarias y
            reglamentarias,</span><span class="c1">&nbsp;recolectar&aacute;n y tratar&aacute; algunos datos personales, el
            tr&aacute;mite de recolecci&oacute;n y tratamiento que se dar&aacute; a los mismos est&aacute;n estipulados en
            la Pol&iacute;tica de Protecci&oacute;n y Tratamiento de Datos Personales. </span></p>
    <p class="c5"><span class="c1">Se entiende que con la aceptaci&oacute;n de los presentes T&eacute;rminos y Condiciones
            da su consentimiento, como titular de datos, para que estos sean tratados en la base de datos de responsabilidad
            de la sociedad NEURON GROUP S.A.S., tal y como est&aacute;n estipulados en la Pol&iacute;tica de
            Protecci&oacute;n y Tratamiento de Datos Personales. Aceptando que cualquier reclamaci&oacute;n la podr&aacute;
            realizar en la sede social del Operador o como se establece en la Pol&iacute;tica. </span></p>
    <p class="c5"><span class="c0">D&Eacute;CIMA S&Eacute;PTIMA. - NO RENUNCIA SOBRE EJECUCI&Oacute;N DEL ACUERDO:
        </span><span class="c1">La no aplicaci&oacute;n del Operador de alguna de las condiciones, t&eacute;rminos y
            derechos incluidos en estos t&eacute;rminos y condiciones no ser&aacute; interpretada como desistimiento o
            renuncia del derecho del Operador en lo sucesivo de hacer cumplir o ejecutar dichas disposiciones.</span></p>
    <p class="c5"><span class="c0">D&Eacute;CIMA</span><span class="c0 c8">&nbsp;</span><span class="c0">OCTAVA</span><span
            class="c0 c8">. &ndash; JURISDICCI&Oacute;N: </span><span class="c8 c27">Para todos los efectos de ley
            ser&aacute; la ciudad de Bogot&aacute; D.C. (Cundinamarca).</span></p>
    <p class="c6" id="h.30j0zll"><span class="c0">D&Eacute;CIMA</span><span class="c0 c8">&nbsp;NOVENA. -
            &nbsp;SOLUCI&Oacute;N DE CONTROVERSIAS:</span><span class="c1">&nbsp;Toda diferencia o controversia relativa a
            los presentes T&eacute;rminos y Condiciones se intentar&aacute; resolver a trav&eacute;s del mecanismo alterno
            de soluci&oacute;n de conflictos de la conciliaci&oacute;n. &nbsp; </span></p>
    <p class="c6"><span class="c1">Si transcurridos treinta (30) d&iacute;as calendario desde la fecha en que surgi&oacute;
            la diferencia no se ha llegado a un acuerdo, someter&aacute;n la decisi&oacute;n a &aacute;rbitros de acuerdo
            con la Ley 1563 del 2012 y dem&aacute;s disposiciones complementarias y reglamentarias, seg&uacute;n las
            siguientes reglas: </span></p>
    <p class="c6"><span class="c1">&nbsp;</span></p>
    <ol class="c18 lst-kix_list_5-0 start" start="1">
        <li class="c6 c12"><span class="c1">El tribunal decidir&aacute; en Derecho.</span></li>
        <li class="c6 c12"><span class="c1">El tribunal estar&aacute; integrado por un (1) &aacute;rbitro, salvo que el
                asunto a debatir sea de mayor cuant&iacute;a caso en el cual estar&aacute; integrado por tres (3)
                &aacute;rbitros. </span></li>
        <li class="c6 c12"><span class="c1">La organizaci&oacute;n interna del tribunal se sujetar&aacute; a las reglas
                previstas para el arbitraje institucional en el reglamento del Centro de Conciliaci&oacute;n, Arbitraje y
                Amigable Composici&oacute;n de la C&aacute;mara de Comercio de Cali.</span></li>
        <li class="c6 c12"><span class="c1">El tribunal funcionar&aacute; en el centro de arbitraje de la referida &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; C&aacute;mara de
                Comercio. </span></li>
        <li class="c6 c12"><span class="c1">El t&eacute;rmino m&aacute;ximo de duraci&oacute;n del arbitraje ser&aacute; de
                dos (2) meses contados desde la primera audiencia de tr&aacute;mite.</span></li>
        <li class="c6 c12"><span class="c1">Todos los honorarios y gastos generados por el tribunal de arbitramento
                ser&aacute;n asumidos por la parte que resulte vencida.</span></li>
    </ol>
    <p class="c23"><span class="c19 c13 c8"></span></p>
    <p class="c23"><span class="c19 c13 c8"></span></p>
    <p class="c23"><span class="c13 c9 c17"></span></p>
    <p class="c23"><span class="c17 c13 c9"></span></p>
    <p class="c23"><span class="c19 c13 c8"></span></p>';
    }

}

