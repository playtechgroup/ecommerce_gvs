<?php

namespace App\Http\Controllers\Web;

use App\City;
use App\Product;
use App\Provider;
use App\FileProduct;
use App\CategoryProduct;
use App\SubcategoryProduct;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');


    }
    public function index()
    {
        //$provider=Provider::where('users_id',Auth::user()->id)->first();
        //$productos = Product::where('Provider_id',$provider->id)->get();
        $productos = Product::all();
        return view('provider.products.products',compact('productos'));
    }
    public function create()
    {

        $categories=CategoryProduct::all();

        $provider=Provider::first();
        $cities=City::all()->sortBy("name");
        $product=Product::where('Provider_id',$provider->id)->first();

        return view('provider.products.createproduct',compact('categories','cities'));
    }
    public function getsubcategory($id)
    {
       $subcategory=SubcategoryProduct::where('CategoryProduct_id',$id)->get();
       return response()->json(['response'=>true,'items'=>$subcategory],200);
    }

    public function createproductform(Request $request)
    {
       // dd($request);
        $input_data = $request->all();

            $validator = Validator::make(
                $input_data, [
                'photos.*' => 'required|mimes:jpg,jpeg,png,bmp|max:1024',
                'invima' => 'max:1024',
                'espefification' => 'max:1024',
                'garanty' => 'max:1024',
                ],[
                    'photos.*.required' => 'Please upload an image',
                    'photos.*.mimes' => 'Only jpeg,png and bmp images are allowed',
                    'photos.*.max' => 'Lo Sentimos! El tamaño por fotos permitido es 1MB',
                    'invima.max' => 'Lo Sentimos! El tamaño del archivo de invima  permitido es 1MB',
                    'espefification.max' => 'Lo Sentimos! El tamaño de las especificaciones permitido es 1MB',
                    'garanty.max' => 'Lo Sentimos! El tamaño del  archivo de la devolución   permitido es 1MB'
                ]
            );
           // dd($validator);
            if ($validator->fails()) {
                dd($validator);
            return redirect()->back()
            ->withInput($request->input())->withErrors($validator);
            }

            //dd('al pelo');
      $provider=Provider::first();
      $a = str_replace(',', '', $request->price);
        $product=new Product([
            'code'=>$request->code,
            'name'=>$request->name,
            'description'=>$request->description,
            'price'=>$a,
            'tax'=>$request->tax,
            'SubcategoryProduct_id'=>$request->SubcategoryProduct_id,
            'Provider_id'=>$provider->id,
            'stock'=>$request->stock,
            'minimumStock'=>$request->minimumStock,
            'date_create'=> date("Y-m-d H:i:s"),
            'active'=>1
        ]);

            try {

        $product->save();
        $url=config('app.url');
        if(isset($product)){
            if($request->hasFile('photos') != null){
                foreach ($request->file('photos') as $key => $value) {


                    $name = $value->getClientOriginalName();
                    if(!File::exists(public_path('filesproduct/'.$product->id.'/'))){
                        File::makeDirectory(public_path('filesproduct/'.$product->id.'/'), $mode = 0777, true, true);
                    }
                    $value->move(public_path('filesproduct/'.$product->id.'/'),$name);
                    $link = 'filesproduct/'.$product->id.'/'.$name;
                    $file=new FileProduct([
                        'name'=>'photo#'.$key,
                        'link'=>$url.$link,
                        'Product_id'=>$product->id,
                        'type'=>'photo'
                    ]);
                    $file->save();
                }
            }

            if($request->hasFile('invima') != null){
                $file = $request->file('invima');
                $name = $file->getClientOriginalName();
                $file->move(public_path('filesproduct/'.$product->id.'/'),$name);
                $link = 'filesproduct/'.$product->id.'/'.$name;
                $file=new FileProduct([
                    'name'=>'Invima',
                    'link'=>$url.$link,
                    'Product_id'=>$product->id,
                    'type'=>'files'
                ]);
                $file->save();
            }

            if($request->hasFile('espefification') != null){
                $file = $request->file('espefification');
                $name = $file->getClientOriginalName();
                $file->move(public_path('filesproduct/'.$product->id.'/'),$name);
                $link = 'filesproduct/'.$product->id.'/'.$name;
                $file=new FileProduct([
                    'name'=>'Especificaciones',
                    'link'=>$url.$link,
                    'Product_id'=>$product->id,
                    'type'=>'files'
                ]);
                $file->save();

            }

            if($request->hasFile('garanty') != null){
                $file = $request->file('garanty');
                $name = $file->getClientOriginalName();
                $file->move(public_path('filesproduct/'.$product->id.'/'),$name);
                $link = 'filesproduct/'.$product->id.'/'.$name;
                $file=new FileProduct([
                    'name'=>'Política de devolución',
                    'link'=>$url.$link,
                    'Product_id'=>$product->id,
                    'type'=>'files'
                ]);
                $file->save();

            }


            flash('Producto Creado')->success();
            return redirect('/products');

        }       //code...
    } catch (\Throwable $th) {

        flash('Ocurrió un problema al crear el producto')->warning();
        return redirect()->back()
        ->withInput($request->input());
    }


    }

    public function viewproduct($id)
    {
        $product = Product::find($id);
        $files=FileProduct::where('Product_id',$product->id)->get();
        return view('provider.products.viewproduct',compact('product','files'));
    }

    public function editproduct($id)
    {
        $product = Product::find($id);
        $files=FileProduct::where('Product_id',$product->id)->get();
        $categories=CategoryProduct::all();
        $cities=City::all()->sortBy("name");
        return view('provider.products.editproduct',compact('product','files','categories', 'cities' ));
    }

    public function editproductform(Request $request)
    {
        $input_data = $request->all();

            $validator = Validator::make(
                $input_data, [
                'photos.*' => 'required|mimes:jpg,jpeg,png,bmp|max:1024',
                'invima' => 'max:1024',
                'espefification' => 'max:1024',
                'garanty' => 'max:1024',
                ],[
                    'photos.*.required' => 'Please upload an image',
                    'photos.*.mimes' => 'Only jpeg,png and bmp images are allowed',
                    'photos.*.max' => 'Lo Sentimos! El tamaño por fotos permitido es 1MB',
                    'invima.max' => 'Lo Sentimos! El tamaño del archivo de invima  permitido es 1MB',
                    'espefification.max' => 'Lo Sentimos! El tamaño de las especificaciones permitido es 1MB',
                    'garanty.max' => 'Lo Sentimos! El tamaño del  archivo de la devolución   permitido es 1MB'
                ]
            );

            if ($validator->fails()) {
            return redirect()->back()
            ->withInput($request->input())->withErrors($validator);
            }


      $provider=Provider::where('users_id',Auth::user()->id)->first();
      $a = str_replace(',', '', $request->price);
        $product=Product::find($request->idProduct);

            try {
        $url=config('app.url');
        if($product){

            $product->code=$request->code;
            $product->name=$request->name;
            $product->description=$request->description;
            $product->price=$a;
            $product->tax=$request->tax;
            $product->SubcategoryProduct_id=$request->SubcategoryProduct_id;
            $product->stock=$request->stock;
            $product->minimumStock=$request->minimumStock;
            $product->active=1;
            $product->save();

            if($request->hasFile('photos') != null){
                foreach ($request->file('photos') as $key => $value) {
                    $name = $value->getClientOriginalName();
                    if(!File::exists(public_path('filesproduct/'.$product->id.'/'))){
                        File::makeDirectory(public_path('filesproduct/'.$product->id.'/'), $mode = 0777, true, true);
                    }
                    $value->move(public_path('filesproduct/'.$product->id.'/'),$name);
                    $link = 'filesproduct/'.$product->id.'/'.$name;
                    $file=new FileProduct([
                        'name'=>'photo#'.$key,
                        'link'=>$url.$link,
                        'Product_id'=>$product->id,
                        'type'=>'photo'
                    ]);
                    $file->save();
                }
            }

            if($request->hasFile('invima') != null){
                $file = $request->file('invima');
                $name = $file->getClientOriginalName();
                $file->move(public_path('filesproduct/'.$product->id.'/'),$name);
                $link = 'filesproduct/'.$product->id.'/'.$name;
                $file=new FileProduct([
                    'name'=>'Invima',
                    'link'=>$url.$link,
                    'Product_id'=>$product->id,
                    'type'=>'files'
                ]);
                $file->save();
            }

            if($request->hasFile('espefification') != null){
                $file = $request->file('espefification');
                $name = $file->getClientOriginalName();
                $file->move(public_path('filesproduct/'.$product->id.'/'),$name);
                $link = 'filesproduct/'.$product->id.'/'.$name;
                $file=new FileProduct([
                    'name'=>'Especificaciones',
                    'link'=>$url.$link,
                    'Product_id'=>$product->id,
                    'type'=>'files'
                ]);
                $file->save();

            }

            if($request->hasFile('garanty') != null){
                $file = $request->file('garanty');
                $name = $file->getClientOriginalName();
                $file->move(public_path('filesproduct/'.$product->id.'/'),$name);
                $link = 'filesproduct/'.$product->id.'/'.$name;
                $file=new FileProduct([
                    'name'=>'Política de devolución',
                    'link'=>$url.$link,
                    'Product_id'=>$product->id,
                    'type'=>'files'
                ]);
                $file->save();

            }



            flash('Producto Editado')->success();
           return $this->viewproduct($request->idProduct);

        }       //code...
    } catch (\Throwable $th) {
        dd($th);
        flash('Ocurrió un problema al crear el producto')->warning();
        return redirect()->back()
        ->withInput($request->input());
    }


    }

    public function deletefile(Request $request)
    {
        $file=FileProduct::find($request->id);
        if (isset($file)) {
            try {
             $file->delete();
             return response()->json(array('response'=>true),200);
            } catch (\Throwable $th) {
                return response()->json(array('response'=>false),200);
            }

        }


    }

    public function update_product_status(Request $request)
    {
        $pro = Product::find($request->id);
        try
        {
            $pro->active = ($request->param=='true') ? 1: 0 ;
            $saved = $pro->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }


}
