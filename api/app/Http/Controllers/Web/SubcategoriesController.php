<?php
namespace App\Http\Controllers\Web;
use App\SubcategoryProduct;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;

class SubcategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Admin');
    }
    public function createsubcategories($id)
    {
        return view('admin.subcategories.create',["id"=>$id]);
    }
    public function createsubcategoriesform(Request $request,$id){
        $url                = config('app.url');
        $SubcategoryProduct = new SubcategoryProduct(['name'=>$request->name,'CategoryProduct_id'=>$id]);
        $ok                 = $SubcategoryProduct->save();
        if ($request->hasFile('url_img') != null) {
            $file = $request->file('url_img');
            $ext  = $file->getClientOriginalExtension();
            $file->move(public_path('img/subcategories_product/'),$SubcategoryProduct->id.".".$ext);
            $url_img = 'img/subcategories_product/'.$SubcategoryProduct->id.'.'.$ext;
            $SubcategoryProduct->url_img = $url.$url_img;
        }
        $ok = $SubcategoryProduct->save();
        if ($ok) {
            flash('Subcategoria creada con exito')->success();
            return redirect('/viewsubcategory/'.$SubcategoryProduct->CategoryProduct_id);
        }else{
            flash('Ocurrió un problema al crear la empresa')->warning();
            return;
        }
    }
    public function editsubcategories($id)
    {
        $SubcategoryProduct  = SubcategoryProduct::where('id',$id)->first();
        return view('admin.subcategories.edit',compact('SubcategoryProduct'));
    }
    public function editsubcategoriesform(Request $request, $idsubcateogory)
    {
        $SubcategoryProduct          = SubcategoryProduct::find($idsubcateogory);
        $SubcategoryProduct->name    = $request->name;
        $SubcategoryProduct->url_img = $request->url_img;
        $ok                          = $SubcategoryProduct->save();
        $url                         = config('app.url');

        if ($request->hasFile('url_img') != null) {//validar que la imagen si es la misma no consuma el save
            // $file = $request->file('url_img');
            // $ext  = $file->getClientOriginalExtension();
            // $file->move(public_path('img/subcategories_product/'),$SubcategoryProduct->id.".".$ext);
            // $url_img = 'img/subcategories_product/'.$SubcategoryProduct->id.'.'.$ext;
            $SubcategoryProduct->url_img = "N/A";
        }else{
             $SubcategoryProduct->url_img = $SubcategoryProduct->url_img;
        }
        $savep = $SubcategoryProduct->save();
        if (isset($savep))
        {
            flash('Subcategoria Editada')->success();
            return redirect('/viewsubcategory/'.$SubcategoryProduct->CategoryProduct_id);
        }
        else
        {
            flash('Ocurrió un problema al editar la Subcategoria')
                ->warning();
            return redirect()
                ->back()
                ->withInput($request->input());
        }
    }
    public function statesubcategory(Request $request)
    {
        $subcategory = SubcategoryProduct::find($request->id);
        try
        {
            $subcategory->active=($request->param=="true")?1:0;
            $saved = $subcategory->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }
}
