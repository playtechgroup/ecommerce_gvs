<?php
namespace App\Http\Controllers\Web;
use App\Order;
use App\OrderStatus;
use App\OrderProduct;
use App\Product;
use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;
use DB;
use App\Quotation;
class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $Order = Order::all();
        foreach ($Order as $key => $value) {
        $contador_total = 0;
            $Products = OrderProduct::where('order_id',$value->id)->get();
            foreach ($Products as $key2 => $value2) {
                $contador_total+=($value2->product->price * $value2->quantity)*(($value2->tax/100)*1)+($value2->product->price * $value2->quantity);
            }
            $value->total=number_format($contador_total);
        }
        return view('admin.orders.orders', compact('Order'));
    }
    public function orderdetail($id)
    {
        $Products = OrderProduct::where('order_id',$id)->get();
        $Order = Order::find($id);
        return view('admin.orders.orderdetail', compact('Products','Order'));
    }

    public function deleteorder(Request $request)
    {
        $order = Order::find($request->id);
        try
        {
            $order->active = ($request->param=='true') ? 1: 0 ;
            $saved = $order->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }
}
