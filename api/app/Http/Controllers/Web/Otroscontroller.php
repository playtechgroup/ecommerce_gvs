<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

class OtrosController extends Controller
{

    public function terminos()
    {
        return view('admin.terminos');
    }
    public function politicas()
    {
        return view('admin.politicas');
    }
    public function perfil()
    {
        $profile  = User::where('id',Auth::user()->id)->first();
        return view('admin.perfil',compact('profile'));
    }
    
    public function editProfileForm(Request $request, $id)
    {
        $emailExists=false;
        if ($request->email!==$request->actualEmail){
            $emailExists    = User::where('email',$request->email)->first();
        }
        if ($emailExists){
            flash('El email ingresado ya existe')->error();
            if (Auth::user()->Role_id==1){
                return redirect('/home');
            } else if (Auth::user()->Role_id==3){
                return redirect('/homeProveedor');
            } else if (Auth::user()->Role_id==4){
                return redirect('/homeInventory');
            } else if (Auth::user()->Role_id==5){
                return redirect('/homePublisher');
            } else{
                return redirect('/');
            }
        } else {
            $user           = User::find($id);
            $user->name     = $request->name;
            $user->email    = $request->email;
            if ($request->pass!=null){
                $user->password = Hash::make($request->pass);
            }
            $ok             = $user->save();
            if (isset($ok))
            {
                flash('Información de perfil actualizada')->success();
                if (Auth::user()->Role_id==1){
                    return redirect('/home');
                } else if (Auth::user()->Role_id==3){
                    return redirect('/homeProveedor');
                } else if (Auth::user()->Role_id==4){
                    return redirect('/homeInventory');
                } else if (Auth::user()->Role_id==5){
                    return redirect('/homePublisher');
                } else{
                    return redirect('/');
                }
            }
            else
            {
                flash('Ocurrió un problema al actualizar la información de perfil')
                    ->warning();
                return redirect()
                    ->back()
                    ->withInput($request->input());
            }
        }
    }
}
