<?php
namespace App\Http\Controllers\Web;

use App\CategoryProduct;
use App\Devolutions;
use App\Events;
use App\Http\Controllers\Controller;
use App\Notices;
use Illuminate\Http\Request;
use App\Order;
use App\Provider;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Error\Notice;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $product = Product::count();
        $categories = CategoryProduct::count();
        $users = User::count();
        return view('admin.home',compact('product','categories','users'));
    }
    public function indexprovider()
    {
        $product = Product::count();
        $orders = Order::count();
        return view('provider.home',compact('orders','product'));
    }
    public function indexpublisher()
    {
        $notices = Notices::count();
        $events = Events::count();
        return view('publisher.home',compact('notices','events'));
    }
    public function indexinventory()
    {
        $product = Product::count();
        $orders = Order::count();
        $devolutions = Devolutions::count();
        return view('inventory.home',compact('orders','product','devolutions'));
    }
}
