<?php

namespace App\Http\Controllers\Web;

use App\Bill;
use DB;
use App\Order;
use App\Product;
use App\Provider;
use App\Quotation;
use App\OrderStatus;
use App\Notification;
use App\OrderProduct;
use App\CategoryProduct;
use App\OrderProductStatus;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mail;

class OrderProviderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $provider = Provider::where('id', '1')->first();
        $Order = Order::whereHas('products', function ($query1) use ($provider) {
            $query1->whereHas('provider', function ($query2) use ($provider) {
                $query2->where('id', $provider->id);
            });
        })->get();

        foreach ($Order as $key => $value) {
            $contador_total = 0;
            $Products = OrderProduct::where('order_id', $value->id)->get();
            foreach ($Products as $key2 => $value2) {
                $contador_total += ($value2->product->price * $value2->quantity) * (($value2->tax / 100) * 1) + ($value2->product->price * $value2->quantity);
            }
            $value->total = number_format($contador_total);
        }

        return view('admin.ordersbyprovider.orders', compact('Order'));
    }
    public function orderdetail($id)
    {
        $OrderStatus = OrderStatus::all()->sortByDesc("name");
        $provider = Provider::first();
        $params = Product::where('Provider_id', $provider->id)->get();

        foreach ($params as $param) {
            $id_product[] = $param->id;
        }

        $Products = OrderProduct::whereIn('Product_id', $id_product)->where('order_id', $id)->get();

        $Order = Order::find($id);
        $Bills = Bill::where('order_id', $id)->get();

        return view('admin.ordersbyprovider.orderdetail', compact('Products', 'Order', 'OrderStatus', 'Bills'));
    }
    public function update_order_status(Request $request)
    {
        $OrderStatus = Order::find($request->id);
        $OrderStatus->OrderStatus_id = $request->new_state;

        // if ($request->new_state == 2 || $request->new_state == 3) {

            $name_product = "";

            // $provider = Provider::where('id', '1')->first();
            // $params = Product::where('Provider_id', $provider->id)->get();

            // foreach ($params as $param) {
            //     $id_product[] = $param->id;
            // }

            $OrderProducts = OrderProduct::where('order_id', $OrderStatus->id)->get();
            $id_product=[];
            foreach ($OrderProducts as $OrderProduct) {
                $id_product[] = $OrderProduct->Product_id;
            }

            $Products = Product::wherein('id', $id_product)->get();

            foreach ($Products as  $Product) {
                $name_product .= $Product->name . ', ';
            }
        // }

        try {
            $saved = $OrderStatus->save();

            if ($request->new_state == 1) {
                $message = 'Su orden ' . $OrderStatus->id . ' con los productos: ' . $name_product . 'ha sido una compra exitosa';  
            } else if ($request->new_state == 2) {
                $message = 'Su orden ' . $OrderStatus->id . ' con los productos: ' . $name_product . 'ha sido recibida';  
            } else if ($request->new_state == 3) {
                $message = 'Su orden ' . $OrderStatus->id . ' con los productos: ' . $name_product . 'ha sido despachada';
            } else if ($request->new_state == 4) {
                $message = 'Su orden ' . $OrderStatus->id . ' con los productos: ' . $name_product . 'ha sido Entregada con Exito, Por favor Calificanos y dejanos un Comentario del Producto';
            } else if ($request->new_state == 6) {
                $message = 'Su orden ' . $OrderStatus->id . ' con los productos: ' . $name_product . 'está pendiente de pago';
            } else if ($request->new_state == 7) {
                $message = 'Su orden ' . $OrderStatus->id . ' con los productos: ' . $name_product . 'ha sido cancelada';
            } else if ($request->new_state == 8) {
                $message = 'Su orden ' . $OrderStatus->id . ' con los productos: ' . $name_product . 'ha sido enviada';
            }

            $notification = Notification::create([
                'message' => $message,
                'state' => '1',
                'date' => date("Y-m-d H:i:s"),
                'Costumer_id' => $OrderStatus->Costumer_id
            ]);

            if (isset($saved)) {
                return response()->json(['r' => true, 'valor' => $request->new_state]);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['r' => false, 'm' => $ex->getMessage(), 'valor' => $request->new_state]);
        }
    }
    public function newNotification(Request $request, $id)
    {
        $message=$request->message;
        $costumerId=$request->costumerId;
        $fecha = date("Y-m-d");
        
        $data = array(
            'name' => $request->costumerName,
            'email' => $request->costumerEmail,
            'message' => $request->message,
        );
        
        $this->sendmail($data);

        $notification=new Notification([
            'message'=>$message,
            'state'=>1,
            'date' => $fecha,
            'Costumer_id'=>$costumerId
        ]);

        $ok = $notification->save();

        if ($ok){
            flash('Notificación enviada exitosamente')->success();
        } else {
            flash('No se ha podido enviar la notificación')->error();
        }
        return redirect('/orderdetail/'.$id);
    }

    
    public function sendMail($data)
    {
        try {
            $titulo = "Notificacion de INMUNOTEK";
            $msj = "Hola ". $data['name'] .", tienes una nueva notificación de Inmunotek: <br>". $data['message'];
            $sendmail = Mail::send('mails.notificaciones', ['title' => $titulo, 'mensaje' => $msj], function ($mail) use ($data) {
                $mail->from('inmunotek@evocompany.co', 'INMUNOTEK');
                $mail->subject('Notificaciones Inmunotek');
                $mail->to($data['email']);

            });

        } catch (\Throwable $th) {
            echo($th);
            return json_encode($th);
        }
    }
}
