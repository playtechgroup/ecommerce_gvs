<?php
namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Provider;
use App\Product;
use App\City;
use App\User;
use App\TypeIdentification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Mail;
class ProviderController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Admin');
    }
    public function index(){
        $providers=Provider::where('active','=','1')->get();
        return view('admin.provider.provider',compact('providers'));
    }
    public function viewcreate(){
        $typeIdentification=TypeIdentification::all();
        $ciudad=City::orderBy('name','ASC')->get();
        return view('admin.provider.createprovider',compact('typeIdentification','ciudad'));
    }
    public function create(Request $request){

        $validation= Validator::make($request->all(),[
            'email' => 'unique:users'
       ],['email.unique'=>'El email ya se encuentra registrado']);

       if ($validation->fails()) {
        return redirect()->back()
        ->withInput($request->input())
        ->withErrors($validation);
    }
        $user=new User([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=> Hash::make('Linkiu'.$request->identification),
            'Role_id'=>3
        ]);
        $url=config('app.url');
            $user->save();
        if($user){
        if($request->hasFile('trade_certificate') != null){
            $file = $request->file('trade_certificate');
            $name = $file->getClientOriginalName();
            File::makeDirectory(public_path('files/'.$user->id.'/'), $mode = 0777, true, true);
            $file->move(public_path('files/'.$user->id.'/'),$name);
            $trade_certificate = 'files/'.$user->id.'/'.$name;
        }
        if($request->hasFile('tax_registration') != null){
            $file = $request->file('tax_registration');
            $name = $file->getClientOriginalName();
            File::makeDirectory(public_path('files/'.$user->id.'/'), $mode = 0777, true, true);
            $file->move(public_path('files/'.$user->id.'/'),$name);
            $tax_registration = 'files/'.$user->id.'/'.$name;
        }
            $provider=new Provider([
                'identification'=>$request->identification ,
                'address'=>$request->address ,
                'contact_person'=>$request->contactperson ,
                'phone'=>$request->phone ,
                'trade_certificate'=>$url.$trade_certificate ,
                'tax_registration'=>$url.$tax_registration ,
                'users_id'=>$user->id ,
                'City_id'=>$request->city ,
                'TypeIdentification_id' =>$request->TypeIdentification_id
                ]);
                $provider->save();
        }
        if(isset($provider)){
            try {
                $sendmail=Mail::send('mails.welcome',['provider'=>$provider],function($mail)use($provider){
                    $mail->from('no-responder@linkiu.app', 'LINKIU');
                    $mail->subject('Bienvenido a Linkiu App');
                    $mail->to($provider->user->email);

                });

                $sendmail2=Mail::send('mails.welcome',['provider'=>$provider],function($mail)use($provider){
                    $mail->from('no-responder@linkiu.app', 'LINKIU');
                    $mail->subject('Bienvenido a Linkiu App');
                    $mail->to('appdigitalco@gmail.com');

                });

            } catch (Exception $e) {
                flash('Ocurrió un problema al crear la empresa')->warning();
                return  redirect()->back();
            }

            return redirect('/provider');
        }else{
            flash('Ocurrió un problema al crear la empresa')->warning();
            return;
        }
    }
    public function viewprovider($id){
        $provider  = Provider::where('id',$id)->first();
        $productos = Product::where('Provider_id',$provider->id)->get();
        return view('admin.provider.viewprovider',compact('provider','productos'));
    }


    public function editprovider($idProvider)
    {
        $provider  = Provider::where('id',$idProvider)->first();
        $typeIdentification=TypeIdentification::all();
        $ciudad=City::orderBy('name','ASC')->get();
        return view('admin.provider.editprovider',compact('provider','typeIdentification','ciudad'));
    }

    public function editproviderform(Request $request,$idProvider)
    {
        $provider  = Provider::find($idProvider);

        if($provider->user->email!=$request->email){
       $validation= Validator::make($request->all(),[
            'email' => 'unique:users'
       ],['email.unique'=>'El email ya se encuentra registrado']);

       if ($validation->fails()) {
        return redirect()->back()
        ->withInput($request->input())
        ->withErrors($validation);
    }
        }
    $user=User::find($provider->users_id);
    $user->email=$request->email;
    $user->name=$request->name;
    $user->save();



    $provider->identification=$request->identification;
    $provider->address=$request->address;
    $provider->contact_person=$request->contactperson;
    $provider->phone=$request->phone;
    $provider->users_id=$user->id;
    $provider->City_id=$request->City_id;
    $provider->TypeIdentification_id=$request->TypeIdentification_id;

    $url=config('app.url');
    if($request->hasFile('trade_certificate') != null){
        $file = $request->file('trade_certificate');
        $name = $file->getClientOriginalName();
        $file->move(public_path('files/'.$user->id.'/'),$name);
        $trade_certificate = 'files/'.$user->id.'/'.$name;
        $provider->trade_certificate=$url.$trade_certificate;
    }
    if($request->hasFile('tax_registration') != null){
        $file = $request->file('tax_registration');
        $name = $file->getClientOriginalName();
        $file->move(public_path('files/'.$user->id.'/'),$name);
        $tax_registration = 'files/'.$user->id.'/'.$name;
        $provider->tax_registration=$url.$tax_registration;
    }

    $savep=$provider->save();

    if(isset($savep)){
        flash('Empresa Editada')->success();
        return redirect('/viewprovider/'.$provider->id);
    }else{
        flash('Ocurrió un problema al editar la empresa')->warning();
        return redirect()->back()
        ->withInput($request->input());

    }

    }



public function deleteprovider(Request $request)
{

        $provider=Provider::find($request->id);
        try
        {
            $provider->active = 0 ;
            $saved = $provider->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    // $provider=Provider::find($request->id);
    // try {
    //     $delete=$provider->delete();
    //     if($delete){
    //         $user=User::find($provider->users_id);
    //         $user->delete();
    //     }

    //     return response()->json(['r'=>true]);
    // } catch (\Illuminate\Database\QueryException $ex) {
    //     return response()->json(['r'=>false,'m'=> $ex->getMessage()]);
    // }


}




}
