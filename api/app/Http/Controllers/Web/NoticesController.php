<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Notices;
use Illuminate\Http\Request;
use PHPUnit\Framework\Error\Notice;

class NoticesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $notices = Notices::where('tipe',0)->get();
        return view('notices.notices', compact('notices'));
    }
    public function create(){
        return view('notices.create');
    }
    public function createnoticeform(Request $request){

        $url      = config('app.url');
        $notice=new Notices([
            'tipe'=>0,
            'title'=>$request->title,
            'description'=>$request->description,
            'photo'=>"",
            'date'=> date("Y-m-d H:i:s")
        ]);
        $ok = $notice->save();
        if ($request->hasFile('photo') != null) {
            $file = $request->file('photo');
            $ext  = $file->getClientOriginalExtension();
            $file->move(public_path('img/notices/'),$notice->id.".".$ext);
            $url_img = 'img/notices/'.$notice->id.'.'.$ext;
            $notice->photo = $url.$url_img;
        }
        $ok= $notice->save();
        if ($ok) {
            flash('Noticia creada con exito')->success();
            return redirect('/notices');
        }else{
            flash('Ocurrió un problema al crear la noticia')->warning();
            return;
        }
    }
    public function editnotice($idNotice)
    {
        $notice  = Notices::where('id',$idNotice)->first();
        return view('notices.edit',compact('notice'));
    }
    public function editnoticeform(Request $request, $idNotice)
    {
        $notice              = Notices::find($idNotice);
        $notice->title       = $request->title;
        $notice->description = $request->description;
        $notice->date        = date("Y-m-d H:i:s");
        $ok                  = $notice->save();
        $url                 = config('app.url');
        if ($request->hasFile('photo') != null) {//validar que la imagen si es la misma no consuma el save
            $file = $request->file('photo');
            $ext  = $file->getClientOriginalExtension();
            $file->move(public_path('img/notices/'),$notice->id.".".$ext);
            $url_img = 'img/notices/'.$notice->id.'.'.$ext;
            $notice->photo = $url.$url_img;
        }
        $savep = $notice->save();
        if (isset($savep))
        {
            flash('Noticia Editada')->success();
            return redirect('/notices/');
        }
        else
        {
            flash('Ocurrió un problema al editar la noticia')
                ->warning();
            return redirect()
                ->back()
                ->withInput($request->input());
        }
    }
    public function deletenotice(Request $request)
    {
        $notice = Notices::find($request->id);
        try
        {
            $notice->active = ($request->param=='true') ? 1: 0 ;
            $saved = $notice->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }
}
