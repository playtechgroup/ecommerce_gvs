<?php

namespace App\Http\Controllers\Web;

use App\Events;
use App\EventType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $events = Events::all();
        return view('events.events', compact('events'));
    }
    public function create(){
        $eventTypes = EventType::all();
        return view('events.create', compact('eventTypes'));
    }
    public function createeventform(Request $request){
        $url      = config('app.url');
        $event=new Events([
            'title'=>$request->title,
            'description'=>$request->description,
            'date'=> $request->date,
            'event_type_id' => $request->eventId
        ]);
        $ok = $event->save();
        if ($request->hasFile('photo') != null) {
            $file = $request->file('photo');
            $ext  = $file->getClientOriginalExtension();
            $file->move(public_path('img/events/'),$event->id.".".$ext);
            $url_img = 'img/events/'.$event->id.'.'.$ext;
            $event->photo = $url.$url_img;
        }
        $ok= $event->save();
        if ($ok) {
            flash('Evento creado con exito')->success();
            return redirect('/events');
        }else{
            flash('Ocurrió un problema al crear el evento')->warning();
            return;
        }
    }
    public function createtype(){
        return view('events.createtype');
    }
    public function createeventtype(Request $request){
        $url      = config('app.url');
        $event=new EventType([
            'description'=>$request->description
        ]);
        $ok = $event->save();
        if ($ok) {
            flash('Tipo de evento creado con exito')->success();
            return redirect('/events');
        }else{
            flash('Ocurrió un problema al crear el tipo de evento')->warning();
            return;
        }
    }
    public function editevent($idEvent)
    {
        $event  = Events::where('id',$idEvent)->first();
        $eventTypes = EventType::all();
        return view('events.edit',compact('event','eventTypes'));
    }
    public function editeventform(Request $request, $idEvent)
    {
        $event                  = Events::find($idEvent);
        $event->title           = $request->title;
        $event->description     = $request->description;
        $event->event_type_id   = $request->eventId;
        $event->date            = $request->date;
        if ($request->hasFile('photo') != null) {//validar que la imagen si es la misma no consuma el save
            $url = config('app.url');
            $file = $request->file('photo');
            $ext  = $file->getClientOriginalExtension();
            $file->move(public_path('img/events/'),$event->id.".".$ext);
            $url_img = 'img/events/'.$event->id.'.'.$ext;
            $event->photo = $url.$url_img;
        }
        $ok                     = $event->save();

        if (isset($ok))
        {
            flash('Evento Editado')->success();
            return redirect('/events/');
        }
        else
        {
            flash('Ocurrió un problema al editar el evento')
                ->warning();
            return redirect()
                ->back()
                ->withInput($request->input());
        }
    }
    public function deleteevent(Request $request)
    {
        $event = Events::find($request->id);
        try
        {
            $event->active = ($request->param=='true') ? 1: 0 ;
            $saved = $event->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }
}
