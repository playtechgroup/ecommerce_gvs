<?php

namespace App\Http\Controllers\Web;

use App\Devolutions;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class DevolutionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $devolutions = Devolutions::all();
        return view('devolutions.devolutions', compact('devolutions'));
    }
    public function create(){
        $products = Product::all();
        return view('devolutions.create', compact('products'));
    }
    public function createdevolutionform(Request $request){
        $url      = config('app.url');
        $devolution=new Devolutions([
            'reason'=>$request->reason,
            'date'=> date("Y-m-d H:i:s"),
            'products' => $request->valores
        ]);
        $ok = $devolution->save();
        if ($ok) {
            flash('Devolución creada con exito')->success();
            return redirect('/devolutions');
        }else{
            flash('Ocurrió un problema al crear la devolución')->warning();
            return;
        }
    }
    public function editdevolution($idDevolution)
    {
        $devolution  = Devolutions::where('id',$idDevolution)->first();
        $products = Product::all();
        return view('devolutions.edit',compact('devolution','products'));
    }
    public function editdevolutionform(Request $request, $idDevolution)
    {
        $devolution             = Devolutions::find($idDevolution);
        $devolution->reason     = $request->reason;
        $devolution->products   = $request->valores;
        $devolution->date       = date("Y-m-d H:i:s");
        $ok                     = $devolution->save();
        if (isset($ok))
        {
            flash('Devolución editada')->success();
            return redirect('/devolutions/');
        }
        else
        {
            flash('Ocurrió un problema al editar la devolución')
                ->warning();
            return redirect()
                ->back()
                ->withInput($request->input());
        }
    }
    public function deletedevolution(Request $request)
    {
        $devolution = Devolutions::find($request->id);
        try
        {
            $devolution->active = ($request->param=='true') ? 1: 0 ;
            $saved = $devolution->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }
}
