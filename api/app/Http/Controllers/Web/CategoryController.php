<?php
namespace App\Http\Controllers\Web;
use App\CategoryProduct;
use App\SubcategoryProduct;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;
class CategoryController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $categories = CategoryProduct::all();
        return view('admin.categories.categories', compact('categories'));
    }
    public function create(){
        return view('admin.categories.create');
    }
    public function createcategoryform(Request $request){
        $url      = config('app.url');
        $category = new CategoryProduct(['name'=>$request->name]);
        $ok = $category->save();
        if ($request->hasFile('url_img') != null) {
            $file = $request->file('url_img');
            $ext  = $file->getClientOriginalExtension();
            $file->move(public_path('img/categories_product/'),$category->id.".".$ext);
            $url_img = 'img/categories_product/'.$category->id.'.'.$ext;
            $category->url_img = $url.$url_img;
        }
        $ok= $category->save();
        if ($ok) {
            flash('Categoria creada con exito')->success();
            return redirect('/categories');
        }else{
            flash('Ocurrió un problema al crear la empresa')->warning();
            return;
        }
    }
    public function editcategory($idCateogory)
    {
        $Category  = CategoryProduct::where('id',$idCateogory)->first();
        return view('admin.categories.edit',compact('Category'));
    }
    public function editcategoryform(Request $request, $idCateogory)
    {
        $category          = CategoryProduct::find($idCateogory);
        $category->name    = $request->name;
        $ok                = $category->save();
        $url               = config('app.url');
        if ($request->hasFile('url_img') != null) {//validar que la imagen si es la misma no consuma el save
            $file = $request->file('url_img');
            $ext  = $file->getClientOriginalExtension();
            $file->move(public_path('img/categories_product/'),$category->id.".".$ext);
            $url_img = 'img/categories_product/'.$category->id.'.'.$ext;
            $category->url_img = $url.$url_img;
        }
        $savep = $category->save();
        if (isset($savep))
        {
            flash('Categoria Editada')->success();
            return redirect('/categories/');
        }
        else
        {
            flash('Ocurrió un problema al editar la Categoria')
                ->warning();
            return redirect()
                ->back()
                ->withInput($request->input());
        }
    }
    public function deletecategory(Request $request)
    {
        $category = CategoryProduct::find($request->id);
        try
        {
            $category->active = ($request->param=='true') ? 1: 0 ;
            $saved = $category->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }
    public function viewsubcategory($id)
    {
        $CategoryProduct  = CategoryProduct::where('id',$id)->first();
        $SubcategoryProduct = SubcategoryProduct::where('CategoryProduct_id',$CategoryProduct->id)->get();
        return view('admin.categories.viewsubcategory',compact('CategoryProduct','SubcategoryProduct'));
    }
}
