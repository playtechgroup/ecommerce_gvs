<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Notices;
use Illuminate\Http\Request;
use PHPUnit\Framework\Error\Notice;

class ClinicalController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $notices = Notices::where('tipe',1)->get();
        return view('clinical.clinical', compact('notices'));
    }
    public function create(){
        return view('clinical.create');
    }
    public function createclinicalform(Request $request){

        $url      = config('app.url');
        $notice=new Notices([
            'tipe'=>1,
            'title'=>$request->title,
            'description'=>$request->description,
            'photo'=>"",
            'date'=> date("Y-m-d H:i:s")
        ]);
        $ok = $notice->save();
        if ($request->hasFile('photo') != null) {
            $file = $request->file('photo');
            $ext  = $file->getClientOriginalExtension();
            $file->move(public_path('img/clinical/'),$notice->id.".".$ext);
            $url_img = 'img/clinical/'.$notice->id.'.'.$ext;
            $notice->photo = $url.$url_img;
        }
        $ok= $notice->save();
        if ($ok) {
            flash('Estudio clínico creado con exito')->success();
            return redirect('/clinical');
        }else{
            flash('Ocurrió un problema al crear el estudio clínico')->warning();
            return;
        }
    }
    public function editclinical($idClinic)
    {
        $notice  = Notices::where('id',$idClinic)->first();
        return view('clinical.edit',compact('notice'));
    }
    public function editclinicalform(Request $request, $idClinic)
    {
        $notice              = Notices::find($idClinic);
        $notice->title       = $request->title;
        $notice->description = $request->description;
        $notice->date        = date("Y-m-d H:i:s");
        $ok                  = $notice->save();
        $url                 = config('app.url');
        if ($request->hasFile('photo') != null) {//validar que la imagen si es la misma no consuma el save
            $file = $request->file('photo');
            $ext  = $file->getClientOriginalExtension();
            $file->move(public_path('img/clinical/'),$notice->id.".".$ext);
            $url_img = 'img/clinical/'.$notice->id.'.'.$ext;
            $notice->photo = $url.$url_img;
        }
        $savep = $notice->save();
        if (isset($savep))
        {
            flash('Estudio clínico Editado')->success();
            return redirect('/clinical/');
        }
        else
        {
            flash('Ocurrió un problema al editar el estudio clínico')
                ->warning();
            return redirect()
                ->back()
                ->withInput($request->input());
        }
    }
    public function deleteclinical(Request $request)
    {
        $notice = Notices::find($request->id);
        try
        {
            $notice->active = ($request->param=='true') ? 1: 0 ;
            $saved = $notice->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }

}
