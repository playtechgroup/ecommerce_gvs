<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\LinkedUser;
use App\Role;
use App\User;
use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mail;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Admin');
    }
    public function index(){
        $users = User::where('Role_id','not like',3)->get();
        return view('admin.users.users', compact('users'));
    }
    public function create(){
        return view('admin.users.create');
    }
    public function createuserform(Request $request){
        $url      = config('app.url');
        $emailExists=false;
        $emailExists    = User::where('email',$request->email)->first();
        if ($emailExists){
            flash('El email ingresado ya existe')->error();
            return redirect('/users');
        } else {
            $user=new User([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=> Hash::make('123456789'),
                'Role_id'=>$request->role
            ]);
            $ok = $user->save();
            if ($ok) {
                $rol = Role::find($request->role);
                $data = array(
                    'name' => $request->name,
                    'email' => $request->email,
                    'role' => $rol->name,
                );

                $this->sendmail($data);
                flash('Usuario creado con exito')->success();
                return redirect('/users');
            }else{
                flash('Ocurrió un problema al crear el usuario')->warning();
                return;
            }
        }
    }
    public function edituser($id)
    {
        $user  = User::where('id',$id)->first();
        return view('admin.users.edit',compact('user'));
    }
    public function edituserform(Request $request, $id)
    {
        $emailExists=false;
        if ($request->email!==$request->actualEmail){
            $emailExists    = User::where('email',$request->email)->first();
        }
        if ($emailExists){
            flash('El email ingresado ya existe')->error();
            return redirect('/users');
        } else {
            $user          = User::find($id);
            $user->name    = $request->name;
            $user->email    = $request->email;
            $user->Role_id   = $request->role;
            $user->linked    = $request->active ? 0 : 1;
            $ok            = $user->save();
            $url           = config('app.url');
            $savep = $user->save();
            if (isset($savep))
            {
                flash('Usuario editado')->success();
                return redirect('/users/');
            }
            else
            {
                flash('Ocurrió un problema al editar el usuario')
                    ->warning();
                return redirect()
                    ->back()
                    ->withInput($request->input());
            }
        }
    }
    public function deleteuser(Request $request)
    {
        $user = User::find($request->id);
        try
        {
            $saved = $user->delete();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }

    public function indexlinked(){
        $users = LinkedUser::all();
        return view('admin.users.linked', compact('users'));
    }
    public function createlinked(){
        return view('admin.users.createlinked');
    }
    public function createlinkedform(Request $request){
        $url      = config('app.url');
        $emailExists=false;
        $emailExists    = LinkedUser::where('email',$request->email)->first();
        if ($emailExists){
            flash('El email ingresado ya existe')->error();
            return redirect('/linked');
        } else {
            $user=new LinkedUser([
                'name'=>$request->name,
                'email'=>$request->email
            ]);
            $ok = $user->save();
            if ($ok) {
                $data = array(
                    'name' => $request->name,
                    'email' => $request->email
                );

                $this->sendMailLinked($data);
                flash('Usuario agregado a médico agremiado con exito')->success();
                return redirect('/linked');
            }else{
                flash('Ocurrió un problema al crear el usuario')->warning();
                return;
            }
        }
    }
    public function editlinked($id)
    {
        $user  = LinkedUser::where('id',$id)->first();
        return view('admin.users.editlinked',compact('user'));
    }
    public function editlinkedform(Request $request, $id)
    {
        $emailExists=false;
        if ($request->email!==$request->actualEmail){
            $emailExists    = LinkedUser::where('email',$request->email)->first();
        }
        if ($emailExists){
            flash('El email ingresado ya existe')->error();
            return redirect('/users');
        } else {
            $user          = LinkedUser::find($id);
            $user->name    = $request->name;
            $user->email    = $request->email;
            $ok            = $user->save();
            $url           = config('app.url');
            $savep = $user->save();
            if (isset($savep))
            {
                flash('Médico agremiado editado')->success();
                return redirect('/linked/');
            }
            else
            {
                flash('Ocurrió un problema al editar el médico agremiado')
                    ->warning();
                return redirect()
                    ->back()
                    ->withInput($request->input());
            }
        }
    }
    public function deletelinked(Request $request)
    {
        $user = LinkedUser::find($request->id);
        try
        {
            $saved = $user->delete();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }

    public function update_linked_status(Request $request)
    {
        $us = User::find($request->id);
        try
        {
            $us->linked = ($request->param=='true') ? 0: 1 ;
            $saved = $us->save();
            if (isset($saved))
            {
                return response()->json(['r' => true]);
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return response()->json(['r' => false, 'm' => $ex->getMessage() ]);
        }
    }

    public function sendMail($data)
    {
        try {
            $titulo = "Bienvenido a INMUNOTEK";
            $msj = "Hola ". $data['name'] .", bienvenido(a) a Inmunotek, sus datos son: <br>";
            $msj .= "<ul>
            <li>Nombre: ". $data['name'] ."</li>
            <li>Email: ". $data['email'] ."</li>
            <li>Rol: ". $data['role'] ."</li>
            </ul>";
            $sendmail = Mail::send('mails.notificaciones', ['title' => $titulo, 'mensaje' => $msj], function ($mail) use ($data) {
                $mail->from('inmunotek@evocompany.co', 'INMUNOTEK');
                $mail->subject('Notificaciones Inmunotek');
                $mail->to($data['email']);

            });

        } catch (\Throwable $th) {
            echo($th);
            return json_encode($th);
        }
    }
    public function sendMailLinked($data)
    {
        try {
            $titulo = "Has sido agregado a médicos agremiados en INMUNOTEK";
            $msj = "Hola ". $data['name'] .", bienvenido(a) a Inmunotek, sus datos son: <br>";
            $msj .= "<ul>
            <li>Nombre: ". $data['name'] ."</li>
            <li>Email: ". $data['email'] ."</li>
            </ul>";
            $sendmail = Mail::send('mails.notificaciones', ['title' => $titulo, 'mensaje' => $msj], function ($mail) use ($data) {
                $mail->from('inmunotek@evocompany.co', 'INMUNOTEK');
                $mail->subject('Notificaciones Inmunotek');
                $mail->to($data['email']);

            });

        } catch (\Throwable $th) {
            echo($th);
            return json_encode($th);
        }
    }
}
