<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Mail;
use App\User;
use Str;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    public function reestablecerPass(Request $request){
        $email= $request['email'];
                $user = User::where('email', $request['email'])->first();
                if ($user){
                    $user->remember_token=Str::random(100);
                    $user->save();
                    $data['email'] = $email;
                    $data['token'] = $user->remember_token;
                    try {
                        $sendmail=Mail::send('mails.restablecerPass',['data'=>$data],function($mail)use($data){
                            $mail->from('inmunotek@evocompany.co', 'INMUNOTEK');
                            $mail->subject('Reestablecer contraseña');
                            $mail->to($data['email']);
    
                        });
    
                        flash('Se ha enviado un correo para restablecer la contraseña, por favor entrar y seguir los pasos.')->success();
                        return view('auth/login');
    
                    } catch (Exception $e) {
    
                        flash('Ocurrio un error intente mas tarde')->error();
                        return view('auth/login');
                    }
    
                    flash('Ocurrio un error intente mas tarde')->error();
                    return view('auth/login');
    
                }else
                 {
                    flash('El email solicitado no se encuentra registrado en la plataforma.')->warning();
                    return view('auth/login');
    
                }
    
            }
    
    

    use SendsPasswordResetEmails;
}
