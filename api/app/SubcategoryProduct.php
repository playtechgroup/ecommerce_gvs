<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\CategoryProduct;

class SubcategoryProduct extends Model
{
    protected $table   = "subcategoryproduct";
    public $timestamps = false;
    protected $fillable = [
        'name','CategoryProduct_id','url_img','active'
    ];

    public function products(){
        return $this->hasMany('App\Product');
       }

    public function categoryproduct(){
        return $this->belongsTo('App\CategoryProduct','CategoryProduct_id');
    }
}
