<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Order;

class OrderStatus extends Model
{
      //
    protected $table    = 'orderstatus';
    public $timestamps  = false;
    protected $fillable = [
       'name'
   ];

   public function orders(){
    return $this->hasMany('App\Order');
    }
}
