<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SubcategoryProduct;
use App\FileProduct;
use App\Order;
use App\Shipping;

class Product extends Model
{
    //
    protected $table   = "product";
    public $timestamps = false;
    protected $fillable = [
       'code','name','description','price','tax','SubcategoryProduct_id','Provider_id','stock','minimumStock','date_create','active'
    ];

    public function subcategoryproduct(){
        return $this->belongsTo('App\SubcategoryProduct', 'SubcategoryProduct_id');
       }


     public function orders()
     {
         return $this->belongsToMany('App\Order','order_has_product');
     }



    public function provider(){
        return $this->belongsTo('App\Provider', 'Provider_id');
    }

    public function fileproducts(){
        return $this->hasMany('App\FileProduct','Product_id');
    }

    public function shipping(){
        return $this->hasMany('App\Shipping','product_id');
    }

    public function categoryproduct(){
        return $this->belongsTo('App\CategoryProduct','CategoryProduct_id');
    }

}
