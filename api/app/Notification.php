<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Costumer;

class Notification extends Model
{
     //
     protected $table = 'notification';
     public $timestamps = false;
     protected $fillable = [
          'message','state','date','Costumer_id'
      ];

      public function costumer(){
        return $this->belongsTo('App\Costumer','Costumer_id');
    }
}
