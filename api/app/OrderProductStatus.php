<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProductStatus extends Model
{
    protected $table    = 'orderproductstatus';
    public $timestamps  = false;
    protected $fillable = [
       'name'
   ];
}
