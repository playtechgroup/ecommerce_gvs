<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
     //
     protected $table='role';
     public $timestamps = false;
     protected $fillable = [
         'name'
     ];

    public function users(){
        return $this->hasMany('App\User');
       }
}
