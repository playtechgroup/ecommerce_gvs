<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $table   = "events";
    public $timestamps = false;
    protected $fillable = [
        'title','date','description','event_type_id','photo'
    ];

    public function eventType(){
        return $this->belongsTo('App\EventType','event_type_id');
    }
}
