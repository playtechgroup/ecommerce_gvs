<?php

namespace App;

use App\Department;
use App\Costumer;
use App\City;
use App\TypeIdentification;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{
   //
   protected $table='city';
   public $timestamps = false;
   protected $fillable = [
       'name','Department_id'
   ];

   public function deparment(){
    return $this->belongsto('App\Department', 'Department_id');
   }

   public function costumers(){
    return $this->hasMany('App\Costumer');
   }

   public function city(){
    return $this->belongsto('App\City', 'City_id');
   }

   public function typeidentification(){
    return $this->belongsto('App\TypeIdentification', 'TypeIdentification_id');
   }

   public function providers(){
    return $this->hasMany('App\Provider');
   }

}
