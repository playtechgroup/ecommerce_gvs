<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use App\Role;
use App\Costumer;
use App\Provider;


class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','Role_id','token','linked'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','Role_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){
        return $this->belongsTo('App\Role','Rol_id');
    }

    public function rol(){
        return $this->belongsTo('App\Role','Role_id');
    }

    public function costumers(){
        return $this->hasMany('App\Costumer');
    }

    public function providers(){
        return $this->hasMany('App\Provider');
       }

    public function hasRole($role)
    {
        if ($this->rol()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }
}
