<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping_method extends Model
{
    //

    protected $table    = 'shippingmethod';
    public $timestamps  = false;
    protected $fillable = [
         'name','description'
    ];
}
