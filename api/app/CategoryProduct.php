<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SubcategoryProduct;

class CategoryProduct extends Model
{
    protected $table   = "categoryproduct";
    public $timestamps = false;
    protected $fillable = [
       'name','url_img','active'
    ];

    public function subcategoryproducts(){
        return $this->hasMany('App\SubcategoryProduct', 'CategoryProduct_id');
    }
}
