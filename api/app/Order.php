<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Costumer;
use App\Payment_method;
use App\OrderStatus;
use App\Bill;
use App\Address;
class Order extends Model
{
     //
    protected $table = 'order';
    public $timestamps = false;
    protected $fillable = [
         'date','Costumer_id','Payment_method_id','OrderStatus_id','commentary','Address_id', 'shipping_value'
     ];

     public function products()
     {
         return $this->belongsToMany('App\Product','order_has_product');
     }

    public function costumer(){
        return $this->belongsTo('App\Costumer','Costumer_id');
    }

    public function address(){
        return $this->belongsTo('App\Address','Address_id');
    }

    public function paymentmethod(){
        return $this->belongsTo('App\Payment_method','Payment_method_id');
    }

    public function orderstatus(){
        return $this->belongsTo('App\OrderStatus','OrderStatus_id');
    }

    public function bills(){
        return $this->hasMany('App\Bill','order_id');
        }
}
