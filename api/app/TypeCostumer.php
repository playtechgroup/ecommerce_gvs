<?php

namespace App;
use App\Customer;

use Illuminate\Database\Eloquent\Model;

class TypeCostumer extends Model
{
    protected $table='typecostumer';
    public $timestamps = false;
    protected $fillable = [
        'name'
    ];

    public function customers(){
          return $this->hasMany('App\Customer');
    }
}
