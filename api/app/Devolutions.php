<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devolutions extends Model
{
    protected $table   = "devolutions";
    public $timestamps = false;
    protected $fillable = [
        'reason','date','products'
    ];
}
