-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-03-2022 a las 15:30:24
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inmunotek`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `Costumer_id` int(11) NOT NULL,
  `City_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `address`
--

INSERT INTO `address` (`id`, `name`, `address`, `Costumer_id`, `City_id`) VALUES
(1, 'casa', 'cali valle', 1, 1009),
(30, 'dasdas', '423423443', 2, 153),
(31, 'Hector Arteaga', 'Urb Libertador II', 2, 154),
(32, 'Casa', 'Cra 83c #17-52', 1, 1031);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bill`
--

CREATE TABLE `bill` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `value` double NOT NULL,
  `coin` varchar(45) NOT NULL,
  `BillStatus_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `bill`
--

INSERT INTO `bill` (`id`, `order_id`, `value`, `coin`, `BillStatus_id`, `date`) VALUES
(1, 24, 50000, 'COP', 4, '2022-01-28 03:25:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `billstatus`
--

CREATE TABLE `billstatus` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `billstatus`
--

INSERT INTO `billstatus` (`id`, `name`) VALUES
(4, 'Approved'),
(5, 'Expired'),
(6, 'Declined');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoryproduct`
--

CREATE TABLE `categoryproduct` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `url_img` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `categoryproduct`
--

INSERT INTO `categoryproduct` (`id`, `name`, `url_img`, `active`) VALUES
(9, 'Prick Test', 'https://inmunotek.evocompany.co/img/categories_product/9.jpg', 1),
(10, 'ALXOID', 'https://inmunotek.evocompany.co/img/categories_product/10.jpg', 1),
(11, 'ORALTEK', 'https://inmunotek.evocompany.co/img/categories_product/11.jpg', 1),
(12, 'PRUEBAS DE PROVOCACION', 'https://inmunotek.evocompany.co/img/categories_product/12.jpg', 1),
(13, 'VENENOS', 'https://inmunotek.evocompany.co/img/categories_product/13.jpg', 1),
(14, 'PRICK FILM', 'https://inmunotek.evocompany.co/img/categories_product/14.jpg', 1),
(15, 'ALUTEK', 'https://inmunotek.evocompany.co/img/categories_product/15.jpg', 1),
(16, 'HAPTENOS', 'https://inmunotek.evocompany.co/img/categories_product/16.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `Department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `city`
--

INSERT INTO `city` (`id`, `name`, `Department_id`) VALUES
(1, 'Medellín', 1),
(2, 'Abejorral', 1),
(3, 'Abriaquí', 1),
(4, 'Alejandría', 1),
(5, 'Amagá', 1),
(6, 'Amalfi', 1),
(7, 'Andes', 1),
(8, 'Angelópolis', 1),
(9, 'Angostura', 1),
(10, 'Anorí', 1),
(11, 'Anza', 1),
(12, 'Apartadó', 1),
(13, 'Arboletes', 1),
(14, 'Argelia', 1),
(15, 'Armenia', 1),
(16, 'Barbosa', 1),
(17, 'Bello', 1),
(18, 'Betania', 1),
(19, 'Betulia', 1),
(20, 'Ciudad Bolívar', 1),
(21, 'Briceño', 1),
(22, 'Buriticá', 1),
(23, 'Cáceres', 1),
(24, 'Caicedo', 1),
(25, 'Caldas', 1),
(26, 'Campamento', 1),
(27, 'Cañasgordas', 1),
(28, 'Caracolí', 1),
(29, 'Caramanta', 1),
(30, 'Carepa', 1),
(31, 'Carolina', 1),
(32, 'Caucasia', 1),
(33, 'Chigorodó', 1),
(34, 'Cisneros', 1),
(35, 'Cocorná', 1),
(36, 'Concepción', 1),
(37, 'Concordia', 1),
(38, 'Copacabana', 1),
(39, 'Dabeiba', 1),
(40, 'Don Matías', 1),
(41, 'Ebéjico', 1),
(42, 'El Bagre', 1),
(43, 'Entrerrios', 1),
(44, 'Envigado', 1),
(45, 'Fredonia', 1),
(46, 'Giraldo', 1),
(47, 'Girardota', 1),
(48, 'Gómez Plata', 1),
(49, 'Guadalupe', 1),
(50, 'Guarne', 1),
(51, 'Guatapé', 1),
(52, 'Heliconia', 1),
(53, 'Hispania', 1),
(54, 'Itagui', 1),
(55, 'Ituango', 1),
(56, 'Jericó', 1),
(57, 'La Ceja', 1),
(58, 'La Estrella', 1),
(59, 'La Pintada', 1),
(60, 'La Unión', 1),
(61, 'Liborina', 1),
(62, 'Maceo', 1),
(63, 'Marinilla', 1),
(64, 'Montebello', 1),
(65, 'Murindó', 1),
(66, 'Mutatá', 1),
(67, 'Nariño', 1),
(68, 'Necoclí', 1),
(69, 'Nechí', 1),
(70, 'Olaya', 1),
(71, 'Peñol', 1),
(72, 'Peque', 1),
(73, 'Pueblorrico', 1),
(74, 'Puerto Berrío', 1),
(75, 'Puerto Nare', 1),
(76, 'Puerto Triunfo', 1),
(77, 'Remedios', 1),
(78, 'Retiro', 1),
(79, 'Rionegro', 1),
(80, 'Sabanalarga', 1),
(81, 'Sabaneta', 1),
(82, 'Salgar', 1),
(83, 'San Francisco', 1),
(84, 'San Jerónimo', 1),
(85, 'San Luis', 1),
(86, 'San Pedro', 1),
(87, 'San Rafael', 1),
(88, 'San Roque', 1),
(89, 'San Vicente', 1),
(90, 'Santa Bárbara', 1),
(91, 'Santo Domingo', 1),
(92, 'El Santuario', 1),
(93, 'Segovia', 1),
(94, 'Sopetrán', 1),
(95, 'Támesis', 1),
(96, 'Tarazá', 1),
(97, 'Tarso', 1),
(98, 'Titiribí', 1),
(99, 'Toledo', 1),
(100, 'Turbo', 1),
(101, 'Uramita', 1),
(102, 'Urrao', 1),
(103, 'Valdivia', 1),
(104, 'Valparaíso', 1),
(105, 'Vegachí', 1),
(106, 'Venecia', 1),
(107, 'Yalí', 1),
(108, 'Yarumal', 1),
(109, 'Yolombó', 1),
(110, 'Yondó', 1),
(111, 'Zaragoza', 1),
(112, 'Frontino', 1),
(113, 'Granada', 1),
(114, 'Jardín', 1),
(115, 'Sonsón', 1),
(116, 'Belmira', 1),
(117, 'San Pedro de Uraba', 1),
(118, 'Santafé de Antioquia', 1),
(119, 'Santa Rosa de Osos', 1),
(120, 'San Andrés de Cuerquía', 1),
(121, 'Vigía del Fuerte', 1),
(122, 'San José de La Montaña', 1),
(123, 'San Juan de Urabá', 1),
(124, 'El Carmen de Viboral', 1),
(125, 'San Carlos', 1),
(126, 'Barranquilla', 2),
(127, 'Baranoa', 2),
(128, 'Candelaria', 2),
(129, 'Galapa', 2),
(130, 'Luruaco', 2),
(131, 'Malambo', 2),
(132, 'Manatí', 2),
(133, 'Piojó', 2),
(134, 'Polonuevo', 2),
(135, 'Sabanagrande', 2),
(136, 'Sabanalarga', 2),
(137, 'Santa Lucía', 2),
(138, 'Santo Tomás', 2),
(139, 'Soledad', 2),
(140, 'Suan', 2),
(141, 'Tubará', 2),
(142, 'Usiacurí', 2),
(143, 'Repelón', 2),
(144, 'Puerto Colombia', 2),
(145, 'Ponedera', 2),
(146, 'Juan de Acosta', 2),
(147, 'Palmar de Varela', 2),
(148, 'Campo de La Cruz', 2),
(149, 'Bogotá D.C.', 3),
(150, 'El Peñón', 4),
(151, 'Achí', 4),
(152, 'Arenal', 4),
(153, 'Arjona', 4),
(154, 'Arroyohondo', 4),
(155, 'Calamar', 4),
(156, 'Cantagallo', 4),
(157, 'Cicuco', 4),
(158, 'Córdoba', 4),
(159, 'Clemencia', 4),
(160, 'El Guamo', 4),
(161, 'Magangué', 4),
(162, 'Mahates', 4),
(163, 'Margarita', 4),
(164, 'Montecristo', 4),
(165, 'Mompós', 4),
(166, 'Morales', 4),
(167, 'Norosí', 4),
(168, 'Pinillos', 4),
(169, 'Regidor', 4),
(170, 'Río Viejo', 4),
(171, 'San Estanislao', 4),
(172, 'San Fernando', 4),
(173, 'San Juan Nepomuceno', 4),
(174, 'Santa Catalina', 4),
(175, 'Santa Rosa', 4),
(176, 'Simití', 4),
(177, 'Soplaviento', 4),
(178, 'Talaigua Nuevo', 4),
(179, 'Tiquisio', 4),
(180, 'Turbaco', 4),
(181, 'Turbaná', 4),
(182, 'Villanueva', 4),
(183, 'Cartagena', 4),
(184, 'María la Baja', 4),
(185, 'San Cristóbal', 4),
(186, 'Zambrano', 4),
(187, 'Barranco de Loba', 4),
(188, 'Santa Rosa del Sur', 4),
(189, 'Hatillo de Loba', 4),
(190, 'El Carmen de Bolívar', 4),
(191, 'San Martín de Loba', 4),
(192, 'Altos del Rosario', 4),
(193, 'San Jacinto del Cauca', 4),
(194, 'San Pablo de Borbur', 4),
(195, 'San Jacinto', 4),
(196, 'Tibasosa', 5),
(197, 'Tunja', 5),
(198, 'Almeida', 5),
(199, 'Aquitania', 5),
(200, 'Arcabuco', 5),
(201, 'Berbeo', 5),
(202, 'Betéitiva', 5),
(203, 'Boavita', 5),
(204, 'Boyacá', 5),
(205, 'Briceño', 5),
(206, 'Buena Vista', 5),
(207, 'Busbanzá', 5),
(208, 'Caldas', 5),
(209, 'Campohermoso', 5),
(210, 'Cerinza', 5),
(211, 'Chinavita', 5),
(212, 'Chiquinquirá', 5),
(213, 'Chiscas', 5),
(214, 'Chita', 5),
(215, 'Chitaraque', 5),
(216, 'Chivatá', 5),
(217, 'Cómbita', 5),
(218, 'Coper', 5),
(219, 'Corrales', 5),
(220, 'Covarachía', 5),
(221, 'Cubará', 5),
(222, 'Cucaita', 5),
(223, 'Cuítiva', 5),
(224, 'Chíquiza', 5),
(225, 'Chivor', 5),
(226, 'Duitama', 5),
(227, 'El Cocuy', 5),
(228, 'El Espino', 5),
(229, 'Firavitoba', 5),
(230, 'Floresta', 5),
(231, 'Gachantivá', 5),
(232, 'Gameza', 5),
(233, 'Garagoa', 5),
(234, 'Guacamayas', 5),
(235, 'Guateque', 5),
(236, 'Guayatá', 5),
(237, 'Güicán', 5),
(238, 'Iza', 5),
(239, 'Jenesano', 5),
(240, 'Jericó', 5),
(241, 'Labranzagrande', 5),
(242, 'La Capilla', 5),
(243, 'La Victoria', 5),
(244, 'Macanal', 5),
(245, 'Maripí', 5),
(246, 'Miraflores', 5),
(247, 'Mongua', 5),
(248, 'Monguí', 5),
(249, 'Moniquirá', 5),
(250, 'Muzo', 5),
(251, 'Nobsa', 5),
(252, 'Nuevo Colón', 5),
(253, 'Oicatá', 5),
(254, 'Otanche', 5),
(255, 'Pachavita', 5),
(256, 'Páez', 5),
(257, 'Paipa', 5),
(258, 'Pajarito', 5),
(259, 'Panqueba', 5),
(260, 'Pauna', 5),
(261, 'Paya', 5),
(262, 'Pesca', 5),
(263, 'Pisba', 5),
(264, 'Puerto Boyacá', 5),
(265, 'Quípama', 5),
(266, 'Ramiriquí', 5),
(267, 'Ráquira', 5),
(268, 'Rondón', 5),
(269, 'Saboyá', 5),
(270, 'Sáchica', 5),
(271, 'Samacá', 5),
(272, 'San Eduardo', 5),
(273, 'San Mateo', 5),
(274, 'Santana', 5),
(275, 'Santa María', 5),
(276, 'Santa Sofía', 5),
(277, 'Sativanorte', 5),
(278, 'Sativasur', 5),
(279, 'Siachoque', 5),
(280, 'Soatá', 5),
(281, 'Socotá', 5),
(282, 'Socha', 5),
(283, 'Sogamoso', 5),
(284, 'Somondoco', 5),
(285, 'Sora', 5),
(286, 'Sotaquirá', 5),
(287, 'Soracá', 5),
(288, 'Susacón', 5),
(289, 'Sutamarchán', 5),
(290, 'Sutatenza', 5),
(291, 'Tasco', 5),
(292, 'Tenza', 5),
(293, 'Tibaná', 5),
(294, 'Tinjacá', 5),
(295, 'Tipacoque', 5),
(296, 'Toca', 5),
(297, 'Tópaga', 5),
(298, 'Tota', 5),
(299, 'Turmequé', 5),
(300, 'Tutazá', 5),
(301, 'Umbita', 5),
(302, 'Ventaquemada', 5),
(303, 'Viracachá', 5),
(304, 'Zetaquira', 5),
(305, 'La Uvita', 5),
(306, 'Belén', 5),
(307, 'Tununguá', 5),
(308, 'Motavita', 5),
(309, 'Ciénega', 5),
(310, 'Togüí', 5),
(311, 'Villa de Leyva', 5),
(312, 'Paz de Río', 5),
(313, 'Santa Rosa de Viterbo', 5),
(314, 'San Pablo de Borbur', 5),
(315, 'San Luis de Gaceno', 5),
(316, 'San José de Pare', 5),
(317, 'San Miguel de Sema', 5),
(318, 'Tuta', 5),
(319, 'Manizales', 6),
(320, 'Aguadas', 6),
(321, 'Anserma', 6),
(322, 'Aranzazu', 6),
(323, 'Belalcázar', 6),
(324, 'Chinchiná', 6),
(325, 'Filadelfia', 6),
(326, 'La Dorada', 6),
(327, 'La Merced', 6),
(328, 'Manzanares', 6),
(329, 'Marmato', 6),
(330, 'Marulanda', 6),
(331, 'Neira', 6),
(332, 'Norcasia', 6),
(333, 'Pácora', 6),
(334, 'Palestina', 6),
(335, 'Pensilvania', 6),
(336, 'Riosucio', 6),
(337, 'Risaralda', 6),
(338, 'Salamina', 6),
(339, 'Samaná', 6),
(340, 'San José', 6),
(341, 'Supía', 6),
(342, 'Victoria', 6),
(343, 'Villamaría', 6),
(344, 'Viterbo', 6),
(345, 'Marquetalia', 6),
(346, 'Milán', 7),
(347, 'Florencia', 7),
(348, 'Albania', 7),
(349, 'Curillo', 7),
(350, 'El Doncello', 7),
(351, 'El Paujil', 7),
(352, 'Morelia', 7),
(353, 'Puerto Rico', 7),
(354, 'La Montañita', 7),
(355, 'San Vicente del Caguán', 7),
(356, 'Solano', 7),
(357, 'Solita', 7),
(358, 'Valparaíso', 7),
(359, 'San José del Fragua', 7),
(360, 'Belén de Los Andaquies', 7),
(361, 'Cartagena del Chairá', 7),
(362, 'Páez', 8),
(363, 'Popayán', 8),
(364, 'Almaguer', 8),
(365, 'Argelia', 8),
(366, 'Balboa', 8),
(367, 'Bolívar', 8),
(368, 'Buenos Aires', 8),
(369, 'Cajibío', 8),
(370, 'Caldono', 8),
(371, 'Caloto', 8),
(372, 'Corinto', 8),
(373, 'El Tambo', 8),
(374, 'Florencia', 8),
(375, 'Guachené', 8),
(376, 'Guapi', 8),
(377, 'Inzá', 8),
(378, 'Jambaló', 8),
(379, 'La Sierra', 8),
(380, 'La Vega', 8),
(381, 'López', 8),
(382, 'Mercaderes', 8),
(383, 'Miranda', 8),
(384, 'Morales', 8),
(385, 'Padilla', 8),
(386, 'Patía', 8),
(387, 'Piamonte', 8),
(388, 'Piendamó', 8),
(389, 'Puerto Tejada', 8),
(390, 'Puracé', 8),
(391, 'Rosas', 8),
(392, 'Santa Rosa', 8),
(393, 'Silvia', 8),
(394, 'Sotara', 8),
(395, 'Suárez', 8),
(396, 'Sucre', 8),
(397, 'Timbío', 8),
(398, 'Timbiquí', 8),
(399, 'Toribio', 8),
(400, 'Totoró', 8),
(401, 'Villa Rica', 8),
(402, 'Santander de Quilichao', 8),
(403, 'San Sebastián', 8),
(404, 'Valledupar', 9),
(405, 'Aguachica', 9),
(406, 'Agustín Codazzi', 9),
(407, 'Astrea', 9),
(408, 'Becerril', 9),
(409, 'Bosconia', 9),
(410, 'Chimichagua', 9),
(411, 'Chiriguaná', 9),
(412, 'Curumaní', 9),
(413, 'El Copey', 9),
(414, 'El Paso', 9),
(415, 'Gamarra', 9),
(416, 'González', 9),
(417, 'La Gloria', 9),
(418, 'Manaure', 9),
(419, 'Pailitas', 9),
(420, 'Pelaya', 9),
(421, 'Pueblo Bello', 9),
(422, 'La Paz', 9),
(423, 'San Alberto', 9),
(424, 'San Diego', 9),
(425, 'San Martín', 9),
(426, 'Tamalameque', 9),
(427, 'Río de Oro', 9),
(428, 'La Jagua de Ibirico', 9),
(429, 'Montelíbano', 10),
(430, 'Montería', 10),
(431, 'Ayapel', 10),
(432, 'Buenavista', 10),
(433, 'Canalete', 10),
(434, 'Cereté', 10),
(435, 'Chimá', 10),
(436, 'Chinú', 10),
(437, 'Cotorra', 10),
(438, 'Lorica', 10),
(439, 'Los Córdobas', 10),
(440, 'Momil', 10),
(441, 'Moñitos', 10),
(442, 'Planeta Rica', 10),
(443, 'Pueblo Nuevo', 10),
(444, 'Puerto Escondido', 10),
(445, 'Purísima', 10),
(446, 'Sahagún', 10),
(447, 'San Andrés Sotavento', 10),
(448, 'San Antero', 10),
(449, 'San Pelayo', 10),
(450, 'Tierralta', 10),
(451, 'Tuchín', 10),
(452, 'Valencia', 10),
(453, 'La Apartada', 10),
(454, 'Puerto Libertador', 10),
(455, 'San Bernardo del Viento', 10),
(456, 'San José de Uré', 10),
(457, 'Ciénaga de Oro', 10),
(458, 'San Carlos', 10),
(459, 'Albán', 11),
(460, 'Anolaima', 11),
(461, 'Chía', 11),
(462, 'El Peñón', 11),
(463, 'Sopó', 11),
(464, 'Gama', 11),
(465, 'Sasaima', 11),
(466, 'Yacopí', 11),
(467, 'Anapoima', 11),
(468, 'Arbeláez', 11),
(469, 'Beltrán', 11),
(470, 'Bituima', 11),
(471, 'Bojacá', 11),
(472, 'Cabrera', 11),
(473, 'Cachipay', 11),
(474, 'Cajicá', 11),
(475, 'Caparrapí', 11),
(476, 'Caqueza', 11),
(477, 'Chaguaní', 11),
(478, 'Chipaque', 11),
(479, 'Choachí', 11),
(480, 'Chocontá', 11),
(481, 'Cogua', 11),
(482, 'Cota', 11),
(483, 'Cucunubá', 11),
(484, 'El Colegio', 11),
(485, 'El Rosal', 11),
(486, 'Fomeque', 11),
(487, 'Fosca', 11),
(488, 'Funza', 11),
(489, 'Fúquene', 11),
(490, 'Gachala', 11),
(491, 'Gachancipá', 11),
(492, 'Gachetá', 11),
(493, 'Girardot', 11),
(494, 'Granada', 11),
(495, 'Guachetá', 11),
(496, 'Guaduas', 11),
(497, 'Guasca', 11),
(498, 'Guataquí', 11),
(499, 'Guatavita', 11),
(500, 'Fusagasugá', 11),
(501, 'Guayabetal', 11),
(502, 'Gutiérrez', 11),
(503, 'Jerusalén', 11),
(504, 'Junín', 11),
(505, 'La Calera', 11),
(506, 'La Mesa', 11),
(507, 'La Palma', 11),
(508, 'La Peña', 11),
(509, 'La Vega', 11),
(510, 'Lenguazaque', 11),
(511, 'Macheta', 11),
(512, 'Madrid', 11),
(513, 'Manta', 11),
(514, 'Medina', 11),
(515, 'Mosquera', 11),
(516, 'Nariño', 11),
(517, 'Nemocón', 11),
(518, 'Nilo', 11),
(519, 'Nimaima', 11),
(520, 'Nocaima', 11),
(521, 'Venecia', 11),
(522, 'Pacho', 11),
(523, 'Paime', 11),
(524, 'Pandi', 11),
(525, 'Paratebueno', 11),
(526, 'Pasca', 11),
(527, 'Puerto Salgar', 11),
(528, 'Pulí', 11),
(529, 'Quebradanegra', 11),
(530, 'Quetame', 11),
(531, 'Quipile', 11),
(532, 'Apulo', 11),
(533, 'Ricaurte', 11),
(534, 'San Bernardo', 11),
(535, 'San Cayetano', 11),
(536, 'San Francisco', 11),
(537, 'Zipaquirá', 11),
(538, 'Sesquilé', 11),
(539, 'Sibaté', 11),
(540, 'Silvania', 11),
(541, 'Simijaca', 11),
(542, 'Soacha', 11),
(543, 'Subachoque', 11),
(544, 'Suesca', 11),
(545, 'Supatá', 11),
(546, 'Susa', 11),
(547, 'Sutatausa', 11),
(548, 'Tabio', 11),
(549, 'Tausa', 11),
(550, 'Tena', 11),
(551, 'Tenjo', 11),
(552, 'Tibacuy', 11),
(553, 'Tibirita', 11),
(554, 'Tocaima', 11),
(555, 'Tocancipá', 11),
(556, 'Topaipí', 11),
(557, 'Ubalá', 11),
(558, 'Ubaque', 11),
(559, 'Une', 11),
(560, 'Útica', 11),
(561, 'Vianí', 11),
(562, 'Villagómez', 11),
(563, 'Villapinzón', 11),
(564, 'Villeta', 11),
(565, 'Viotá', 11),
(566, 'Zipacón', 11),
(567, 'Facatativá', 11),
(568, 'San Juan de Río Seco', 11),
(569, 'Villa de San Diego de Ubate', 11),
(570, 'Guayabal de Siquima', 11),
(571, 'San Antonio del Tequendama', 11),
(572, 'Agua de Dios', 11),
(573, 'Carmen de Carupa', 11),
(574, 'Vergara', 11),
(575, 'Carmen del Darien', 12),
(576, 'Tadó', 12),
(577, 'Quibdó', 12),
(578, 'Acandí', 12),
(579, 'Alto Baudo', 12),
(580, 'Atrato', 12),
(581, 'Bagadó', 12),
(582, 'Bahía Solano', 12),
(583, 'Bajo Baudó', 12),
(584, 'Bojaya', 12),
(585, 'Unión Panamericana', 12),
(586, 'Cértegui', 12),
(587, 'Condoto', 12),
(588, 'Juradó', 12),
(589, 'Lloró', 12),
(590, 'Medio Atrato', 12),
(591, 'Medio Baudó', 12),
(592, 'Medio San Juan', 12),
(593, 'Nóvita', 12),
(594, 'Nuquí', 12),
(595, 'Río Iro', 12),
(596, 'Río Quito', 12),
(597, 'Riosucio', 12),
(598, 'Sipí', 12),
(599, 'Unguía', 12),
(600, 'Istmina', 12),
(601, 'El Litoral del San Juan', 12),
(602, 'El Cantón del San Pablo', 12),
(603, 'El Carmen de Atrato', 12),
(604, 'San José del Palmar', 12),
(605, 'Belén de Bajira', 12),
(606, 'Neiva', 13),
(607, 'Acevedo', 13),
(608, 'Agrado', 13),
(609, 'Aipe', 13),
(610, 'Algeciras', 13),
(611, 'Altamira', 13),
(612, 'Baraya', 13),
(613, 'Campoalegre', 13),
(614, 'Colombia', 13),
(615, 'Elías', 13),
(616, 'Garzón', 13),
(617, 'Gigante', 13),
(618, 'Guadalupe', 13),
(619, 'Hobo', 13),
(620, 'Iquira', 13),
(621, 'Isnos', 13),
(622, 'La Argentina', 13),
(623, 'La Plata', 13),
(624, 'Nátaga', 13),
(625, 'Oporapa', 13),
(626, 'Paicol', 13),
(627, 'Palermo', 13),
(628, 'Palestina', 13),
(629, 'Pital', 13),
(630, 'Pitalito', 13),
(631, 'Rivera', 13),
(632, 'Saladoblanco', 13),
(633, 'Santa María', 13),
(634, 'Suaza', 13),
(635, 'Tarqui', 13),
(636, 'Tesalia', 13),
(637, 'Tello', 13),
(638, 'Teruel', 13),
(639, 'Timaná', 13),
(640, 'Villavieja', 13),
(641, 'Yaguará', 13),
(642, 'San Agustín', 13),
(643, 'Riohacha', 14),
(644, 'Albania', 14),
(645, 'Barrancas', 14),
(646, 'Dibula', 14),
(647, 'Distracción', 14),
(648, 'El Molino', 14),
(649, 'Fonseca', 14),
(650, 'Hatonuevo', 14),
(651, 'Maicao', 14),
(652, 'Manaure', 14),
(653, 'Uribia', 14),
(654, 'Urumita', 14),
(655, 'Villanueva', 14),
(656, 'La Jagua del Pilar', 14),
(657, 'San Juan del Cesar', 14),
(658, 'Santa Bárbara de Pinto', 15),
(659, 'Pueblo Viejo', 15),
(660, 'Santa Marta', 15),
(661, 'Algarrobo', 15),
(662, 'Aracataca', 15),
(663, 'Ariguaní', 15),
(664, 'Cerro San Antonio', 15),
(665, 'Chivolo', 15),
(666, 'Concordia', 15),
(667, 'El Banco', 15),
(668, 'El Piñon', 15),
(669, 'El Retén', 15),
(670, 'Fundación', 15),
(671, 'Guamal', 15),
(672, 'Nueva Granada', 15),
(673, 'Pedraza', 15),
(674, 'Pivijay', 15),
(675, 'Plato', 15),
(676, 'Remolino', 15),
(677, 'Salamina', 15),
(678, 'San Zenón', 15),
(679, 'Santa Ana', 15),
(680, 'Sitionuevo', 15),
(681, 'Tenerife', 15),
(682, 'Zapayán', 15),
(683, 'Zona Bananera', 15),
(684, 'Ciénaga', 15),
(685, 'San Sebastián de Buenavista', 15),
(686, 'Sabanas de San Angel', 15),
(687, 'Pijiño del Carmen', 15),
(688, 'Castilla la Nueva', 16),
(689, 'Villavicencio', 16),
(690, 'Acacias', 16),
(691, 'Cabuyaro', 16),
(692, 'Cubarral', 16),
(693, 'Cumaral', 16),
(694, 'El Calvario', 16),
(695, 'El Castillo', 16),
(696, 'El Dorado', 16),
(697, 'Granada', 16),
(698, 'Guamal', 16),
(699, 'Mapiripán', 16),
(700, 'Mesetas', 16),
(701, 'La Macarena', 16),
(702, 'Uribe', 16),
(703, 'Lejanías', 16),
(704, 'Puerto Concordia', 16),
(705, 'Puerto Gaitán', 16),
(706, 'Puerto López', 16),
(707, 'Puerto Lleras', 16),
(708, 'Puerto Rico', 16),
(709, 'Restrepo', 16),
(710, 'San Juanito', 16),
(711, 'San Martín', 16),
(712, 'Vista Hermosa', 16),
(713, 'Barranca de Upía', 16),
(714, 'Fuente de Oro', 16),
(715, 'San Carlos de Guaroa', 16),
(716, 'San Juan de Arama', 16),
(717, 'Buesaco', 17),
(718, 'San Andrés de Tumaco', 17),
(719, 'Belén', 17),
(720, 'Chachagüí', 17),
(721, 'Arboleda', 17),
(722, 'Pasto', 17),
(723, 'Albán', 17),
(724, 'Aldana', 17),
(725, 'Ancuyá', 17),
(726, 'Barbacoas', 17),
(727, 'Colón', 17),
(728, 'Consaca', 17),
(729, 'Contadero', 17),
(730, 'Córdoba', 17),
(731, 'Cuaspud', 17),
(732, 'Cumbal', 17),
(733, 'Cumbitara', 17),
(734, 'El Charco', 17),
(735, 'El Peñol', 17),
(736, 'El Rosario', 17),
(737, 'El Tambo', 17),
(738, 'Funes', 17),
(739, 'Guachucal', 17),
(740, 'Guaitarilla', 17),
(741, 'Gualmatán', 17),
(742, 'Iles', 17),
(743, 'Imués', 17),
(744, 'Ipiales', 17),
(745, 'La Cruz', 17),
(746, 'La Florida', 17),
(747, 'La Llanada', 17),
(748, 'La Tola', 17),
(749, 'La Unión', 17),
(750, 'Leiva', 17),
(751, 'Linares', 17),
(752, 'Los Andes', 17),
(753, 'Magüí', 17),
(754, 'Mallama', 17),
(755, 'Mosquera', 17),
(756, 'Nariño', 17),
(757, 'Olaya Herrera', 17),
(758, 'Ospina', 17),
(759, 'Francisco Pizarro', 17),
(760, 'Policarpa', 17),
(761, 'Potosí', 17),
(762, 'Providencia', 17),
(763, 'Puerres', 17),
(764, 'Pupiales', 17),
(765, 'Ricaurte', 17),
(766, 'Roberto Payán', 17),
(767, 'Samaniego', 17),
(768, 'Sandoná', 17),
(769, 'San Bernardo', 17),
(770, 'San Lorenzo', 17),
(771, 'San Pablo', 17),
(772, 'Santa Bárbara', 17),
(773, 'Sapuyes', 17),
(774, 'Taminango', 17),
(775, 'Tangua', 17),
(776, 'Santacruz', 17),
(777, 'Túquerres', 17),
(778, 'Yacuanquer', 17),
(779, 'San Pedro de Cartago', 17),
(780, 'El Tablón de Gómez', 17),
(781, 'Pamplona', 18),
(782, 'Pamplonita', 18),
(783, 'Cúcuta', 18),
(784, 'El Carmen', 18),
(785, 'Silos', 18),
(786, 'Cácota', 18),
(787, 'Toledo', 18),
(788, 'Mutiscua', 18),
(789, 'El Zulia', 18),
(790, 'Salazar', 18),
(791, 'Cucutilla', 18),
(792, 'Puerto Santander', 18),
(793, 'Gramalote', 18),
(794, 'El Tarra', 18),
(795, 'Teorama', 18),
(796, 'Arboledas', 18),
(797, 'Lourdes', 18),
(798, 'Bochalema', 18),
(799, 'Convención', 18),
(800, 'Hacarí', 18),
(801, 'Herrán', 18),
(802, 'Tibú', 18),
(803, 'San Cayetano', 18),
(804, 'San Calixto', 18),
(805, 'La Playa', 18),
(806, 'Chinácota', 18),
(807, 'Ragonvalia', 18),
(808, 'La Esperanza', 18),
(809, 'Villa del Rosario', 18),
(810, 'Chitagá', 18),
(811, 'Sardinata', 18),
(812, 'Abrego', 18),
(813, 'Los Patios', 18),
(814, 'Ocaña', 18),
(815, 'Bucarasica', 18),
(816, 'Santiago', 18),
(817, 'Labateca', 18),
(818, 'Cachirá', 18),
(819, 'Villa Caro', 18),
(820, 'Durania', 18),
(821, 'Calarcá', 19),
(822, 'Génova', 19),
(823, 'Armenia', 19),
(824, 'Buenavista', 19),
(825, 'Circasia', 19),
(826, 'Córdoba', 19),
(827, 'Filandia', 19),
(828, 'La Tebaida', 19),
(829, 'Montenegro', 19),
(830, 'Pijao', 19),
(831, 'Quimbaya', 19),
(832, 'Salento', 19),
(833, 'Pereira', 20),
(834, 'Apía', 20),
(835, 'Balboa', 20),
(836, 'Dosquebradas', 20),
(837, 'Guática', 20),
(838, 'La Celia', 20),
(839, 'La Virginia', 20),
(840, 'Marsella', 20),
(841, 'Mistrató', 20),
(842, 'Pueblo Rico', 20),
(843, 'Quinchía', 20),
(844, 'Santuario', 20),
(845, 'Santa Rosa de Cabal', 20),
(846, 'Belén de Umbría', 20),
(847, 'Chimá', 21),
(848, 'Capitanejo', 21),
(849, 'El Peñón', 21),
(850, 'Puerto Wilches', 21),
(851, 'Puerto Parra', 21),
(852, 'Bucaramanga', 21),
(853, 'Aguada', 21),
(854, 'Albania', 21),
(855, 'Aratoca', 21),
(856, 'Barbosa', 21),
(857, 'Barichara', 21),
(858, 'Barrancabermeja', 21),
(859, 'Betulia', 21),
(860, 'Bolívar', 21),
(861, 'Cabrera', 21),
(862, 'California', 21),
(863, 'Carcasí', 21),
(864, 'Cepitá', 21),
(865, 'Cerrito', 21),
(866, 'Charalá', 21),
(867, 'Charta', 21),
(868, 'Chipatá', 21),
(869, 'Cimitarra', 21),
(870, 'Concepción', 21),
(871, 'Confines', 21),
(872, 'Contratación', 21),
(873, 'Coromoro', 21),
(874, 'Curití', 21),
(875, 'El Guacamayo', 21),
(876, 'El Playón', 21),
(877, 'Encino', 21),
(878, 'Enciso', 21),
(879, 'Florián', 21),
(880, 'Floridablanca', 21),
(881, 'Galán', 21),
(882, 'Gambita', 21),
(883, 'Girón', 21),
(884, 'Guaca', 21),
(885, 'Guadalupe', 21),
(886, 'Guapotá', 21),
(887, 'Guavatá', 21),
(888, 'Güepsa', 21),
(889, 'Jesús María', 21),
(890, 'Jordán', 21),
(891, 'La Belleza', 21),
(892, 'Landázuri', 21),
(893, 'La Paz', 21),
(894, 'Lebríja', 21),
(895, 'Los Santos', 21),
(896, 'Macaravita', 21),
(897, 'Málaga', 21),
(898, 'Matanza', 21),
(899, 'Mogotes', 21),
(900, 'Molagavita', 21),
(901, 'Ocamonte', 21),
(902, 'Oiba', 21),
(903, 'Onzaga', 21),
(904, 'Palmar', 21),
(905, 'Páramo', 21),
(906, 'Piedecuesta', 21),
(907, 'Pinchote', 21),
(908, 'Puente Nacional', 21),
(909, 'Rionegro', 21),
(910, 'San Andrés', 21),
(911, 'San Gil', 21),
(912, 'San Joaquín', 21),
(913, 'San Miguel', 21),
(914, 'Santa Bárbara', 21),
(915, 'Simacota', 21),
(916, 'Socorro', 21),
(917, 'Suaita', 21),
(918, 'Sucre', 21),
(919, 'Suratá', 21),
(920, 'Tona', 21),
(921, 'Vélez', 21),
(922, 'Vetas', 21),
(923, 'Villanueva', 21),
(924, 'Zapatoca', 21),
(925, 'Palmas del Socorro', 21),
(926, 'San Vicente de Chucurí', 21),
(927, 'San José de Miranda', 21),
(928, 'Santa Helena del Opón', 21),
(929, 'Sabana de Torres', 21),
(930, 'El Carmen de Chucurí', 21),
(931, 'Valle de San José', 21),
(932, 'San Benito', 21),
(933, 'Hato', 21),
(934, 'Sampués', 22),
(935, 'Corozal', 22),
(936, 'Sincelejo', 22),
(937, 'Buenavista', 22),
(938, 'Caimito', 22),
(939, 'Coloso', 22),
(940, 'Coveñas', 22),
(941, 'Chalán', 22),
(942, 'El Roble', 22),
(943, 'Galeras', 22),
(944, 'Guaranda', 22),
(945, 'La Unión', 22),
(946, 'Los Palmitos', 22),
(947, 'Majagual', 22),
(948, 'Morroa', 22),
(949, 'Ovejas', 22),
(950, 'Palmito', 22),
(951, 'San Benito Abad', 22),
(952, 'San Marcos', 22),
(953, 'San Onofre', 22),
(954, 'San Pedro', 22),
(955, 'Sucre', 22),
(956, 'Tolú Viejo', 22),
(957, 'San Luis de Sincé', 22),
(958, 'San Juan de Betulia', 22),
(959, 'Santiago de Tolú', 22),
(960, 'Casabianca', 23),
(961, 'Anzoátegui', 23),
(962, 'Ibagué', 23),
(963, 'Líbano', 23),
(964, 'Lérida', 23),
(965, 'Suárez', 23),
(966, 'Alpujarra', 23),
(967, 'Alvarado', 23),
(968, 'Ambalema', 23),
(969, 'Armero', 23),
(970, 'Ataco', 23),
(971, 'Cajamarca', 23),
(972, 'Chaparral', 23),
(973, 'Coello', 23),
(974, 'Coyaima', 23),
(975, 'Cunday', 23),
(976, 'Dolores', 23),
(977, 'Espinal', 23),
(978, 'Falan', 23),
(979, 'Flandes', 23),
(980, 'Fresno', 23),
(981, 'Guamo', 23),
(982, 'Herveo', 23),
(983, 'Honda', 23),
(984, 'Icononzo', 23),
(985, 'Mariquita', 23),
(986, 'Melgar', 23),
(987, 'Murillo', 23),
(988, 'Natagaima', 23),
(989, 'Ortega', 23),
(990, 'Palocabildo', 23),
(991, 'Piedras', 23),
(992, 'Planadas', 23),
(993, 'Prado', 23),
(994, 'Purificación', 23),
(995, 'Rio Blanco', 23),
(996, 'Roncesvalles', 23),
(997, 'Rovira', 23),
(998, 'Saldaña', 23),
(999, 'Santa Isabel', 23),
(1000, 'Venadillo', 23),
(1001, 'Villahermosa', 23),
(1002, 'Villarrica', 23),
(1003, 'Valle de San Juan', 23),
(1004, 'Carmen de Apicala', 23),
(1005, 'San Luis', 23),
(1006, 'San Antonio', 23),
(1007, 'Tuluá', 24),
(1008, 'Florida', 24),
(1009, 'Jamundí', 24),
(1010, 'Buenaventura', 24),
(1011, 'El Dovio', 24),
(1012, 'Roldanillo', 24),
(1013, 'Argelia', 24),
(1014, 'Sevilla', 24),
(1015, 'Zarzal', 24),
(1016, 'El Cerrito', 24),
(1017, 'Cartago', 24),
(1018, 'Caicedonia', 24),
(1019, 'El Cairo', 24),
(1020, 'La Unión', 24),
(1021, 'Restrepo', 24),
(1022, 'Dagua', 24),
(1023, 'Guacarí', 24),
(1024, 'Ansermanuevo', 24),
(1025, 'Bugalagrande', 24),
(1026, 'La Victoria', 24),
(1027, 'Ginebra', 24),
(1028, 'Yumbo', 24),
(1029, 'Obando', 24),
(1030, 'Bolívar', 24),
(1031, 'Cali', 24),
(1032, 'San Pedro', 24),
(1033, 'Guadalajara de Buga', 24),
(1034, 'Calima', 24),
(1035, 'Andalucía', 24),
(1036, 'Pradera', 24),
(1037, 'Yotoco', 24),
(1038, 'Palmira', 24),
(1039, 'Riofrío', 24),
(1040, 'Alcalá', 24),
(1041, 'Versalles', 24),
(1042, 'El Águila', 24),
(1043, 'Toro', 24),
(1044, 'Candelaria', 24),
(1045, 'La Cumbre', 24),
(1046, 'Ulloa', 24),
(1047, 'Trujillo', 24),
(1048, 'Vijes', 24),
(1049, 'Arauquita', 25),
(1050, 'Cravo Norte', 25),
(1051, 'Fortul', 25),
(1052, 'Puerto Rondón', 25),
(1053, 'Saravena', 25),
(1054, 'Tame', 25),
(1055, 'Arauca', 25),
(1056, 'Nunchía', 26),
(1057, 'Maní', 26),
(1058, 'Támara', 26),
(1059, 'Orocué', 26),
(1060, 'Yopal', 26),
(1061, 'Aguazul', 26),
(1062, 'Chámeza', 26),
(1063, 'Hato Corozal', 26),
(1064, 'La Salina', 26),
(1065, 'Monterrey', 26),
(1066, 'Pore', 26),
(1067, 'Recetor', 26),
(1068, 'Sabanalarga', 26),
(1069, 'Sácama', 26),
(1070, 'Tauramena', 26),
(1071, 'Trinidad', 26),
(1072, 'Villanueva', 26),
(1073, 'San Luis de Gaceno', 26),
(1074, 'Paz de Ariporo', 26),
(1075, 'Puerto Asís', 27),
(1076, 'Villagarzón', 27),
(1077, 'Mocoa', 27),
(1078, 'Colón', 27),
(1079, 'Orito', 27),
(1080, 'Puerto Caicedo', 27),
(1081, 'Puerto Guzmán', 27),
(1082, 'Leguízamo', 27),
(1083, 'Sibundoy', 27),
(1084, 'San Francisco', 27),
(1085, 'San Miguel', 27),
(1086, 'Santiago', 27),
(1087, 'Valle de Guamez', 27),
(1088, 'Providencia', 28),
(1089, 'San Andrés', 28),
(1090, 'Miriti Paraná', 29),
(1091, 'Leticia', 29),
(1092, 'El Encanto', 29),
(1093, 'La Chorrera', 29),
(1094, 'La Pedrera', 29),
(1095, 'La Victoria', 29),
(1096, 'Puerto Arica', 29),
(1097, 'Puerto Nariño', 29),
(1098, 'Puerto Santander', 29),
(1099, 'Tarapacá', 29),
(1100, 'Puerto Alegría', 29),
(1101, 'Inírida', 30),
(1102, 'Barranco Minas', 30),
(1103, 'Mapiripana', 30),
(1104, 'San Felipe', 30),
(1105, 'Puerto Colombia', 30),
(1106, 'La Guadalupe', 30),
(1107, 'Cacahual', 30),
(1108, 'Pana Pana', 30),
(1109, 'Morichal', 30),
(1110, 'Calamar', 31),
(1111, 'San José del Guaviare', 31),
(1112, 'Miraflores', 31),
(1113, 'El Retorno', 31),
(1114, 'Yavaraté', 32),
(1115, 'Mitú', 32),
(1116, 'Caruru', 32),
(1117, 'Pacoa', 32),
(1118, 'Taraira', 32),
(1119, 'Papunaua', 32),
(1120, 'Puerto Carreño', 33),
(1121, 'La Primavera', 33),
(1122, 'Santa Rosalía', 33),
(1123, 'Cumaribo', 33);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costumer`
--

CREATE TABLE `costumer` (
  `id` int(11) NOT NULL,
  `address` varchar(50) NOT NULL,
  `identification` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `users_id` bigint(20) UNSIGNED NOT NULL,
  `TypeIdentification_id` int(11) NOT NULL,
  `TypeCostumer_id` int(11) NOT NULL,
  `City_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `costumer`
--

INSERT INTO `costumer` (`id`, `address`, `identification`, `phone`, `occupation`, `users_id`, `TypeIdentification_id`, `TypeCostumer_id`, `City_id`) VALUES
(1, 'cali', '1151947929', '3157164899', 'estudio', 58, 1, 2, 1031),
(2, 'San Felipe', '7047097', '988788885', 'Embajador', 64, 1, 2, 128);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'COLOMBIA'),
(2, 'CHINA'),
(3, 'TAIWAN'),
(4, 'MEXICO'),
(5, 'ESTADOS UNIDOS'),
(6, 'ALEMANIA'),
(7, 'BRASIL'),
(8, 'CANADA'),
(9, 'REINO UNIDO (INGLATERRA)'),
(10, 'ESPAÑA'),
(11, 'FINLANDIA'),
(12, 'FRANCIA'),
(13, 'PAISES BAJOS (HOLANDA)'),
(14, 'INDIA'),
(15, 'CHILE'),
(16, 'VENEZUELA'),
(17, 'ECUADOR'),
(18, 'BOLIVIA'),
(19, 'PORTUGAL'),
(20, 'PERU'),
(21, 'MEXICO'),
(22, 'ITALIA'),
(23, 'KOREA'),
(24, 'POLONIA'),
(25, 'TURKIYE'),
(26, 'PANAMA'),
(27, 'SIN CIUDAD'),
(28, 'ARUBA'),
(29, 'NETHERLANDS ANTILLES'),
(30, 'SAUDI ARABIA'),
(31, 'ARGELIA'),
(32, 'ARGENTINA'),
(33, 'URUGUAY'),
(34, 'AUSTRALIA'),
(35, 'BELGICA'),
(36, 'COSTA DE MARFIL'),
(37, 'COSTA RICA'),
(38, 'CUBA'),
(39, 'BENIN'),
(40, 'EL SALVADOR'),
(41, 'UNITED ARAB EMIRATES'),
(42, 'ESPANA'),
(43, 'GRECIA'),
(44, 'GUATEMALA'),
(45, 'GUYANA'),
(46, 'HAITI'),
(47, 'HOLANDA'),
(48, 'HONG KONG'),
(49, 'IRLANDA(EIRE)'),
(50, 'ISRAEL'),
(51, 'JAMAICA'),
(52, 'KENIA'),
(53, 'KUWAIT'),
(54, 'LITHUANIA'),
(55, 'MAROCCO'),
(56, 'CHILE'),
(57, 'UNITED ARAB EMIRATES'),
(58, 'PUERTO RICO'),
(59, 'REPUBLICA CHECA'),
(60, 'REPUBLICA DOMINICANA'),
(61, 'SENEGAL'),
(62, 'SOUTH AFRICA'),
(63, 'SWEDEN'),
(64, 'SUIZA'),
(65, 'COLOMBIA'),
(66, 'VENEZUELA'),
(67, 'TOGO'),
(68, 'ZAMBIA'),
(69, 'USA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `Country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `department`
--

INSERT INTO `department` (`id`, `name`, `Country_id`) VALUES
(1, 'ANTIOQUIA', 1),
(2, 'ATLANTICO', 1),
(3, 'BOGOTA', 1),
(4, 'BOLIVAR', 1),
(5, 'BOYACA', 1),
(6, 'CALDAS', 1),
(7, 'CAQUETA', 1),
(8, 'CAUCA', 1),
(9, 'CESAR', 1),
(10, 'CORDOBA', 1),
(11, 'CUNDINAMARCA', 1),
(12, 'CHOCO', 1),
(13, 'HUILA', 1),
(14, 'LA GUAJIRA', 1),
(15, 'MAGDALENA', 1),
(16, 'META', 1),
(17, 'NARIÑO', 1),
(18, 'NORTE SANTANDER', 1),
(19, 'QUINDIO', 1),
(20, 'RISARALDA', 1),
(21, 'SANTANDER', 1),
(22, 'SUCRE', 1),
(23, 'TOLIMA', 1),
(24, 'VALLE', 1),
(25, 'ARAUCA', 1),
(26, 'CASANARE', 1),
(27, 'PUTUMAYO', 1),
(28, 'SAN ANDRES', 1),
(29, 'AMAZONAS', 1),
(30, 'GUAINIA', 1),
(31, 'GUAVIARE', 1),
(32, 'VAUPES', 1),
(33, 'VICHADA', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devolutions`
--

CREATE TABLE `devolutions` (
  `id` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `products` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `devolutions`
--

INSERT INTO `devolutions` (`id`, `reason`, `date`, `products`, `active`) VALUES
(1, 'Mal Estado', '2021-07-14', '{\"elements\":[{\"product\":\"1\",\"productName\":\"ALFA-LACTOALBUMINA\",\"quantity\":\"2\"}]}', 1),
(7, 'producto vencido', '2021-07-07', '{\"elements\":[{\"product\":\"2\",\"productName\":\"ALMENDRA\",\"quantity\":\"2\"},{\"product\":\"6\",\"productName\":\"ALTERNARIA ALTERNATA\",\"quantity\":\"2\"}]}', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `event_type_id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `photo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `events`
--

INSERT INTO `events` (`id`, `title`, `date`, `description`, `event_type_id`, `active`, `photo`) VALUES
(1, 'Seminario Actualización de Alergias', '2021-07-16', '<p>Seminario Actualizaci&oacute;n de Alergias</p>', 1, 1, 'http://127.0.0.1:8000/img/notices/1.jpeg'),
(2, 'as,lams', '2022-02-25', '<p>asasas</p>', 1, 1, 'http://127.0.0.1:8000/img/events/2.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `event_type`
--

CREATE TABLE `event_type` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `event_type`
--

INSERT INTO `event_type` (`id`, `description`) VALUES
(1, 'Virtual');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fileproduct`
--

CREATE TABLE `fileproduct` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `link` longtext NOT NULL,
  `Product_id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `fileproduct`
--

INSERT INTO `fileproduct` (`id`, `name`, `link`, `Product_id`, `type`) VALUES
(56, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/56/Grupo-Alimentos.png', 56, 'photo'),
(63, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/63/Grupo-Alimentos.png', 63, 'photo'),
(92, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/87/ALXOID.jpg', 87, 'photo'),
(93, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/89/ALXOID.jpg', 89, 'photo'),
(94, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/88/ALXOID.jpg', 88, 'photo'),
(95, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/86/ALXOID.jpg', 86, 'photo'),
(96, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/85/ALXOID.jpg', 85, 'photo'),
(97, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/84/ALXOID.jpg', 84, 'photo'),
(98, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/83/ALXOID.jpg', 83, 'photo'),
(99, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/82/ALXOID.jpg', 82, 'photo'),
(100, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/81/ALXOID.jpg', 81, 'photo'),
(101, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/80/ALXOID.jpg', 80, 'photo'),
(103, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/9/ORALTEK.jpg', 9, 'photo'),
(110, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/91/ORALTEK.png', 91, 'photo'),
(111, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/92/ORALTEK.png', 92, 'photo'),
(112, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/93/ORALTEK.png', 93, 'photo'),
(113, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/94/ORALTEK.png', 94, 'photo'),
(114, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/95/ORALTEK.png', 95, 'photo'),
(115, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/96/ORALTEK.png', 96, 'photo'),
(116, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/97/ORALTEK.png', 97, 'photo'),
(117, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/98/ORALTEK.png', 98, 'photo'),
(118, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/99/ORALTEK.png', 99, 'photo'),
(119, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/90/ALUTEK.jpg', 90, 'photo'),
(120, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/89/16112339_2021000545.pdf', 89, 'files'),
(121, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/89/Anexo_II_Ficha Ténica ALXOID (2).pdf', 89, 'files'),
(122, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/80/16112339_2021000545.pdf', 80, 'files'),
(123, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/80/Anexo_II_Ficha Ténica ALXOID (2).pdf', 80, 'files'),
(124, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/82/16112339_2021000545.pdf', 82, 'files'),
(125, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/82/Anexo_II_Ficha Ténica ALXOID (2).pdf', 82, 'files'),
(126, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/83/16112339_2021000545.pdf', 83, 'files'),
(127, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/83/Anexo_II_Ficha Ténica ALXOID (2).pdf', 83, 'files'),
(128, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/86/16112339_2021000545.pdf', 86, 'files'),
(129, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/86/Anexo_II_Ficha Ténica ALXOID (2).pdf', 86, 'files'),
(130, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/87/Anexo_II_Ficha Ténica ALXOID (2).pdf', 87, 'files'),
(131, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/88/16112339_2021000545.pdf', 88, 'files'),
(132, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/88/Anexo_II_Ficha Ténica ALXOID (2).pdf', 88, 'files'),
(133, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/89/2021 Política de devoluciones línea Alergénica.pdf', 89, 'files'),
(134, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/88/2021 Política de devoluciones línea Alergénica.pdf', 88, 'files'),
(135, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/99/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 99, 'files'),
(136, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/99/2021 Política de devoluciones línea Alergénica.pdf', 99, 'files'),
(137, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/98/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 98, 'files'),
(140, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/98/2021 Política de devoluciones línea Alergénica.pdf', 98, 'files'),
(141, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/97/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 97, 'files'),
(142, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/97/2021 Política de devoluciones línea Alergénica.pdf', 97, 'files'),
(143, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/96/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 96, 'files'),
(144, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/96/2021 Política de devoluciones línea Alergénica.pdf', 96, 'files'),
(145, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/95/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 95, 'files'),
(146, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/95/2021 Política de devoluciones línea Alergénica.pdf', 95, 'files'),
(147, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/94/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 94, 'files'),
(148, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/94/2021 Política de devoluciones línea Alergénica.pdf', 94, 'files'),
(149, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/93/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 93, 'files'),
(150, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/93/2021 Política de devoluciones línea Alergénica.pdf', 93, 'files'),
(151, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/92/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 92, 'files'),
(152, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/92/2021 Política de devoluciones línea Alergénica.pdf', 92, 'files'),
(153, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/91/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 91, 'files'),
(154, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/91/2021 Política de devoluciones línea Alergénica.pdf', 91, 'files'),
(155, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/8/ALXOID.jpg', 8, 'photo'),
(156, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/8/16112339_2021000545.pdf', 8, 'files'),
(157, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/8/Anexo_II_Ficha Ténica ALXOID (2).pdf', 8, 'files'),
(158, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/8/2021 Política de devoluciones línea Alergénica.pdf', 8, 'files'),
(159, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/1/PRICK BLANCO.jpg', 1, 'photo'),
(160, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/1/16107664_2021000497.pdf', 1, 'files'),
(161, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/1/SmCP_PRICKS_032016.pdf', 1, 'files'),
(162, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/1/2021 Política de devoluciones línea Alergénica.pdf', 1, 'files'),
(163, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/2/PRICK BLANCO.jpg', 2, 'photo'),
(164, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/2/16107664_2021000497.pdf', 2, 'files'),
(165, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/2/SmCP_PRICKS_032016.pdf', 2, 'files'),
(166, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/2/2021 Política de devoluciones línea Alergénica.pdf', 2, 'files'),
(167, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/3/PRICK BLANCO.jpg', 3, 'photo'),
(168, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/3/16107664_2021000497.pdf', 3, 'files'),
(169, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/3/SmCP_PRICKS_032016.pdf', 3, 'files'),
(170, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/3/2021 Política de devoluciones línea Alergénica.pdf', 3, 'files'),
(171, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/4/16107664_2021000497.pdf', 4, 'files'),
(172, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/4/SmCP_PRICKS_032016.pdf', 4, 'files'),
(173, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/4/2021 Política de devoluciones línea Alergénica.pdf', 4, 'files'),
(174, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/4/PRICK NEGRO.jpg', 4, 'photo'),
(175, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/5/PRICK ROJO.jpg', 5, 'photo'),
(176, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/5/SmCP_PRICKS_032016.pdf', 5, 'files'),
(177, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/5/2021 Política de devoluciones línea Alergénica.pdf', 5, 'files'),
(178, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/6/PRICK AMARILLO.jpg', 6, 'photo'),
(179, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/6/16107664_2021000497.pdf', 6, 'files'),
(180, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/6/SmCP_PRICKS_032016.pdf', 6, 'files'),
(181, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/6/2021 Política de devoluciones línea Alergénica.pdf', 6, 'files'),
(182, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/7/PRICK AMARILLO.jpg', 7, 'photo'),
(183, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/7/SmCP_PRICKS_032016.pdf', 7, 'files'),
(184, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/7/2021 Política de devoluciones línea Alergénica.pdf', 7, 'files'),
(185, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/10/PRICK VERDE.jpg', 10, 'photo'),
(186, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/10/16107664_2021000497.pdf', 10, 'files'),
(187, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/10/SmCP_PRICKS_032016.pdf', 10, 'files'),
(188, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/10/2021 Política de devoluciones línea Alergénica.pdf', 10, 'files'),
(189, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/11/PRICK VERDE.jpg', 11, 'photo'),
(190, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/11/16107664_2021000497.pdf', 11, 'files'),
(191, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/11/SmCP_PRICKS_032016.pdf', 11, 'files'),
(192, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/11/2021 Política de devoluciones línea Alergénica.pdf', 11, 'files'),
(193, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/12/PRICK BLANCO.jpg', 12, 'photo'),
(194, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/12/16107664_2021000497.pdf', 12, 'files'),
(195, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/12/SmCP_PRICKS_032016.pdf', 12, 'files'),
(196, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/12/2021 Política de devoluciones línea Alergénica.pdf', 12, 'files'),
(197, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/13/PRICK BLANCO.jpg', 13, 'photo'),
(198, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/13/SmCP_PRICKS_032016.pdf', 13, 'files'),
(199, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/13/2021 Política de devoluciones línea Alergénica.pdf', 13, 'files'),
(200, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/14/PRICK BLANCO.jpg', 14, 'photo'),
(201, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/14/16107664_2021000497.pdf', 14, 'files'),
(202, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/14/SmCP_PRICKS_032016.pdf', 14, 'files'),
(203, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/14/2021 Política de devoluciones línea Alergénica.pdf', 14, 'files'),
(204, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/16/PRICK BLANCO.jpg', 16, 'photo'),
(205, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/16/16107664_2021000497.pdf', 16, 'files'),
(206, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/16/SmCP_PRICKS_032016.pdf', 16, 'files'),
(207, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/16/2021 Política de devoluciones línea Alergénica.pdf', 16, 'files'),
(208, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/17/PRICK NEGRO.jpg', 17, 'photo'),
(209, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/17/16107664_2021000497.pdf', 17, 'files'),
(210, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/17/SmCP_PRICKS_032016.pdf', 17, 'files'),
(211, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/17/2021 Política de devoluciones línea Alergénica.pdf', 17, 'files'),
(212, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/18/PRICK BLANCO.jpg', 18, 'photo'),
(213, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/18/SmCP_PRICKS_032016.pdf', 18, 'files'),
(214, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/18/2021 Política de devoluciones línea Alergénica.pdf', 18, 'files'),
(215, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/19/PRICK BLANCO.jpg', 19, 'photo'),
(216, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/19/16107664_2021000497.pdf', 19, 'files'),
(217, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/19/SmCP_PRICKS_032016.pdf', 19, 'files'),
(218, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/19/2021 Política de devoluciones línea Alergénica.pdf', 19, 'files'),
(219, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/20/PRICK AMARILLO.jpg', 20, 'photo'),
(220, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/20/SmCP_PRICKS_032016.pdf', 20, 'files'),
(221, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/20/2021 Política de devoluciones línea Alergénica.pdf', 20, 'files'),
(222, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/21/PRICK BLANCO.jpg', 21, 'photo'),
(223, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/21/16107664_2021000497.pdf', 21, 'files'),
(224, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/21/SmCP_PRICKS_032016.pdf', 21, 'files'),
(225, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/21/2021 Política de devoluciones línea Alergénica.pdf', 21, 'files'),
(226, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/22/PRICK BLANCO.jpg', 22, 'photo'),
(227, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/22/SmCP_PRICKS_032016.pdf', 22, 'files'),
(228, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/22/2021 Política de devoluciones línea Alergénica.pdf', 22, 'files'),
(229, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/23/PRICK BLANCO.jpg', 23, 'photo'),
(230, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/23/16107664_2021000497.pdf', 23, 'files'),
(231, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/23/SmCP_PRICKS_032016.pdf', 23, 'files'),
(232, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/23/2021 Política de devoluciones línea Alergénica.pdf', 23, 'files'),
(233, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/24/PRICK BLANCO.jpg', 24, 'photo'),
(234, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/24/16107664_2021000497.pdf', 24, 'files'),
(235, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/24/SmCP_PRICKS_032016.pdf', 24, 'files'),
(236, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/24/2021 Política de devoluciones línea Alergénica.pdf', 24, 'files'),
(237, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/25/PRICK AMARILLO.jpg', 25, 'photo'),
(238, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/25/SmCP_PRICKS_032016.pdf', 25, 'files'),
(239, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/25/2021 Política de devoluciones línea Alergénica.pdf', 25, 'files'),
(240, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/26/PRICK BLANCO.jpg', 26, 'photo'),
(241, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/26/16107664_2021000497.pdf', 26, 'files'),
(242, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/26/SmCP_PRICKS_032016.pdf', 26, 'files'),
(243, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/26/2021 Política de devoluciones línea Alergénica.pdf', 26, 'files'),
(244, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/27/PRICK VERDE.jpg', 27, 'photo'),
(245, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/27/16107664_2021000497.pdf', 27, 'files'),
(246, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/27/SmCP_PRICKS_032016.pdf', 27, 'files'),
(247, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/27/2021 Política de devoluciones línea Alergénica.pdf', 27, 'files'),
(248, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/28/PRICK VERDE.jpg', 28, 'photo'),
(249, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/28/16107664_2021000497.pdf', 28, 'files'),
(250, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/28/SmCP_PRICKS_032016.pdf', 28, 'files'),
(251, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/28/2021 Política de devoluciones línea Alergénica.pdf', 28, 'files'),
(252, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/29/PRICK NEGRO.jpg', 29, 'photo'),
(253, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/29/16107664_2021000497.pdf', 29, 'files'),
(254, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/29/SmCP_PRICKS_032016.pdf', 29, 'files'),
(255, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/29/2021 Política de devoluciones línea Alergénica.pdf', 29, 'files'),
(256, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/30/PRICK NEGRO.jpg', 30, 'photo'),
(257, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/30/16107664_2021000497.pdf', 30, 'files'),
(258, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/30/SmCP_PRICKS_032016.pdf', 30, 'files'),
(259, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/30/2021 Política de devoluciones línea Alergénica.pdf', 30, 'files'),
(260, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/31/PRICK AZUL.jpg', 31, 'photo'),
(261, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/31/16107664_2021000497.pdf', 31, 'files'),
(262, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/31/SmCP_PRICKS_032016.pdf', 31, 'files'),
(263, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/31/2021 Política de devoluciones línea Alergénica.pdf', 31, 'files'),
(264, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/32/PRICK AZUL.jpg', 32, 'photo'),
(265, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/32/16107664_2021000497.pdf', 32, 'files'),
(266, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/32/SmCP_PRICKS_032016.pdf', 32, 'files'),
(267, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/32/2021 Política de devoluciones línea Alergénica.pdf', 32, 'files'),
(268, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/33/PRICK AZUL.jpg', 33, 'photo'),
(269, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/33/16107664_2021000497.pdf', 33, 'files'),
(270, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/33/SmCP_PRICKS_032016.pdf', 33, 'files'),
(271, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/33/2021 Política de devoluciones línea Alergénica.pdf', 33, 'files'),
(272, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/34/PRICK AZUL.jpg', 34, 'photo'),
(273, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/34/SmCP_PRICKS_032016.pdf', 34, 'files'),
(274, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/34/2021 Política de devoluciones línea Alergénica.pdf', 34, 'files'),
(275, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/35/PRICK VERDE.jpg', 35, 'photo'),
(276, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/35/16107664_2021000497.pdf', 35, 'files'),
(277, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/35/SmCP_PRICKS_032016.pdf', 35, 'files'),
(278, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/35/2021 Política de devoluciones línea Alergénica.pdf', 35, 'files'),
(279, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/36/PRICK BLANCO.jpg', 36, 'photo'),
(280, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/36/16107664_2021000497.pdf', 36, 'files'),
(281, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/36/SmCP_PRICKS_032016.pdf', 36, 'files'),
(282, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/36/2021 Política de devoluciones línea Alergénica.pdf', 36, 'files'),
(283, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/37/PRICK BLANCO.jpg', 37, 'photo'),
(284, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/37/16107664_2021000497.pdf', 37, 'files'),
(285, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/37/SmCP_PRICKS_032016.pdf', 37, 'files'),
(286, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/37/2021 Política de devoluciones línea Alergénica.pdf', 37, 'files'),
(287, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/38/PRICK BLANCO.jpg', 38, 'photo'),
(288, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/38/SmCP_PRICKS_032016.pdf', 38, 'files'),
(289, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/38/2021 Política de devoluciones línea Alergénica.pdf', 38, 'files'),
(290, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/39/PRICK BLANCO.jpg', 39, 'photo'),
(291, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/39/16107664_2021000497.pdf', 39, 'files'),
(292, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/39/SmCP_PRICKS_032016.pdf', 39, 'files'),
(293, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/39/2021 Política de devoluciones línea Alergénica.pdf', 39, 'files'),
(294, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/40/PRICK BLANCO.jpg', 40, 'photo'),
(295, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/40/16107664_2021000497.pdf', 40, 'files'),
(296, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/40/SmCP_PRICKS_032016.pdf', 40, 'files'),
(297, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/40/2021 Política de devoluciones línea Alergénica.pdf', 40, 'files'),
(298, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/41/PRICK BLANCO.jpg', 41, 'photo'),
(299, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/41/16107664_2021000497.pdf', 41, 'files'),
(300, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/41/SmCP_PRICKS_032016.pdf', 41, 'files'),
(301, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/41/2021 Política de devoluciones línea Alergénica.pdf', 41, 'files'),
(303, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/42/PRICK BLANCO.jpg', 42, 'photo'),
(304, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/42/16107664_2021000497.pdf', 42, 'files'),
(305, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/42/SmCP_PRICKS_032016.pdf', 42, 'files'),
(306, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/42/2021 Política de devoluciones línea Alergénica.pdf', 42, 'files'),
(307, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/43/PRICK BLANCO.jpg', 43, 'photo'),
(308, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/43/SmCP_PRICKS_032016.pdf', 43, 'files'),
(309, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/43/2021 Política de devoluciones línea Alergénica.pdf', 43, 'files'),
(310, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/44/PRICK BLANCO.jpg', 44, 'photo'),
(311, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/44/16107664_2021000497.pdf', 44, 'files'),
(312, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/44/SmCP_PRICKS_032016.pdf', 44, 'files'),
(313, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/44/2021 Política de devoluciones línea Alergénica.pdf', 44, 'files'),
(314, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/45/PRICK BLANCO.jpg', 45, 'photo'),
(315, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/45/16107664_2021000497.pdf', 45, 'files'),
(316, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/45/SmCP_PRICKS_032016.pdf', 45, 'files'),
(317, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/45/2021 Política de devoluciones línea Alergénica.pdf', 45, 'files'),
(318, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/46/PRICK BLANCO.jpg', 46, 'photo'),
(319, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/46/SmCP_PRICKS_032016.pdf', 46, 'files'),
(320, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/46/2021 Política de devoluciones línea Alergénica.pdf', 46, 'files'),
(321, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/47/PRICK BLANCO.jpg', 47, 'photo'),
(322, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/47/16107664_2021000497.pdf', 47, 'files'),
(323, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/47/SmCP_PRICKS_032016.pdf', 47, 'files'),
(324, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/47/2021 Política de devoluciones línea Alergénica.pdf', 47, 'files'),
(325, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/48/PRICK BLANCO.jpg', 48, 'photo'),
(326, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/48/16107664_2021000497.pdf', 48, 'files'),
(327, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/48/SmCP_PRICKS_032016.pdf', 48, 'files'),
(328, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/48/2021 Política de devoluciones línea Alergénica.pdf', 48, 'files'),
(329, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/49/PRICK BLANCO.jpg', 49, 'photo'),
(330, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/49/16107664_2021000497.pdf', 49, 'files'),
(331, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/49/SmCP_PRICKS_032016.pdf', 49, 'files'),
(332, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/49/2021 Política de devoluciones línea Alergénica.pdf', 49, 'files'),
(333, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/50/PRICK BLANCO.jpg', 50, 'photo'),
(334, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/50/16107664_2021000497.pdf', 50, 'files'),
(335, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/50/SmCP_PRICKS_032016.pdf', 50, 'files'),
(336, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/50/2021 Política de devoluciones línea Alergénica.pdf', 50, 'files'),
(337, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/51/PRICK VERDE.jpg', 51, 'photo'),
(338, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/51/SmCP_PRICKS_032016.pdf', 51, 'files'),
(339, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/51/2021 Política de devoluciones línea Alergénica.pdf', 51, 'files'),
(340, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/52/PRICK VERDE.jpg', 52, 'photo'),
(341, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/52/SmCP_PRICKS_032016.pdf', 52, 'files'),
(342, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/52/2021 Política de devoluciones línea Alergénica.pdf', 52, 'files'),
(344, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/53/PRICK VERDE.jpg', 53, 'photo'),
(345, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/53/16107664_2021000497.pdf', 53, 'files'),
(346, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/53/SmCP_PRICKS_032016.pdf', 53, 'files'),
(347, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/53/2021 Política de devoluciones línea Alergénica.pdf', 53, 'files'),
(349, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/54/PRICK BLANCO.jpg', 54, 'photo'),
(350, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/54/16107664_2021000497.pdf', 54, 'files'),
(351, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/54/SmCP_PRICKS_032016.pdf', 54, 'files'),
(352, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/54/2021 Política de devoluciones línea Alergénica.pdf', 54, 'files'),
(354, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/55/PRICK BLANCO.jpg', 55, 'photo'),
(355, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/55/16107664_2021000497.pdf', 55, 'files'),
(356, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/55/SmCP_PRICKS_032016.pdf', 55, 'files'),
(357, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/55/2021 Política de devoluciones línea Alergénica.pdf', 55, 'files'),
(359, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/57/PRICK BLANCO.jpg', 57, 'photo'),
(360, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/57/16107664_2021000497.pdf', 57, 'files'),
(361, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/57/SmCP_PRICKS_032016.pdf', 57, 'files'),
(362, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/57/2021 Política de devoluciones línea Alergénica.pdf', 57, 'files'),
(364, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/58/PRICK AMARILLO.jpg', 58, 'photo'),
(365, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/58/16107664_2021000497.pdf', 58, 'files'),
(366, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/58/SmCP_PRICKS_032016.pdf', 58, 'files'),
(367, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/58/2021 Política de devoluciones línea Alergénica.pdf', 58, 'files'),
(368, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/59/PRICK BLANCO.jpg', 59, 'photo'),
(369, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/59/16107664_2021000497.pdf', 59, 'files'),
(370, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/59/SmCP_PRICKS_032016.pdf', 59, 'files'),
(371, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/59/2021 Política de devoluciones línea Alergénica.pdf', 59, 'files'),
(374, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/108/Haptenos (2).jpg', 108, 'photo'),
(375, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/109/Haptenos (2).jpg', 109, 'photo'),
(376, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/110/Haptenos (2).jpg', 110, 'photo'),
(377, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/70/PRICK BLANCO.jpg', 70, 'photo'),
(378, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/70/SmCP_PRICKS_032016.pdf', 70, 'files'),
(379, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/70/2021 Política de devoluciones línea Alergénica.pdf', 70, 'files'),
(380, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/69/PRICK VERDE.jpg', 69, 'photo'),
(381, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/69/16107664_2021000497.pdf', 69, 'files'),
(382, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/69/SmCP_PRICKS_032016.pdf', 69, 'files'),
(383, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/69/2021 Política de devoluciones línea Alergénica.pdf', 69, 'files'),
(384, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/68/PRICK AZUL.jpg', 68, 'photo'),
(385, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/68/16107664_2021000497.pdf', 68, 'files'),
(386, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/68/SmCP_PRICKS_032016.pdf', 68, 'files'),
(387, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/68/2021 Política de devoluciones línea Alergénica.pdf', 68, 'files'),
(388, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/67/PRICK AZUL.jpg', 67, 'photo'),
(389, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/67/16107664_2021000497.pdf', 67, 'files'),
(390, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/67/SmCP_PRICKS_032016.pdf', 67, 'files'),
(391, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/67/2021 Política de devoluciones línea Alergénica.pdf', 67, 'files'),
(392, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/66/PRICK BLANCO.jpg', 66, 'photo'),
(393, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/66/SmCP_PRICKS_032016.pdf', 66, 'files'),
(397, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/66/2021 Política de devoluciones línea Alergénica.pdf', 66, 'files'),
(398, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/65/PRICK VERDE.jpg', 65, 'photo'),
(399, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/65/16107664_2021000497.pdf', 65, 'files'),
(400, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/65/SmCP_PRICKS_032016.pdf', 65, 'files'),
(401, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/65/2021 Política de devoluciones línea Alergénica.pdf', 65, 'files'),
(402, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/64/PRICK BLANCO.jpg', 64, 'photo'),
(403, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/64/16107664_2021000497.pdf', 64, 'files'),
(404, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/64/SmCP_PRICKS_032016.pdf', 64, 'files'),
(405, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/64/2021 Política de devoluciones línea Alergénica.pdf', 64, 'files'),
(406, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/62/PRICK VERDE.jpg', 62, 'photo'),
(407, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/62/SmCP_PRICKS_032016.pdf', 62, 'files'),
(408, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/62/2021 Política de devoluciones línea Alergénica.pdf', 62, 'files'),
(409, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/61/PRICK BLANCO.jpg', 61, 'photo'),
(410, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/61/16107664_2021000497.pdf', 61, 'files'),
(411, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/61/SmCP_PRICKS_032016.pdf', 61, 'files'),
(412, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/61/2021 Política de devoluciones línea Alergénica.pdf', 61, 'files'),
(413, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/71/PRICK BLANCO.jpg', 71, 'photo'),
(414, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/71/16107664_2021000497.pdf', 71, 'files'),
(415, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/71/SmCP_PRICKS_032016.pdf', 71, 'files'),
(416, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/71/2021 Política de devoluciones línea Alergénica.pdf', 71, 'files'),
(417, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/72/PRICK BLANCO.jpg', 72, 'photo'),
(418, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/72/16107664_2021000497.pdf', 72, 'files'),
(419, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/72/SmCP_PRICKS_032016.pdf', 72, 'files'),
(420, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/72/2021 Política de devoluciones línea Alergénica.pdf', 72, 'files'),
(421, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/73/PRICK BLANCO.jpg', 73, 'photo'),
(422, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/73/16107664_2021000497.pdf', 73, 'files'),
(423, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/73/SmCP_PRICKS_032016.pdf', 73, 'files'),
(424, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/73/2021 Política de devoluciones línea Alergénica.pdf', 73, 'files'),
(425, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/74/PRICK BLANCO.jpg', 74, 'photo'),
(426, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/74/16107664_2021000497.pdf', 74, 'files'),
(427, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/74/SmCP_PRICKS_032016.pdf', 74, 'files'),
(428, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/74/2021 Política de devoluciones línea Alergénica.pdf', 74, 'files'),
(429, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/75/PRICK BLANCO.jpg', 75, 'photo'),
(430, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/75/16107664_2021000497.pdf', 75, 'files'),
(431, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/75/SmCP_PRICKS_032016.pdf', 75, 'files'),
(432, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/75/2021 Política de devoluciones línea Alergénica.pdf', 75, 'files'),
(433, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/76/PRICK BLANCO.jpg', 76, 'photo'),
(434, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/76/SmCP_PRICKS_032016.pdf', 76, 'files'),
(435, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/76/2021 Política de devoluciones línea Alergénica.pdf', 76, 'files'),
(436, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/77/PRICK BLANCO.jpg', 77, 'photo'),
(437, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/77/16107664_2021000497.pdf', 77, 'files'),
(438, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/77/SmCP_PRICKS_032016.pdf', 77, 'files'),
(439, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/77/2021 Política de devoluciones línea Alergénica.pdf', 77, 'files'),
(440, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/78/PRICK BLANCO.jpg', 78, 'photo'),
(441, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/78/16107664_2021000497.pdf', 78, 'files'),
(442, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/78/SmCP_PRICKS_032016.pdf', 78, 'files'),
(443, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/78/2021 Política de devoluciones línea Alergénica.pdf', 78, 'files'),
(444, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/79/PRICK BLANCO.jpg', 79, 'photo'),
(445, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/79/16107664_2021000497.pdf', 79, 'files'),
(446, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/79/SmCP_PRICKS_032016.pdf', 79, 'files'),
(447, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/79/2021 Política de devoluciones línea Alergénica.pdf', 79, 'files'),
(448, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/80/2021 Política de devoluciones línea Alergénica.pdf', 80, 'files'),
(449, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/106/PRICK FILM ADULTO.jpg', 106, 'photo'),
(451, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/106/2021 Política de devoluciones línea Alergénica.pdf', 106, 'files'),
(452, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/81/Anexo_II_Ficha Ténica ALXOID (2).pdf', 81, 'files'),
(453, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/81/2021 Política de devoluciones línea Alergénica.pdf', 81, 'files'),
(454, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/107/PRICK FILM INFANTIL.jpg', 107, 'photo'),
(456, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/107/2021 Política de devoluciones línea Alergénica.pdf', 107, 'files'),
(457, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/105/PROVOCACION NASAL.jpg', 105, 'photo'),
(458, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/105/SmCP_Nasal-Provocation-Test-1.pdf', 105, 'files'),
(459, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/105/2021 Política de devoluciones línea Alergénica.pdf', 105, 'files'),
(460, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/104/PROVOCACION NASAL.jpg', 104, 'photo'),
(461, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/104/SmCP_Nasal-Provocation-Test-1.pdf', 104, 'files'),
(462, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/104/2021 Política de devoluciones línea Alergénica.pdf', 104, 'files'),
(463, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/103/PROVOCACION NASAL.jpg', 103, 'photo'),
(464, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/103/SmCP_Nasal-Provocation-Test-1.pdf', 103, 'files'),
(465, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/103/2021 Política de devoluciones línea Alergénica.pdf', 103, 'files'),
(466, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/102/PROVOCACION NASAL.jpg', 102, 'photo'),
(467, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/102/SmCP_Nasal-Provocation-Test-1.pdf', 102, 'files'),
(468, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/102/2021 Política de devoluciones línea Alergénica.pdf', 102, 'files'),
(469, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/101/PROVOCACION NASAL.jpg', 101, 'photo'),
(470, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/101/SmCP_Nasal-Provocation-Test-1.pdf', 101, 'files'),
(471, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/101/2021 Política de devoluciones línea Alergénica.pdf', 101, 'files'),
(472, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/100/PROVOCACION NASAL.jpg', 100, 'photo'),
(473, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/100/SmCP_Nasal-Provocation-Test-1.pdf', 100, 'files'),
(474, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/100/2021 Política de devoluciones línea Alergénica.pdf', 100, 'files'),
(475, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/90/16107664_2021000497.pdf', 90, 'files'),
(476, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/90/SmCP_ALUTEK_052017.pdf', 90, 'files'),
(477, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/90/2021 Política de devoluciones línea Alergénica.pdf', 90, 'files'),
(478, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/86/2021 Política de devoluciones línea Alergénica.pdf', 86, 'files'),
(479, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/85/Anexo_II_Ficha Ténica ALXOID (2).pdf', 85, 'files'),
(480, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/85/2021 Política de devoluciones línea Alergénica.pdf', 85, 'files'),
(481, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/84/Anexo_II_Ficha Ténica ALXOID (2).pdf', 84, 'files'),
(482, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/84/2021 Política de devoluciones línea Alergénica.pdf', 84, 'files'),
(483, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/83/2021 Política de devoluciones línea Alergénica.pdf', 83, 'files'),
(484, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/82/2021 Política de devoluciones línea Alergénica.pdf', 82, 'files'),
(485, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/70/16107664_2021000497.pdf', 70, 'files'),
(486, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/60/SmCP_PRICKS_032016.pdf', 60, 'files'),
(487, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/60/2021 Política de devoluciones línea Alergénica.pdf', 60, 'files'),
(488, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/60/PRICK BLANCO.jpg', 60, 'photo'),
(490, 'photo#0', 'https://inmunotek.evocompany.co/filesproduct/15/PRICK BLANCO.jpg', 15, 'photo'),
(491, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/15/16107664_2021000497.pdf', 15, 'files'),
(492, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/15/SmCP_PRICKS_032016.pdf', 15, 'files'),
(493, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/15/2021 Política de devoluciones línea Alergénica.pdf', 15, 'files'),
(494, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/9/SmCP_ORALTEK_ORA-LATAM-V1 03-18.pdf', 9, 'files'),
(495, 'Política de devolución', 'https://inmunotek.evocompany.co/filesproduct/9/2021 Política de devoluciones línea Alergénica.pdf', 9, 'files'),
(496, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/105/Autorización 2019000891_.pdf', 105, 'files'),
(497, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/104/Autorización 2019000891_.pdf', 104, 'files'),
(498, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/103/Autorización 2019000891_.pdf', 103, 'files'),
(499, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/102/Autorización 2019000891_.pdf', 102, 'files'),
(500, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/101/Autorización 2019000891_.pdf', 101, 'files'),
(501, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/100/Autorización 2019000481.pdf', 100, 'files'),
(502, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/99/Autorizacion 2020000083.pdf', 99, 'files'),
(503, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/98/Autorizacion 2020000083.pdf', 98, 'files'),
(504, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/97/Autorización 2020000342.pdf', 97, 'files'),
(505, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/96/Autorización 2019000685.pdf', 96, 'files'),
(506, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/95/16063547_2021000275.pdf', 95, 'files'),
(507, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/94/Autorización 2021000116.pdf', 94, 'files'),
(508, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/93/Autorización 2020000342.pdf', 93, 'files'),
(509, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/92/Autorizacion 2020000083.pdf', 92, 'files'),
(510, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/91/Autorizacion 2020000083.pdf', 91, 'files'),
(511, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/85/Autorización 2021000116.pdf', 85, 'files'),
(512, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/84/16122799_2021000616.pdf', 84, 'files'),
(513, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/81/Autorización 2021000116.pdf', 81, 'files'),
(514, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/76/16063547_2021000275.pdf', 76, 'files'),
(515, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/62/Autorización 2020000172.pdf', 62, 'files'),
(516, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/60/16063547_2021000275.pdf', 60, 'files'),
(517, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/56/16107664_2021000497 (2).pdf', 56, 'files'),
(518, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/51/Autorización 2021000116.pdf', 51, 'files'),
(519, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/46/16122799_2021000616.pdf', 46, 'files'),
(520, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/43/16122799_2021000616.pdf', 43, 'files'),
(521, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/38/16122799_2021000616.pdf', 38, 'files'),
(522, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/34/Autorización 2021000116.pdf', 34, 'files'),
(523, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/25/16122799_2021000616.pdf', 25, 'files'),
(524, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/22/16122799_2021000616.pdf', 22, 'files'),
(525, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/20/16063547_2021000275.pdf', 20, 'files'),
(526, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/18/Autorización 2021000116.pdf', 18, 'files'),
(527, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/13/Autorización 2018000383.pdf', 13, 'files'),
(528, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/9/Autorización 2021000116.pdf', 9, 'files'),
(529, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/7/16122799_2021000616.pdf', 7, 'files'),
(530, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/5/16122799_2021000616.pdf', 5, 'files'),
(531, 'Invima', 'https://inmunotek.evocompany.co/filesproduct/66/16063547_2021000275.pdf', 66, 'files'),
(532, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/107/Ficha Técnica Prick-Film 06-2021.pdf', 107, 'files'),
(533, 'Especificaciones', 'https://inmunotek.evocompany.co/filesproduct/106/Ficha Técnica Prick-Film 06-2021.pdf', 106, 'files'),
(534, 'photo#0', 'http://127.0.0.1:8000/filesproduct/111/APIS-MELLIFERA-INICIO.png', 111, 'photo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linked_users`
--

CREATE TABLE `linked_users` (
  `id` int(11) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `linked_users`
--

INSERT INTO `linked_users` (`id`, `name`, `email`) VALUES
(1, 'Laura Sarmiento', 'lsofia@outloook.com'),
(2, 'Andres', 'andres@appdigital.com.co');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `new`
--

CREATE TABLE `new` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `img` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notices`
--

CREATE TABLE `notices` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `tipe` tinyint(1) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `date` date NOT NULL,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `notices`
--

INSERT INTO `notices` (`id`, `title`, `tipe`, `photo`, `description`, `date`, `active`) VALUES
(1, 'Conoce Sobre las Alergias', 0, 'https://inmunotek.evocompany.co/img/notices/1.jpg', '<p>Las alergias&nbsp; se han convertido en un padecimiento que afecta un gran n&uacute;mero de personas, volvi&eacute;ndose en una de las enfermedades m&aacute;s frecuentes en la actualidad.</p>\r\n\r\n<p>Las alergias&nbsp; se han convertido en un padecimient', '2021-08-09', 1),
(2, 'Alergoides conjugados con manano y monocitos', 1, 'http://127.0.0.1:8000/img/clinical/2.JPG', '<p>Los alergoides conjugados con manano suponen una nueva generaci&oacute;n de vacunas antial&eacute;rgicas dirigidas a c&eacute;lulas dendr&iacute;ticas desarrolladas por INMUNOTEK. Estas c&eacute;lulas juegan un papel fundamental en la generaci&oacute;n de las c&eacute;lulas (c&eacute;l. T reg) responsables de la tolerancia al&eacute;rgeno-espec&iacute;fica en los pacientes al&eacute;rgicos que reciben inmunoterapia.</p>\r\n\r\n<p>Estudios anteriores hab&iacute;an demostrado la capacidad de los conjugados alergoide-manano para la generaci&oacute;n de c&eacute;lulas Treg a partir de la inducci&oacute;n de c&eacute;lulas dendr&iacute;ticas tolerog&eacute;nicas. Ahora este estudio demuestra que dicha inducci&oacute;n tambi&eacute;n puede realizarse a partir de monocitos, las c&eacute;lulas que llegan al sitio de la inyecci&oacute;n de la vacuna.</p>\r\n\r\n<p>Este estudio ha sido realizado en el Departamento de Bioqu&iacute;mica, Facultad de Qu&iacute;micas, Universidad Complutense de Madrid, con la colaboraci&oacute;n del Institute of Biomedicine and Translational Medicine, University of Tartu (Estonia ) e INMUNOTEK.</p>\r\n\r\n<p><a href=\"https://www.jacionline.org/action/showPdf?pii=S0091-6749%2821%2900968-4\">Ver noticia completa</a>&nbsp;</p>', '2021-11-02', 1),
(3, 'Estudio Clinico 1', 1, 'http://127.0.0.1:8000/img/clinical/3.jpeg', '<p>&nbsp;</p>\r\n\r\n<p>Los alergoides conjugados con manano suponen una nueva generaci&oacute;n de vacunas antial&eacute;rgicas dirigidas a c&eacute;lulas dendr&iacute;ticas desarrolladas por INMUNOTEK. Estas c&eacute;lulas juegan un papel fundamental en la generaci&oacute;n de las c&eacute;lulas (c&eacute;l. T reg) responsables de la tolerancia al&eacute;rgeno-espec&iacute;fica en los pacientes al&eacute;rgicos que reciben inmunoterapia.</p>\r\n\r\n<p>Estudios anteriores hab&iacute;an demostrado la capacidad de los conjugados alergoide-manano para la generaci&oacute;n de c&eacute;lulas Treg a partir de la inducci&oacute;n de c&eacute;lulas dendr&iacute;ticas tolerog&eacute;nicas. Ahora este estudio demuestra que dicha inducci&oacute;n tambi&eacute;n puede realizarse a partir de monocitos, las c&eacute;lulas que llegan al sitio de la inyecci&oacute;n de la vacuna.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Este estudio ha sido realizado en el Departamento de Bioqu&iacute;mica, Facultad de Qu&iacute;micas, Universidad Complutense de Madrid, con la colaboraci&oacute;n del Institute of Biomedicine and Translational Medicine, University of Tartu (Estonia ) e INMUNOTEK.</p>', '2021-11-02', 1),
(4, 'Estudio Clinica 3', 1, 'http://127.0.0.1:8000/img/clinical/4.jpeg', '<p>tales y pascuales</p>', '2021-11-02', 1),
(5, 'Estudio 4', 1, 'http://127.0.0.1:8000/img/clinical/5.jpeg', '<p>Tales</p>', '2021-11-02', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `state` tinyint(4) NOT NULL,
  `date` date NOT NULL,
  `Costumer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `notification`
--

INSERT INTO `notification` (`id`, `message`, `state`, `date`, `Costumer_id`) VALUES
(1, 'Su orden 34 con los productos: APIS MELLIFERA INICIO, ha sido recibida', 1, '2022-02-26', 1),
(2, 'Su orden 35 con los productos BATERÍA COSMÉTICO,  ha sido creada.', 1, '2022-02-26', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('00fcc2c4fbd0e8f5422b239b54515ebbda5ba4ddaf2e8270927b5657b5396eef6db3abc426ec62d6', NULL, 1, 'covidcap', '[]', 0, '2021-08-30 06:25:56', '2021-08-30 06:25:56', '2022-08-30 02:25:56'),
('0117b18d2e5271dde3e9b7aea6864447bf0f89e1212cf2a38680370153c1118c47e0facb0c3566f0', 5, 1, 'covidcap', '[]', 0, '2021-07-28 07:29:34', '2021-07-28 07:29:34', '2022-07-28 03:29:34'),
('012e60125e1015fc18e31aba20c25e1d30a724f6e68c2485295171a923c370318ab56943556894cf', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:16:51', '2020-08-24 07:16:51', '2021-08-24 07:16:51'),
('0287c43400dcd842cab5b309812efa831e97bc1b4f0f0dc0fd3f21dfba2cb5d4f99021a2cf98efb5', 36, 7, 'covidcap', '[]', 0, '2020-08-03 06:55:07', '2020-08-03 06:55:07', '2021-08-03 06:55:07'),
('03a48209e952a1a41d792606ae76fbd86c40da6809d5e6b6825ed0d454d7116d1dc149dadb2064e3', 12, 1, 'covidcap', '[]', 0, '2020-04-11 05:37:52', '2020-04-11 05:37:52', '2021-04-11 05:37:52'),
('0473a452514e60ab92a381f0d5def91ce5c3b630f10d475d3612bba03da67b4ede04dc21da7c90bc', 3, 1, 'covidcap', '[]', 0, '2021-10-12 21:40:30', '2021-10-12 21:40:30', '2022-10-12 17:40:30'),
('0586150f188a2c0fcdda0fdb6e28670cf3911143a956efa1cf9841d11f6b2178e321389a834df8d2', 6, 1, 'covidcap', '[]', 0, '2021-08-18 01:19:54', '2021-08-18 01:19:54', '2022-08-17 21:19:54'),
('06596583bb927632bb14d0ce0a946da4c0e7419f0736e34ab9bccbc4faacea518c477b542cf7b7bd', 62, 7, 'covidcap', '[]', 0, '2020-10-15 21:26:04', '2020-10-15 21:26:04', '2021-10-15 16:26:04'),
('06e097157b18ce5095bd584ed9c60b98ef856daeb0a2f3a2cdc0bbe4bac599d6725346102684825f', 58, 7, 'covidcap', '[]', 0, '2021-07-22 07:18:26', '2021-07-22 07:18:26', '2022-07-22 02:18:26'),
('085e3c948972b1cc9cb06ab073f13f2937bf0e6d5eb3727eec9e316b67c6c4e4f5203c901b007512', 64, 7, 'covidcap', '[]', 0, '2020-11-17 06:48:26', '2020-11-17 06:48:26', '2021-11-17 01:48:26'),
('0ab9cf33379e9f8c54cdf1b5f6803ee80c9aa020560c9b5ff3c80464e8ca07c608cc4285f95e6478', 4, 7, 'covidcap', '[]', 0, '2020-08-24 03:35:56', '2020-08-24 03:35:56', '2021-08-24 03:35:56'),
('0c264a729d85db2360ab61999a7588645a174fb5f182c00b536db73abacfdf6fbc6ea76b9d68050c', 58, 7, 'covidcap', '[]', 0, '2021-06-29 09:14:49', '2021-06-29 09:14:49', '2022-06-29 04:14:49'),
('0d156d10ff93fa6449322d7ad1745ec7c1526bc704c014e3b808e42ea78d5207f2c2f5e900ef9000', 58, 7, 'covidcap', '[]', 0, '2022-01-27 21:10:57', '2022-01-27 21:10:57', '2023-01-27 16:10:57'),
('0d91a689d8b0cf1a260d0156d1492a355a7d3d2bdc0b1dc1936fc9cbd3f42d47c69ae4c62837aba5', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:16:59', '2020-08-24 07:16:59', '2021-08-24 07:16:59'),
('0e7d2abedaf2e28c439ae54f68c8464e4ee6e38b5c605e7d26ed6a61909476504119bd256091a02f', 58, 7, 'covidcap', '[]', 0, '2021-07-24 07:09:32', '2021-07-24 07:09:32', '2022-07-24 02:09:32'),
('0f4712c7c7d32f8dbd96d0fef61f8d312ce6081c0a77b60b1255c9caeecd211005d66c1c71eae041', 9, 1, 'covidcap', '[]', 0, '2021-10-12 21:49:03', '2021-10-12 21:49:03', '2022-10-12 17:49:03'),
('0fb8c844881e7310c53b82ca8bd4c5a93ce08b6d5b6023ed45241e6136b35c98956e2c042c366993', 4, 7, 'covidcap', '[]', 0, '2020-08-18 05:37:12', '2020-08-18 05:37:12', '2021-08-18 05:37:12'),
('13c11201b70297a6ac72aa24f64b8bceba6bbdb6511e1938c2347e8c42e777bb3a7fea7e03c1c01d', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:17:05', '2020-08-24 07:17:05', '2021-08-24 07:17:05'),
('13e538096d6c02a93fdb6845569f15a5157bd0c8e416830a142b27b2b85b379fda40b2261c5fd5db', 9, 1, 'covidcap', '[]', 0, '2021-10-12 21:48:22', '2021-10-12 21:48:22', '2022-10-12 17:48:22'),
('1835b371ac7faa5c19ce759aaaa7371b34b6f7689c3b77b9390eabacac4b2e5f8e3b93a233be0bcb', 58, 7, 'covidcap', '[]', 0, '2022-01-27 03:23:00', '2022-01-27 03:23:00', '2023-01-26 22:23:00'),
('1d79bfb4ada428fb8ccd73b53371d9d893b8654b6c18c28c2f56f76f251deb39bbfbf86ec316ebc9', NULL, 1, 'covidcap', '[]', 0, '2021-08-18 01:18:40', '2021-08-18 01:18:40', '2022-08-17 21:18:40'),
('22d61b520f827b7793df754ee09d72e8cdf3b58ecb6386be3a842f60dfaf1b8418aef891fcb0b2ab', 5, 1, 'covidcap', '[]', 0, '2021-08-04 22:15:43', '2021-08-04 22:15:43', '2022-08-04 18:15:43'),
('22f42b9db0c19736d3f8bbe31808a81f4fe437918944044b2b3dbe5c80f4dbc86db80b7e6955bc19', NULL, 1, 'covidcap', '[]', 0, '2021-08-24 21:27:16', '2021-08-24 21:27:16', '2022-08-24 17:27:16'),
('2406d9daad6e434817749a48b880099e8be06bdf21d5e30f1d4e3dd5bb46bad053cc64ec210cc2db', 5, 1, 'covidcap', '[]', 0, '2021-07-29 18:43:38', '2021-07-29 18:43:38', '2022-07-29 14:43:38'),
('273d7336004b1a276ee3b82915bc0a81acd2ce0941916541ae1231329b53f890bca62e048da1a66f', 8, 1, 'covidcap', '[]', 0, '2021-08-24 23:15:07', '2021-08-24 23:15:07', '2022-08-24 19:15:07'),
('27d010e936e9c73c288ec8f9333ef3cea125ccc2b0ae2ced991117acd124a06382dc9e1459cdc807', 58, 7, 'covidcap', '[]', 0, '2021-07-22 21:36:09', '2021-07-22 21:36:09', '2022-07-22 16:36:09'),
('2d1d1e73677e383ac2dbf6cd8b55a4026590d4ae13424d40820c31afb178d200ed9cc965cee27e26', 5, 1, 'covidcap', '[]', 0, '2021-07-29 20:53:08', '2021-07-29 20:53:08', '2022-07-29 16:53:08'),
('2e26cda514e54660e2c1d9a104f8679abb03d7ee98addc0379d91ad08b11b569b1e4418867ca419e', 58, 7, 'covidcap', '[]', 0, '2020-09-10 03:47:30', '2020-09-10 03:47:30', '2021-09-10 03:47:30'),
('345e524950f607841b67b544da03e91f6604a5357c383968dc256c0c0aa7cbdf7fedccca00c3c1e6', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:16:52', '2020-08-24 07:16:52', '2021-08-24 07:16:52'),
('377e4941381aca777cbee2a3370d60b187be5acf77fe3fa55fa3e670be271ac45f2b6c99d04fef68', 62, 7, 'covidcap', '[]', 0, '2020-10-15 21:55:31', '2020-10-15 21:55:31', '2021-10-15 16:55:31'),
('399d976308143525ffb97c19de1d8a8bfd7eaecbb45b01ba1e9ec83459df074efe8a6787ff37a6a8', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:16:44', '2020-08-24 07:16:44', '2021-08-24 07:16:44'),
('39a7b1ae16c39caee862f18eff5fcea13b550ba36cec7cbb27dcd2bfadb7116f505cc0cbedb56332', 62, 7, 'covidcap', '[]', 0, '2020-10-15 21:01:42', '2020-10-15 21:01:42', '2021-10-15 16:01:42'),
('39d06d247c844dda4604f0a29924ff182d553978e3f4e4cd77c4815440ae5def45fa77319dc991b2', 1, 1, 'covidcap', '[]', 0, '2021-08-23 18:07:00', '2021-08-23 18:07:00', '2022-08-23 14:07:00'),
('4099f0b8ccf2f07245af0414b1d2733311fd998d0d4d7d2451eccecb14e73eb536b8ee2f34fa8dd6', 15, 7, 'covidcap', '[]', 0, '2020-09-08 02:07:17', '2020-09-08 02:07:17', '2021-09-08 02:07:17'),
('40ade3eceb3b3a8b8326444ae1e649787cf0a1bab727a81abe64d4b9b7fda5678de83ac23cf23e98', 58, 7, 'covidcap', '[]', 0, '2022-01-27 21:12:56', '2022-01-27 21:12:56', '2023-01-27 16:12:56'),
('42355b5e973292b933b3d102893ad9e1ce2157464b0ab594d63ccad2fa88488ef9fd0cdd96276431', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:17:03', '2020-08-24 07:17:03', '2021-08-24 07:17:03'),
('44a59889318e28ab328a5e7ccac8c196ba7040e0d755966dda0902499f5f92c9c7b445175f67f56a', 58, 7, 'covidcap', '[]', 0, '2021-06-16 11:16:02', '2021-06-16 11:16:02', '2022-06-16 06:16:02'),
('44e0e0c25858943f81290cde35369132acd14160bf636aab7174bae4cf69a5dd840ea8676b789741', 58, 7, 'covidcap', '[]', 0, '2022-01-27 03:32:05', '2022-01-27 03:32:05', '2023-01-26 22:32:05'),
('45006f00335800271ab2a2b1e5ddaad935d458808a5c8593312b7cfc09c80f602d495d068344fc85', 58, 7, 'covidcap', '[]', 0, '2021-07-22 21:52:30', '2021-07-22 21:52:30', '2022-07-22 16:52:30'),
('45888a6b2dfe1a4532ebe2eaebae072e5b22d526baab30126a7466468923ace31f04fd59e3e16064', 58, 7, 'covidcap', '[]', 0, '2021-11-12 22:01:16', '2021-11-12 22:01:16', '2022-11-12 17:01:16'),
('46162422d44b3af9d9bfb4ad15a34b07c61438681daee2c52ee3525d40c1c6c7327f4556816b5528', 5, 1, 'covidcap', '[]', 0, '2021-08-04 22:20:50', '2021-08-04 22:20:50', '2022-08-04 18:20:50'),
('468d2d4932ad362d62919d2e8e4f3896930f81fce2946ed5775799df1cab146461cfa2e17d1a6067', 5, 1, 'covidcap', '[]', 0, '2021-08-04 02:30:16', '2021-08-04 02:30:16', '2022-08-03 22:30:16'),
('4e217f168ca1ebf7bd053fe47b66523bb099db6524dfcaeac24b819d6dc70e153894b8b4a06d30e6', 36, 7, 'covidcap', '[]', 0, '2020-06-16 06:29:28', '2020-06-16 06:29:28', '2021-06-16 06:29:28'),
('511f972c65dcfcf44690401bc6f3c17f79e742a5131da63c0f6c952a65a38c8086d83a7c1860eae3', NULL, 1, 'covidcap', '[]', 0, '2021-08-30 09:01:54', '2021-08-30 09:01:54', '2022-08-30 05:01:54'),
('5839bd144372e93d2c1799b55c371515279272b64e52a1430f691f2a4a78ed4b0775356f6418d330', 4, 7, 'covidcap', '[]', 0, '2020-08-24 06:58:59', '2020-08-24 06:58:59', '2021-08-24 06:58:59'),
('58c8a3cc7d68d7e4cc7733ae1c6f79f40939999a4ea640cfae98402705573283f63b2569fed00ce7', 58, 7, 'covidcap', '[]', 0, '2021-11-12 22:13:07', '2021-11-12 22:13:07', '2022-11-12 17:13:07'),
('59794d777ded494d78a73fb6470de445d3929cc86b1e5badde439f154530d85562b3f4b05141ad25', 14, 1, 'covidcap', '[]', 0, '2020-04-11 06:15:02', '2020-04-11 06:15:02', '2021-04-11 06:15:02'),
('5b4f336231be783a4276fde9ba8a0bdc24ac981aa13af13a0916d4b76e144c1c0142280065f986cd', 58, 7, 'covidcap', '[]', 0, '2022-02-02 20:19:40', '2022-02-02 20:19:40', '2023-02-02 15:19:40'),
('5d887f23082b9c15b54a490cc510192552d7c336667b0df90399abcb0c65c420ab72b6b8813aeee0', 62, 7, 'covidcap', '[]', 0, '2020-10-15 21:31:01', '2020-10-15 21:31:01', '2021-10-15 16:31:01'),
('5f61a5dd197631091b1591bfdef426a9a4e60ee50e46ed5375e357a5c6c13511a39b741a5d2c3e6c', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:16:55', '2020-08-24 07:16:55', '2021-08-24 07:16:55'),
('5f99413da9c6970c0bc8ee677d1d2548a5519aef844865249b1f3598713b309f72482ad017c0dcee', 4, 7, 'covidcap', '[]', 0, '2020-08-18 01:49:07', '2020-08-18 01:49:07', '2021-08-18 01:49:07'),
('614b0cd3188b489bf75e85b34435171e3a605c6d27219b5088c3e6f01dfc1b2df4235612d4eb96ff', 14, 1, 'covidcap', '[]', 0, '2020-04-11 05:45:23', '2020-04-11 05:45:23', '2021-04-11 05:45:23'),
('62a88083f2b9a0c7afd5fb6d4eb49504335008cd3135af159c22b3e1bd03432ffe048312000c2164', 5, 1, 'covidcap', '[]', 0, '2021-08-03 01:33:31', '2021-08-03 01:33:31', '2022-08-02 21:33:31'),
('6484f09d0b63dee7651e38baec99381a218db1ab2d7accbe06911a3a6efab109f0549028f37604a1', NULL, 1, 'covidcap', '[]', 0, '2021-10-28 21:25:59', '2021-10-28 21:25:59', '2022-10-28 17:25:59'),
('65451acd188704138da62e85ff12302034516ab15c4b2f79a9fbfd3481a90a72daf8a0ad76aaa640', 4, 7, 'covidcap', '[]', 0, '2020-08-24 05:45:33', '2020-08-24 05:45:33', '2021-08-24 05:45:33'),
('6744460838b40ad7b02eede8da3bdd0f0b49615c6b2d25a57e4facdb0f336cb04f0c80ad2a5fd358', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:17:01', '2020-08-24 07:17:01', '2021-08-24 07:17:01'),
('6b29e35a2ccfafc520f9ea875fbf1a0ea84480e9f4afd1409a311591832cdcdc6accc964abe38d92', 5, 1, 'covidcap', '[]', 0, '2021-08-02 21:00:54', '2021-08-02 21:00:54', '2022-08-02 17:00:54'),
('6b60daf0770a93ce7951eb8ec390957bd0ffc325b0fee2af092bcfb16383a5b3fdb9398fc621e883', 38, 7, 'covidcap', '[]', 0, '2020-07-21 02:27:35', '2020-07-21 02:27:35', '2021-07-21 02:27:35'),
('6c85f06251ead288590e424e109d4cb2c921cc2312b0f48d591892b5547846f7e916464413706e3c', 15, 7, 'covidcap', '[]', 0, '2020-08-03 15:43:39', '2020-08-03 15:43:39', '2021-08-03 15:43:39'),
('6ecc494e3fbf0cf13d6b6bd1f44fabc106878eb6b79645aa5625c6d1fcb1a5c7e22581e1a48904f9', 4, 7, 'covidcap', '[]', 0, '2020-08-24 05:45:26', '2020-08-24 05:45:26', '2021-08-24 05:45:26'),
('702ebb3e9d8109d9afdf3afb0691d573612304803278301beb494aa76e38b2a71013568a53bd326b', 58, 7, 'covidcap', '[]', 0, '2021-07-24 07:04:58', '2021-07-24 07:04:58', '2022-07-24 02:04:58'),
('70a41cf40133dd262f0bd1db1ddbdde827db1d6aca6711fa226f0f17a8dc713263528112a55b6840', 9, 1, 'covidcap', '[]', 0, '2021-10-12 21:42:11', '2021-10-12 21:42:11', '2022-10-12 17:42:11'),
('76aa9d49d54ba0f3534905dd65b8dc6b057a19e22fff9f58144619b47f6aa00198c8f21d6a561d0e', 41, 7, 'covidcap', '[]', 0, '2020-08-18 13:31:10', '2020-08-18 13:31:10', '2021-08-18 13:31:10'),
('76dc218fead8cf2d177fd194873e887381854fb0ef10b54e4b7f0a5492c2820a3ebc02feb962579d', 62, 7, 'covidcap', '[]', 0, '2020-10-15 21:37:31', '2020-10-15 21:37:31', '2021-10-15 16:37:31'),
('78512e99f81c3d8029b8287f8a8932b12ac6bd707b7d919357f39d1465990d07aa5b5984f67190ca', 58, 7, 'covidcap', '[]', 0, '2021-11-12 20:55:33', '2021-11-12 20:55:33', '2022-11-12 15:55:33'),
('7bc576cee871af454fdb1bfd4c6188138e1f40e3c0d19f9bb834a2b7661fe96a49a63ae2f18cf5fd', 11, 1, 'covidcap', '[]', 0, '2020-04-11 05:32:24', '2020-04-11 05:32:24', '2021-04-11 05:32:24'),
('7cedf0f7dd1e4cf9565bdb347156ab7e7b093462e66afd9678884023f40887325ca160fcac135834', NULL, 1, 'covidcap', '[]', 0, '2021-08-30 06:27:56', '2021-08-30 06:27:56', '2022-08-30 02:27:56'),
('7ecc9071afcf55420216e2d02ccc98252fc81197d07cc69a49d78cadc597378431a905f2104fdb18', 6, 1, 'covidcap', '[]', 0, '2021-08-18 01:19:28', '2021-08-18 01:19:28', '2022-08-17 21:19:28'),
('7f558858bf9b270b523db13368d070da8ab7aa6d671063a17d3dc44365fd8f386faaccf06594ce4f', 9, 1, 'covidcap', '[]', 0, '2021-10-12 21:48:14', '2021-10-12 21:48:14', '2022-10-12 17:48:14'),
('86b64e3c333f5f14291c59c9b779a2a90d3c66e2694bbcaa664ace7de6e790588a0614f68332fe47', 58, 7, 'covidcap', '[]', 0, '2021-11-02 07:34:28', '2021-11-02 07:34:28', '2022-11-02 02:34:28'),
('88c5ac143b6bc5e67690d58cff52e689c7882d346c22e87689633fbcee45bc926e276d0d40728c4e', 14, 1, 'covidcap', '[]', 0, '2020-04-11 06:12:40', '2020-04-11 06:12:40', '2021-04-11 06:12:40'),
('8acdd4c05c47cd3ae50cb46c0782850ae974f8e2df47ae88703173c1635c89ab62892d3394c8c88d', 58, 7, 'covidcap', '[]', 0, '2021-07-24 07:09:19', '2021-07-24 07:09:19', '2022-07-24 02:09:19'),
('8d742e60e1cedb8a4a96a17233365350ae704e242a10b43c8aeaf525efac6b2518689384e18c8a11', 4, 7, 'covidcap', '[]', 0, '2020-08-24 05:46:06', '2020-08-24 05:46:06', '2021-08-24 05:46:06'),
('8e8b3057800c50a7fb464304abb45526aee62c0d50810a19366e742275ea741557132017c80dc255', 58, 7, 'covidcap', '[]', 0, '2022-02-25 04:59:45', '2022-02-25 04:59:45', '2023-02-24 23:59:45'),
('8fd78aff51862a16c3c0e1ca14e773e69bd276dc6cababeec850497df717b34c11f8f4ea1b539a47', 4, 7, 'covidcap', '[]', 0, '2020-09-08 01:37:40', '2020-09-08 01:37:40', '2021-09-08 01:37:40'),
('907dd73eebad34fe0927e69702c5a2e9d94ac5a248cf340a60ee12a2d90ebc6544331695a7980c04', 60, 7, 'covidcap', '[]', 0, '2020-09-16 01:20:10', '2020-09-16 01:20:10', '2021-09-16 01:20:10'),
('94399c2b5f9dbfe29c58104ec5515d349bc79d06001edfda1770f91b593f5b7c9475c2093e457973', 60, 7, 'covidcap', '[]', 0, '2020-09-16 01:20:10', '2020-09-16 01:20:10', '2021-09-16 01:20:10'),
('9460691f3bb11662a89c03223de224995ac48c53a5b7f2d9f20d52c1c9bd15dc0c13836d915f03c7', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:16:57', '2020-08-24 07:16:57', '2021-08-24 07:16:57'),
('95a8e1348fd803c3c09961d2a3f7b1dcbb78132d8d433314840da340717c91026c9dda6d36989660', 58, 7, 'covidcap', '[]', 0, '2021-11-18 20:38:22', '2021-11-18 20:38:22', '2022-11-18 15:38:22'),
('963156ccdd0fa29de938ac22c837c87634ad494603f6054317487c403700fef723d3929677dfbdcc', 35, 7, 'covidcap', '[]', 0, '2020-06-18 12:45:55', '2020-06-18 12:45:55', '2021-06-18 12:45:55'),
('98b2b54c8826674f12f4272418da6ecfbe7090529edcf57ad3738f0b9d7247938d1c202becbeca33', 8, 1, 'covidcap', '[]', 0, '2021-08-30 15:31:34', '2021-08-30 15:31:34', '2022-08-30 11:31:34'),
('9b70bb230e3f953ab98db6b060953b41ad847efba3febadce3731d37c32a604802a67d16e5c655f7', 39, 7, 'covidcap', '[]', 0, '2020-07-21 03:22:10', '2020-07-21 03:22:10', '2021-07-21 03:22:10'),
('9dba4cfbfa29dd8e39136db429f5f410743c61466e05cde10b6e29467f7f2425ffd2bdb8dbf19e08', 64, 7, 'covidcap', '[]', 0, '2020-10-16 04:33:16', '2020-10-16 04:33:16', '2021-10-15 23:33:16'),
('9e4609de5d02e8043df50ffe500bcc9dc195521626d48ecab0323524e808eb5c9207855edc13afa6', 5, 1, 'covidcap', '[]', 0, '2021-08-06 19:36:49', '2021-08-06 19:36:49', '2022-08-06 15:36:49'),
('a0e817ca27c658f87cb82e4960d39fa75099200a3768e18809163283ec011d264995239db10c7344', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:26:52', '2020-08-24 07:26:52', '2021-08-24 07:26:52'),
('a67f9ce0955fdf34d11d01854f8287903d0f30d0a80cb4a575fc50e08578b4d3f8c58ff5659b24d6', 64, 7, 'covidcap', '[]', 0, '2020-11-16 02:43:18', '2020-11-16 02:43:18', '2021-11-15 21:43:18'),
('acf05bc4587fedf73b6cae083ea86c26dd66e6cd0d8c8b40bd7eed85fb72755fcfcb3d735b443ee7', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:17:04', '2020-08-24 07:17:04', '2021-08-24 07:17:04'),
('af47f4ed0886fba7aac07469f3fd9adbe69eef529bcda832c3bcb1c9ab966bce99d07eefe67db2d9', 5, 1, 'covidcap', '[]', 0, '2021-08-05 01:00:54', '2021-08-05 01:00:54', '2022-08-04 21:00:54'),
('b3a48cfbb2822a3c9471fe6121ba25861af06a96c7028adb6dd1e211eff8480f070bbc6b1284ddaa', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:16:57', '2020-08-24 07:16:57', '2021-08-24 07:16:57'),
('b6a1a57fc8368ae57297d8c57406b5e6e77a2cc67a85cb8ed56817f1978499924a19c103b0e5d9ea', 58, 7, 'covidcap', '[]', 0, '2021-07-24 07:13:36', '2021-07-24 07:13:36', '2022-07-24 02:13:36'),
('b756745245588716589d4bd9c6c407a381039250912142fe8838a66a7c92f8b3214a0a503ab52de4', 58, 7, 'covidcap', '[]', 0, '2020-11-22 18:20:02', '2020-11-22 18:20:02', '2021-11-22 13:20:02'),
('b98c6f4d3247651d20ec43a7f77aa7e000c65c5c3f73981cecb90a12bbaee75491f3c8ec12072586', 62, 7, 'covidcap', '[]', 0, '2020-10-15 21:47:51', '2020-10-15 21:47:51', '2021-10-15 16:47:51'),
('b9ddf8d7308ceb9bfd0850505e640415407147f4d19e3295cbebcb0231e372804a7fe4df64bb42d5', 64, 7, 'covidcap', '[]', 0, '2020-11-17 02:29:38', '2020-11-17 02:29:38', '2021-11-16 21:29:38'),
('ba5d2dd21a9c7baeef59412ea7c71aba85f8865cd4bb743622fffafe42bc7317287e66e922665580', 4, 7, 'covidcap', '[]', 0, '2020-08-24 06:10:57', '2020-08-24 06:10:57', '2021-08-24 06:10:57'),
('bcf04516e326acf93381e6668f11cbaba454028b4fa9ff29d6ec1e018a5b577c894f91649143f346', 62, 7, 'covidcap', '[]', 0, '2020-10-15 21:50:09', '2020-10-15 21:50:09', '2021-10-15 16:50:09'),
('bd36304acc2c8e4287e8b713cf55113362c8c4b7327e4393268bca86690436de3522fcf78480467e', 5, 1, 'covidcap', '[]', 0, '2021-07-29 18:40:47', '2021-07-29 18:40:47', '2022-07-29 14:40:47'),
('c1d9f5f167c9a743f4c4b7426a34e7acdacb930328a12a6be68f0d028d8c37dd01e0e2c64d44db7f', NULL, 1, 'covidcap', '[]', 0, '2021-08-18 01:21:21', '2021-08-18 01:21:21', '2022-08-17 21:21:21'),
('c2610eb5af41dc6ef5d720a99450952ed682e0aa8415e59567d105708c1c2bfbe317acb30fd772e2', 36, 7, 'covidcap', '[]', 0, '2020-08-03 02:59:33', '2020-08-03 02:59:33', '2021-08-03 02:59:33'),
('c53803e4f4a610512a3c7ae2060ba11fd27043eb1c5b2500ace9c686811a891b7a1f5e4b4524f439', 5, 1, 'covidcap', '[]', 0, '2021-08-04 22:22:00', '2021-08-04 22:22:00', '2022-08-04 18:22:00'),
('c6029c7c2fe4e82d22b83130bbc86d9a82c5031ab7c3b26d7290c22bd9b3932067667b2daee43d44', 58, 7, 'covidcap', '[]', 0, '2022-01-27 03:23:51', '2022-01-27 03:23:51', '2023-01-26 22:23:51'),
('c7de2e4c4d1061a47556588d436151f25538a2c8bce98fe2862e89f118e68fa1e3c17a29cd6544cf', 64, 7, 'covidcap', '[]', 0, '2020-11-14 22:14:05', '2020-11-14 22:14:05', '2021-11-14 17:14:05'),
('c7fba1b3754ba8f7f189d51f610ce32c69e563745472de670ff00140d979a5c05b0d795c86b5e4c0', 64, 7, 'covidcap', '[]', 0, '2020-10-16 04:28:18', '2020-10-16 04:28:18', '2021-10-15 23:28:18'),
('cd922584f1c2dc2a73fb4cafbdaff3cc0c5f4e972bff22d58b57301dff3925199e5b1ae207aad580', 62, 7, 'covidcap', '[]', 0, '2020-10-16 04:02:50', '2020-10-16 04:02:50', '2021-10-15 23:02:50'),
('cfda7a9301badcbbb30421401814100e92f972d63f7bc20a6c9f1e739749ead9518085dd8de650e8', 58, 7, 'covidcap', '[]', 0, '2022-01-27 03:31:41', '2022-01-27 03:31:41', '2023-01-26 22:31:41'),
('d007a71068e0ef3890ace9f220d557d88cbeaafb87ce5b41370aa10e5ce1baf68683e7c2cf4a1d8c', 4, 7, 'covidcap', '[]', 0, '2020-08-17 03:41:22', '2020-08-17 03:41:22', '2021-08-17 03:41:22'),
('d0f374062d322c1ff458605ff245f2b66c56a4af6c72d7e3dec4b16c76eff8c68a09eb35ff6dbb52', 5, 1, 'covidcap', '[]', 0, '2021-08-03 16:35:36', '2021-08-03 16:35:36', '2022-08-03 12:35:36'),
('d23dc3753b0b0343c073c0765f3398151cf61204d5c5519b8e75f92bcb61cf7588d48bd6abccd91b', 58, 7, 'covidcap', '[]', 0, '2022-01-28 08:35:03', '2022-01-28 08:35:03', '2023-01-28 03:35:03'),
('d29e25ecb455db8a543632f451dacc8b0dc98745257754bc49997995ad6b8e90ce29ba5edaa00c99', 4, 7, 'covidcap', '[]', 0, '2020-08-24 06:10:42', '2020-08-24 06:10:42', '2021-08-24 06:10:42'),
('d2eeb836a16a06ede308fe06ab00c0901f6cd73fd5803d7633241b99f9ab2915a8c00e4951ef5d5a', 9, 1, 'covidcap', '[]', 0, '2021-10-12 21:38:34', '2021-10-12 21:38:34', '2022-10-12 17:38:34'),
('d32fc032079169c6d978b3fcd298c63d8cfd2128ee71f8d3aeefef1f5ccb595d6e18220d42682703', 10, 1, 'covidcap', '[]', 0, '2021-08-30 07:07:29', '2021-08-30 07:07:29', '2022-08-30 03:07:29'),
('d86a10387bd63f6a79ded6c9a82601340748054e2560def73542645060cbcbd4c915dfb248915d34', 5, 1, 'covidcap', '[]', 0, '2021-07-28 06:26:58', '2021-07-28 06:26:58', '2022-07-28 02:26:58'),
('dcfd7b3772a92c4dbb7c5d0e2c5494bf2b486ab3dda7bdb575fb8d30a3a846959827e89575d175ab', 9, 1, 'covidcap', '[]', 0, '2021-10-12 21:41:39', '2021-10-12 21:41:39', '2022-10-12 17:41:39'),
('dd709ffc153628d0e5d5f9e979686c51fcc916e1f98fb17a5644dc0c8770dba5025eb0d24290934b', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:17:00', '2020-08-24 07:17:00', '2021-08-24 07:17:00'),
('e434dd116225522a80a120e5b176ebf18aa5e09a5d60c257bc76ec4b67ff6cff8a18707e4af195a8', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:16:53', '2020-08-24 07:16:53', '2021-08-24 07:16:53'),
('e936181fec445bcea736e1d613dd4288dcaf11d24b9b419de86af919c5ea5a702bf43117d4b31736', 58, 7, 'covidcap', '[]', 0, '2021-07-24 07:05:25', '2021-07-24 07:05:25', '2022-07-24 02:05:25'),
('ea3f86975993b77e6416d770a0a8ee77ee337e661fd7980a9aecfc7dcae97f8970471a228cfe0665', 58, 7, 'covidcap', '[]', 0, '2020-09-10 05:44:02', '2020-09-10 05:44:02', '2021-09-10 05:44:02'),
('f094d2108c18211bac353bb8434a4b440f5714881da283d42a58b689d47186b3c3396e7ab7201ed4', 5, 1, 'covidcap', '[]', 0, '2021-08-04 22:20:28', '2021-08-04 22:20:28', '2022-08-04 18:20:28'),
('f595e3b32bb25469c88006f729fcce0479b06db6fe9dadcdbee26ec8208e8164036c7603e8bf88e6', 64, 7, 'covidcap', '[]', 0, '2020-11-17 04:48:24', '2020-11-17 04:48:24', '2021-11-16 23:48:24'),
('f7e84c0c29c2d714af3c7481bfdaee428d56555f9056fd497dee7701995a0c40373d12c10a2651f2', 58, 7, 'covidcap', '[]', 0, '2022-01-27 21:11:20', '2022-01-27 21:11:20', '2023-01-27 16:11:20'),
('f943189465cd16e4f18d7f62e7474079164381b43aa595fb26f22cafe4edc63d50f0697f8328f5db', 7, 1, 'covidcap', '[]', 0, '2021-10-12 21:49:38', '2021-10-12 21:49:38', '2022-10-12 17:49:38'),
('fa87f4f9b04ff03981569c2be1bbf6a25984430bc6aaf2d030ce8c54ad37d5fa95d4674b85675382', 41, 7, 'covidcap', '[]', 0, '2020-08-15 23:20:57', '2020-08-15 23:20:57', '2021-08-15 23:20:57'),
('fadf54087781d6e07e652ab582e402d47b56179776c2b09f33c07b607f4c6c6d03b9f5621ea4d902', 13, 1, 'covidcap', '[]', 0, '2020-04-11 05:40:37', '2020-04-11 05:40:37', '2021-04-11 05:40:37'),
('fb6c8ff7fb928f4d7754e2bb185dc1d7866f253e4083af491b2ef10de733a331fe15ea9a597c8bba', 4, 7, 'covidcap', '[]', 0, '2020-08-24 05:46:24', '2020-08-24 05:46:24', '2021-08-24 05:46:24'),
('fbca29a35f3dc8071633844f48d5d866532cb91775b63491c11e9e2c9cc50f2a23867e16e70ccf93', 4, 7, 'covidcap', '[]', 0, '2020-08-24 07:16:49', '2020-08-24 07:16:49', '2021-08-24 07:16:49'),
('fce3dc14a6229861b14bfaf90536690a33dc3c81c76c5d7dee82488f02db67237830bb28cee73e0e', 8, 1, 'covidcap', '[]', 0, '2021-08-25 17:31:41', '2021-08-25 17:31:41', '2022-08-25 13:31:41'),
('feb6e6aa47f0265e3dc2dc537d418ca64eeddc78bdac7abc1e41c4a041112a4f6ebec7ed849f6646', 4, 7, 'covidcap', '[]', 0, '2020-08-17 03:45:22', '2020-08-17 03:45:22', '2021-08-17 03:45:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'BOIFaM6Obh5vzbLXBr4sfWday8vJtwv09RsSfArO', 'http://localhost', 1, 0, 0, '2020-04-11 05:31:01', '2020-04-11 05:31:01'),
(2, NULL, 'Laravel Password Grant Client', 'Zwm3hkM1o8I9rjOdh1TRbE5SlsEdlvTGFoM4LCPI', 'http://localhost', 0, 1, 0, '2020-04-11 05:31:02', '2020-04-11 05:31:02'),
(3, NULL, 'LINKIU Personal Access Client', 'gUevHdfh8BBsioWR1v8imvtiyrKUjAS044oUhqHH', 'http://localhost', 1, 0, 0, '2020-06-16 02:18:41', '2020-06-16 02:18:41'),
(4, NULL, 'LINKIU Password Grant Client', 'WT4RvUeGsJDZBvhr9y7emE30fRdGnGfTilxqxtQb', 'http://localhost', 0, 1, 0, '2020-06-16 02:18:41', '2020-06-16 02:18:41'),
(5, NULL, 'LINKIU Personal Access Client', 'HLG1v1hqk0YPJKy8GCYfCzOmRfPT819h0FYvfPhQ', 'http://localhost', 1, 0, 0, '2020-06-16 02:21:00', '2020-06-16 02:21:00'),
(6, NULL, 'LINKIU Password Grant Client', 'U5h5yNhUKbbHuuFJQFbZZRLWF9PDbj2zaigUhymu', 'http://localhost', 0, 1, 0, '2020-06-16 02:21:01', '2020-06-16 02:21:01'),
(7, NULL, 'LINKIU Personal Access Client', 'ZnrZXV4REgVdDH7D1ZSThGhfkqGMLO0zMO7UM1Gl', 'http://localhost', 1, 0, 0, '2020-06-16 06:14:49', '2020-06-16 06:14:49'),
(8, NULL, 'LINKIU Password Grant Client', '3GnwES7CidFdywswfQGAW999yN16ckDLEtNPkaus', 'http://localhost', 0, 1, 0, '2020-06-16 06:14:49', '2020-06-16 06:14:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-04-11 05:31:02', '2020-04-11 05:31:02'),
(2, 3, '2020-06-16 02:18:41', '2020-06-16 02:18:41'),
(3, 5, '2020-06-16 02:21:01', '2020-06-16 02:21:01'),
(4, 7, '2020-06-16 06:14:49', '2020-06-16 06:14:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `Costumer_id` int(11) NOT NULL,
  `Payment_method_id` int(11) NOT NULL,
  `OrderStatus_id` int(11) NOT NULL,
  `commentary` varchar(255) DEFAULT NULL,
  `Address_id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `shipping_value` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `order`
--

INSERT INTO `order` (`id`, `date`, `Costumer_id`, `Payment_method_id`, `OrderStatus_id`, `commentary`, `Address_id`, `active`, `shipping_value`) VALUES
(20, '2021-11-02', 1, 1, 2, NULL, 32, 1, 0),
(21, '2021-11-12', 1, 1, 6, NULL, 1, 1, 0),
(22, '2021-11-18', 1, 2, 6, NULL, 32, 1, 0),
(23, '2022-01-26', 1, 2, 6, NULL, 32, 1, 0),
(24, '2022-01-26', 1, 2, 1, NULL, 1, 1, 0),
(25, '2022-01-26', 1, 2, 6, NULL, 1, 1, 0),
(26, '2022-01-26', 1, 2, 6, NULL, 32, 1, 0),
(27, '2022-01-27', 1, 2, 6, NULL, 1, 1, 0),
(28, '2022-01-28', 1, 1, 6, NULL, 1, 1, 0),
(29, '2022-01-28', 1, 1, 6, NULL, 32, 1, 0),
(30, '2022-01-28', 1, 1, 6, NULL, 32, 1, 0),
(31, '2022-01-28', 1, 2, 6, NULL, 32, 1, 0),
(32, '2022-01-28', 1, 2, 6, NULL, 32, 1, 0),
(33, '2022-01-28', 1, 2, 6, NULL, 32, 1, 0),
(34, '2022-02-26', 1, 1, 2, NULL, 1, 1, NULL),
(35, '2022-02-26', 1, 1, 6, NULL, 32, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orderproductstatus`
--

CREATE TABLE `orderproductstatus` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT 'Pendiente pago'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `orderproductstatus`
--

INSERT INTO `orderproductstatus` (`id`, `name`) VALUES
(1, 'Orden Recibida'),
(2, 'Orden Despachada'),
(3, 'Orden Entregada'),
(4, 'Pendiente pago');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orderstatus`
--

CREATE TABLE `orderstatus` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `orderstatus`
--

INSERT INTO `orderstatus` (`id`, `name`) VALUES
(1, 'Pedido recibido'),
(2, 'Orden recibida'),
(3, 'Pedido enviado'),
(6, 'Orden creada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_has_product`
--

CREATE TABLE `order_has_product` (
  `order_id` int(11) NOT NULL,
  `Product_id` int(11) NOT NULL,
  `OrderProductStatus_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `delivery_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `order_has_product`
--

INSERT INTO `order_has_product` (`order_id`, `Product_id`, `OrderProductStatus_id`, `quantity`, `rate`, `delivery_date`) VALUES
(20, 2, 4, 1, 0, NULL),
(20, 10, 4, 2, 0, NULL),
(21, 2, 4, 1, 0, NULL),
(22, 81, 4, 1, 0, NULL),
(23, 111, 4, 1, 0, NULL),
(24, 111, 4, 1, 0, NULL),
(25, 111, 4, 2, 0, NULL),
(26, 111, 4, 1, 0, NULL),
(27, 1, 4, 3, 0, NULL),
(28, 111, 4, 1, 0, NULL),
(29, 1, 4, 1, 0, NULL),
(30, 111, 4, 1, 0, NULL),
(31, 2, 4, 1, 0, NULL),
(32, 2, 4, 1, 0, NULL),
(33, 2, 4, 1, 0, NULL),
(34, 111, 4, 1, 0, NULL),
(35, 110, 4, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `payment_method`
--

INSERT INTO `payment_method` (`id`, `name`) VALUES
(1, 'Pago en linea'),
(2, 'Pago a Cuotas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext CHARACTER SET utf8 NOT NULL,
  `price` double NOT NULL,
  `tax` double NOT NULL,
  `SubcategoryProduct_id` int(11) NOT NULL,
  `Provider_id` int(11) NOT NULL,
  `date_create` datetime DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `estimatedDeliveryDays` int(11) DEFAULT NULL,
  `code` varchar(45) NOT NULL,
  `stock` int(11) NOT NULL,
  `minimumStock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `price`, `tax`, `SubcategoryProduct_id`, `Provider_id`, `date_create`, `active`, `estimatedDeliveryDays`, `code`, `stock`, `minimumStock`) VALUES
(1, 'ALFA-LACTOALBUMINA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-06-29 03:20:55', 1, NULL, 'F076', 7, 5),
(2, 'ALMENDRA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-06-29 03:20:55', 1, NULL, 'F020', 7, 5),
(3, 'ATUN', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-06-29 03:20:55', 1, NULL, 'F040', 7, 5),
(4, 'GLICEROL SALINO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 14, 1, '2021-06-29 03:28:28', 1, NULL, 'K100', 7, 5),
(5, 'HISTAMINA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 14, 1, '2021-06-29 03:28:28', 1, NULL, 'K200', 7, 5),
(6, 'ALTERNARIA ALTERNATA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 8, 1, '2021-06-29 03:52:23', 1, NULL, 'P901', 7, 5),
(7, 'ASPERGILLUS FUMIGATUS', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 8, 1, '2021-06-29 03:52:23', 1, NULL, 'P902', 7, 5),
(8, 'ALXOID BB BLOMIA TROPICALIS 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 15, 1, '2021-06-29 04:05:56', 1, NULL, 'M608', 7, 5),
(9, 'ORALTEK CUPRESSUS SEMPERVIRENS', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA \r\n\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios e', 144000, 0, 16, 1, '2021-07-14 03:31:09', 1, NULL, 'T524', 7, 5),
(10, 'AMBROSIA ELATIOR / AMBROSIA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 9, 1, '2021-08-04 23:59:35', 1, NULL, 'W301', 10, 5),
(11, 'ARTEMISIA VULGARIS / ARTEMISA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 9, 1, '2021-08-05 00:02:11', 1, NULL, 'W302', 10, 5),
(12, 'AVELLANA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 15:57:25', 1, NULL, 'F017', 10, 5),
(13, 'AVENA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 16:03:28', 1, NULL, 'F007', 10, 5),
(14, 'BACALAO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 16:06:10', 1, NULL, 'F003', 10, 5),
(15, 'BACALAO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 16:06:18', 1, NULL, 'F003', 10, 5),
(16, 'BETA-LACTOGLOBULINA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 16:08:14', 1, NULL, 'F077', 10, 5),
(17, 'BLOMIA TROPICALIS', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 10, 1, '2021-08-05 16:10:45', 1, NULL, 'M608', 10, 5),
(18, 'CALAMAR', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 16:13:34', 1, NULL, 'F258', 10, 5),
(19, 'CAMARON', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 16:15:39', 1, NULL, 'F024', 10, 5),
(20, 'CANDIDA ALBICANS', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 8, 1, '2021-08-05 16:19:14', 1, NULL, 'P904', 10, 5),
(21, 'CANGREJO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 16:22:06', 1, NULL, 'F023', 10, 5),
(22, 'CASEINA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 16:31:01', 1, NULL, 'F078', 10, 5),
(23, 'CERDO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 20:45:11', 1, NULL, 'F026', 10, 5),
(24, 'CHOCOLATE', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 20:47:18', 1, NULL, 'F093', 10, 5),
(25, 'CLADOSPORIUM HERBARUM', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 8, 1, '2021-08-05 20:50:09', 1, NULL, 'P905', 10, 5),
(26, 'CLARA HUEVO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 20:54:14', 1, NULL, 'F001', 10, 5),
(27, 'CUPRESSUS SEMPERVIRENS / CIPRES', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 11, 1, '2021-08-05 21:01:31', 1, NULL, 'T524', 10, 5),
(28, 'CYNODON DACTYLON /GRAMA COMUN', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 12, 1, '2021-08-05 21:11:01', 1, NULL, 'G102', 10, 5),
(29, 'DERMATOPHAGOIDES FARINAE', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 10, 1, '2021-08-05 21:20:59', 1, NULL, 'M605', 10, 5),
(30, 'DERMATOPHAGOIDES PTERONYSSINUS', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 10, 1, '2021-08-05 21:23:04', 1, NULL, 'M601', 10, 5),
(31, 'EPITELIO CABALLO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 13, 1, '2021-08-05 21:25:06', 1, NULL, 'E807', 10, 5),
(32, 'EPITELIO CONEJO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 13, 1, '2021-08-05 21:27:06', 1, NULL, 'F213', 10, 5),
(33, 'EPITELIO GATO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 13, 1, '2021-08-05 21:30:13', 1, NULL, 'E801', 10, 5),
(34, 'EPITELIO PERRO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 13, 1, '2021-08-05 21:32:33', 1, NULL, 'E802', 10, 5),
(35, 'FRAXINUS EXCELSIOR/FRESNO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 11, 1, '2021-08-05 21:34:49', 1, NULL, 'T508', 10, 5),
(36, 'FRESA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:36:45', 1, NULL, 'F044', 10, 5),
(37, 'HEVEA BRASILIENSIS-LATEX', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:38:25', 1, NULL, 'L001', 10, 5),
(38, 'HORMIGA ROJA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:40:30', 1, NULL, 'I708', 10, 5),
(39, 'HUEVO ENTERO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:41:50', 1, NULL, 'F245', 10, 5),
(40, 'KIWI', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:43:26', 1, NULL, 'F084', 10, 5),
(41, 'LANGOSTINO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:50:23', 1, NULL, 'F304', 10, 5),
(42, 'LECHE FRESCA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:51:30', 1, NULL, 'F002', 10, 5),
(43, 'LENTEJAS', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:52:40', 1, NULL, 'F235', 10, 5),
(44, 'MAIZ', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:54:43', 1, NULL, 'F008', 10, 5),
(45, 'MANI', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:56:39', 1, NULL, 'F013', 10, 5),
(46, 'MANZANA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:57:55', 1, NULL, 'F049', 10, 5),
(47, 'MEJILLON', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 21:59:40', 1, NULL, 'F037', 10, 5),
(48, 'MELOCOTON', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:02:02', 1, NULL, 'F095', 10, 5),
(49, 'MELON', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:03:30', 1, NULL, 'F087', 10, 5),
(50, 'MERLUZA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:05:13', 1, NULL, 'F307', 10, 5),
(51, 'MEZCLA 4 CEREALES', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 12, 1, '2021-08-05 22:07:57', 1, NULL, 'MG02', 10, 5),
(52, 'MEZCLA 6 GRAMINEAS', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 12, 1, '2021-08-05 22:09:32', 1, NULL, 'MG601', 10, 5),
(53, 'MEZCLA DE MALEZAS 100%', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 9, 1, '2021-08-05 22:11:49', 1, NULL, 'MW57', 10, 5),
(54, 'OVOALBUMINA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:13:27', 1, NULL, 'F246', 10, 5),
(55, 'OVOMUCUIDE', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:15:00', 1, NULL, 'F247', 10, 5),
(56, 'OVOMUCUIDE', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:15:00', 1, NULL, 'F247', 10, 5),
(57, 'PAPA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:16:24', 1, NULL, 'F035', 10, 5),
(58, 'PENICILLIUM NOTATUM', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 8, 1, '2021-08-05 22:18:00', 1, NULL, 'P908', 10, 5),
(59, 'PERIPLANETA AMERICANA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:19:29', 1, NULL, 'I703', 10, 5),
(60, 'PESCADO AZUL (F040,F041)', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:21:53', 1, NULL, 'MF10', 10, 5),
(61, 'PESCADO BLANCO (F003,F307,F337)', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:23:09', 1, NULL, 'MF11', 10, 5),
(62, 'PHLEUM PRATENSE /HIERBA TIMOTEA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 12, 1, '2021-08-05 22:25:00', 1, NULL, 'G110', 10, 5),
(63, 'PINA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:27:13', 1, NULL, 'F210', 10, 5),
(64, 'PIÑA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:27:17', 1, NULL, 'F210', 10, 5),
(65, 'PINUS SP', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 11, 1, '2021-08-05 22:28:35', 1, NULL, 'T526', 10, 5),
(66, 'PLATANO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:30:13', 1, NULL, 'F092', 10, 5),
(67, 'PLUMAS CANARIO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 13, 1, '2021-08-05 22:32:49', 1, NULL, 'E813', 10, 5),
(68, 'PLUMAS PERIQUITO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 13, 1, '2021-08-05 22:34:15', 1, NULL, 'E814', 10, 5),
(69, 'POA PRATENSIS / ESPIGUILLA, POA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 12, 1, '2021-08-05 22:36:05', 1, NULL, 'G111', 10, 5),
(70, 'CARNE DE POLLO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:37:05', 1, NULL, 'F083', 10, 5),
(71, 'SALMON', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:38:14', 1, NULL, 'F041', 10, 5),
(72, 'SARDINA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:39:34', 1, NULL, 'F308', 10, 5),
(73, 'SOJA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:40:35', 1, NULL, 'F014', 10, 5),
(74, 'TOMATE', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:42:26', 1, NULL, 'F025', 10, 5),
(75, 'TRIGO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:43:52', 1, NULL, 'F004', 10, 5),
(76, 'TRUCHA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:45:06', 1, NULL, 'F204', 10, 5),
(77, 'CARNE DE TERNERA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:46:15', 1, NULL, 'F027', 10, 5),
(78, 'YEMA HUEVO', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:47:24', 1, NULL, 'F075', 10, 5),
(79, 'ZANAHORIA', 'PRICK TEST es una preparaci?n glicerinada que contiene un extracto nativo alerg?nico disuelto en partes iguales de soluci?n salina tamponada est?ril y glicerol. \r\n\r\n?	FORMA FARMAC?UTICA \r\nEl producto farmac?utico es una soluci?n glicerinada para la realiz', 87000, 0, 7, 1, '2021-08-05 22:49:05', 1, NULL, 'F031', 10, 5),
(80, 'ALXOID BB EPITELIO DE GATO 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 18, 1, '2021-09-22 21:46:21', 1, NULL, 'E801', 7, 5),
(81, 'ALXOID BB EPITELIO PERRO 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 18, 1, '2021-09-22 21:47:46', 1, NULL, 'E802', 7, 5),
(82, 'ALXOID BB FARINAE-PTERO 50%-50%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 17, 1, '2021-09-22 21:49:04', 1, NULL, 'MM09', 7, 5),
(83, 'ALXOID BB FARINE-PTERO-BLOMIA 33%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 17, 1, '2021-09-22 21:52:21', 1, NULL, 'MM10', 7, 5),
(84, 'ALXOID BB MEZCLA 6 GRAMÍNEAS', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 19, 1, '2021-09-22 21:53:41', 1, NULL, 'MG601', 7, 5),
(85, 'ALXOID BB CUPRESSUS SEMPERVIRENS 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 20, 1, '2021-09-22 21:54:43', 1, NULL, 'T524', 7, 5),
(86, 'ALXOID BB MALEZAS 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 21, 1, '2021-09-22 21:56:36', 1, NULL, 'MW57', 7, 5),
(87, 'ALXOID BB CYNODON 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 19, 1, '2021-09-22 21:57:32', 1, NULL, 'G102', 7, 5),
(88, 'ALXOID BB DERMATOPHAGOIDES PTERONYSSINUS 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 17, 1, '2021-09-22 21:58:32', 1, NULL, 'M601', 7, 5),
(89, 'ALXOID BB DERMATOPHAGOIDES FARINAE 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\n\r\nALXOID? es la marca comercial de un conjunto de tratamientos de inmunoterapia,\r\nlos cuales consisten en preparados alerg?nicos que se administran por v?a subcut?nea para el tratamiento de enfermedades al?rgicas re', 610000, 0, 17, 1, '2021-09-22 22:00:42', 1, NULL, 'M605', 7, 5),
(90, 'ALUTEK EPITELIO DE CABALLO 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA \r\nALUTEK? es una suspensi?n de uno o varios extractos nativos alerg?nicos adsorbidos en hidr?xido de aluminio como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: \r\nLa sustancia activa es uno o varios ext', 266000, 0, 32, 1, '2021-09-22 22:02:10', 1, NULL, 'E807', 7, 5),
(91, 'ORALTEK CYNODON DACTYLON 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios extr', 144000, 0, 23, 1, '2021-09-22 22:12:47', 1, NULL, 'G102', 7, 5),
(92, 'ORALTEK EPITELIO CABALLO 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios extr', 144000, 0, 24, 1, '2021-09-23 17:31:37', 1, NULL, 'E807', 7, 5),
(93, 'ORALTEK EPITELIO GATO 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios extr', 144000, 0, 24, 1, '2021-09-23 17:32:42', 1, NULL, 'E801', 7, 5),
(94, 'ORALTEK EPITELIO PERRO 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios extr', 144000, 0, 24, 1, '2021-09-23 17:33:53', 1, NULL, 'E802', 7, 5),
(95, 'ORALTEK FARINAE-PTERONYSSINUS 50%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios extr', 144000, 0, 25, 1, '2021-09-23 17:35:27', 1, NULL, 'MM09', 7, 5),
(96, 'ORALTEK FARINE-PTERO-BLOMIA 33%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios extr', 144000, 0, 25, 1, '2021-09-23 17:36:37', 1, NULL, 'MM10', 7, 5),
(97, 'ORALTEK HORMIGA ROJA 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios extr', 144000, 0, 25, 1, '2021-09-23 17:39:03', 1, NULL, 'I708', 7, 5),
(98, 'ORALTEK MEZCLA 6 GRAMÍNEAS', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios extr', 144000, 0, 23, 1, '2021-09-23 17:44:36', 1, NULL, 'MG601', 7, 5),
(99, 'ORALTEK PERIPLANETA AMERICANA/ CUCARACHA', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nORALTEK? es una soluci?n glicerinada que contiene uno o varios extractos alerg?nicos nativos (no modificados) como sustancia activa de esta formulaci?n. \r\n\r\nSustancia activa: La sustancia activa es uno o varios extr', 144000, 0, 24, 1, '2021-09-23 17:45:33', 1, NULL, 'I703', 7, 5),
(100, 'PRUEBAS DE PROVOCACION DERMATOPHAGOIDES FARIN', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nEl Test de Provocaci?n Nasal es un f?rmaco formado por dos componentes: un preparado liofilizado para reconstituci?n que contiene un extracto alerg?nico para el diagn?stico de la enfermedad al?rgica y una soluci?n r', 247100, 0, 27, 1, '2021-09-27 21:35:32', 1, NULL, 'M605', 7, 5),
(101, 'PRUEBAS DE PROVOCACION DERMATOPHAGOIDES PTERO', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nEl Test de Provocaci?n Nasal es un f?rmaco formado por dos componentes: un preparado liofilizado para reconstituci?n que contiene un extracto alerg?nico para el diagn?stico de la enfermedad al?rgica y una soluci?n r', 247100, 0, 27, 1, '2021-09-27 21:47:10', 1, NULL, 'M601', 7, 5),
(102, 'PRUEBAS DE PROVOCACION BLOMIA TROPICALIS 100%', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nEl Test de Provocaci?n Nasal es un f?rmaco formado por dos componentes: un preparado liofilizado para reconstituci?n que contiene un extracto alerg?nico para el diagn?stico de la enfermedad al?rgica y una soluci?n r', 247100, 0, 27, 1, '2021-09-27 21:48:52', 1, NULL, 'M608', 7, 5),
(103, 'PRUEBAS DE PROVOCACION EPITELIO GATO', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nEl Test de Provocaci?n Nasal es un f?rmaco formado por dos componentes: un preparado liofilizado para reconstituci?n que contiene un extracto alerg?nico para el diagn?stico de la enfermedad al?rgica y una soluci?n r', 247100, 0, 27, 1, '2021-09-27 21:49:43', 1, NULL, 'E801', 7, 5),
(104, 'PRUEBAS DE PROVOCACION EPITELIO PERRO', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nEl Test de Provocaci?n Nasal es un f?rmaco formado por dos componentes: un preparado liofilizado para reconstituci?n que contiene un extracto alerg?nico para el diagn?stico de la enfermedad al?rgica y una soluci?n r', 247100, 0, 27, 1, '2021-09-27 21:50:33', 1, NULL, 'E802', 7, 5),
(105, 'PRUEBAS DE PROVOCACION PHLEUM PRATENSE /HIERB', 'COMPOSICI?N CUALITATIVA Y CUANTITATIVA\r\nEl Test de Provocaci?n Nasal es un f?rmaco formado por dos componentes: un preparado liofilizado para reconstituci?n que contiene un extracto alerg?nico para el diagn?stico de la enfermedad al?rgica y una soluci?n r', 247100, 0, 28, 1, '2021-09-27 21:51:28', 1, NULL, 'G110', 7, 5),
(106, 'PRICK FILM ADULTO', 'El dispositivo Prick-Film? tiene como finalidad complementar pruebas diagn?sticas de enfermedad al?rgica al facilitar la realizaci?n de las pruebas al?rgicas por punci?n cut?nea (Prick test) y permitir guardar un registro gr?fico del resultado. \r\n\r\nDESCRI', 179200, 0, 30, 1, '2021-09-27 22:06:53', 1, NULL, 'QT', 7, 5),
(107, 'PRICK FILM INFANTIL', 'El dispositivo Prick-Film? tiene como finalidad complementar pruebas diagn?sticas de enfermedad al?rgica al facilitar la realizaci?n de las pruebas al?rgicas por punci?n cut?nea (Prick test) y permitir guardar un registro gr?fico del resultado. \r\n\r\nDESCRI', 179200, 0, 30, 1, '2021-09-27 22:07:58', 1, NULL, 'ST', 7, 5),
(108, 'BATERÍA ESTÁNDAR', 'El sistema de allergEAZE  le ofrece un sistema completo para todas sus necesidades de prueba de parche.\r\n\r\nLos al?rgenos se enumeran por tipo de producto y pueden incluir m?ltiples concentraciones y / o veh?culos de un solo al?rgeno basados en diferentes ', 900000, 0, 33, 1, '2021-09-27 22:12:28', 1, NULL, 'HA01', 7, 5),
(109, 'BATERÍA CALZADO', 'El sistema de allergEAZE  le ofrece un sistema completo para todas sus necesidades de prueba de parche.\r\n\r\nLos al?rgenos se enumeran por tipo de producto y pueden incluir m?ltiples concentraciones y / o veh?culos de un solo al?rgeno basados en diferentes ', 900000, 0, 34, 1, '2021-09-27 22:13:22', 1, NULL, 'HA02', 7, 5),
(110, 'BATERÍA COSMÉTICO', 'El sistema de allergEAZE  le ofrece un sistema completo para todas sus necesidades de prueba de parche.\r\n\r\nLos al?rgenos se enumeran por tipo de producto y pueden incluir m?ltiples concentraciones y / o veh?culos de un solo al?rgeno basados en diferentes ', 900000, 0, 35, 1, '2021-09-27 22:14:38', 1, NULL, 'HA03', 7, 5),
(111, 'APIS MELLIFERA INICIO', 'COMPOSICIÓN CUALITATIVA Y CUANTITATIVA \r\nVENOX® es una marca para un tratamiento de inmunoterapia elaborado a partir de extracto de veneno de himenópteros (avispa o abeja) administrado por vía subcutánea y utilizado para el tratamiento de enfermedades alérgicas de tipo I inmediatas (mediadas por IgE) hipersensibilidad al veneno de insectos del orden de los himenópteros. Es un medicamento formado por dos componentes: una preparación liofilizada para reconstitución que contiene el veneno de himenópteros y una solución regeneradora para reconstitución. Está destinado a la vía subcutánea y debe ser administrado por personal clínico capacitado en hospitales, clínicas y consultorios provistos de los elementos necesarios para tratar una reacción adversa sistémica.\r\n\r\nSustancias activas:\r\nEl principio activo es el extracto de veneno de himenópteros que contiene todos los alérgenos relevantes y siempre se trata de preparaciones únicas; solo contiene un extracto de veneno de especie de himenópteros. VENOX® se fabrica estandarizado en unidades de masa, en microgramos de proteína (μg / mL) a cinco concentraciones diferentes siguiendo un programa de dosis hasta que se alcanza la concentración terapéutica (Vial No. 5).', 369900, 0, 29, 1, '2021-11-22 00:59:46', 1, NULL, 'HX2', 7, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provider`
--

CREATE TABLE `provider` (
  `id` int(11) NOT NULL,
  `identification` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `trade_certificate` longtext,
  `tax_registration` longtext,
  `users_id` bigint(20) UNSIGNED NOT NULL,
  `City_id` int(11) NOT NULL,
  `TypeIdentification_id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `provider`
--

INSERT INTO `provider` (`id`, `identification`, `address`, `contact_person`, `phone`, `trade_certificate`, `tax_registration`, `users_id`, `City_id`, `TypeIdentification_id`, `active`) VALUES
(1, '901269920', 'Calle 85c # 17 32', 'Andres Satizabal', '3157164899', 'http://100.25.193.30/files/55/CW8190819HMG5I720190919043425.pdf', 'http://100.25.193.30/files/55/rut.pdf', 55, 1031, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Costumer'),
(3, 'Provider'),
(4, 'Inventory'),
(5, 'Publisher');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shipping`
--

CREATE TABLE `shipping` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `regions` longtext NOT NULL,
  `value` double NOT NULL,
  `ShippingMethod_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `delivery_days` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `shipping`
--

INSERT INTO `shipping` (`id`, `name`, `quantity`, `regions`, `value`, `ShippingMethod_id`, `product_id`, `delivery_days`) VALUES
(1, 'Envio Local', 0, '[\"1031\"]', 0, 2, 1, 1),
(2, 'Envio Nacional', 0, '[\"149\"]', 30000, 2, 1, 8),
(3, 'Envio Especial', 0, '[\"910\"]', 50000, 2, 1, 15),
(4, 'Envio Local', 5, '[\"812\"]', 50, 1, 3, 5),
(5, 'Envio Nacional', 8, '[\"2\",\"3\",\"690\",\"578\",\"607\",\"151\",\"608\",\"572\",\"405\",\"853\",\"320\",\"1061\",\"406\",\"609\",\"644\",\"348\",\"854\",\"459\",\"723\",\"1040\",\"724\",\"4\",\"661\",\"610\",\"364\",\"198\",\"966\",\"611\",\"579\",\"192\",\"967\",\"5\",\"6\",\"968\",\"467\",\"725\",\"1035\",\"7\",\"8\",\"9\",\"460\",\"10\",\"321\",\"1024\",\"11\",\"961\",\"12\",\"532\",\"834\",\"199\",\"662\",\"322\",\"855\",\"1055\",\"1049\",\"468\",\"721\",\"796\",\"13\",\"200\",\"152\",\"365\",\"14\",\"1013\",\"663\",\"153\",\"823\",\"15\",\"969\",\"154\",\"407\",\"970\",\"580\",\"431\",\"581\",\"582\",\"583\",\"366\",\"835\",\"127\",\"612\",\"726\",\"16\",\"856\",\"857\",\"713\",\"858\",\"645\",\"1102\",\"187\",\"126\",\"408\",\"323\",\"17\",\"116\",\"469\",\"719\",\"306\",\"605\",\"360\",\"846\",\"201\",\"18\",\"19\",\"859\",\"202\",\"470\",\"203\",\"798\",\"149\",\"471\",\"584\",\"1030\",\"367\",\"860\",\"409\",\"204\",\"21\",\"205\",\"852\",\"815\",\"206\",\"1010\",\"937\",\"824\",\"432\",\"368\",\"717\",\"1025\",\"22\",\"207\",\"861\",\"472\",\"691\",\"1107\",\"473\",\"818\",\"24\",\"1018\",\"938\",\"971\",\"369\",\"474\",\"1110\",\"155\",\"821\",\"208\",\"25\",\"370\",\"1031\",\"862\",\"1034\",\"371\",\"26\",\"148\",\"613\",\"209\",\"433\",\"128\",\"1044\",\"156\",\"475\",\"848\",\"476\",\"28\",\"29\",\"863\",\"30\",\"1004\",\"573\",\"575\",\"31\",\"183\",\"361\",\"1017\",\"1116\",\"960\",\"688\",\"32\",\"27\",\"864\",\"434\",\"210\",\"865\",\"664\",\"720\",\"477\",\"941\",\"972\",\"866\",\"867\",\"33\",\"410\",\"435\",\"847\",\"211\",\"324\",\"806\",\"436\",\"478\",\"868\",\"212\",\"411\",\"213\",\"214\",\"810\",\"215\",\"216\",\"665\",\"225\",\"479\",\"480\",\"1062\",\"461\",\"224\",\"157\",\"869\",\"825\",\"34\",\"20\",\"684\",\"457\",\"309\",\"159\",\"35\",\"973\",\"481\",\"614\",\"939\",\"727\",\"1078\",\"36\",\"870\",\"37\",\"666\",\"587\",\"871\",\"728\",\"729\",\"872\",\"799\",\"38\",\"218\",\"372\",\"873\",\"935\",\"219\",\"482\",\"437\",\"220\",\"940\",\"974\",\"1050\",\"731\",\"692\",\"221\",\"222\",\"483\",\"791\",\"693\",\"1123\",\"732\",\"733\",\"975\",\"349\",\"874\",\"412\",\"223\",\"23\",\"786\",\"586\",\"217\",\"158\",\"730\",\"826\",\"783\",\"39\",\"1022\",\"646\",\"647\",\"976\",\"40\",\"836\",\"226\",\"820\",\"41\",\"42\",\"667\",\"1019\",\"694\",\"602\",\"784\",\"603\",\"190\",\"930\",\"124\",\"695\",\"1016\",\"734\",\"227\",\"484\",\"413\",\"350\",\"696\",\"1011\",\"1092\",\"228\",\"875\",\"160\",\"601\",\"648\",\"414\",\"351\",\"735\",\"150\",\"849\",\"462\",\"668\",\"876\",\"1113\",\"669\",\"942\",\"485\",\"736\",\"92\",\"780\",\"373\",\"737\",\"794\",\"789\",\"1042\",\"615\",\"877\",\"878\",\"43\",\"44\",\"977\",\"567\",\"978\",\"325\",\"827\",\"229\",\"979\",\"347\",\"374\",\"230\",\"1008\",\"880\",\"879\",\"486\",\"649\",\"1051\",\"487\",\"759\",\"45\",\"980\",\"112\",\"714\",\"670\",\"738\",\"488\",\"500\",\"489\",\"490\",\"491\",\"231\",\"492\",\"129\",\"943\",\"881\",\"464\",\"415\",\"882\",\"232\",\"233\",\"616\",\"617\",\"1027\",\"46\",\"493\",\"47\",\"883\",\"416\",\"793\",\"494\",\"113\",\"697\",\"884\",\"234\",\"1023\",\"375\",\"495\",\"739\",\"1033\",\"618\",\"49\",\"885\",\"496\",\"740\",\"741\",\"698\",\"671\",\"981\",\"376\",\"886\",\"944\",\"50\",\"497\",\"51\",\"498\",\"499\",\"235\",\"887\",\"570\",\"501\",\"236\",\"502\",\"837\",\"822\",\"48\",\"888\",\"237\",\"800\",\"189\",\"933\",\"1063\",\"650\",\"52\",\"801\",\"982\",\"53\",\"619\",\"983\",\"962\",\"984\",\"742\",\"743\",\"377\",\"1101\",\"744\",\"620\",\"621\",\"600\",\"54\",\"55\",\"238\",\"378\",\"1009\",\"114\",\"239\",\"240\",\"56\",\"503\",\"889\",\"890\",\"146\",\"504\",\"588\",\"453\",\"622\",\"891\",\"505\",\"242\",\"57\",\"838\",\"1093\",\"745\",\"1045\",\"326\",\"808\",\"58\",\"746\",\"417\",\"1106\",\"428\",\"656\",\"747\",\"701\",\"327\",\"506\",\"354\",\"507\",\"422\",\"893\",\"1094\",\"508\",\"59\",\"623\",\"805\",\"1121\",\"1064\",\"379\",\"828\",\"748\",\"1020\",\"945\",\"749\",\"60\",\"305\",\"380\",\"509\",\"243\",\"1095\",\"1026\",\"839\",\"817\",\"241\",\"892\",\"894\",\"1082\",\"750\",\"703\",\"510\",\"1091\",\"61\",\"751\",\"589\",\"438\",\"752\",\"439\",\"946\",\"813\",\"895\",\"797\",\"130\",\"964\",\"963\",\"381\",\"244\",\"896\",\"62\",\"511\",\"512\",\"161\",\"753\",\"162\",\"651\",\"947\",\"131\",\"754\",\"132\",\"652\",\"418\",\"319\",\"513\",\"328\",\"1057\",\"1103\",\"699\",\"163\",\"63\",\"245\",\"985\",\"329\",\"345\",\"840\",\"330\",\"184\",\"898\",\"1\",\"514\",\"590\",\"591\",\"592\",\"986\",\"382\",\"700\",\"346\",\"1112\",\"246\",\"383\",\"1090\",\"841\",\"1115\",\"1077\",\"899\",\"900\",\"440\",\"165\",\"247\",\"248\",\"249\",\"64\",\"164\",\"429\",\"829\",\"1065\",\"430\",\"166\",\"384\",\"352\",\"1109\",\"948\",\"515\",\"755\",\"308\",\"441\",\"987\",\"65\",\"66\",\"788\",\"250\",\"897\",\"67\",\"756\",\"516\",\"988\",\"69\",\"68\",\"331\",\"606\",\"517\",\"518\",\"519\",\"251\",\"520\",\"332\",\"167\",\"672\",\"252\",\"1056\",\"594\",\"624\",\"593\",\"1029\",\"901\",\"814\",\"902\",\"253\",\"70\",\"757\",\"903\",\"625\",\"1079\",\"1059\",\"989\",\"758\",\"254\",\"949\",\"255\",\"522\",\"1117\",\"385\",\"626\",\"419\",\"523\",\"257\",\"258\",\"627\",\"334\",\"628\",\"904\",\"147\",\"925\",\"1038\",\"950\",\"990\",\"781\",\"782\",\"1108\",\"524\",\"259\",\"1119\",\"525\",\"526\",\"722\",\"386\",\"260\",\"261\",\"1074\",\"312\",\"673\",\"420\",\"335\",\"72\",\"833\",\"262\",\"71\",\"387\",\"906\",\"991\",\"388\",\"830\",\"687\",\"907\",\"168\",\"133\",\"263\",\"629\",\"630\",\"674\",\"992\",\"442\",\"675\",\"760\",\"134\",\"145\",\"363\",\"1066\",\"761\",\"1036\",\"993\",\"762\",\"1088\",\"421\",\"443\",\"842\",\"659\",\"73\",\"908\",\"763\",\"1100\",\"1096\",\"1075\",\"74\",\"264\",\"1080\",\"1120\",\"1105\",\"144\",\"704\",\"444\",\"705\",\"1081\",\"454\",\"707\",\"706\",\"75\",\"1097\",\"851\",\"708\",\"353\",\"1052\",\"527\",\"1098\",\"792\",\"389\",\"76\",\"850\",\"528\",\"764\",\"390\",\"994\",\"445\",\"333\",\"362\",\"256\",\"905\",\"529\",\"530\",\"577\",\"831\",\"843\",\"531\",\"265\",\"807\",\"266\",\"1067\",\"169\",\"77\",\"676\",\"143\",\"709\",\"1021\",\"78\",\"765\",\"533\",\"995\",\"1039\",\"643\",\"909\",\"79\",\"597\",\"336\",\"337\",\"631\",\"766\",\"1012\",\"996\",\"268\",\"391\",\"997\",\"267\",\"595\",\"596\",\"170\",\"427\",\"929\",\"135\",\"1068\",\"136\",\"80\",\"686\",\"81\",\"269\",\"446\",\"632\",\"338\",\"677\",\"790\",\"998\",\"832\",\"82\",\"271\",\"767\",\"339\",\"934\",\"642\",\"423\",\"1089\",\"910\",\"447\",\"120\",\"718\",\"448\",\"1006\",\"571\",\"932\",\"951\",\"534\",\"769\",\"455\",\"804\",\"458\",\"125\",\"715\",\"803\",\"535\",\"185\",\"424\",\"272\",\"171\",\"1104\",\"172\",\"83\",\"536\",\"1084\",\"911\",\"195\",\"193\",\"84\",\"912\",\"340\",\"122\",\"927\",\"316\",\"456\",\"359\",\"1111\",\"604\",\"173\",\"716\",\"958\",\"568\",\"123\",\"657\",\"710\",\"770\",\"1005\",\"85\",\"315\",\"1073\",\"957\",\"952\",\"711\",\"425\",\"191\",\"273\",\"1085\",\"913\",\"317\",\"953\",\"771\",\"314\",\"194\",\"86\",\"1032\",\"954\",\"779\",\"117\",\"449\",\"87\",\"88\",\"403\",\"685\",\"89\",\"926\",\"355\",\"678\",\"768\",\"679\",\"914\",\"90\",\"772\",\"658\",\"174\",\"928\",\"999\",\"137\",\"660\",\"275\",\"633\",\"392\",\"175\",\"845\",\"119\",\"313\",\"188\",\"1122\",\"276\",\"776\",\"118\",\"274\",\"402\",\"816\",\"1086\",\"959\",\"91\",\"138\",\"844\",\"773\",\"1053\",\"811\",\"465\",\"277\",\"278\",\"93\",\"538\",\"1014\",\"279\",\"539\",\"1083\",\"785\",\"540\",\"393\",\"915\",\"541\",\"176\",\"936\",\"598\",\"680\",\"542\",\"280\",\"282\",\"916\",\"281\",\"283\",\"356\",\"139\",\"357\",\"284\",\"115\",\"94\",\"177\",\"463\",\"285\",\"287\",\"286\",\"394\",\"917\",\"140\",\"634\",\"543\",\"396\",\"918\",\"955\",\"544\",\"545\",\"341\",\"919\",\"546\",\"288\",\"289\",\"547\",\"290\",\"965\",\"395\",\"1069\",\"270\",\"548\",\"576\",\"178\",\"426\",\"1054\"]', 40, 2, 3, 4),
(6, 'Envio Local', 5, '[\"2\"]', 50, 1, 4, 5),
(7, 'Envio Nacional', 4, '[\"812\",\"3\",\"690\",\"578\",\"607\",\"151\",\"608\",\"572\",\"405\",\"853\",\"320\",\"1061\",\"406\",\"609\",\"644\",\"348\",\"854\",\"459\",\"723\",\"1040\",\"724\",\"4\",\"661\",\"610\",\"364\",\"198\",\"966\",\"611\",\"579\",\"192\",\"967\",\"5\",\"6\",\"968\",\"467\",\"725\",\"1035\",\"7\",\"8\",\"9\",\"460\",\"10\",\"321\",\"1024\",\"11\",\"961\",\"12\",\"532\",\"834\",\"199\",\"662\",\"322\",\"855\",\"1055\",\"1049\",\"468\",\"721\",\"796\",\"13\",\"200\",\"152\",\"365\",\"14\",\"1013\",\"663\",\"153\",\"823\",\"15\",\"969\",\"154\",\"407\",\"970\",\"580\",\"431\",\"581\",\"582\",\"583\",\"366\",\"835\",\"127\",\"612\",\"726\",\"16\",\"856\",\"857\",\"713\",\"858\",\"645\",\"1102\",\"187\",\"126\",\"408\",\"323\",\"17\",\"116\",\"469\",\"719\",\"306\",\"605\",\"360\",\"846\",\"201\",\"18\",\"19\",\"859\",\"202\",\"470\",\"203\",\"798\",\"149\",\"471\",\"584\",\"1030\",\"367\",\"860\",\"409\",\"204\",\"21\",\"205\",\"852\",\"815\",\"206\",\"1010\",\"937\",\"824\",\"432\",\"368\",\"717\",\"1025\",\"22\",\"207\",\"861\",\"472\",\"691\",\"1107\",\"473\",\"818\",\"24\",\"1018\",\"938\",\"971\",\"369\",\"474\",\"1110\",\"155\",\"821\",\"208\",\"25\",\"370\",\"1031\",\"862\",\"1034\",\"371\",\"26\",\"148\",\"613\",\"209\",\"433\",\"128\",\"1044\",\"156\",\"475\",\"848\",\"476\",\"28\",\"29\",\"863\",\"30\",\"1004\",\"573\",\"575\",\"31\",\"183\",\"361\",\"1017\",\"1116\",\"960\",\"688\",\"32\",\"27\",\"864\",\"434\",\"210\",\"865\",\"664\",\"720\",\"477\",\"941\",\"972\",\"866\",\"867\",\"33\",\"410\",\"435\",\"847\",\"211\",\"324\",\"806\",\"436\",\"478\",\"868\",\"212\",\"411\",\"213\",\"214\",\"810\",\"215\",\"216\",\"665\",\"225\",\"479\",\"480\",\"1062\",\"461\",\"224\",\"157\",\"869\",\"825\",\"34\",\"20\",\"684\",\"457\",\"309\",\"159\",\"35\",\"973\",\"481\",\"614\",\"939\",\"727\",\"1078\",\"36\",\"870\",\"37\",\"666\",\"587\",\"871\",\"728\",\"729\",\"872\",\"799\",\"38\",\"218\",\"372\",\"873\",\"935\",\"219\",\"482\",\"437\",\"220\",\"940\",\"974\",\"1050\",\"731\",\"692\",\"221\",\"222\",\"483\",\"791\",\"693\",\"1123\",\"732\",\"733\",\"975\",\"349\",\"874\",\"412\",\"223\",\"23\",\"786\",\"586\",\"217\",\"158\",\"730\",\"826\",\"783\",\"39\",\"1022\",\"646\",\"647\",\"976\",\"40\",\"836\",\"226\",\"820\",\"41\",\"42\",\"667\",\"1019\",\"694\",\"602\",\"784\",\"603\",\"190\",\"930\",\"124\",\"695\",\"1016\",\"734\",\"227\",\"484\",\"413\",\"350\",\"696\",\"1011\",\"1092\",\"228\",\"875\",\"160\",\"601\",\"648\",\"414\",\"351\",\"735\",\"150\",\"849\",\"462\",\"668\",\"876\",\"1113\",\"669\",\"942\",\"485\",\"736\",\"92\",\"780\",\"373\",\"737\",\"794\",\"789\",\"1042\",\"615\",\"877\",\"878\",\"43\",\"44\",\"977\",\"567\",\"978\",\"325\",\"827\",\"229\",\"979\",\"347\",\"374\",\"230\",\"1008\",\"880\",\"879\",\"486\",\"649\",\"1051\",\"487\",\"759\",\"45\",\"980\",\"112\",\"714\",\"670\",\"738\",\"488\",\"500\",\"489\",\"490\",\"491\",\"231\",\"492\",\"129\",\"943\",\"881\",\"464\",\"415\",\"882\",\"232\",\"233\",\"616\",\"617\",\"1027\",\"46\",\"493\",\"47\",\"883\",\"416\",\"793\",\"494\",\"113\",\"697\",\"884\",\"234\",\"1023\",\"375\",\"495\",\"739\",\"1033\",\"618\",\"49\",\"885\",\"496\",\"740\",\"741\",\"698\",\"671\",\"981\",\"376\",\"886\",\"944\",\"50\",\"497\",\"51\",\"498\",\"499\",\"235\",\"887\",\"570\",\"501\",\"236\",\"502\",\"837\",\"822\",\"48\",\"888\",\"237\",\"800\",\"189\",\"933\",\"1063\",\"650\",\"52\",\"801\",\"982\",\"53\",\"619\",\"983\",\"962\",\"984\",\"742\",\"743\",\"377\",\"1101\",\"744\",\"620\",\"621\",\"600\",\"54\",\"55\",\"238\",\"378\",\"1009\",\"114\",\"239\",\"240\",\"56\",\"503\",\"889\",\"890\",\"146\",\"504\",\"588\",\"453\",\"622\",\"891\",\"505\",\"242\",\"57\",\"838\",\"1093\",\"745\",\"1045\",\"326\",\"808\",\"58\",\"746\",\"417\",\"1106\",\"428\",\"656\",\"747\",\"701\",\"327\",\"506\",\"354\",\"507\",\"422\",\"893\",\"1094\",\"508\",\"59\",\"623\",\"805\",\"1121\",\"1064\",\"379\",\"828\",\"748\",\"1020\",\"945\",\"749\",\"60\",\"305\",\"380\",\"509\",\"243\",\"1095\",\"1026\",\"839\",\"817\",\"241\",\"892\",\"894\",\"1082\",\"750\",\"703\",\"510\",\"1091\",\"61\",\"751\",\"589\",\"438\",\"752\",\"439\",\"946\",\"813\",\"895\",\"797\",\"130\",\"964\",\"963\",\"381\",\"244\",\"896\",\"62\",\"511\",\"512\",\"161\",\"753\",\"162\",\"651\",\"947\",\"131\",\"754\",\"132\",\"652\",\"418\",\"319\",\"513\",\"328\",\"1057\",\"1103\",\"699\",\"163\",\"63\",\"245\",\"985\",\"329\",\"345\",\"840\",\"330\",\"184\",\"898\",\"1\",\"514\",\"590\",\"591\",\"592\",\"986\",\"382\",\"700\",\"346\",\"1112\",\"246\",\"383\",\"1090\",\"841\",\"1115\",\"1077\",\"899\",\"900\",\"440\",\"165\",\"247\",\"248\",\"249\",\"64\",\"164\",\"429\",\"829\",\"1065\",\"430\",\"166\",\"384\",\"352\",\"1109\",\"948\",\"515\",\"755\",\"308\",\"441\",\"987\",\"65\",\"66\",\"788\",\"250\",\"897\",\"67\",\"756\",\"516\",\"988\",\"69\",\"68\",\"331\",\"606\",\"517\",\"518\",\"519\",\"251\",\"520\",\"332\",\"167\",\"672\",\"252\",\"1056\",\"594\",\"624\",\"593\",\"1029\",\"901\",\"814\",\"902\",\"253\",\"70\",\"757\",\"903\",\"625\",\"1079\",\"1059\",\"989\",\"758\",\"254\",\"949\",\"255\",\"522\",\"1117\",\"385\",\"626\",\"419\",\"523\",\"257\",\"258\",\"627\",\"334\",\"628\",\"904\",\"147\",\"925\",\"1038\",\"950\",\"990\",\"781\",\"782\",\"1108\",\"524\",\"259\",\"1119\",\"525\",\"526\",\"722\",\"386\",\"260\",\"261\",\"1074\",\"312\",\"673\",\"420\",\"335\",\"72\",\"833\",\"262\",\"71\",\"387\",\"906\",\"991\",\"388\",\"830\",\"687\",\"907\",\"168\",\"133\",\"263\",\"629\",\"630\",\"674\",\"992\",\"442\",\"675\",\"760\",\"134\",\"145\",\"363\",\"1066\",\"761\",\"1036\",\"993\",\"762\",\"1088\",\"421\",\"443\",\"842\",\"659\",\"73\",\"908\",\"763\",\"1100\",\"1096\",\"1075\",\"74\",\"264\",\"1080\",\"1120\",\"1105\",\"144\",\"704\",\"444\",\"705\",\"1081\",\"454\",\"707\",\"706\",\"75\",\"1097\",\"851\",\"708\",\"353\",\"1052\",\"527\",\"1098\",\"792\",\"389\",\"76\",\"850\",\"528\",\"764\",\"390\",\"994\",\"445\",\"333\",\"362\",\"256\",\"905\",\"529\",\"530\",\"577\",\"831\",\"843\",\"531\",\"265\",\"807\",\"266\",\"1067\",\"169\",\"77\",\"676\",\"143\",\"709\",\"1021\",\"78\",\"765\",\"533\",\"995\",\"1039\",\"643\",\"909\",\"79\",\"597\",\"336\",\"337\",\"631\",\"766\",\"1012\",\"996\",\"268\",\"391\",\"997\",\"267\",\"595\",\"596\",\"170\",\"427\",\"929\",\"135\",\"1068\",\"136\",\"80\",\"686\",\"81\",\"269\",\"446\",\"632\",\"338\",\"677\",\"790\",\"998\",\"832\",\"82\",\"271\",\"767\",\"339\",\"934\",\"642\",\"423\",\"1089\",\"910\",\"447\",\"120\",\"718\",\"448\",\"1006\",\"571\",\"932\",\"951\",\"534\",\"769\",\"455\",\"804\",\"458\",\"125\",\"715\",\"803\",\"535\",\"185\",\"424\",\"272\",\"171\",\"1104\",\"172\",\"83\",\"536\",\"1084\",\"911\",\"195\",\"193\",\"84\",\"912\",\"340\",\"122\",\"927\",\"316\",\"456\",\"359\",\"1111\",\"604\",\"173\",\"716\",\"958\",\"568\",\"123\",\"657\",\"710\",\"770\",\"1005\",\"85\",\"315\",\"1073\",\"957\",\"952\",\"711\",\"425\",\"191\",\"273\",\"1085\",\"913\",\"317\",\"953\",\"771\",\"314\",\"194\",\"86\",\"1032\",\"954\",\"779\",\"117\",\"449\",\"87\",\"88\",\"403\",\"685\",\"89\",\"926\",\"355\",\"678\",\"768\",\"679\",\"914\",\"90\",\"772\",\"658\",\"174\",\"928\",\"999\",\"137\",\"660\",\"275\",\"633\",\"392\",\"175\",\"845\",\"119\",\"313\",\"188\",\"1122\",\"276\",\"776\",\"118\",\"274\",\"402\",\"816\",\"1086\",\"959\",\"91\",\"138\",\"844\",\"773\",\"1053\",\"811\",\"465\",\"277\",\"278\",\"93\",\"538\",\"1014\",\"279\",\"539\",\"1083\",\"785\",\"540\",\"393\",\"915\",\"541\",\"176\",\"936\",\"598\",\"680\",\"542\",\"280\",\"282\",\"916\",\"281\",\"283\",\"356\",\"139\",\"357\",\"284\",\"115\",\"94\",\"177\",\"463\",\"285\",\"287\",\"286\",\"394\",\"917\",\"140\",\"634\",\"543\",\"396\",\"918\",\"955\",\"544\",\"545\",\"341\",\"919\",\"546\",\"288\",\"289\",\"547\",\"290\",\"965\",\"395\",\"1069\",\"270\",\"548\",\"576\",\"178\",\"426\",\"1054\"]', 40, 2, 4, 4),
(8, 'Envio Local', 5, '[\"607\"]', 5, 2, 5, 5),
(9, 'Envio Nacional', 5, '[\"2\",\"812\",\"3\",\"690\",\"578\",\"151\",\"608\",\"572\",\"405\",\"853\",\"320\",\"1061\",\"406\",\"609\",\"644\",\"348\",\"854\",\"459\",\"723\",\"1040\",\"724\",\"4\",\"661\",\"610\",\"364\",\"198\",\"966\",\"611\",\"579\",\"192\",\"967\",\"5\",\"6\",\"968\",\"467\",\"725\",\"1035\",\"7\",\"8\",\"9\",\"460\",\"10\",\"321\",\"1024\",\"11\",\"961\",\"12\",\"532\",\"834\",\"199\",\"662\",\"322\",\"855\",\"1055\",\"1049\",\"468\",\"721\",\"796\",\"13\",\"200\",\"152\",\"365\",\"14\",\"1013\",\"663\",\"153\",\"823\",\"15\",\"969\",\"154\",\"407\",\"970\",\"580\",\"431\",\"581\",\"582\",\"583\",\"366\",\"835\",\"127\",\"612\",\"726\",\"16\",\"856\",\"857\",\"713\",\"858\",\"645\",\"1102\",\"187\",\"126\",\"408\",\"323\",\"17\",\"116\",\"469\",\"719\",\"306\",\"605\",\"360\",\"846\",\"201\",\"18\",\"19\",\"859\",\"202\",\"470\",\"203\",\"798\",\"149\",\"471\",\"584\",\"1030\",\"367\",\"860\",\"409\",\"204\",\"21\",\"205\",\"852\",\"815\",\"206\",\"1010\",\"937\",\"824\",\"432\",\"368\",\"717\",\"1025\",\"22\",\"207\",\"861\",\"472\",\"691\",\"1107\",\"473\",\"818\",\"24\",\"1018\",\"938\",\"971\",\"369\",\"474\",\"1110\",\"155\",\"821\",\"208\",\"25\",\"370\",\"1031\",\"862\",\"1034\",\"371\",\"26\",\"148\",\"613\",\"209\",\"433\",\"128\",\"1044\",\"156\",\"475\",\"848\",\"476\",\"28\",\"29\",\"863\",\"30\",\"1004\",\"573\",\"575\",\"31\",\"183\",\"361\",\"1017\",\"1116\",\"960\",\"688\",\"32\",\"27\",\"864\",\"434\",\"210\",\"865\",\"664\",\"720\",\"477\",\"941\",\"972\",\"866\",\"867\",\"33\",\"410\",\"435\",\"847\",\"211\",\"324\",\"806\",\"436\",\"478\",\"868\",\"212\",\"411\",\"213\",\"214\",\"810\",\"215\",\"216\",\"665\",\"225\",\"479\",\"480\",\"1062\",\"461\",\"224\",\"157\",\"869\",\"825\",\"34\",\"20\",\"684\",\"457\",\"309\",\"159\",\"35\",\"973\",\"481\",\"614\",\"939\",\"727\",\"1078\",\"36\",\"870\",\"37\",\"666\",\"587\",\"871\",\"728\",\"729\",\"872\",\"799\",\"38\",\"218\",\"372\",\"873\",\"935\",\"219\",\"482\",\"437\",\"220\",\"940\",\"974\",\"1050\",\"731\",\"692\",\"221\",\"222\",\"483\",\"791\",\"693\",\"1123\",\"732\",\"733\",\"975\",\"349\",\"874\",\"412\",\"223\",\"23\",\"786\",\"586\",\"217\",\"158\",\"730\",\"826\",\"783\",\"39\",\"1022\",\"646\",\"647\",\"976\",\"40\",\"836\",\"226\",\"820\",\"41\",\"42\",\"667\",\"1019\",\"694\",\"602\",\"784\",\"603\",\"190\",\"930\",\"124\",\"695\",\"1016\",\"734\",\"227\",\"484\",\"413\",\"350\",\"696\",\"1011\",\"1092\",\"228\",\"875\",\"160\",\"601\",\"648\",\"414\",\"351\",\"735\",\"150\",\"849\",\"462\",\"668\",\"876\",\"1113\",\"669\",\"942\",\"485\",\"736\",\"92\",\"780\",\"373\",\"737\",\"794\",\"789\",\"1042\",\"615\",\"877\",\"878\",\"43\",\"44\",\"977\",\"567\",\"978\",\"325\",\"827\",\"229\",\"979\",\"347\",\"374\",\"230\",\"1008\",\"880\",\"879\",\"486\",\"649\",\"1051\",\"487\",\"759\",\"45\",\"980\",\"112\",\"714\",\"670\",\"738\",\"488\",\"500\",\"489\",\"490\",\"491\",\"231\",\"492\",\"129\",\"943\",\"881\",\"464\",\"415\",\"882\",\"232\",\"233\",\"616\",\"617\",\"1027\",\"46\",\"493\",\"47\",\"883\",\"416\",\"793\",\"494\",\"113\",\"697\",\"884\",\"234\",\"1023\",\"375\",\"495\",\"739\",\"1033\",\"618\",\"49\",\"885\",\"496\",\"740\",\"741\",\"698\",\"671\",\"981\",\"376\",\"886\",\"944\",\"50\",\"497\",\"51\",\"498\",\"499\",\"235\",\"887\",\"570\",\"501\",\"236\",\"502\",\"837\",\"822\",\"48\",\"888\",\"237\",\"800\",\"189\",\"933\",\"1063\",\"650\",\"52\",\"801\",\"982\",\"53\",\"619\",\"983\",\"962\",\"984\",\"742\",\"743\",\"377\",\"1101\",\"744\",\"620\",\"621\",\"600\",\"54\",\"55\",\"238\",\"378\",\"1009\",\"114\",\"239\",\"240\",\"56\",\"503\",\"889\",\"890\",\"146\",\"504\",\"588\",\"453\",\"622\",\"891\",\"505\",\"242\",\"57\",\"838\",\"1093\",\"745\",\"1045\",\"326\",\"808\",\"58\",\"746\",\"417\",\"1106\",\"428\",\"656\",\"747\",\"701\",\"327\",\"506\",\"354\",\"507\",\"422\",\"893\",\"1094\",\"508\",\"59\",\"623\",\"805\",\"1121\",\"1064\",\"379\",\"828\",\"748\",\"1020\",\"945\",\"749\",\"60\",\"305\",\"380\",\"509\",\"243\",\"1095\",\"1026\",\"839\",\"817\",\"241\",\"892\",\"894\",\"1082\",\"750\",\"703\",\"510\",\"1091\",\"61\",\"751\",\"589\",\"438\",\"752\",\"439\",\"946\",\"813\",\"895\",\"797\",\"130\",\"964\",\"963\",\"381\",\"244\",\"896\",\"62\",\"511\",\"512\",\"161\",\"753\",\"162\",\"651\",\"947\",\"131\",\"754\",\"132\",\"652\",\"418\",\"319\",\"513\",\"328\",\"1057\",\"1103\",\"699\",\"163\",\"63\",\"245\",\"985\",\"329\",\"345\",\"840\",\"330\",\"184\",\"898\",\"1\",\"514\",\"590\",\"591\",\"592\",\"986\",\"382\",\"700\",\"346\",\"1112\",\"246\",\"383\",\"1090\",\"841\",\"1115\",\"1077\",\"899\",\"900\",\"440\",\"165\",\"247\",\"248\",\"249\",\"64\",\"164\",\"429\",\"829\",\"1065\",\"430\",\"166\",\"384\",\"352\",\"1109\",\"948\",\"515\",\"755\",\"308\",\"441\",\"987\",\"65\",\"66\",\"788\",\"250\",\"897\",\"67\",\"756\",\"516\",\"988\",\"69\",\"68\",\"331\",\"606\",\"517\",\"518\",\"519\",\"251\",\"520\",\"332\",\"167\",\"672\",\"252\",\"1056\",\"594\",\"624\",\"593\",\"1029\",\"901\",\"814\",\"902\",\"253\",\"70\",\"757\",\"903\",\"625\",\"1079\",\"1059\",\"989\",\"758\",\"254\",\"949\",\"255\",\"522\",\"1117\",\"385\",\"626\",\"419\",\"523\",\"257\",\"258\",\"627\",\"334\",\"628\",\"904\",\"147\",\"925\",\"1038\",\"950\",\"990\",\"781\",\"782\",\"1108\",\"524\",\"259\",\"1119\",\"525\",\"526\",\"722\",\"386\",\"260\",\"261\",\"1074\",\"312\",\"673\",\"420\",\"335\",\"72\",\"833\",\"262\",\"71\",\"387\",\"906\",\"991\",\"388\",\"830\",\"687\",\"907\",\"168\",\"133\",\"263\",\"629\",\"630\",\"674\",\"992\",\"442\",\"675\",\"760\",\"134\",\"145\",\"363\",\"1066\",\"761\",\"1036\",\"993\",\"762\",\"1088\",\"421\",\"443\",\"842\",\"659\",\"73\",\"908\",\"763\",\"1100\",\"1096\",\"1075\",\"74\",\"264\",\"1080\",\"1120\",\"1105\",\"144\",\"704\",\"444\",\"705\",\"1081\",\"454\",\"707\",\"706\",\"75\",\"1097\",\"851\",\"708\",\"353\",\"1052\",\"527\",\"1098\",\"792\",\"389\",\"76\",\"850\",\"528\",\"764\",\"390\",\"994\",\"445\",\"333\",\"362\",\"256\",\"905\",\"529\",\"530\",\"577\",\"831\",\"843\",\"531\",\"265\",\"807\",\"266\",\"1067\",\"169\",\"77\",\"676\",\"143\",\"709\",\"1021\",\"78\",\"765\",\"533\",\"995\",\"1039\",\"643\",\"909\",\"79\",\"597\",\"336\",\"337\",\"631\",\"766\",\"1012\",\"996\",\"268\",\"391\",\"997\",\"267\",\"595\",\"596\",\"170\",\"427\",\"929\",\"135\",\"1068\",\"136\",\"80\",\"686\",\"81\",\"269\",\"446\",\"632\",\"338\",\"677\",\"790\",\"998\",\"832\",\"82\",\"271\",\"767\",\"339\",\"934\",\"642\",\"423\",\"1089\",\"910\",\"447\",\"120\",\"718\",\"448\",\"1006\",\"571\",\"932\",\"951\",\"534\",\"769\",\"455\",\"804\",\"458\",\"125\",\"715\",\"803\",\"535\",\"185\",\"424\",\"272\",\"171\",\"1104\",\"172\",\"83\",\"536\",\"1084\",\"911\",\"195\",\"193\",\"84\",\"912\",\"340\",\"122\",\"927\",\"316\",\"456\",\"359\",\"1111\",\"604\",\"173\",\"716\",\"958\",\"568\",\"123\",\"657\",\"710\",\"770\",\"1005\",\"85\",\"315\",\"1073\",\"957\",\"952\",\"711\",\"425\",\"191\",\"273\",\"1085\",\"913\",\"317\",\"953\",\"771\",\"314\",\"194\",\"86\",\"1032\",\"954\",\"779\",\"117\",\"449\",\"87\",\"88\",\"403\",\"685\",\"89\",\"926\",\"355\",\"678\",\"768\",\"679\",\"914\",\"90\",\"772\",\"658\",\"174\",\"928\",\"999\",\"137\",\"660\",\"275\",\"633\",\"392\",\"175\",\"845\",\"119\",\"313\",\"188\",\"1122\",\"276\",\"776\",\"118\",\"274\",\"402\",\"816\",\"1086\",\"959\",\"91\",\"138\",\"844\",\"773\",\"1053\",\"811\",\"465\",\"277\",\"278\",\"93\",\"538\",\"1014\",\"279\",\"539\",\"1083\",\"785\",\"540\",\"393\",\"915\",\"541\",\"176\",\"936\",\"598\",\"680\",\"542\",\"280\",\"282\",\"916\",\"281\",\"283\",\"356\",\"139\",\"357\",\"284\",\"115\",\"94\",\"177\",\"463\",\"285\",\"287\",\"286\",\"394\",\"917\",\"140\",\"634\",\"543\",\"396\",\"918\",\"955\",\"544\",\"545\",\"341\",\"919\",\"546\",\"288\",\"289\",\"547\",\"290\",\"965\",\"395\",\"1069\",\"270\",\"548\",\"576\",\"178\",\"426\",\"1054\"]', 5, 1, 5, 5),
(10, 'Envio Local', 9, '[\"1031\"]', 50, 2, 6, 5),
(11, 'Envio Nacional', 4, '[\"798\",\"149\"]', 40, 2, 6, 4),
(12, 'Envio Especial', 7, '[\"910\"]', 30, 1, 6, 5),
(13, 'Envio Local', 1, '[\"1031\"]', 10000, 1, 7, 10),
(14, 'Envio Nacional', 3, '[\"149\"]', 20000, 1, 7, 20),
(15, 'Envio Especial', 4, '[\"910\"]', 30000, 2, 7, 30),
(16, 'Envio Local', 1, '[\"1031\"]', 0, 2, 1, 3),
(17, 'Envio Nacional', 1, '[\"2\",\"812\",\"3\",\"690\",\"578\",\"607\",\"151\",\"608\",\"572\",\"405\",\"853\",\"320\",\"1061\",\"406\",\"609\",\"644\",\"348\",\"854\",\"459\",\"723\",\"1040\",\"724\",\"4\",\"661\",\"610\",\"364\",\"198\",\"966\",\"611\",\"579\",\"192\",\"967\",\"5\",\"6\",\"968\",\"467\",\"725\",\"1035\",\"7\",\"8\",\"9\",\"460\",\"10\",\"321\",\"1024\",\"11\",\"961\",\"12\",\"532\",\"834\",\"199\",\"662\",\"322\",\"855\",\"1055\",\"1049\",\"468\",\"721\",\"796\",\"13\",\"200\",\"152\",\"365\",\"14\",\"1013\",\"663\",\"153\",\"823\",\"15\",\"969\",\"154\",\"407\",\"970\",\"580\",\"431\",\"581\",\"582\",\"583\",\"366\",\"835\",\"127\",\"612\",\"726\",\"16\",\"856\",\"857\",\"713\",\"858\",\"645\",\"1102\",\"187\",\"126\",\"408\",\"323\",\"17\",\"116\",\"469\",\"719\",\"306\",\"605\",\"360\",\"846\",\"201\",\"18\",\"19\",\"859\",\"202\",\"470\",\"203\",\"798\",\"149\",\"471\",\"584\",\"1030\",\"367\",\"860\",\"409\",\"204\",\"21\",\"205\",\"852\",\"815\",\"206\",\"1010\",\"937\",\"824\",\"432\",\"368\",\"717\",\"1025\",\"22\",\"207\",\"861\",\"472\",\"691\",\"1107\",\"473\",\"818\",\"24\",\"1018\",\"938\",\"971\",\"369\",\"474\",\"1110\",\"155\",\"821\",\"208\",\"25\",\"370\",\"862\",\"1034\",\"371\",\"26\",\"148\",\"613\",\"209\",\"433\",\"128\",\"1044\",\"156\",\"475\",\"848\",\"476\",\"28\",\"29\",\"863\",\"30\",\"1004\",\"573\",\"575\",\"31\",\"183\",\"361\",\"1017\",\"1116\",\"960\",\"688\",\"32\",\"27\",\"864\",\"434\",\"210\",\"865\",\"664\",\"720\",\"477\",\"941\",\"972\",\"866\",\"867\",\"33\",\"410\",\"435\",\"847\",\"211\",\"324\",\"806\",\"436\",\"478\",\"868\",\"212\",\"411\",\"213\",\"214\",\"810\",\"215\",\"216\",\"665\",\"225\",\"479\",\"480\",\"1062\",\"461\",\"224\",\"157\",\"869\",\"825\",\"34\",\"20\",\"684\",\"457\",\"309\",\"159\",\"35\",\"973\",\"481\",\"614\",\"939\",\"727\",\"1078\",\"36\",\"870\",\"37\",\"666\",\"587\",\"871\",\"728\",\"729\",\"872\",\"799\",\"38\",\"218\",\"372\",\"873\",\"935\",\"219\",\"482\",\"437\",\"220\",\"940\",\"974\",\"1050\",\"731\",\"692\",\"221\",\"222\",\"483\",\"791\",\"693\",\"1123\",\"732\",\"733\",\"975\",\"349\",\"874\",\"412\",\"223\",\"23\",\"786\",\"586\",\"217\",\"158\",\"730\",\"826\",\"783\",\"39\",\"1022\",\"646\",\"647\",\"976\",\"40\",\"836\",\"226\",\"820\",\"41\",\"42\",\"667\",\"1019\",\"694\",\"602\",\"784\",\"603\",\"190\",\"930\",\"124\",\"695\",\"1016\",\"734\",\"227\",\"484\",\"413\",\"350\",\"696\",\"1011\",\"1092\",\"228\",\"875\",\"160\",\"601\",\"648\",\"414\",\"351\",\"735\",\"150\",\"849\",\"462\",\"668\",\"876\",\"1113\",\"669\",\"942\",\"485\",\"736\",\"92\",\"780\",\"373\",\"737\",\"794\",\"789\",\"1042\",\"615\",\"877\",\"878\",\"43\",\"44\",\"977\",\"567\",\"978\",\"325\",\"827\",\"229\",\"979\",\"347\",\"374\",\"230\",\"1008\",\"880\",\"879\",\"486\",\"649\",\"1051\",\"487\",\"759\",\"45\",\"980\",\"112\",\"714\",\"670\",\"738\",\"488\",\"500\",\"489\",\"490\",\"491\",\"231\",\"492\",\"129\",\"943\",\"881\",\"464\",\"415\",\"882\",\"232\",\"233\",\"616\",\"617\",\"1027\",\"46\",\"493\",\"47\",\"883\",\"416\",\"793\",\"494\",\"113\",\"697\",\"884\",\"234\",\"1023\",\"375\",\"495\",\"739\",\"1033\",\"618\",\"49\",\"885\",\"496\",\"740\",\"741\",\"698\",\"671\",\"981\",\"376\",\"886\",\"944\",\"50\",\"497\",\"51\",\"498\",\"499\",\"235\",\"887\",\"570\",\"501\",\"236\",\"502\",\"837\",\"822\",\"48\",\"888\",\"237\",\"800\",\"189\",\"933\",\"1063\",\"650\",\"52\",\"801\",\"982\",\"53\",\"619\",\"983\",\"962\",\"984\",\"742\",\"743\",\"377\",\"1101\",\"744\",\"620\",\"621\",\"600\",\"54\",\"55\",\"238\",\"378\",\"1009\",\"114\",\"239\",\"240\",\"56\",\"503\",\"889\",\"890\",\"146\",\"504\",\"588\",\"453\",\"622\",\"891\",\"505\",\"242\",\"57\",\"838\",\"1093\",\"745\",\"1045\",\"326\",\"808\",\"58\",\"746\",\"417\",\"1106\",\"428\",\"656\",\"747\",\"701\",\"327\",\"506\",\"354\",\"507\",\"422\",\"893\",\"1094\",\"508\",\"59\",\"623\",\"805\",\"1121\",\"1064\",\"379\",\"828\",\"748\",\"1020\",\"945\",\"749\",\"60\",\"305\",\"380\",\"509\",\"243\",\"1095\",\"1026\",\"839\",\"817\",\"241\",\"892\",\"894\",\"1082\",\"750\",\"703\",\"510\",\"1091\",\"61\",\"751\",\"589\",\"438\",\"752\",\"439\",\"946\",\"813\",\"895\",\"797\",\"130\",\"964\",\"963\",\"381\",\"244\",\"896\",\"62\",\"511\",\"512\",\"161\",\"753\",\"162\",\"651\",\"947\",\"131\",\"754\",\"132\",\"652\",\"418\",\"319\",\"513\",\"328\",\"1057\",\"1103\",\"699\",\"163\",\"63\",\"245\",\"985\",\"329\",\"345\",\"840\",\"330\",\"184\",\"898\",\"1\",\"514\",\"590\",\"591\",\"592\",\"986\",\"382\",\"700\",\"346\",\"1112\",\"246\",\"383\",\"1090\",\"841\",\"1115\",\"1077\",\"899\",\"900\",\"440\",\"165\",\"247\",\"248\",\"249\",\"64\",\"164\",\"429\",\"829\",\"1065\",\"430\",\"166\",\"384\",\"352\",\"1109\",\"948\",\"515\",\"755\",\"308\",\"441\",\"987\",\"65\",\"66\",\"788\",\"250\",\"897\",\"67\",\"756\",\"516\",\"988\",\"69\",\"68\",\"331\",\"606\",\"517\",\"518\",\"519\",\"251\",\"520\",\"332\",\"167\",\"672\",\"252\",\"1056\",\"594\",\"624\",\"593\",\"1029\",\"901\",\"814\",\"902\",\"253\",\"70\",\"757\",\"903\",\"625\",\"1079\",\"1059\",\"989\",\"758\",\"254\",\"949\",\"255\",\"522\",\"1117\",\"385\",\"626\",\"419\",\"523\",\"257\",\"258\",\"627\",\"334\",\"628\",\"904\",\"147\",\"925\",\"1038\",\"950\",\"990\",\"781\",\"782\",\"1108\",\"524\",\"259\",\"1119\",\"525\",\"526\",\"722\",\"386\",\"260\",\"261\",\"1074\",\"312\",\"673\",\"420\",\"335\",\"72\",\"833\",\"262\",\"71\",\"387\",\"906\",\"991\",\"388\",\"830\",\"687\",\"907\",\"168\",\"133\",\"263\",\"629\",\"630\",\"674\",\"992\",\"442\",\"675\",\"760\",\"134\",\"145\",\"363\",\"1066\",\"761\",\"1036\",\"993\",\"762\",\"1088\",\"421\",\"443\",\"842\",\"659\",\"73\",\"908\",\"763\",\"1100\",\"1096\",\"1075\",\"74\",\"264\",\"1080\",\"1120\",\"1105\",\"144\",\"704\",\"444\",\"705\",\"1081\",\"454\",\"707\",\"706\",\"75\",\"1097\",\"851\",\"708\",\"353\",\"1052\",\"527\",\"1098\",\"792\",\"389\",\"76\",\"850\",\"528\",\"764\",\"390\",\"994\",\"445\",\"333\",\"362\",\"256\",\"905\",\"529\",\"530\",\"577\",\"831\",\"843\",\"531\",\"265\",\"807\",\"266\",\"1067\",\"169\",\"77\",\"676\",\"143\",\"709\",\"1021\",\"78\",\"765\",\"533\",\"995\",\"1039\",\"643\",\"909\",\"79\",\"597\",\"336\",\"337\",\"631\",\"766\",\"1012\",\"996\",\"268\",\"391\",\"997\",\"267\",\"595\",\"596\",\"170\",\"427\",\"929\",\"135\",\"1068\",\"136\",\"80\",\"686\",\"81\",\"269\",\"446\",\"632\",\"338\",\"677\",\"790\",\"998\",\"832\",\"82\",\"271\",\"767\",\"339\",\"934\",\"642\",\"423\",\"1089\",\"910\",\"447\",\"120\",\"718\",\"448\",\"1006\",\"571\",\"932\",\"951\",\"534\",\"769\",\"455\",\"804\",\"458\",\"125\",\"715\",\"803\",\"535\",\"185\",\"424\",\"272\",\"171\",\"1104\",\"172\",\"83\",\"536\",\"1084\",\"911\",\"195\",\"193\",\"84\",\"912\",\"340\",\"122\",\"927\",\"316\",\"456\",\"359\",\"1111\",\"604\",\"173\",\"716\",\"958\",\"568\",\"123\",\"657\",\"710\",\"770\",\"1005\",\"85\",\"315\",\"1073\",\"957\",\"952\",\"711\",\"425\",\"191\",\"273\",\"1085\",\"913\",\"317\",\"953\",\"771\",\"314\",\"194\",\"86\",\"1032\",\"954\",\"779\",\"117\",\"449\",\"87\",\"88\",\"403\",\"685\",\"89\",\"926\",\"355\",\"678\",\"768\",\"679\",\"914\",\"90\",\"772\",\"658\",\"174\",\"928\",\"999\",\"137\",\"660\",\"275\",\"633\",\"392\",\"175\",\"845\",\"119\",\"313\",\"188\",\"1122\",\"276\",\"776\",\"118\",\"274\",\"402\",\"816\",\"1086\",\"959\",\"91\",\"138\",\"844\",\"773\",\"1053\",\"811\",\"465\",\"277\",\"278\",\"93\",\"538\",\"1014\",\"279\",\"539\",\"1083\",\"785\",\"540\",\"393\",\"915\",\"541\",\"176\",\"936\",\"598\",\"680\",\"542\",\"280\",\"282\",\"916\",\"281\",\"283\",\"356\",\"139\",\"357\",\"284\",\"115\",\"94\",\"177\",\"463\",\"285\",\"287\",\"286\",\"394\",\"917\",\"140\",\"634\",\"543\",\"396\",\"918\",\"955\",\"544\",\"545\",\"341\",\"919\",\"546\",\"288\",\"289\",\"547\",\"290\",\"965\",\"395\",\"1069\",\"270\",\"548\",\"576\",\"178\",\"426\",\"1054\",\"774\",\"775\",\"1118\",\"1099\",\"96\",\"635\",\"97\",\"291\",\"1070\",\"549\",\"637\",\"550\",\"681\",\"551\",\"292\",\"795\",\"638\",\"636\",\"552\",\"293\",\"196\",\"553\",\"802\",\"450\",\"639\",\"398\",\"397\",\"294\",\"295\",\"179\",\"98\",\"296\",\"554\",\"555\",\"310\",\"99\",\"787\",\"956\",\"920\",\"556\",\"399\",\"1043\",\"298\",\"400\",\"1071\",\"1047\",\"141\",\"451\",\"1007\",\"197\",\"307\",\"180\",\"181\",\"100\",\"299\",\"318\",\"300\",\"1058\",\"95\",\"297\",\"777\",\"557\",\"558\",\"1046\",\"301\",\"559\",\"599\",\"585\",\"101\",\"702\",\"653\",\"102\",\"654\",\"142\",\"103\",\"452\",\"1087\",\"931\",\"1003\",\"404\",\"104\",\"358\",\"105\",\"1000\",\"106\",\"521\",\"302\",\"574\",\"1041\",\"922\",\"561\",\"342\",\"121\",\"1048\",\"819\",\"401\",\"311\",\"569\",\"809\",\"1076\",\"562\",\"1001\",\"343\",\"655\",\"923\",\"1072\",\"182\",\"563\",\"1002\",\"689\",\"640\",\"564\",\"565\",\"303\",\"712\",\"344\",\"921\",\"466\",\"778\",\"641\",\"107\",\"108\",\"1114\",\"109\",\"110\",\"1060\",\"1037\",\"1028\",\"186\",\"924\",\"682\",\"111\",\"1015\",\"304\",\"566\",\"537\",\"683\",\"560\"]', 0, 2, 1, 6),
(18, 'Envio Especial', 1, '[\"1089\"]', 0, 2, 1, 6),
(19, 'Envio Local', 1, '[\"1031\"]', 0, 2, 4, 1),
(20, 'Envio Nacional', 1, '[\"149\"]', 0, 2, 4, 1),
(21, 'Envio Especial', 1, '[\"910\"]', 0, 2, 4, 1),
(22, 'Envio Local', 1, '[\"1031\"]', 0, 2, 6, 1),
(23, 'Envio Nacional', 1, '[\"149\"]', 0, 2, 6, 1),
(24, 'Envio Especial', 1, '[\"910\"]', 0, 2, 6, 1),
(25, 'Envio Local', 1, '[\"1031\"]', 0, 2, 8, 1),
(26, 'Envio Nacional', 1, '[\"149\"]', 0, 2, 8, 1),
(27, 'Envio Especial', 1, '[\"910\"]', 0, 2, 8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shippingmethod`
--

CREATE TABLE `shippingmethod` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `shippingmethod`
--

INSERT INTO `shippingmethod` (`id`, `name`, `description`) VALUES
(1, '	Precio fijo', 'Te permite cobrar un precio fijo por envío.'),
(2, '	Envío gratuito', 'Envío gratuito es un método especial que puede ser provocado con cupones o gastos mínimos.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoryproduct`
--

CREATE TABLE `subcategoryproduct` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `CategoryProduct_id` int(11) NOT NULL,
  `url_img` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `subcategoryproduct`
--

INSERT INTO `subcategoryproduct` (`id`, `name`, `CategoryProduct_id`, `url_img`, `active`) VALUES
(7, 'Grupo Alimentos', 9, NULL, 1),
(8, 'Grupo Hongos', 9, NULL, 1),
(9, 'Grupo Polen de arbustos, malezas y flores', 9, NULL, 1),
(10, 'Grupo Ácaros', 9, NULL, 1),
(11, 'Grupo Polen de árboles', 9, NULL, 1),
(12, 'Grupo Polen de gramíneas', 9, NULL, 1),
(13, 'Grupo Epitelios, plumas e insectos', 9, NULL, 1),
(14, 'Controles y otros extractos alergénicos', 9, NULL, 1),
(15, 'ALXOID', 10, NULL, 1),
(16, 'ORALTEK', 11, NULL, 1),
(17, 'Grupo Àcaros', 10, NULL, 1),
(18, 'Grupo Epitelios, plumas e insectos', 10, NULL, 1),
(19, 'Grupo Polen de gramíneas', 10, NULL, 1),
(20, 'Grupo Polen de Àrboles', 10, NULL, 1),
(21, 'Grupo Polen de arbustos, malezas y flores', 10, NULL, 1),
(22, 'Grupo Polen de Árboles', 11, NULL, 1),
(23, 'Grupo Polen de gramíneas', 11, NULL, 1),
(24, 'Grupo Epitelios, plumas e insectos', 11, NULL, 1),
(25, 'Grupo Ácaros', 11, NULL, 1),
(26, 'Grupo Polen de gramíneas', 11, NULL, 1),
(27, 'Grupo Àcaros', 12, NULL, 1),
(28, 'Grupo Polen de gramíneas', 12, NULL, 1),
(29, 'Grupo Venenos', 13, NULL, 1),
(30, 'Dispositivo Cutaneo', 14, NULL, 1),
(31, 'Dispositivo Cutaneo', 14, NULL, 1),
(32, 'Grupo Epitelios, plumas e insectos', 15, NULL, 1),
(33, 'Estandar', 16, NULL, 1),
(34, 'Calzado', 16, NULL, 1),
(35, 'Cosmetico', 16, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typecostumer`
--

CREATE TABLE `typecostumer` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `typecostumer`
--

INSERT INTO `typecostumer` (`id`, `name`) VALUES
(1, 'PERSONA NATURAL'),
(2, 'PERSONA JURIDICA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typeidentification`
--

CREATE TABLE `typeidentification` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `typeidentification`
--

INSERT INTO `typeidentification` (`id`, `name`) VALUES
(1, 'CC'),
(2, 'NIT');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Role_id` int(11) NOT NULL,
  `token` longtext COLLATE utf8mb4_unicode_ci,
  `linked` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `Role_id`, `token`, `linked`) VALUES
(1, 'Admin', 'inmunotek@gmail.com', NULL, '$2y$10$PK8Mcbe2.2vLztddp/f.xuBYRxqW9RLJUbClGIJbjte7zKFS2xiKG', NULL, '2020-04-12 03:45:49', '2020-04-12 03:45:49', 1, '', 0),
(54, 'biotest', 'bio@bsltda.com', NULL, '$2y$10$/OwnuUbdEXurfCTTcTgzKuFoxyjWSXkY26dPs7om2EjwWuDIq1/1G', NULL, '2020-09-09 13:42:58', '2020-09-09 13:42:58', 3, NULL, 0),
(55, 'Helppiu SAS', 'andresfsati02@gmail.com', NULL, '$2y$10$Brp7AK0GjcXL4lBQ5ITXGuKf8b/6/hMtTe/p6knIxgH4gc2jDdJeW', NULL, '2020-09-10 02:42:36', '2020-09-10 02:42:36', 3, NULL, 0),
(56, 'Andres  Satizabal', 'andres2@appdigital.com.co', NULL, '$2y$10$l.J6eYHbYM8AtM.DqCMddOrHjH7CubTpxG.5o3PfHufJ40IqwGhyG', NULL, '2020-09-10 03:46:29', '2020-09-10 03:46:29', 2, NULL, 0),
(58, 'Miguel', 'andres@appdigital.com.co', NULL, '$2y$10$G2EZg1cuBiZat0E90iONx.DZPiIZUCAlxb5FWY5400ncwhj32Oml.', 'Brb1FEQFTquc7UCYdeMkYtVuoPIdOEezbr5bFhoHcphVYGNc2bQ3W52Zk6ofz6sS12vFSBiuXXjG8HF682EdQnaXKem3wsEKYy7x', '2020-09-10 03:47:30', '2022-02-25 05:16:29', 2, 'd4rjWtbo19c:APA91bGl-XjvlrM9VLQG7ow2cNXzRnLi-n0JWFxRykii5Zw5gQXkPWMGj8_InAMrBI7G5eoX_TJY0ppNBgldaI3uneDLSo1VgzKm0eansCsCBc2_Z0XNmoVSh9hdtAVMJ4RyuJ_pi8y1', 0),
(59, 'IMPORTADORA TORVAR 777 SAS', 'gerenciatorvar@gmail.com', NULL, '$2y$10$X.oVowRJiU9bzqqjrAhH/ueBnbSbLJDE3BH2ZJYak0.I9cx92P5ru', NULL, '2020-09-11 18:56:40', '2020-09-11 18:56:40', 3, NULL, 0),
(60, 'dyego vallejo', 'dyegovallejob@gmail.com', NULL, '$2y$10$JmzpFrW/sm7VZJEHow7oNeSqnrlNih.zJ124MqMj593LaODsndf.q', NULL, '2020-09-16 01:18:59', '2020-09-16 01:18:59', 2, NULL, 0),
(62, 'perucash', 'hector12arteaga@gmail.com', NULL, '$2y$10$t3cMVHTTY5Wa23RaIDh4zeuvWq5omZG6H8qpVh8X7YGAc/awA7kGe', NULL, '2020-09-26 05:15:13', '2020-09-26 05:15:13', 3, NULL, 0),
(63, 'sheyla', 'sheyla_462@hotmail.com', NULL, '$2y$10$g8Z/vBxP1UuOGhxc6EU9VOtEq/IiHW//4i.PKjgiQOw0HuW1nrg2K', NULL, '2020-10-15 20:55:31', '2020-10-15 20:55:31', 3, NULL, 0),
(64, 'Daniel Gutierrez Pineda Ramirez', 'hector.a.arteaga@gmail.com', NULL, '$2y$10$yYCrbiQTg5avRvSvOvqs/OrKG1gAvwH9TUkrHd6NK10rQPxHfzQx.', NULL, '2020-10-16 04:28:18', '2020-10-16 04:28:18', 2, NULL, 0),
(66, 'Andres', 'inventario@inmunotek.com', NULL, '$2y$10$9ycVxlNq4o2TxXHU5d/FTelV35QDglGYe0n2G6UF.0LXk5gD5DLDC', NULL, '2021-07-07 17:29:49', '2021-07-07 17:29:49', 4, NULL, 0),
(67, 'Bernardo Salazar', 'publicador@inmunotek.com', NULL, '$2y$10$0.mCQynUNwtf6/OXbbxVBO525gFoNNbSE/1o4t7V.x2zU.oI61gj2', NULL, '2021-07-09 02:51:32', '2021-07-09 02:51:32', 5, NULL, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_Address_Costumer1_idx` (`Costumer_id`) USING BTREE,
  ADD KEY `fk_Address_City1_idx` (`City_id`) USING BTREE;

--
-- Indices de la tabla `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_Bill_order1_idx` (`order_id`) USING BTREE,
  ADD KEY `fk_Bill_BillStatus1_idx` (`BillStatus_id`) USING BTREE;

--
-- Indices de la tabla `billstatus`
--
ALTER TABLE `billstatus`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `categoryproduct`
--
ALTER TABLE `categoryproduct`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_City_Department1_idx` (`Department_id`) USING BTREE;

--
-- Indices de la tabla `costumer`
--
ALTER TABLE `costumer`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_Costumer_users1_idx` (`users_id`) USING BTREE,
  ADD KEY `fk_Costumer_TypeIdentification1_idx` (`TypeIdentification_id`) USING BTREE,
  ADD KEY `fk_Costumer_TypeCostumer1_idx` (`TypeCostumer_id`) USING BTREE,
  ADD KEY `fk_Costumer_City1_idx` (`City_id`) USING BTREE;

--
-- Indices de la tabla `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_Department_Country_idx` (`Country_id`) USING BTREE;

--
-- Indices de la tabla `devolutions`
--
ALTER TABLE `devolutions`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `event_type_id` (`event_type_id`);

--
-- Indices de la tabla `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `fileproduct`
--
ALTER TABLE `fileproduct`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_FileProduct_Product1_idx` (`Product_id`) USING BTREE;

--
-- Indices de la tabla `linked_users`
--
ALTER TABLE `linked_users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `new`
--
ALTER TABLE `new`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_notification_Costumer2_idx` (`Costumer_id`) USING BTREE;

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`) USING BTREE;

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`) USING BTREE;

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `oauth_clients_user_id_index` (`user_id`) USING BTREE;

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_order_Payment_method1_idx` (`Payment_method_id`) USING BTREE,
  ADD KEY `fk_order_OrderStatus1_idx` (`OrderStatus_id`) USING BTREE,
  ADD KEY `fk_order_Costumer1_idx` (`Costumer_id`) USING BTREE,
  ADD KEY `fk_order_Address1_idx` (`Address_id`) USING BTREE;

--
-- Indices de la tabla `orderproductstatus`
--
ALTER TABLE `orderproductstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orderstatus`
--
ALTER TABLE `orderstatus`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `order_has_product`
--
ALTER TABLE `order_has_product`
  ADD PRIMARY KEY (`order_id`,`Product_id`) USING BTREE,
  ADD KEY `fk_order_has_Product_Product1_idx` (`Product_id`) USING BTREE,
  ADD KEY `fk_order_has_Product_order1_idx` (`order_id`) USING BTREE,
  ADD KEY `OrderProductStatus_id` (`OrderProductStatus_id`);

--
-- Indices de la tabla `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_Product_SubcategoryProduct1_idx` (`SubcategoryProduct_id`) USING BTREE,
  ADD KEY `fk_Product_Provider1_idx` (`Provider_id`) USING BTREE;

--
-- Indices de la tabla `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_Provider_users1_idx` (`users_id`) USING BTREE,
  ADD KEY `fk_Provider_City1_idx` (`City_id`) USING BTREE,
  ADD KEY `fk_Provider_TypeIdentification1_idx` (`TypeIdentification_id`) USING BTREE;

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_Shipping_ShippingMethod1_idx` (`ShippingMethod_id`) USING BTREE,
  ADD KEY `fk_shipping_product1_idx` (`product_id`);

--
-- Indices de la tabla `shippingmethod`
--
ALTER TABLE `shippingmethod`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `subcategoryproduct`
--
ALTER TABLE `subcategoryproduct`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_SubcategoryProduct_CategoryProduct1_idx` (`CategoryProduct_id`) USING BTREE;

--
-- Indices de la tabla `typecostumer`
--
ALTER TABLE `typecostumer`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `typeidentification`
--
ALTER TABLE `typeidentification`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  ADD KEY `fk_users_Role1_idx` (`Role_id`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `bill`
--
ALTER TABLE `bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `billstatus`
--
ALTER TABLE `billstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `categoryproduct`
--
ALTER TABLE `categoryproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1124;

--
-- AUTO_INCREMENT de la tabla `costumer`
--
ALTER TABLE `costumer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT de la tabla `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `devolutions`
--
ALTER TABLE `devolutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `event_type`
--
ALTER TABLE `event_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fileproduct`
--
ALTER TABLE `fileproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=535;

--
-- AUTO_INCREMENT de la tabla `linked_users`
--
ALTER TABLE `linked_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `new`
--
ALTER TABLE `new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `notices`
--
ALTER TABLE `notices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `orderproductstatus`
--
ALTER TABLE `orderproductstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `orderstatus`
--
ALTER TABLE `orderstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT de la tabla `provider`
--
ALTER TABLE `provider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `shipping`
--
ALTER TABLE `shipping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `shippingmethod`
--
ALTER TABLE `shippingmethod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `subcategoryproduct`
--
ALTER TABLE `subcategoryproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `typecostumer`
--
ALTER TABLE `typecostumer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `typeidentification`
--
ALTER TABLE `typeidentification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `fk_Address_City1` FOREIGN KEY (`City_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Address_Costumer1` FOREIGN KEY (`Costumer_id`) REFERENCES `costumer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `bill`
--
ALTER TABLE `bill`
  ADD CONSTRAINT `fk_Bill_BillStatus1` FOREIGN KEY (`BillStatus_id`) REFERENCES `billstatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Bill_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `fk_City_Department1` FOREIGN KEY (`Department_id`) REFERENCES `department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `costumer`
--
ALTER TABLE `costumer`
  ADD CONSTRAINT `fk_Costumer_City1` FOREIGN KEY (`City_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Costumer_TypeCostumer1` FOREIGN KEY (`TypeCostumer_id`) REFERENCES `typecostumer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Costumer_TypeIdentification1` FOREIGN KEY (`TypeIdentification_id`) REFERENCES `typeidentification` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Costumer_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `fk_Department_Country` FOREIGN KEY (`Country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`);

--
-- Filtros para la tabla `fileproduct`
--
ALTER TABLE `fileproduct`
  ADD CONSTRAINT `fk_FileProduct_Product1` FOREIGN KEY (`Product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `fk_notification_Costumer2` FOREIGN KEY (`Costumer_id`) REFERENCES `costumer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_Address1` FOREIGN KEY (`Address_id`) REFERENCES `address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_Costumer1` FOREIGN KEY (`Costumer_id`) REFERENCES `costumer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_OrderStatus1` FOREIGN KEY (`OrderStatus_id`) REFERENCES `orderstatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_Payment_method1` FOREIGN KEY (`Payment_method_id`) REFERENCES `payment_method` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `order_has_product`
--
ALTER TABLE `order_has_product`
  ADD CONSTRAINT `fk_order_has_Product_Product1` FOREIGN KEY (`Product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_has_Product_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_has_Product_order_status1` FOREIGN KEY (`OrderProductStatus_id`) REFERENCES `orderproductstatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_Product_Provider1` FOREIGN KEY (`Provider_id`) REFERENCES `provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Product_SubcategoryProduct1` FOREIGN KEY (`SubcategoryProduct_id`) REFERENCES `subcategoryproduct` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `provider`
--
ALTER TABLE `provider`
  ADD CONSTRAINT `fk_Provider_City1` FOREIGN KEY (`City_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Provider_TypeIdentification1` FOREIGN KEY (`TypeIdentification_id`) REFERENCES `typeidentification` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Provider_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `shipping`
--
ALTER TABLE `shipping`
  ADD CONSTRAINT `fk_Shipping_ShippingMethod1` FOREIGN KEY (`ShippingMethod_id`) REFERENCES `shippingmethod` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_shipping_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `subcategoryproduct`
--
ALTER TABLE `subcategoryproduct`
  ADD CONSTRAINT `fk_SubcategoryProduct_CategoryProduct1` FOREIGN KEY (`CategoryProduct_id`) REFERENCES `categoryproduct` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_Role1` FOREIGN KEY (`Role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
